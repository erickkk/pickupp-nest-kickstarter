-- CreateTable
CREATE TABLE "AgentTrip" (
    "id" TEXT NOT NULL,
    "agentId" TEXT NOT NULL,
    "poolTripId" TEXT NOT NULL,
    "lastAgentTripTimeLineId" TEXT NOT NULL,
    "completedAt" TIMESTAMP(3),
    "failedAt" TIMESTAMP(3),
    "startedAt" TIMESTAMP(3),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "deletedAt" TIMESTAMP(3),

    CONSTRAINT "AgentTrip_pkey" PRIMARY KEY ("id")
);
