/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'alfred';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  lockerInfo: Locker | undefined;
  trackResponse: OrderTrack | undefined;
  trackResponseHistory: OrderTrack[];
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  errorMessage: string;
}

export interface OrderResponse {
  SUCCEED: OrderResponse_SucceedObj[];
  badParameter: string[];
  alreadyExist: string[];
}

export interface OrderResponse_SucceedObj {
  orderId: string;
  shipmentId: string;
  trackingNumber: string;
  additionalTrackingNumber: string;
  errorMessage: string;
  waybillLink: string;
}

export interface OrderTrack {
  serviceType: string;
  trackingNumber: string;
  additionalTrackingNumber: string;
  referenceNumber: string;
  locationId: string;
  accessCode: string;
  recipientPhone: string;
  recipientEmail: string;
  recipientAddress: string;
  recipientName: string;
  note: string;
  charge: number;
  status: string;
  statusTime: string;
  statusTimestamp: string;
  weight: string;
  length: string;
  width: string;
  height: string;
  senderPhone: string;
  senderEmail: string;
  senderAddress: string;
  expressNumber: string;
  locationName: string;
  compartmentId: string;
  compartmentNumber: string;
  event: string;
  operator: string;
  orderFlag: string[];
}

export interface Shipment {
  ordersId: string;
  trackingNumber: string;
  additionalTrackingNumber: string;
  referenceNumber: string;
  totalCount: string;
  status: string;
  statusDescription: string;
  statusTime: string;
  statusTimeStamp: string;
  serviceType: string;
  storeUser: string;
  location: Shipment_LocationContent | undefined;
  recipient: Shipment_RecipientContent | undefined;
  accessCode: string;
  timeStored: string;
  timeCollected: string;
  timeOverdue: string;
  company: Shipment_CompanyContent | undefined;
  shortLink: string;
  createTime: string;
  creator: Shipment_CeatorContent | undefined;
}

export interface Shipment_LocationContent {
  locationName: string;
  locationId: string;
  boxTypeName: string;
  boxName: string;
}

export interface Shipment_RecipientContent {
  recipientPhone: string;
  recipientMail: string;
  recipientName: string;
}

export interface Shipment_CompanyContent {
  companyId: string;
  companyName: string;
}

export interface Shipment_CeatorContent {
  createdBy: string;
  createdUserId: string;
  createdUserLoginName: string;
}

export interface Locker {
  locationId: string;
  locationName: string;
  locationType: string;
  services: string[];
  longitude: string;
  latitude: string;
  country: string;
  countryCn: string;
  province: string;
  provinceCn: string;
  city: string;
  cityCn: string;
  district: string;
  districtCn: string;
  postalCode?: string | undefined;
  address1: string;
  address1En: string;
  address2?: string | undefined;
  address2En?: string | undefined;
  operatingTime?: string | undefined;
  description?: string | undefined;
  companyId: string;
  companyName: string;
  locationGroup?: string | undefined;
  detailHours: Locker_DetailHours[];
  displayHours: string;
  displayHoursCn: string;
  createdAt: string;
  updatedAt: string;
}

export interface Locker_DetailHours {
  weekday: string;
  opening1: string;
  closing1: string;
  opening2: string;
  closing2: string;
}

export const ALFRED_PACKAGE_NAME = 'alfred';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
