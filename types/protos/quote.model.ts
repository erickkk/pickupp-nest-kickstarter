/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import {
  Locale,
  DistanceModelPricing,
  DimensionWeightModel,
} from './merchant.model';

export const protobufPackage = 'quote';

export interface Service {
  id: string;
  name: string;
  serviceType: string;
  serviceTime: number;
  isDefault: boolean;
  enabled: boolean;
  earliestPickupId: string;
  latestPickupId: string;
  earliestDropoffId: string;
  latestDropoffId: string;
  pricingId: string;
  orderFlowId: string;
  displayName: Locale | undefined;
}

export interface Timeslot {
  id: string;
  name: string;
  mode: Timeslot_Mode;
  target1: string;
  target2: string;
  value: string;
  custom: boolean;
}

export enum Timeslot_Mode {
  NOW = 0,
  RELATIVE = 1,
  PERCENTAGE = 2,
  UNRECOGNIZED = -1,
}

export interface Configuration {
  id: string;
  type: string;
  typeId: string;
  key: string;
  value: string;
}

export interface Pricing {
  id: string;
  name: string;
  distanceModel: DistanceModelPricing[];
  dimensionWeightModel: DimensionWeightModel[];
  pickupSurcharge: boolean;
  dropoffSurcharge: boolean;
  expressSurcharge: boolean;
  expressRate: number;
  wallSurcharge: boolean;
  discountRate: number;
  fixPrice: string;
}

export interface OrderFlow {
  id: string;
  name: string;
}

export interface EntityService {
  id: string;
  entityId: number;
  serviceId: string;
  priority: number;
}

export const QUOTE_PACKAGE_NAME = 'quote';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
