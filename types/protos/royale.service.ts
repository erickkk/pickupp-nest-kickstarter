/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';
import { Store, Order } from './royale.model';

export const protobufPackage = 'royale';

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface SyncStoreRequest {}

export interface SyncStoreResponse {
  success: boolean;
}

export interface GetStoreRequest {
  locationId: string;
}

export interface GetStoreResponse {
  store: Store | undefined;
}

export interface GetStoresRequest {
  locationIds: string[];
}

export interface GetStoresResponse {
  stores: Store[];
}

export interface GetWarehouseRequest {}

export interface GetWarehouseResponse {
  id: string;
}

export interface UpdateOrderRequest {
  merchantId: string;
  merchantRef: string;
  waybillNo: string;
  status: string;
  statusResult: string;
  lastEventId: string;
  lastEventDescription: string;
  lastEventDate: string;
  lastEventTime: string;
  pudoVerifyCode: string;
}

export interface UpdateOrderResponse {
  result: string;
}

export interface GetOrdersRequest {
  orderIds: string[];
  orderNumbers: string[];
}

export interface GetOrdersResponse {
  orders: Order[];
}

export interface GetOrderRequest {
  id: string;
}

export interface GetWaybillsRequest {
  orderNumbers: string[];
}

export interface GetWaybillsResponse {
  waybills: GetWaybillsResponse_Waybill[];
  merchantId: string;
}

export interface GetWaybillsResponse_Waybill {
  orderNumber: string;
  waybillNumber: string;
}

export const ROYALE_PACKAGE_NAME = 'royale';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

export interface StoresClient {
  getWarehouse(request: GetWarehouseRequest): Observable<GetWarehouseResponse>;

  syncStore(request: SyncStoreRequest): Observable<SyncStoreResponse>;

  getStore(request: GetStoreRequest): Observable<GetStoreResponse>;

  getStores(request: GetStoresRequest): Observable<GetStoresResponse>;
}

export interface StoresController {
  getWarehouse(
    request: GetWarehouseRequest,
  ):
    | Promise<GetWarehouseResponse>
    | Observable<GetWarehouseResponse>
    | GetWarehouseResponse;

  syncStore(
    request: SyncStoreRequest,
  ):
    | Promise<SyncStoreResponse>
    | Observable<SyncStoreResponse>
    | SyncStoreResponse;

  getStore(
    request: GetStoreRequest,
  ):
    | Promise<GetStoreResponse>
    | Observable<GetStoreResponse>
    | GetStoreResponse;

  getStores(
    request: GetStoresRequest,
  ):
    | Promise<GetStoresResponse>
    | Observable<GetStoresResponse>
    | GetStoresResponse;
}

export function StoresControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getWarehouse',
      'syncStore',
      'getStore',
      'getStores',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Stores', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Stores', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const STORES_SERVICE_NAME = 'Stores';

export interface OrdersClient {
  updateOrder(request: UpdateOrderRequest): Observable<UpdateOrderResponse>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;

  getOrder(request: GetOrderRequest): Observable<Order>;

  getWaybills(request: GetWaybillsRequest): Observable<GetWaybillsResponse>;
}

export interface OrdersController {
  updateOrder(
    request: UpdateOrderRequest,
  ):
    | Promise<UpdateOrderResponse>
    | Observable<UpdateOrderResponse>
    | UpdateOrderResponse;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;

  getOrder(
    request: GetOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  getWaybills(
    request: GetWaybillsRequest,
  ):
    | Promise<GetWaybillsResponse>
    | Observable<GetWaybillsResponse>
    | GetWaybillsResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'updateOrder',
      'getOrders',
      'getOrder',
      'getWaybills',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
