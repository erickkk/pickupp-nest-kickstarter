/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'fulfillment';

export interface InboundRequest {
  id: string;
  entityId: string;
  status: string;
  estimateDeliveryDate: string;
  contactAddressLine1: string;
  contactAddressLine2: string;
  contactAddressLatitude: number;
  contactAddressLongitude: number;
  contactPersonName: string;
  contactPersonPhone: string;
  orderId?: string | undefined;
  warehouseId: string;
  createdAt: string;
  updatedAt: string;
  requestNumber: string;
  remark: string;
  inboundItems: InboundItem[];
  clientReferenceNumber: string;
}

export interface InboundItem {
  id: string;
  name: string;
  inboundRequestId?: string | undefined;
  refInventoryId?: string | undefined;
  upc: string;
  weight: number;
  width?: number | undefined;
  height?: number | undefined;
  length?: number | undefined;
  note?: string | undefined;
  quantity: number;
  expiredAt: string;
  manufacturedAt: string;
  createdAt: string;
  updatedAt: string;
  batchNumber?: string | undefined;
  photoLink?: string | undefined;
  price: number;
  inventory?: Inventory | undefined;
  inventoryStock?: InventoryStock | undefined;
}

export interface Inventory {
  id: string;
  entityId: string;
  name: string;
  photo: string;
  upc: string;
  weight: number;
  width: number;
  height: number;
  length: number;
  createdAt: string;
  updatedAt: string;
  specialCare: string;
  price: number;
  inventoryStocks: InventoryStock[];
}

export interface InventoryStock {
  id: string;
  inventoryId: string;
  quantity: number;
  expiredAt: string;
  holdingBy: string;
  locationId: string;
  createdAt: string;
  updatedAt: string;
  batchNumber: string;
  manufacturedAt: string;
  remarks: string;
  pendingQuantity: number;
  upc: string;
}

export interface InboundReceipt {
  id: string;
  inboundItemId: string;
  inventoryStockId: string;
  quantity: number;
  remark: string;
  received: number;
  receivedBy: string;
  createdAt: string;
  updatedAt: string;
}

export interface RequestWithUuid {
  id: string;
}

export interface OutboundItem {
  id: string;
  name: string;
  outboundRequestId?: string | undefined;
  refInventoryId?: string | undefined;
  upc: string;
  length?: number | undefined;
  width?: number | undefined;
  height?: number | undefined;
  weight: number;
  quantity: number;
  createdAt: string;
  updatedAt: string;
  value: number;
  inventory: Inventory | undefined;
  batchNumber?: string | undefined;
}

export interface OutboundRequest {
  id: string;
  requestNumber: string;
  entityId: string;
  warehouseId: string;
  status: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  dropoffAddressLatitude: number;
  dropoffAddressLongitude: number;
  usePickuppDelivery: boolean;
  estimatedDeliveryDate: string;
  createdAt: string;
  updatedAt: string;
  orderId: string;
  outboundItems: OutboundItem[];
}

export interface PickRequest {
  id: string;
  requestNumber: string;
  warehouseId: string;
  status: string;
  handleBy: string;
  createdAt: string;
  updatedAt: string;
  pickItems: PickItem[];
  outboundRequests: OutboundRequest[];
  packStationId: string;
}

export interface PickItem {
  id: string;
  inventoryStockId: string;
  pickRequestId: string;
  status: string;
  upc: string;
  createdAt: string;
  updatedAt: string;
  quantity: number;
}

export interface PickItemDetail {
  pickItem: PickItem | undefined;
  inventory: Inventory | undefined;
  inventoryStocks: InventoryStock | undefined;
}

export interface PackStation {
  id: string;
  name: string;
  status: string;
  warehouseId: string;
  sections: PackSection[];
  createdAt: string;
  updatedAt: string;
  deletedAt?: string | undefined;
  remark: string;
}

export interface PackSection {
  id: string;
  sectionNumber: number;
  packStationId: string;
  createdAt: string;
  updatedAt: string;
  packItems: PackItem[];
}

export interface PackItem {
  id: string;
  inventoryId: string;
  pickRequestId: string;
  outboundRequestId: string;
  packSectionId: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  inventory: Inventory | undefined;
}

export const FULFILLMENT_PACKAGE_NAME = 'fulfillment';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
