/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'dhl';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: string;
  trackResponse: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  success: boolean;
  errorMessage: string;
  warehouseId: string;
}

export const DHL_PACKAGE_NAME = 'dhl';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
