/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'abx';

export interface OrderResponse {
  res: OrderResponse_Response | undefined;
}

export interface OrderResponse_Shipment {
  conNo: string;
  statusCode: string;
  statusDesc: string;
}

export interface OrderResponse_Response {
  shipment: OrderResponse_Shipment | undefined;
}

export interface UpdateTripRequest {
  req: UpdateTripRequest_Request | undefined;
}

export interface UpdateTripRequest_Status {
  conNo: string;
  refNo: string;
  location: string;
  statusCode: string;
  statusDate: string;
  statusDesc: string;
  updateDate: string;
}

export interface UpdateTripRequest_Request {
  status: UpdateTripRequest_Status | undefined;
}

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: UpdateTripRequest[];
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  errorMessage: string;
}

export interface UpdateTripResponse {
  res: UpdateTripResponse_Response | undefined;
}

export interface UpdateTripResponse_Status {
  statusCode: string;
  statusDesc: string;
}

export interface UpdateTripResponse_Response {
  status: UpdateTripResponse_Status | undefined;
}

export const ABX_PACKAGE_NAME = 'abx';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
