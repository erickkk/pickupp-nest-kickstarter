/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'agent';

export interface CurrentUser {
  id: number;
  email: string;
  deliveryAgentTypeId: number;
  status: string;
}

export interface TierMeta {
  currentPoints: number;
  currentOntime: number;
  currentRating: number;
}

export interface AgentResponse {
  id: number;
  email: string;
  name: string;
  phone: string;
  profileImageUrl: string;
  status: string;
  lastLoginAt: string;
  type: DeliveryAgentType | undefined;
  createdAt: string;
  updatedAt: string;
  registrationId: string;
  platform: string;
  latitude: string;
  longitude: string;
  isOnline: boolean;
  locationUpdatedAt: string;
  rating: string;
  outstandingCod: string;
  address: string;
  firstActivationTimestamp: string;
  firstTripTimestamp: string;
  locale: string;
  tagIds: number[];
  score: string;
  tierMeta: TierMeta | undefined;
}

export interface TierRequirement {
  requiredPoints: number;
  requiredOntime: number;
  requiredRating: number;
  maintainPoints: number;
}

export interface DeliveryAgentType {
  id: number;
  tierName: string;
  maxWeight: number;
  maxConcurrentTrip: number;
  commissionRate: string;
  isVisible: boolean;
  isVehicle: boolean;
  canBatch: boolean;
  canFragile: boolean;
  canCod: boolean;
  dispatchRadius: number;
  averageSpeed: number;
  core: boolean;
  poolRadius: number;
  penaltyRate: string;
  maxCodAmount: number;
  admin: boolean;
  canPrematch: boolean;
  fixPay: string;
  isNonBidding: boolean;
  canAcceptJob: boolean;
  payOnSuccess: boolean;
  fragileCap: number;
  tierPublicName: string;
  maxWidth: number;
  maxHeight: number;
  maxLength: number;
  weightThreshold: number;
  typeName: string;
  typePublicName: string;
  rank: number;
  tierRequirement: TierRequirement | undefined;
  nextTier?: DeliveryAgentType | undefined;
  maxVolume: number;
}

export interface RequestWithDeliveryAgentId {
  deliveryAgentId: number;
}

export interface LocaleTrainingDescription {
  details: string;
  attendance: string;
  test: string;
  material: string;
}

export interface Skill {
  id: string;
  name: string;
  enDisplay: string;
  zhDisplay: string;
  description: { [key: string]: string };
  enRequirements: { [key: string]: string };
  zhRequirements: { [key: string]: string };
  myRequirements: { [key: string]: string };
  trainingDescription: { [key: string]: LocaleTrainingDescription };
  resultUrl: string;
  validityPeriod: number;
  minTierActivation: number[];
  isActive: boolean;
  imageUrl: string;
}

export interface Skill_DescriptionEntry {
  key: string;
  value: string;
}

export interface Skill_EnRequirementsEntry {
  key: string;
  value: string;
}

export interface Skill_ZhRequirementsEntry {
  key: string;
  value: string;
}

export interface Skill_MyRequirementsEntry {
  key: string;
  value: string;
}

export interface Skill_TrainingDescriptionEntry {
  key: string;
  value: LocaleTrainingDescription | undefined;
}

export interface AgentSkill {
  id: string;
  deliveryAgentId: number;
  skillId: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  expireAt: string;
  skillInfo: Skill | undefined;
  deliveryAgent: AgentResponse | undefined;
}

export interface DistributionProperty {
  mean: number;
  max: number;
  min: number;
  stdDev: number;
}

export interface AgentProfile {
  tripsInBundle: number;
  pickupAddressType: { [key: string]: number };
  dropoffAddressType: { [key: string]: number };
  dweightRange: { [key: string]: number };
  puDistrictL3: { [key: string]: number };
  doDistrictL3: { [key: string]: number };
  distance: DistributionProperty | undefined;
  distanceToPu: DistributionProperty | undefined;
  timeDistance: DistributionProperty | undefined;
  pickupTimeOfDay: DistributionProperty | undefined;
  dropoffTimeOfDay: DistributionProperty | undefined;
  question1: string;
  question2: string;
  question3: string;
}

export interface AgentProfile_PickupAddressTypeEntry {
  key: string;
  value: number;
}

export interface AgentProfile_DropoffAddressTypeEntry {
  key: string;
  value: number;
}

export interface AgentProfile_DweightRangeEntry {
  key: string;
  value: number;
}

export interface AgentProfile_PuDistrictL3Entry {
  key: string;
  value: number;
}

export interface AgentProfile_DoDistrictL3Entry {
  key: string;
  value: number;
}

export const AGENT_PACKAGE_NAME = 'agent';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
