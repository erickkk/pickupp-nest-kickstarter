/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'geo';

export interface Geo {
  id: string;
  lat: number;
  lng: number;
  addrType: string;
  address: string;
  building: string;
  houseNumber: string;
  roadName: string;
  postal: string;
  district: string;
  area: string;
  city: string;
  source: string;
  score: number;
  similarityScore: number;
  before: string;
  matched: string;
  after: string;
  buildingType: string;
  partialMatch: boolean;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  estate: string;
  googleResponse: string;
  recognizedAddress: string;
  block: string;
  phase: string;
  segments: { [key: string]: string };
  section: string;
  lane: string;
  alley: string;
}

export interface Geo_SegmentsEntry {
  key: string;
  value: string;
}

export interface Suggest {
  id: number;
  query: string;
  address: string;
  expiredAt: string;
  source: string;
}

export interface Dictionary {
  id: number;
  word: string;
  attribute: string;
  tfIdf: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}

export interface SearchRequest {
  search: string;
  limit: number;
  offset: number;
}

export interface IDRequest {
  id: number;
}

export const GEO_PACKAGE_NAME = 'geo';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
