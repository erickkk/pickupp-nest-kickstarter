/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'cvs';

export interface Store {
  id: string;
  storeNumber: string;
  storeName: string;
  storeTel: string;
  storeCity: string;
  storeCountry: string;
  storeAddress: string;
  zipCode: string;
  dcrono: string;
  startDate: string;
  endDate: string;
  stnewno: string;
  area: string;
  rsno: string;
  equipmentId: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
}

export interface Order {
  id: number;
  orderId: string;
  orderNumber: string;
  ECNO: string;
  ODNO: string;
  STNO: string;
  AMT: number;
  CUTKNM: string;
  CUTKTL: string;
  PRODNM: string;
  ECWEB: string;
  ECSERTEL: string;
  REALAMT: number;
  TRADETYPE: string;
  SERCODE: string;
  EDCNO: string;
  ErrCode: string;
  ErrDesc: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
}

export const CVS_PACKAGE_NAME = 'cvs';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
