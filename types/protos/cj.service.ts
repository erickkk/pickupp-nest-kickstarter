/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';
import { TrackRequest, TrackResponse, Order } from './cj.model';

export const protobufPackage = 'cj';

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface UpdateTripRequest {
  TRACK: TrackRequest[];
}

export interface UpdateTripResponse {
  RESPONSE: TrackResponse[];
}

export interface GetOrderRequest {
  id: string;
}

export const CJ_PACKAGE_NAME = 'cj';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

export interface TripClient {
  updateTrip(request: UpdateTripRequest): Observable<UpdateTripResponse>;
}

export interface TripController {
  updateTrip(
    request: UpdateTripRequest,
  ):
    | Promise<UpdateTripResponse>
    | Observable<UpdateTripResponse>
    | UpdateTripResponse;
}

export function TripControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['updateTrip'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Trip', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Trip', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRIP_SERVICE_NAME = 'Trip';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<Order>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getOrder'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
