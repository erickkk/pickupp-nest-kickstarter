/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'merchant';

export enum MeilisearchIndex {
  SHOP = 0,
  SUGGESTION = 1,
  PRODUCT = 2,
  UNRECOGNIZED = -1,
}

export enum Partner {
  zipx = 0,
  poslaju = 1,
  UNRECOGNIZED = -1,
}

export interface CurrentUser {
  id: number;
  email: string;
  entityId: number;
  role: string;
  region: string;
  userStatus: string;
  entityStatus: string;
  core: boolean;
  name: string;
  express: boolean;
  fragile: boolean;
  deliveryNote: boolean;
  cashOnDelivery: boolean;
  reliable: boolean;
  payOnSuccess: boolean;
  tierId: string;
  nextTierId: string;
  publicName: string;
  pickupAddressId: string;
  locale: string;
  freightCollect: boolean;
  options: UserOptions | undefined;
  notificationOptions: NotificationOptions | undefined;
}

export interface TWInvoiceOptions {
  donation: boolean;
  taxIdNumber: string;
  loveCode: string;
  email: string;
}

export interface EntityReferralCodes {
  sop?: string | undefined;
  delivery?: string | undefined;
}

export interface Entity {
  id: number;
  name: string;
  website: string;
  phone: string;
  contactPersonName: string;
  type: string;
  status: string;
  emailEnabled: boolean;
  payOnSuccess: boolean;
  webhook: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
  masterCode: string;
  core: boolean;
  fragile: boolean;
  deliveryNote: boolean;
  cashOnDelivery: boolean;
  bankName: string;
  bankAccount: string;
  bankHolderName: string;
  pickUpSmsEnabled: boolean;
  reliable: boolean;
  tierId: string;
  nextTierId: string;
  isTierSubscribed: boolean;
  businessRegistration: string;
  isCreditLine: boolean;
  tierSubscribedAt: string;
  referral: string;
  logo: string;
  isPrematchable: boolean;
  isOwnCrew: boolean;
  useOwnSurge: boolean;
  surgeCap: string;
  surgeStep: string;
  surgeSd: string;
  checkClientReferenceNumber: boolean;
  skipGoogle: boolean;
  canOutsource: boolean;
  dropoffOptionsOld: string;
  canExchange: boolean;
  exchangeRate: string;
  canInitialBundle: boolean;
  publicName: string;
  ratingSmsEnabled: boolean;
  enrouteSmsEnabled: boolean;
  exchangeDestination: string;
  rescheduleSmsEnabled: boolean;
  maskItemEnabled: boolean;
  allowUnableToPickup: string;
  warehouseWhitelist: string[];
  customNotification: string[];
  pickupOptionsOld: string;
  project: Tag | undefined;
  fixedOutsourcePartner: string;
  freightCollect: boolean;
  portals: EntityPortal[];
  shopifyEmailEnabled: boolean;
  matchingOptions: MatchingOptions | undefined;
  sopOptions: SopOptions | undefined;
  convenienceStoreEnabled?: boolean | undefined;
  canMultiParcel: boolean;
  twInvoiceOptions: TWInvoiceOptions | undefined;
  referralCodes: EntityReferralCodes | undefined;
  fulfillmentWarehouses: string[];
  pickupOptions: { [key: string]: PickupOptions };
  dropoffOptions: { [key: string]: DropoffOptions };
}

export interface Entity_PickupOptionsEntry {
  key: string;
  value: PickupOptions | undefined;
}

export interface Entity_DropoffOptionsEntry {
  key: string;
  value: DropoffOptions | undefined;
}

export interface User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  profileImageUrl: string;
  status: string;
  lastLoginAt: string;
  role: string;
  createdAt: string;
  updatedAt: string;
  locale: string;
  entityId: number;
  fcmToken: string;
  platform: string;
  portals: UsersPortal[];
  pickupAddressId: string;
  shops: Shop[];
  options: UserOptions | undefined;
  notificationOptions: NotificationOptions | undefined;
}

export interface NotificationOptions {
  pushSopPoFulfillment: boolean;
  pushSopPoCancellation: boolean;
  pushDeliveryOrderCreated: boolean;
  pushDeliveryOrderRescheduled: boolean;
  pushDeliveryOrderAccepted: boolean;
  pushDeliveryReviveOrder: boolean;
  pushDeliveryOrderExpired: boolean;
  pushDeliveryOrderDroppedOff: boolean;
  pushDeliveryOrderCancellation: boolean;
  pushDeliveryOrderUnableToPickup: boolean;
  pushDeliveryOrderUnableToDeliver: boolean;
  pushDeliveryOrderEnroute: boolean;
  pushDeliveryOrderInboundToWarehouse: boolean;
  emailSopPoFulfillment: boolean;
  emailSopPoCancellation: boolean;
  emailDeliveryOrderExpired: boolean;
}

export interface UserOptions {
  hidePricing: boolean;
}

export interface EmptyRequest {}

export interface RequestWithSession {
  currentUser: CurrentUser | undefined;
}

export interface UpdatePromotionRequest {
  id: number;
  orderId: string;
}

export interface RequestWithSessionAndId {
  id: number;
  currentUser: CurrentUser | undefined;
}

export interface RequestWithOrderId {
  orderId: string;
}

export interface RequestWithEntityId {
  entityId: number;
}

export interface RequestWithAgentId {
  agentId: number;
}

export interface RequestWithUuid {
  id: string;
}

export interface RequestWithId {
  id: number;
}

export interface EmptyResponse {}

export interface SuccessResponse {
  success: boolean;
}

export interface Tag {
  id: number;
  name: string;
  /** @deprecated */
  isDaVisible: boolean;
  createdAt: string;
  updatedAt: string;
  systemTag: boolean;
  displayName: Locale | undefined;
  /** @deprecated */
  isCategory: boolean;
  /** using type for project & category type from now on */
  type: string;
  rank: number;
  fixedTripPrice: number;
  slug: string;
  instantAssignment: boolean;
}

export interface SummaryConfig {
  id: number;
  entityId: number;
  paymentTerms: string;
  paymentInstructions: string;
  summaryNotes: string;
  customDeal: boolean;
  customContent: string;
  officialName: string;
  addressLine1: string;
  addressLine2: string;
  attn: string;
  ccList: string[];
}

export interface Blacklist {
  id: number;
  entityId: number;
  agentId: number;
}

export interface Notification {
  id: number;
  entityId: number;
  type: string;
  templateId: string;
  customContent: string;
  triggerAt: string;
  sendTo: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  title: string;
}

export interface Contact {
  id: number;
  email: string;
  name: string;
  mobileNumber: string;
  createdAt: string;
  updatedAt: string;
}

export interface Address {
  id: number;
  type: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  latitude: string;
  longitude: string;
  phone: string;
  createdAt: string;
  updatedAt: string;
  frequency: number;
  entityId: string;
  userId: string;
}

export interface Item {
  id: number;
  name: string;
  width: number;
  height: number;
  length: number;
  weight: number;
  createdAt: string;
  updatedAt: string;
  frequency: number;
}

export interface DimensionWeightModel {
  dimension: string;
  weight: string;
  price: string;
}

export interface DistanceModelPricing {
  distance: string;
  price: string;
}

export interface PickupOptions {
  actions: PickupOptions_PickupActions | undefined;
  disableContactSender: boolean;
}

export interface PickupOptions_PickupActions {
  geolocation: boolean;
  qrOrPasscode: boolean;
  pickupPhoto: boolean;
  pickupCallLog: boolean;
  reportedSubOrderCount: boolean;
}

export interface DropoffOptions {
  disableNoSignature: boolean;
  disableReschedule: boolean;
  disableDropOff: boolean;
  enableSelfCollection: boolean;
  actions: DropoffOptions_DropoffActions | undefined;
  disableBackToPickupPoint: boolean;
  disableContactRecipient: boolean;
}

export interface DropoffOptions_DropoffActions {
  geolocation: boolean;
  qrOrPasscode: boolean;
  dropoffPhoto: boolean;
  signaturePhoto: boolean;
  recipientVerifyCode: boolean;
}

export interface SopOptions {
  sopCommission: number;
  referralPromoCode: string;
}

export interface MatchingOptions {
  canPool: boolean;
  canDispatch: boolean;
  canPrematch: boolean;
  canJobAssign: boolean;
}

export interface MerchantOption {
  id: number;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  dropoffTimeOverwrite: { [key: string]: string };
  earliestPickupTime: string;
  firstLeg: boolean;
  timeWindow: string;
  cutoffTime: string;
  divertTime: string;
}

export interface MerchantOption_DropoffTimeOverwriteEntry {
  key: string;
  value: string;
}

export interface MerchantContract {
  id: string;
  entityId: number;
  contractStart: string;
  contractEnd: string;
  contractLink: string;
  paymentMethod: string;
  serviceType: string;
  creditTerms: string;
  remarks: string;
}

export interface UsersPortal {
  id: number;
  portalId: string;
  portal: string;
  userId: number;
  email: string;
  createdAt: string;
  updatedAt: string;
  originalResponse: string;
}

export interface PromotionCode {
  id: number;
  promotionId: number;
  adminId: number;
  code: string;
  expiredAt: string;
  createdAt: string;
  updatedAt: string;
  redeemCount: number;
  metadata?: PromotionCode_Metadata | undefined;
  allowedEntityId: number;
}

export interface PromotionCode_Metadata {
  senderEntityId?: number | undefined;
  sourceMerchantPromoId?: string | undefined;
}

export interface PromotionRule {
  type: string;
  value: string;
  children: PromotionRule[];
}

export interface Promotion {
  id: number;
  amount: number;
  daysValid: number;
  description: string;
  redeemCount: number;
  rules: PromotionRule[];
  name: string;
  createdAt: string;
  updatedAt: string;
  codesCount: number;
  key: string;
  percentage: number;
  expireAt: string;
}

export interface Qr {
  id: string;
  promotion: Promotion | undefined;
  entityId: string;
  orderId: string;
  createdAt: string;
  updatedAt: string;
  batchId: string;
}

export interface Batch {
  batchId: string;
  promotion: Promotion | undefined;
  createdAt: string;
  total: number;
}

export interface Locale {
  en: string;
  zh: string;
  ms: string;
  vi: string;
  th: string;
}

export interface ShopTimeslot {
  from: string;
  to: string;
  id: number;
  shopId: number;
  cutoffDays: number;
  cutoffTime: string;
  type: string;
}

export interface AdditionalNotes {
  enabled: boolean;
  label: Locale | undefined;
  required: boolean;
}

export interface DynamicLink {
  title: string;
  description: string;
  imageUrl: string;
}

export interface TimeSlotDateRange {
  startDate: string;
  endDate: string;
}

export interface Shop {
  id: number;
  entityId: number;
  name: string;
  addressLine1: string;
  addressLine2: string;
  latitude: number;
  longitude: number;
  createdAt: string;
  updatedAt: string;
  timeslots: ShopTimeslot[];
  maxDaysAhead: number;
  excludedWeekDays: string[];
  entity: Entity | undefined;
  link: string;
  dynamicLink: DynamicLink | undefined;
  tags: Tag[];
  bannerImageUrl: string;
  description: Locale | undefined;
  status: string;
  deliveryInfo: Locale | undefined;
  profileImageUrl: string;
  deliveryFee: number;
  districts: string[];
  checkoutThreshold: number;
  freeDeliveryThreshold: number;
  categories: Tag[];
  groupId: number;
  pickupTimeBuffer: number;
  timeslotDateRanges: TimeSlotDateRange[];
  project: Tag | undefined;
  contactPersonName: string;
  contactPersonPhone: string;
  searchKeywords: string;
  ignoreHolidays: boolean;
  faq: Locale | undefined;
  slug: string;
  deliveryOptions: string[];
}

export interface Product {
  id: string;
  name: string;
  longDescription: Locale | undefined;
  price: number;
  width: number;
  height: number;
  length: number;
  weight: number;
  imageUrl: string;
  createdAt: string;
  updatedAt: string;
  link: string;
  entity: Entity | undefined;
  additionalNotes: AdditionalNotes | undefined;
  dynamicLink: DynamicLink | undefined;
  tags: Tag[];
  shortDescription: Locale | undefined;
  shops: Shop[];
  collections: Collection[];
  packs: Pack[];
  entityId: number;
  type: string;
  originalPrice: number;
  slug: string;
}

export interface PurchaseOrderItem {
  id: string;
  buyerId: number;
  sellerId: number;
  purchaseOrderId: string;
  productId: string;
  quantity: number;
  subtotal: number;
  discount: number;
  total: number;
  createdAt: string;
  updatedAt: string;
  product: Product | undefined;
  notes: string;
  shopTimeslotId: number;
  dropoffDate: string;
  notesLabel: Locale | undefined;
  packs: PurchaseOrderItem_PackItem[];
  groupId: string;
  purchaseOrder: PurchaseOrder | undefined;
}

export interface PurchaseOrderItem_ProductItem {
  productId: string;
  quantity: number;
  note: string;
}

export interface PurchaseOrderItem_PackItem {
  packId: string;
  products: PurchaseOrderItem_ProductItem[];
}

export interface PurchaseOrder {
  id: string;
  buyerId: number;
  sellerId: number;
  shopId: number;
  orderNumber: string;
  referenceCode: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  dropoffLat: number;
  dropoffLng: number;
  dropoffNotes: string;
  description: string;
  width: number;
  length: number;
  height: number;
  dimension: number;
  weight: number;
  status: string;
  subtotal: number;
  discount: number;
  tax: number;
  deliveryPrice: number;
  totalPrice: number;
  paymentMethod: string;
  paymentStatus: string;
  transactionId: number;
  createdAt: string;
  updatedAt: string;
  buyer: Entity | undefined;
  seller: Entity | undefined;
  shop: Shop | undefined;
  items: PurchaseOrderItem[];
  orderId: string;
  shopTimeslotId: number;
  dropoffDate: string;
  timeslot: ShopTimeslot | undefined;
  origin: string;
  buyerUserId: number;
  deliveryOption: string;
  confirmationCode: string;
  paymentToken: string;
}

export interface OrderOverride {
  id: number;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  orderDefaults: { [key: string]: string };
  createdAt: string;
  updatedAt: string;
}

export interface OrderOverride_OrderDefaultsEntry {
  key: string;
  value: string;
}

export interface ServiceType {
  id: number;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  timeUnit: string;
  createdAt: string;
  updatedAt: string;
  isDefault: boolean;
}

export interface Geometry {
  lat: string;
  lng: string;
}

export interface PricingModel {
  id: number;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  distancePricingModel: DistanceModelPricing[];
  dimensionWeightModel: DimensionWeightModel[];
  createdAt: string;
  updatedAt: string;
  type: string;
  omitPrice: string;
  discountRate: number;
  fixPrice: string;
  customPricingEnabled: boolean;
  city: string;
}

export interface Inventory {
  id: string;
  productId: string;
  shopId: number;
  shop: Shop | undefined;
  product: Product | undefined;
  stock: number;
  timeslotCapacity: number;
  purchaseCap: number;
  useStock: boolean;
  status: string;
  fromDate: string;
  toDate: string;
  originalStock: number;
}

export interface Collection {
  id: number;
  entityId: number;
  label: Locale | undefined;
  required: boolean;
  products: Product[];
  description: Locale | undefined;
  name: string;
  shops: Shop[];
}

export interface ShopCollection {
  id: number;
  shopId: number;
  collectionId: number;
  collection: Collection | undefined;
  type: string;
  rank: number;
}

export interface PackItem {
  id: number;
  packId: string;
  productId: string;
  label: string;
  price: number;
}

export interface Pack {
  id: string;
  entityId: number;
  name: string;
  items: Pack_ProductItem[];
  createdAt: string;
  updatedAt: string;
  productPack: ProductPack | undefined;
}

export interface Pack_ProductItem {
  id: string;
  name: string;
  status: string;
  price: string;
  packItem: PackItem | undefined;
}

export interface ProductPack {
  id: number;
  productId: string;
  packId: string;
  label: Locale | undefined;
  required: boolean;
  multiplePerChoice: boolean;
  minQuantity: number;
  maxQuantity: number;
  createdAt: string;
  updatedAt: string;
  pack: Pack | undefined;
}

export interface InventoryLog {
  id: number;
  inventoryId: string;
  quantity: number;
  balance: number;
  purchaseOrderItemId: string;
  adminId: number;
  userId: number;
  purchaseOrderItem: PurchaseOrderItem | undefined;
  createdAt: string;
  updatedAt: string;
}

export interface EntityPortal {
  id: number;
  entityId: number;
  type: string;
  value: string;
  options: { [key: string]: string };
}

export interface EntityPortal_OptionsEntry {
  key: string;
  value: string;
}

export interface Owner {
  id: number;
  adminId: number;
  entityId: number;
  ownershipStartDate: string;
  ownershipEndDate: string;
  ownerType: string;
  createdAt: string;
  updatedAt: string;
}

export interface VolumetricPricingModel {
  weight: string;
  price: string;
}

export interface LineHaul {
  id: string;
  fromCity: string;
  fromRegion: string;
  toCity: string;
  toRegion: string;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  warehouseId: string;
  createdAt: string;
  updatedAt: string;
  volumetricPricingModel: VolumetricPricingModel[];
}

export interface MerchantPromotion {
  id: number;
  entityId: number;
  promotionId: number;
  promotionCodeId: number;
  userId: number;
  orderId: string;
  purchaseOrderId: string;
  createdAt: string;
}

export interface Qualification {
  id: string;
  entityId: string;
  name: string;
  value: string;
  createdAt: string;
  updatedAt: string;
}

export const MERCHANT_PACKAGE_NAME = 'merchant';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
