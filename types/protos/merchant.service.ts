/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import {
  Partner,
  MeilisearchIndex,
  UsersPortal,
  User,
  CurrentUser,
  MerchantContract,
  Entity,
  ServiceType,
  Blacklist,
  OrderOverride,
  MerchantOption,
  EntityPortal,
  Address,
  Item,
  Notification,
  Contact,
  Tag,
  Locale,
  PromotionCode,
  Promotion,
  PromotionCode_Metadata,
  Qr,
  Batch,
  Shop,
  Inventory,
  ShopTimeslot,
  InventoryLog,
  Product,
  ProductPack,
  PurchaseOrder,
  Geometry,
  PricingModel,
  Collection,
  ShopCollection,
  Pack,
  LineHaul,
  VolumetricPricingModel,
  Qualification,
  EmptyResponse,
  SuccessResponse,
  SummaryConfig,
  MerchantPromotion,
  Owner,
  RequestWithSession,
  RequestWithSessionAndId,
  RequestWithEntityId,
  RequestWithId,
  RequestWithUuid,
  RequestWithOrderId,
  UpdatePromotionRequest,
  EmptyRequest,
  PackItem,
} from './merchant.model';
import { Observable } from 'rxjs';
import { Order } from './order.model';
import { Region } from './address.model';

export const protobufPackage = 'merchant';

export interface AuthRequest {
  token: string;
}

export interface SignupRequest {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  phone: string;
  website: string;
  companyName: string;
  locale: string;
  type: string;
  referral: string;
  platform: string;
  promo: string;
  oauthData: UsersPortal | undefined;
  qualification: { [key: string]: string };
  origin: string;
}

export interface SignupRequest_QualificationEntry {
  key: string;
  value: string;
}

export interface LoginRequest {
  email: string;
  password: string;
  ipAddress: string;
  userAgent: string;
  portalId: string;
  portalType: string;
}

export interface LoginResponse {
  token: string;
  loginEmail: string;
}

export interface LogoutRequest {
  id: number;
  token: string;
}

export interface ForgotPasswordRequest {
  email: string;
  phone: string;
  origin: string;
}

export interface UpdatePasswordRequest {
  password: string;
  token: string;
}

export interface GetLoginRequest {
  id: number;
}

export interface ResendVerificationRequest {
  email: string;
  phone: string;
  origin: string;
}

export interface GetUsersResponse {
  users: User[];
  total: number;
}

export interface CreateUserRequest {
  currentUser: CurrentUser | undefined;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  password: string;
}

export interface UpdateUserRequest {
  currentUser: CurrentUser | undefined;
  id: number;
  user: User | undefined;
}

export interface ResetPasswordRequest {
  currentUser: CurrentUser | undefined;
  id: string;
  password: string;
  token: string;
}

export interface GetUsersByEntityIdsRequest {
  entityIds: number[];
}

export interface FcmTokenRequest {
  id: string;
  fcmToken: string;
  platform: string;
}

export interface LinkOauthToUserRequest {
  oauthData: UsersPortal | undefined;
}

export interface RequestFilter {
  attribute: string;
  operation: string;
  value: string;
}

export interface FetchUsersRequest {
  limit: string;
  offset: string;
  query: string;
  sortBy: string;
  sortDirection: string;
  filters: RequestFilter[];
  includes: string[];
  attributes: string[];
}

export interface FetchEntitiesRequest {
  limit: string;
  offset: string;
  query: string;
  sortBy: string;
  sortDirection: string;
  filters: RequestFilter[];
  attributes: string[];
  tags: number[];
  hasShop: boolean;
}

export interface GetMerchantContractsRequest {
  entityId: number;
}

export interface GetMerchantContractsResponse {
  merchantContracts: MerchantContract[];
  total: number;
}

export interface GetEntitiesByIdsRequest {
  ids: number[];
}

export interface GetEntitiesResponse {
  entities: Entity[];
  total: number;
}

export interface UpdateEntityRequest {
  currentUser: CurrentUser | undefined;
  entity: Entity | undefined;
}

export interface GetServiceTypesByEntityIdResponse {
  serviceTypes: ServiceType[];
}

export interface VerifyEntityRequest {
  token: string;
}

export interface UpdateMasterCodeResponse {
  masterCode: string;
}

export interface CreditLineEntitiesRequest {}

export interface CreditLineEntitiesResponse {
  entities: Entity[];
}

export interface GetSummaryConfigRequest {
  entityId: number;
}

export interface GetBlacklistsByAgentIdRequest {
  agentId: number;
  limit: number;
  offset: number;
}

export interface GetBlacklistsResponse {
  list: Blacklist[];
  total: number;
}

export interface CreateEntityBlacklistRequest {
  entityId: number;
  agentId: number;
}

export interface BulkEntityBlacklistRequest {
  agentIds: number[];
  entityIds: number[];
}

export interface RemoveEntityBlacklistRequest {
  id: number;
}

export interface RedeemPromoCodeRequest {
  entityId: number;
  promo: string;
  creditAmount: number;
}

export interface RedeemPromoCodeResponse {
  amount: string;
  daysValid: number;
  merchantPromotionId: number;
}

export interface GetOrderOverridesByEntityIdResponse {
  overrides: OrderOverride[];
}

export interface CreateEntityOrderOverrideRequest {
  entityId: number;
  serviceType: string;
  serviceTime: number;
  orderDefaults: { [key: string]: string };
}

export interface CreateEntityOrderOverrideRequest_OrderDefaultsEntry {
  key: string;
  value: string;
}

export interface UpdateEntityOrderOverrideRequest {
  id: number;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  orderDefaults: { [key: string]: string };
}

export interface UpdateEntityOrderOverrideRequest_OrderDefaultsEntry {
  key: string;
  value: string;
}

export interface GetMerchantOptionByServiceRequest {
  entityId: number;
  serviceType: string;
  serviceTime: number;
}

export interface GetMerchantOptionsResponse {
  merchantOptions: MerchantOption[];
}

export interface EntityPortalRequest {
  entityId: number;
  type: string;
  value: string;
}

export interface EntityPortalResponse {
  portals: EntityPortal[];
}

export interface GetAddressesRequest {
  currentUser: CurrentUser | undefined;
  query: string;
  type: string;
  limit: string;
  offset: string;
}

export interface CreateAddressRequest {
  currentUser: CurrentUser | undefined;
  type: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  latitude: string;
  longitude: string;
  phone: string;
  isDefault: boolean;
}

export interface UpdateAddressRequest {
  currentUser: CurrentUser | undefined;
  id: string;
  type: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  latitude: string;
  longitude: string;
  phone: string;
  isDefault: boolean;
}

export interface GetAddressesResponse {
  addresses: Address[];
  total: number;
}

export interface GetItemsRequest {
  currentUser: CurrentUser | undefined;
  name: string;
  limit: string;
}

export interface GetItemsResponse {
  items: Item[];
  total: number;
}

export interface CreateItemRequest {
  currentUser: CurrentUser | undefined;
  name: string;
  width: string;
  height: string;
  length: string;
  weight: string;
}

export interface GetNotificationsRequest {
  currentUser: CurrentUser | undefined;
  limit: string;
  offset: string;
  status: string;
}

export interface GetNotificationsResponse {
  notifications: Notification[];
  total: number;
}

export interface CreateNotificationRequest {
  currentUser: CurrentUser | undefined;
  type: string;
  templateId: string;
  customContent: string;
  triggerAt: string;
  sendTo: string;
  status: string;
  title: string;
}

export interface UpdateNotificationRequest {
  id: number;
  type: string;
  templateId: string;
  customContent: string;
  triggerAt: string;
  sendTo: string;
  status: string;
  title: string;
  currentUser: CurrentUser | undefined;
}

export interface TriggerNotificationRequest {
  order: Order | undefined;
}

export interface ContactsResponse {
  contacts: Contact[];
}

export interface GetContactsRequest {
  id: number;
  email: string;
  mobileNumber: string;
  limit: string;
  offset: string;
}

export interface GetContactsResponse {
  contacts: Contact[];
  total: number;
}

export interface CreateContactRequest {
  email: string;
  name: string;
  mobileNumber: string;
  type: string;
}

export interface CreateContactsRequest {
  contacts: CreateContactRequest[];
  orderId: string;
  currentUser: CurrentUser | undefined;
}

export interface RemoveNotificationRequestRequest {
  contactId: number;
  orderId: string;
}

export interface GetTagsRequest {
  query: string;
  /** @deprecated */
  isDaVisible: boolean;
  limit: string;
  offset: string;
  /** @deprecated */
  isCategory: boolean;
  types: string[];
  ids: number[];
  instantAssignment: boolean;
}

export interface GetTagsResponse {
  tags: Tag[];
  total: number;
}

export interface CreateTagRequest {
  name: string;
  displayName: Locale | undefined;
  /** @deprecated */
  isCategory: boolean;
  type: string;
  fixedTripPrice: number;
  instantAssignment: boolean;
}

export interface UpdateTagRequest {
  id: number;
  name: string;
  /** @deprecated */
  isDaVisible: boolean;
  displayName: Locale | undefined;
  /** @deprecated */
  isCategory: boolean;
  type: string;
  fixedTripPrice: number;
  slug: string;
  instantAssignment: boolean;
}

export interface UpdateShopTagsRequest {
  shopId: number;
  tagIds: number[];
}

export interface AssociateProductTagRequest {
  id: number;
  productId: string;
}

export interface GetMerchantReferralHistoryRequest {
  entityId: string;
  type: string;
  offset: number;
  limit: number;
}

export interface GetMerchantReferralHistoryResponse {
  total: number;
  items: GetMerchantReferralHistoryResponse_ReferenceHistoryItem[];
}

export interface GetMerchantReferralHistoryResponse_ReferenceHistoryItem {
  entity: Entity | undefined;
  redeemed: boolean;
  promotionCode: PromotionCode | undefined;
}

export interface GetPromotionsRequest {
  query: string;
  limit: string;
  offset: string;
  type: string;
}

export interface GetPromotionsResponse {
  promotions: Promotion[];
  total: number;
}

export interface GetPromotionRequest {
  id: number;
  type: string;
  origin: string;
}

export interface ValidatePromotionCodeRequest {
  code: string;
  payload: { [key: string]: string };
}

export interface ValidatePromotionCodeRequest_PayloadEntry {
  key: string;
  value: string;
}

export interface BulkCreatePromotionCodeRequest {
  quantity: number;
  promotionId: number;
  adminId: number;
  expiredAt: string;
  allowedEntityId: number;
  metadata?: PromotionCode_Metadata | undefined;
}

export interface GetPromotionCodesRequest {
  promotionId: number;
  limit: string;
  offset: string;
  query: string;
}

export interface GetPromotionCodeRequest {
  id: number;
  code: string;
}

export interface GetPromotionCodesResponse {
  promotionCodes: PromotionCode[];
  total: number;
}

export interface RemovePromotionCodeRequest {
  id: number;
  promotionId: number;
}

export interface GetPromotionCodeUsageRequest {
  id: number;
  limit: string;
  offset: string;
}

export interface GetPromotionCodeUsageResponse {
  merchants: GetPromotionCodeUsageResponse_PromotionUsage[];
  total: number;
}

export interface GetPromotionCodeUsageResponse_PromotionUsage {
  merchantName: string;
  entityId: number;
  promotionId: number;
  promotionCodeId: number;
  userId: number;
  orderId: string;
  purchaseOrderId: string;
  createdAt: string;
}

export interface CreateReceiverPromotionCodeRequest {
  type: string;
  adminId: number;
  metadata: CreateReceiverPromotionCodeRequest_Metadata | undefined;
}

export interface CreateReceiverPromotionCodeRequest_Metadata {
  senderEntityId?: number | undefined;
}

export interface GenerateSenderPromotionCodeRequest {
  origin: string;
  entityId: number;
  purchaseOrderId: string;
}

export interface GetQrListRequest {
  query: string;
  limit: string;
  offset: string;
  sortBy: string;
  batchId: string;
  status: string;
  orderId: string;
  entityId: number;
}

export interface GetQrListResponse {
  qrList: Qr[];
  total: number;
}

export interface CreateQrListRequest {
  promotionId: string;
  batchId: string;
  count: number;
}

export interface UpdateQrRequest {
  id: string;
  promotionId: string;
  entityId: string;
  orderId: string;
}

export interface GetBatchesResponse {
  batches: Batch[];
}

export interface CreateBatchRequest {
  count: number;
  promotionId: string;
}

export interface GetShopRequest {
  id: string;
  slug: string;
}

export interface ReorderShopCollectionsRequest {
  shopId: number;
  collectionIds: number[];
}

export interface RequestWithShopId {
  shopId: string;
}

export interface GetBestShopRequest {
  shopId: number;
  districts: string[];
  shopSlug: string;
}

export interface GetShopsRequest {
  query: string;
  limit: string;
  offset: string;
  entityId: number;
  category: string;
  statuses: string[];
  districts: string[];
  ids: number[];
  tagId: number;
  primaryOnly: boolean;
  tagSlug: string;
}

export interface GetShopsResponse {
  shops: Shop[];
  total: number;
}

export interface GetInventoryRequest {
  shopId: number;
  productId: string;
  productSlug: string;
}

export interface GetBestInventoryRequest {
  productId: string;
  districts: string[];
  productSlug: string;
}

export interface GetInventoriesRequest {
  shopId: number;
  limit: number;
  offset: number;
  productIds: string[];
  productTypes: string[];
  query: string;
  sortBy: string;
  hideUnlimited: boolean;
  statuses: string[];
  ids: string[];
}

export interface GetInventoriesResponse {
  total: number;
  inventories: Inventory[];
}

export interface UpdateInventoryRequest {
  productId: string;
  shopId: number;
  stock: number;
  timeslotCapacity: number;
  adminId: number;
  purchaseCap: number;
  useStock: boolean;
  status: string;
  fromDate: string;
  toDate: string;
  originalStock: number;
}

export interface UpdateShopDistrictsRequest {
  shopId: number;
  districts: string[];
}

export interface GetShopTimeslotsRequest {
  shopId: number;
}

export interface GetShopTimeslotsResponse {
  slots: ShopTimeslot[];
  total: number;
}

export interface GetShopAvailableTimeslotsRequest {
  shopId: number;
  productIds: string[];
}

export interface GetShopAvailableTimeslotsResponse {
  timeSlots: GetShopAvailableTimeslotsResponse_TimeSlot[];
}

export interface GetShopAvailableTimeslotsResponse_RemainingCapacity {
  productId: string;
  capacity: number;
}

export interface GetShopAvailableTimeslotsResponse_DeliverySlot {
  id: number;
  from: string;
  to: string;
  remainingCapacity: GetShopAvailableTimeslotsResponse_RemainingCapacity[];
  cutoffDays: number;
  cutoffTime: string;
  type: string;
}

export interface GetShopAvailableTimeslotsResponse_TimeSlot {
  date: string;
  slots: GetShopAvailableTimeslotsResponse_DeliverySlot[];
}

export interface GetAvailableTimeslotsRequest {
  shopIds: number[];
}

export interface GetAvailableTimeslotsResponse {
  timeslots: GetAvailableTimeslotsResponse_TimeSlot[];
}

export interface GetAvailableTimeslotsResponse_TimeSlot {
  shopId: number;
  date?: string | undefined;
  slot?: ShopTimeslot | undefined;
}

export interface GetShopUsersResponse {
  total: number;
  users: User[];
}

export interface UpdateShopUsersRequest {
  shopId: number;
  userIds: number[];
}

export interface GetInventoryLogsRequest {
  productId: string;
  shopId: number;
  offset: number;
  limit: number;
  order: string;
  orderBy: string;
}

export interface GetInventoryLogsResponse {
  logs: InventoryLog[];
  total: number;
}

export interface ReorderTagShopsRequest {
  tagId: number;
  shopIds: number[];
}

export interface ReorderTagsRequest {
  tagIds: number[];
}

export interface GetProductRequest {
  id: string;
  slug: string;
}

export interface GetProductsRequest {
  entityId: number;
  limit: string;
  offset: string;
  query: string;
  ids: string[];
  types: string[];
  shopId: number;
  includeShops: boolean;
  includeCollections: boolean;
  sortBy: string;
  collectionIds: string[];
}

export interface GetProductsResponse {
  products: Product[];
  total: number;
}

export interface GetProductPacksRequest {
  productIds: string[];
  packId: string;
  offset: number;
  limit: number;
  collectionIds: number[];
  inventoryStatuses: string[];
  slugs: string[];
}

export interface GetProductPacksResponse {
  productPacks: ProductPack[];
  total: number;
}

export interface GetDatafeedRequest {
  limit: number;
  offset: number;
}

export interface GetDatafeedResponse {
  products: GetDatafeedResponse_Item[];
  total: number;
}

export interface GetDatafeedResponse_Item {
  product: Product | undefined;
  inventories: Inventory[];
}

export interface AssociateProductPackRequest {
  packId: string;
  productId: string;
  label: Locale | undefined;
  required: boolean;
  multiplePerChoice: boolean;
  minQuantity: number;
  maxQuantity: number;
}

export interface DisassociateProductPackRequest {
  packId: string;
  productId: string;
}

export interface ReorderProductPacksRequest {
  productId: string;
  packIds: string[];
}

export interface GetPurchaseOrderRequest {
  id: string;
  referenceCode: string;
  entityId: number;
}

export interface GetPurchaseOrdersRequest {
  buyerId: string;
  sellerId: string;
  limit: string;
  offset: string;
  status: string;
  paymentStatus: string;
  statuses: string[];
  sort: GetPurchaseOrdersRequest_Sort[];
  shopId: number;
  userId: number;
  query: string;
  startTime: string;
  endTime: string;
  referenceCodes: string[];
  dropoffStartTime: string;
  dropoffEndTime: string;
  deliveryType: string[];
}

export interface GetPurchaseOrdersRequest_Sort {
  field: string;
  order: string;
}

export interface GetPurchaseOrdersResponse {
  list: PurchaseOrder[];
  total: number;
}

export interface CreatePurchaseOrderRequest {
  purchaseOrder: PurchaseOrder | undefined;
  promotionCode: string;
  paymentType: string;
  paymentVendor: string;
  clientIp: string;
}

export interface CancelPurchaseOrderRequest {
  id: string;
  refundAmount: string;
  sendEmail: boolean;
  adminId?: number | undefined;
}

export interface ValidateOrderRequest {
  order: Order | undefined;
  entityId: number;
  pristine: boolean;
  omitCalculateTime: boolean;
}

export interface ValidateOrderResponse {
  errors: { [key: string]: string };
  isValid: boolean;
}

export interface ValidateOrderResponse_ErrorsEntry {
  key: string;
  value: string;
}

export interface QuoteResponse {
  total: string;
  originalPrice: string;
  currency: string;
  breakdown: QuoteResponse_Breakdown | undefined;
  taxPrice: string;
  partner: string;
  errors: { [key: string]: string };
  metadata: { [key: string]: string };
}

export interface QuoteResponse_Breakdown {
  basicRate: string;
  distanceRate: string;
  pickupSurcharge: string;
  dropoffSurcharge: string;
  expressRate: string;
  expressSurcharge: string;
  reliabilityRate: string;
  adjustmentRate: string;
  discountRate: string;
  weightRate: string;
  wallSurcharge: string;
  exchangeRate: string;
}

export interface QuoteResponse_ErrorsEntry {
  key: string;
  value: string;
}

export interface QuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface GetQuoteRequest {
  dimension: number;
  weight: number;
  distance: number;
  from: Geometry | undefined;
  to: Geometry[];
  pickupTime: string;
  dropoffTime: string;
  reliability: boolean;
  discount: number;
  canExpress: boolean;
  entityId: number;
  exchangeRate: number;
  serviceType: string;
  serviceTime: number;
  canOutsource: boolean;
  fixedOutsourcePartner: string;
  pickupZipCode: string;
  pickupCity: string;
  dropoffZipCode: string;
  dropoffCity: string;
  length: number;
  height: number;
  width: number;
  omitCalculateTime: boolean;
  pickupAddressLine1: string;
  pickupAddressLine2: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  outsourceId: string;
  pickupRegions: Region[];
  dropoffRegions: Region[];
  items: Item[];
  partner: Partner;
}

export interface GetPricingModelsRequest {
  type: string;
  entityId: string;
  city: string;
}

export interface GetPricingModelsResponse {
  pricingModels: PricingModel[];
}

export interface GetCollectionRequest {
  id: number;
  includeShops: boolean;
}

export interface CollectionListResponse {
  total: number;
  collections: Collection[];
}

export interface CollectionProductAssociationRequest {
  collectionId: number;
  productId: string;
}

export interface GetCollectionListRequest {
  limit: string;
  offset: string;
  ids: number[];
  entityId: number;
  query: string;
  shopId: number;
  includeShops: boolean;
  sortBy: string;
  inventoryStatuses: string[];
}

export interface ReorderCollectionProductsRequest {
  collectionId: number;
  productIds: string[];
}

export interface ShopCollectionsAssociationRequest {
  shopId: number;
  collectionId: number;
  type: string;
}

export interface GetShopCollectionsResponse {
  total: number;
  shopCollections: ShopCollection[];
}

export interface GetShopCollectionsRequest {
  limit: string;
  offset: string;
  ids: number[];
  shopId: number;
  inventoryStatuses: string[];
}

export interface GetPackRequest {
  id: string;
}

export interface GetPacksRequest {
  limit: number;
  offset: number;
  query: string;
  ids: string[];
  entityId: number;
  sortBy: string;
}

export interface GetPacksResponse {
  packs: Pack[];
  total: number;
}

export interface DisassociatePackItemRequest {
  packId: string;
  productId: string;
}

export interface ReorderPackItemsRequest {
  packId: string;
  productIds: string[];
}

export interface IndexRequest {
  table: MeilisearchIndex;
}

export interface SuggestionsRequest {
  query: string;
  offset: number;
  limit: number;
  index: MeilisearchIndex;
  facetFilters: SuggestionsRequest_FacetString[];
}

export interface SuggestionsRequest_FacetString {
  filters: string[];
}

export interface SuggestionsResponse {
  suggestions: SuggestionsResponse_Suggestion[];
  total: string;
}

export interface SuggestionsResponse_Suggestion {
  id: string;
  name: string;
  matchesInfo: SuggestionsResponse_Suggestion_MatchesInfo | undefined;
  formatted: SuggestionsResponse_Suggestion_Formatted | undefined;
}

export interface SuggestionsResponse_Suggestion_MatchInfo {
  start: number;
  length: number;
}

export interface SuggestionsResponse_Suggestion_MatchesInfo {
  name: SuggestionsResponse_Suggestion_MatchInfo[];
}

export interface SuggestionsResponse_Suggestion_Formatted {
  name: string;
}

export interface GetOwnersRequest {
  query: string;
}

export interface GetOwnersResponse {
  owners: GetOwnersResponse_Owner[];
}

export interface GetOwnersResponse_Owner {
  id: number;
  adminId: number;
  entityId: number;
  ownershipStartDate: string;
  ownershipEndDate: string;
  ownerType: string;
  adminName: string;
}

export interface GetOwnerRequest {
  id: number;
}

export interface RemoveOwnerRequest {
  id: number;
}

export interface GetLineHaulRequest {
  fromCity: string;
  toCity: string;
  entityId: number;
  serviceType: string;
  serviceTime: number;
}

export interface GetLineHaulResponse {
  lineHaul: LineHaul | undefined;
}

export interface GetLineHaulsRequest {
  entityId: number;
  serviceType: string;
  serviceTime: number;
}

export interface GetLineHaulsResponse {
  lineHauls: LineHaul[];
}

export interface UpsertLineHaulRequest {
  fromCity: string;
  fromRegion: string;
  toCity: string;
  toRegion: string;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  warehouseId: string;
  volumetricPricingModel: VolumetricPricingModel[];
}

export interface UpsertLineHaulResponse {
  lineHaul: LineHaul | undefined;
}

export interface DeleteLineHaulRequest {
  id: string;
}

export interface GetDropoffCountryRequest {
  region: string;
  entityId: number;
}

export interface GetDropoffCountryResponse {
  countryLists: string[];
}

export interface GetPickupCountryRequest {
  region: string;
  entityId: number;
}

export interface GetPickupCountryResponse {
  countryLists: string[];
}

export interface QualificationRequest {
  entityId: number;
  names: string[];
}

export interface QualificationResponse {
  qualifications: Qualification[];
  total: number;
}

export interface CreateQualificationRequest {
  name: string;
  value: string;
  entityId: number;
}

export const MERCHANT_PACKAGE_NAME = 'merchant';

export interface SessionsClient {
  auth(request: AuthRequest): Observable<CurrentUser>;

  signup(request: SignupRequest): Observable<User>;

  login(request: LoginRequest): Observable<LoginResponse>;

  logout(request: LogoutRequest): Observable<EmptyResponse>;

  forgotPassword(request: ForgotPasswordRequest): Observable<EmptyResponse>;

  updatePassword(request: UpdatePasswordRequest): Observable<EmptyResponse>;

  getLogin(request: GetLoginRequest): Observable<LoginResponse>;

  resendVerification(
    request: ResendVerificationRequest,
  ): Observable<SuccessResponse>;
}

export interface SessionsController {
  auth(
    request: AuthRequest,
  ): Promise<CurrentUser> | Observable<CurrentUser> | CurrentUser;

  signup(request: SignupRequest): Promise<User> | Observable<User> | User;

  login(
    request: LoginRequest,
  ): Promise<LoginResponse> | Observable<LoginResponse> | LoginResponse;

  logout(
    request: LogoutRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  forgotPassword(
    request: ForgotPasswordRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updatePassword(
    request: UpdatePasswordRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getLogin(
    request: GetLoginRequest,
  ): Promise<LoginResponse> | Observable<LoginResponse> | LoginResponse;

  resendVerification(
    request: ResendVerificationRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function SessionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'auth',
      'signup',
      'login',
      'logout',
      'forgotPassword',
      'updatePassword',
      'getLogin',
      'resendVerification',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SESSIONS_SERVICE_NAME = 'Sessions';

export interface UsersClient {
  getUsers(request: RequestWithSession): Observable<GetUsersResponse>;

  getUser(request: RequestWithSessionAndId): Observable<User>;

  getUsersByEntityIds(
    request: GetUsersByEntityIdsRequest,
  ): Observable<GetUsersResponse>;

  fetchUsers(request: FetchUsersRequest): Observable<GetUsersResponse>;

  createUser(request: CreateUserRequest): Observable<User>;

  updateUser(request: UpdateUserRequest): Observable<User>;

  resetPassword(request: ResetPasswordRequest): Observable<EmptyResponse>;

  updateFcmToken(request: FcmTokenRequest): Observable<SuccessResponse>;

  linkOauthToUser(request: LinkOauthToUserRequest): Observable<User>;
}

export interface UsersController {
  getUsers(
    request: RequestWithSession,
  ):
    | Promise<GetUsersResponse>
    | Observable<GetUsersResponse>
    | GetUsersResponse;

  getUser(
    request: RequestWithSessionAndId,
  ): Promise<User> | Observable<User> | User;

  getUsersByEntityIds(
    request: GetUsersByEntityIdsRequest,
  ):
    | Promise<GetUsersResponse>
    | Observable<GetUsersResponse>
    | GetUsersResponse;

  fetchUsers(
    request: FetchUsersRequest,
  ):
    | Promise<GetUsersResponse>
    | Observable<GetUsersResponse>
    | GetUsersResponse;

  createUser(
    request: CreateUserRequest,
  ): Promise<User> | Observable<User> | User;

  updateUser(
    request: UpdateUserRequest,
  ): Promise<User> | Observable<User> | User;

  resetPassword(
    request: ResetPasswordRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updateFcmToken(
    request: FcmTokenRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  linkOauthToUser(
    request: LinkOauthToUserRequest,
  ): Promise<User> | Observable<User> | User;
}

export function UsersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getUsers',
      'getUser',
      'getUsersByEntityIds',
      'fetchUsers',
      'createUser',
      'updateUser',
      'resetPassword',
      'updateFcmToken',
      'linkOauthToUser',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Users', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Users', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const USERS_SERVICE_NAME = 'Users';

export interface EntitiesClient {
  fetchEntities(request: FetchEntitiesRequest): Observable<GetEntitiesResponse>;

  getEntitiesByIds(
    request: GetEntitiesByIdsRequest,
  ): Observable<GetEntitiesResponse>;

  getEntity(request: RequestWithSession): Observable<Entity>;

  updateEntity(request: UpdateEntityRequest): Observable<Entity>;

  getServiceTypesByEntityId(
    request: RequestWithEntityId,
  ): Observable<GetServiceTypesByEntityIdResponse>;

  updateEntityServiceType(request: ServiceType): Observable<EmptyResponse>;

  removeEntityServiceType(request: ServiceType): Observable<EmptyResponse>;

  verifyEntity(request: VerifyEntityRequest): Observable<Entity>;

  adminUpdateEntity(request: Entity): Observable<Entity>;

  updateMasterCode(
    request: RequestWithSession,
  ): Observable<UpdateMasterCodeResponse>;

  setMonthlyFlag(request: RequestWithSession): Observable<Entity>;

  unsetMonthlyFlag(request: RequestWithSession): Observable<Entity>;

  creditLineEntities(
    request: CreditLineEntitiesRequest,
  ): Observable<CreditLineEntitiesResponse>;

  getSummaryConfig(request: GetSummaryConfigRequest): Observable<SummaryConfig>;

  setSummaryConfig(request: SummaryConfig): Observable<SummaryConfig>;

  getBlacklistsByEntityId(
    request: RequestWithEntityId,
  ): Observable<GetBlacklistsResponse>;

  getBlacklistsByAgentId(
    request: GetBlacklistsByAgentIdRequest,
  ): Observable<GetBlacklistsResponse>;

  createEntityBlacklist(
    request: CreateEntityBlacklistRequest,
  ): Observable<Blacklist>;

  removeEntityBlacklist(
    request: RemoveEntityBlacklistRequest,
  ): Observable<EmptyResponse>;

  bulkCreateEntitiesBlacklist(
    request: BulkEntityBlacklistRequest,
  ): Observable<EmptyResponse>;

  bulkRemoveEntitiesBlacklist(
    request: BulkEntityBlacklistRequest,
  ): Observable<EmptyResponse>;

  redeemPromoCode(
    request: RedeemPromoCodeRequest,
  ): Observable<RedeemPromoCodeResponse>;

  getEntityOrderOverrides(
    request: RequestWithEntityId,
  ): Observable<GetOrderOverridesByEntityIdResponse>;

  createEntityOrderOverride(
    request: CreateEntityOrderOverrideRequest,
  ): Observable<OrderOverride>;

  updateEntityOrderOverride(
    request: UpdateEntityOrderOverrideRequest,
  ): Observable<OrderOverride>;

  removeEntityOrderOverride(request: RequestWithId): Observable<EmptyResponse>;

  getMerchantOptions(
    request: RequestWithEntityId,
  ): Observable<GetMerchantOptionsResponse>;

  getMerchantOptionByService(
    request: GetMerchantOptionByServiceRequest,
  ): Observable<MerchantOption>;

  updateMerchantOptionByService(
    request: MerchantOption,
  ): Observable<MerchantOption>;

  getEntityPortal(request: EntityPortalRequest): Observable<EntityPortal>;

  getEntityPortals(
    request: EntityPortalRequest,
  ): Observable<EntityPortalResponse>;

  linkEntityPortal(request: EntityPortal): Observable<EntityPortal>;

  unlinkEntityPortal(request: EntityPortalRequest): Observable<EmptyResponse>;

  createMerchantContract(
    request: MerchantContract,
  ): Observable<MerchantContract>;

  getMerchantContracts(
    request: GetMerchantContractsRequest,
  ): Observable<GetMerchantContractsResponse>;

  updateMerchantContract(
    request: MerchantContract,
  ): Observable<MerchantContract>;

  deleteMerchantContract(request: RequestWithUuid): Observable<EmptyResponse>;

  getQualifications(
    request: QualificationRequest,
  ): Observable<QualificationResponse>;

  createQualification(
    request: CreateQualificationRequest,
  ): Observable<Qualification>;
}

export interface EntitiesController {
  fetchEntities(
    request: FetchEntitiesRequest,
  ):
    | Promise<GetEntitiesResponse>
    | Observable<GetEntitiesResponse>
    | GetEntitiesResponse;

  getEntitiesByIds(
    request: GetEntitiesByIdsRequest,
  ):
    | Promise<GetEntitiesResponse>
    | Observable<GetEntitiesResponse>
    | GetEntitiesResponse;

  getEntity(
    request: RequestWithSession,
  ): Promise<Entity> | Observable<Entity> | Entity;

  updateEntity(
    request: UpdateEntityRequest,
  ): Promise<Entity> | Observable<Entity> | Entity;

  getServiceTypesByEntityId(
    request: RequestWithEntityId,
  ):
    | Promise<GetServiceTypesByEntityIdResponse>
    | Observable<GetServiceTypesByEntityIdResponse>
    | GetServiceTypesByEntityIdResponse;

  updateEntityServiceType(
    request: ServiceType,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  removeEntityServiceType(
    request: ServiceType,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  verifyEntity(
    request: VerifyEntityRequest,
  ): Promise<Entity> | Observable<Entity> | Entity;

  adminUpdateEntity(
    request: Entity,
  ): Promise<Entity> | Observable<Entity> | Entity;

  updateMasterCode(
    request: RequestWithSession,
  ):
    | Promise<UpdateMasterCodeResponse>
    | Observable<UpdateMasterCodeResponse>
    | UpdateMasterCodeResponse;

  setMonthlyFlag(
    request: RequestWithSession,
  ): Promise<Entity> | Observable<Entity> | Entity;

  unsetMonthlyFlag(
    request: RequestWithSession,
  ): Promise<Entity> | Observable<Entity> | Entity;

  creditLineEntities(
    request: CreditLineEntitiesRequest,
  ):
    | Promise<CreditLineEntitiesResponse>
    | Observable<CreditLineEntitiesResponse>
    | CreditLineEntitiesResponse;

  getSummaryConfig(
    request: GetSummaryConfigRequest,
  ): Promise<SummaryConfig> | Observable<SummaryConfig> | SummaryConfig;

  setSummaryConfig(
    request: SummaryConfig,
  ): Promise<SummaryConfig> | Observable<SummaryConfig> | SummaryConfig;

  getBlacklistsByEntityId(
    request: RequestWithEntityId,
  ):
    | Promise<GetBlacklistsResponse>
    | Observable<GetBlacklistsResponse>
    | GetBlacklistsResponse;

  getBlacklistsByAgentId(
    request: GetBlacklistsByAgentIdRequest,
  ):
    | Promise<GetBlacklistsResponse>
    | Observable<GetBlacklistsResponse>
    | GetBlacklistsResponse;

  createEntityBlacklist(
    request: CreateEntityBlacklistRequest,
  ): Promise<Blacklist> | Observable<Blacklist> | Blacklist;

  removeEntityBlacklist(
    request: RemoveEntityBlacklistRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  bulkCreateEntitiesBlacklist(
    request: BulkEntityBlacklistRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  bulkRemoveEntitiesBlacklist(
    request: BulkEntityBlacklistRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  redeemPromoCode(
    request: RedeemPromoCodeRequest,
  ):
    | Promise<RedeemPromoCodeResponse>
    | Observable<RedeemPromoCodeResponse>
    | RedeemPromoCodeResponse;

  getEntityOrderOverrides(
    request: RequestWithEntityId,
  ):
    | Promise<GetOrderOverridesByEntityIdResponse>
    | Observable<GetOrderOverridesByEntityIdResponse>
    | GetOrderOverridesByEntityIdResponse;

  createEntityOrderOverride(
    request: CreateEntityOrderOverrideRequest,
  ): Promise<OrderOverride> | Observable<OrderOverride> | OrderOverride;

  updateEntityOrderOverride(
    request: UpdateEntityOrderOverrideRequest,
  ): Promise<OrderOverride> | Observable<OrderOverride> | OrderOverride;

  removeEntityOrderOverride(
    request: RequestWithId,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getMerchantOptions(
    request: RequestWithEntityId,
  ):
    | Promise<GetMerchantOptionsResponse>
    | Observable<GetMerchantOptionsResponse>
    | GetMerchantOptionsResponse;

  getMerchantOptionByService(
    request: GetMerchantOptionByServiceRequest,
  ): Promise<MerchantOption> | Observable<MerchantOption> | MerchantOption;

  updateMerchantOptionByService(
    request: MerchantOption,
  ): Promise<MerchantOption> | Observable<MerchantOption> | MerchantOption;

  getEntityPortal(
    request: EntityPortalRequest,
  ): Promise<EntityPortal> | Observable<EntityPortal> | EntityPortal;

  getEntityPortals(
    request: EntityPortalRequest,
  ):
    | Promise<EntityPortalResponse>
    | Observable<EntityPortalResponse>
    | EntityPortalResponse;

  linkEntityPortal(
    request: EntityPortal,
  ): Promise<EntityPortal> | Observable<EntityPortal> | EntityPortal;

  unlinkEntityPortal(
    request: EntityPortalRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  createMerchantContract(
    request: MerchantContract,
  ):
    | Promise<MerchantContract>
    | Observable<MerchantContract>
    | MerchantContract;

  getMerchantContracts(
    request: GetMerchantContractsRequest,
  ):
    | Promise<GetMerchantContractsResponse>
    | Observable<GetMerchantContractsResponse>
    | GetMerchantContractsResponse;

  updateMerchantContract(
    request: MerchantContract,
  ):
    | Promise<MerchantContract>
    | Observable<MerchantContract>
    | MerchantContract;

  deleteMerchantContract(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getQualifications(
    request: QualificationRequest,
  ):
    | Promise<QualificationResponse>
    | Observable<QualificationResponse>
    | QualificationResponse;

  createQualification(
    request: CreateQualificationRequest,
  ): Promise<Qualification> | Observable<Qualification> | Qualification;
}

export function EntitiesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'fetchEntities',
      'getEntitiesByIds',
      'getEntity',
      'updateEntity',
      'getServiceTypesByEntityId',
      'updateEntityServiceType',
      'removeEntityServiceType',
      'verifyEntity',
      'adminUpdateEntity',
      'updateMasterCode',
      'setMonthlyFlag',
      'unsetMonthlyFlag',
      'creditLineEntities',
      'getSummaryConfig',
      'setSummaryConfig',
      'getBlacklistsByEntityId',
      'getBlacklistsByAgentId',
      'createEntityBlacklist',
      'removeEntityBlacklist',
      'bulkCreateEntitiesBlacklist',
      'bulkRemoveEntitiesBlacklist',
      'redeemPromoCode',
      'getEntityOrderOverrides',
      'createEntityOrderOverride',
      'updateEntityOrderOverride',
      'removeEntityOrderOverride',
      'getMerchantOptions',
      'getMerchantOptionByService',
      'updateMerchantOptionByService',
      'getEntityPortal',
      'getEntityPortals',
      'linkEntityPortal',
      'unlinkEntityPortal',
      'createMerchantContract',
      'getMerchantContracts',
      'updateMerchantContract',
      'deleteMerchantContract',
      'getQualifications',
      'createQualification',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Entities', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Entities', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ENTITIES_SERVICE_NAME = 'Entities';

export interface AddressesClient {
  getAddress(request: RequestWithUuid): Observable<Address>;

  getAddresses(request: GetAddressesRequest): Observable<GetAddressesResponse>;

  createAddress(request: CreateAddressRequest): Observable<Address>;

  updateAddress(request: UpdateAddressRequest): Observable<Address>;

  removeAddress(request: RequestWithUuid): Observable<EmptyResponse>;
}

export interface AddressesController {
  getAddress(
    request: RequestWithUuid,
  ): Promise<Address> | Observable<Address> | Address;

  getAddresses(
    request: GetAddressesRequest,
  ):
    | Promise<GetAddressesResponse>
    | Observable<GetAddressesResponse>
    | GetAddressesResponse;

  createAddress(
    request: CreateAddressRequest,
  ): Promise<Address> | Observable<Address> | Address;

  updateAddress(
    request: UpdateAddressRequest,
  ): Promise<Address> | Observable<Address> | Address;

  removeAddress(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function AddressesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getAddress',
      'getAddresses',
      'createAddress',
      'updateAddress',
      'removeAddress',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Addresses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Addresses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ADDRESSES_SERVICE_NAME = 'Addresses';

export interface ItemsClient {
  getItems(request: GetItemsRequest): Observable<GetItemsResponse>;

  createItem(request: CreateItemRequest): Observable<Item>;
}

export interface ItemsController {
  getItems(
    request: GetItemsRequest,
  ):
    | Promise<GetItemsResponse>
    | Observable<GetItemsResponse>
    | GetItemsResponse;

  createItem(
    request: CreateItemRequest,
  ): Promise<Item> | Observable<Item> | Item;
}

export function ItemsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getItems', 'createItem'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Items', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Items', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ITEMS_SERVICE_NAME = 'Items';

export interface NotificationsClient {
  getNotifications(
    request: GetNotificationsRequest,
  ): Observable<GetNotificationsResponse>;

  createNotification(
    request: CreateNotificationRequest,
  ): Observable<Notification>;

  updateNotification(
    request: UpdateNotificationRequest,
  ): Observable<Notification>;

  triggerNotification(
    request: TriggerNotificationRequest,
  ): Observable<EmptyResponse>;
}

export interface NotificationsController {
  getNotifications(
    request: GetNotificationsRequest,
  ):
    | Promise<GetNotificationsResponse>
    | Observable<GetNotificationsResponse>
    | GetNotificationsResponse;

  createNotification(
    request: CreateNotificationRequest,
  ): Promise<Notification> | Observable<Notification> | Notification;

  updateNotification(
    request: UpdateNotificationRequest,
  ): Promise<Notification> | Observable<Notification> | Notification;

  triggerNotification(
    request: TriggerNotificationRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function NotificationsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getNotifications',
      'createNotification',
      'updateNotification',
      'triggerNotification',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Notifications', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Notifications', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const NOTIFICATIONS_SERVICE_NAME = 'Notifications';

export interface ContactsClient {
  getContacts(request: GetContactsRequest): Observable<GetContactsResponse>;

  createContacts(request: CreateContactsRequest): Observable<ContactsResponse>;

  removeNotificationRequest(
    request: RemoveNotificationRequestRequest,
  ): Observable<EmptyResponse>;

  getContactsByOrderId(
    request: RequestWithOrderId,
  ): Observable<GetContactsResponse>;
}

export interface ContactsController {
  getContacts(
    request: GetContactsRequest,
  ):
    | Promise<GetContactsResponse>
    | Observable<GetContactsResponse>
    | GetContactsResponse;

  createContacts(
    request: CreateContactsRequest,
  ):
    | Promise<ContactsResponse>
    | Observable<ContactsResponse>
    | ContactsResponse;

  removeNotificationRequest(
    request: RemoveNotificationRequestRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getContactsByOrderId(
    request: RequestWithOrderId,
  ):
    | Promise<GetContactsResponse>
    | Observable<GetContactsResponse>
    | GetContactsResponse;
}

export function ContactsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getContacts',
      'createContacts',
      'removeNotificationRequest',
      'getContactsByOrderId',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Contacts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Contacts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CONTACTS_SERVICE_NAME = 'Contacts';

export interface TagsClient {
  getTags(request: GetTagsRequest): Observable<GetTagsResponse>;

  getTagsByEntity(request: RequestWithSession): Observable<GetTagsResponse>;

  createTag(request: CreateTagRequest): Observable<Tag>;

  updateTag(request: UpdateTagRequest): Observable<Tag>;

  removeTag(request: RequestWithUuid): Observable<EmptyResponse>;

  associateTag(request: RequestWithSessionAndId): Observable<SuccessResponse>;

  associateProductTag(
    request: AssociateProductTagRequest,
  ): Observable<EmptyResponse>;

  disassociateTag(
    request: RequestWithSessionAndId,
  ): Observable<SuccessResponse>;

  disassociateProductTag(
    request: AssociateProductTagRequest,
  ): Observable<EmptyResponse>;

  updateShopTags(request: UpdateShopTagsRequest): Observable<EmptyResponse>;

  reorderTagShops(request: ReorderTagShopsRequest): Observable<EmptyResponse>;

  reorderTags(request: ReorderTagsRequest): Observable<EmptyResponse>;
}

export interface TagsController {
  getTags(
    request: GetTagsRequest,
  ): Promise<GetTagsResponse> | Observable<GetTagsResponse> | GetTagsResponse;

  getTagsByEntity(
    request: RequestWithSession,
  ): Promise<GetTagsResponse> | Observable<GetTagsResponse> | GetTagsResponse;

  createTag(request: CreateTagRequest): Promise<Tag> | Observable<Tag> | Tag;

  updateTag(request: UpdateTagRequest): Promise<Tag> | Observable<Tag> | Tag;

  removeTag(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  associateTag(
    request: RequestWithSessionAndId,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  associateProductTag(
    request: AssociateProductTagRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  disassociateTag(
    request: RequestWithSessionAndId,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  disassociateProductTag(
    request: AssociateProductTagRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updateShopTags(
    request: UpdateShopTagsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  reorderTagShops(
    request: ReorderTagShopsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  reorderTags(
    request: ReorderTagsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function TagsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getTags',
      'getTagsByEntity',
      'createTag',
      'updateTag',
      'removeTag',
      'associateTag',
      'associateProductTag',
      'disassociateTag',
      'disassociateProductTag',
      'updateShopTags',
      'reorderTagShops',
      'reorderTags',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Tags', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Tags', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TAGS_SERVICE_NAME = 'Tags';

export interface PromotionsClient {
  getPromotions(
    request: GetPromotionsRequest,
  ): Observable<GetPromotionsResponse>;

  getPromotion(request: GetPromotionRequest): Observable<Promotion>;

  createPromotion(request: Promotion): Observable<Promotion>;

  updatePromotion(request: Promotion): Observable<Promotion>;

  removePromotion(request: RequestWithId): Observable<EmptyResponse>;

  validatePromotionCode(
    request: ValidatePromotionCodeRequest,
  ): Observable<Promotion>;

  getPromotionCodes(
    request: GetPromotionCodesRequest,
  ): Observable<GetPromotionCodesResponse>;

  getPromotionCode(request: GetPromotionCodeRequest): Observable<PromotionCode>;

  createPromotionCode(request: PromotionCode): Observable<PromotionCode>;

  bulkCreatePromotionCode(
    request: BulkCreatePromotionCodeRequest,
  ): Observable<EmptyResponse>;

  updatePromotionCode(request: PromotionCode): Observable<PromotionCode>;

  removePromotionCode(
    request: RemovePromotionCodeRequest,
  ): Observable<EmptyResponse>;

  getPromotionCodeUsage(
    request: GetPromotionCodeUsageRequest,
  ): Observable<GetPromotionCodeUsageResponse>;

  createReceiverPromotionCode(
    request: CreateReceiverPromotionCodeRequest,
  ): Observable<PromotionCode>;

  getMerchantPromotion(
    request: MerchantPromotion,
  ): Observable<MerchantPromotion>;

  updateMerchantPromotion(
    request: UpdatePromotionRequest,
  ): Observable<MerchantPromotion>;

  generateSenderPromotionCode(
    request: GenerateSenderPromotionCodeRequest,
  ): Observable<EmptyResponse>;

  getMerchantReferralHistory(
    request: GetMerchantReferralHistoryRequest,
  ): Observable<GetMerchantReferralHistoryResponse>;
}

export interface PromotionsController {
  getPromotions(
    request: GetPromotionsRequest,
  ):
    | Promise<GetPromotionsResponse>
    | Observable<GetPromotionsResponse>
    | GetPromotionsResponse;

  getPromotion(
    request: GetPromotionRequest,
  ): Promise<Promotion> | Observable<Promotion> | Promotion;

  createPromotion(
    request: Promotion,
  ): Promise<Promotion> | Observable<Promotion> | Promotion;

  updatePromotion(
    request: Promotion,
  ): Promise<Promotion> | Observable<Promotion> | Promotion;

  removePromotion(
    request: RequestWithId,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  validatePromotionCode(
    request: ValidatePromotionCodeRequest,
  ): Promise<Promotion> | Observable<Promotion> | Promotion;

  getPromotionCodes(
    request: GetPromotionCodesRequest,
  ):
    | Promise<GetPromotionCodesResponse>
    | Observable<GetPromotionCodesResponse>
    | GetPromotionCodesResponse;

  getPromotionCode(
    request: GetPromotionCodeRequest,
  ): Promise<PromotionCode> | Observable<PromotionCode> | PromotionCode;

  createPromotionCode(
    request: PromotionCode,
  ): Promise<PromotionCode> | Observable<PromotionCode> | PromotionCode;

  bulkCreatePromotionCode(
    request: BulkCreatePromotionCodeRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updatePromotionCode(
    request: PromotionCode,
  ): Promise<PromotionCode> | Observable<PromotionCode> | PromotionCode;

  removePromotionCode(
    request: RemovePromotionCodeRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getPromotionCodeUsage(
    request: GetPromotionCodeUsageRequest,
  ):
    | Promise<GetPromotionCodeUsageResponse>
    | Observable<GetPromotionCodeUsageResponse>
    | GetPromotionCodeUsageResponse;

  createReceiverPromotionCode(
    request: CreateReceiverPromotionCodeRequest,
  ): Promise<PromotionCode> | Observable<PromotionCode> | PromotionCode;

  getMerchantPromotion(
    request: MerchantPromotion,
  ):
    | Promise<MerchantPromotion>
    | Observable<MerchantPromotion>
    | MerchantPromotion;

  updateMerchantPromotion(
    request: UpdatePromotionRequest,
  ):
    | Promise<MerchantPromotion>
    | Observable<MerchantPromotion>
    | MerchantPromotion;

  generateSenderPromotionCode(
    request: GenerateSenderPromotionCodeRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getMerchantReferralHistory(
    request: GetMerchantReferralHistoryRequest,
  ):
    | Promise<GetMerchantReferralHistoryResponse>
    | Observable<GetMerchantReferralHistoryResponse>
    | GetMerchantReferralHistoryResponse;
}

export function PromotionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPromotions',
      'getPromotion',
      'createPromotion',
      'updatePromotion',
      'removePromotion',
      'validatePromotionCode',
      'getPromotionCodes',
      'getPromotionCode',
      'createPromotionCode',
      'bulkCreatePromotionCode',
      'updatePromotionCode',
      'removePromotionCode',
      'getPromotionCodeUsage',
      'createReceiverPromotionCode',
      'getMerchantPromotion',
      'updateMerchantPromotion',
      'generateSenderPromotionCode',
      'getMerchantReferralHistory',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Promotions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Promotions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PROMOTIONS_SERVICE_NAME = 'Promotions';

export interface QRClient {
  getQr(request: RequestWithUuid): Observable<Qr>;

  getQrList(request: GetQrListRequest): Observable<GetQrListResponse>;

  createQr(request: CreateQrListRequest): Observable<SuccessResponse>;

  updateQr(request: UpdateQrRequest): Observable<Qr>;

  removeQr(request: RequestWithUuid): Observable<EmptyResponse>;

  getBatches(request: EmptyRequest): Observable<GetBatchesResponse>;

  createBatch(request: CreateBatchRequest): Observable<SuccessResponse>;
}

export interface QRController {
  getQr(request: RequestWithUuid): Promise<Qr> | Observable<Qr> | Qr;

  getQrList(
    request: GetQrListRequest,
  ):
    | Promise<GetQrListResponse>
    | Observable<GetQrListResponse>
    | GetQrListResponse;

  createQr(
    request: CreateQrListRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  updateQr(request: UpdateQrRequest): Promise<Qr> | Observable<Qr> | Qr;

  removeQr(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getBatches(
    request: EmptyRequest,
  ):
    | Promise<GetBatchesResponse>
    | Observable<GetBatchesResponse>
    | GetBatchesResponse;

  createBatch(
    request: CreateBatchRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function QRControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getQr',
      'getQrList',
      'createQr',
      'updateQr',
      'removeQr',
      'getBatches',
      'createBatch',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('QR', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('QR', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const Q_R_SERVICE_NAME = 'QR';

export interface ShopsClient {
  getShop(request: GetShopRequest): Observable<Shop>;

  getBestShop(request: GetBestShopRequest): Observable<Shop>;

  getShops(request: GetShopsRequest): Observable<GetShopsResponse>;

  createShop(request: Shop): Observable<Shop>;

  updateShop(request: Shop): Observable<Shop>;

  removeShop(request: RequestWithUuid): Observable<EmptyResponse>;

  getInventory(request: GetInventoryRequest): Observable<Inventory>;

  getBestInventory(request: GetBestInventoryRequest): Observable<Inventory>;

  getInventories(
    request: GetInventoriesRequest,
  ): Observable<GetInventoriesResponse>;

  updateInventory(request: UpdateInventoryRequest): Observable<EmptyResponse>;

  updateShopDistricts(
    request: UpdateShopDistrictsRequest,
  ): Observable<SuccessResponse>;

  getShopTimeslots(
    request: RequestWithShopId,
  ): Observable<GetShopTimeslotsResponse>;

  createShopTimeslot(request: ShopTimeslot): Observable<ShopTimeslot>;

  updateShopTimeslot(request: ShopTimeslot): Observable<ShopTimeslot>;

  removeShopTimeslot(request: RequestWithId): Observable<EmptyResponse>;

  getShopAvailableTimeslots(
    request: GetShopAvailableTimeslotsRequest,
  ): Observable<GetShopAvailableTimeslotsResponse>;

  getAvailableTimeslots(
    request: GetAvailableTimeslotsRequest,
  ): Observable<GetAvailableTimeslotsResponse>;

  getShopUsers(request: RequestWithShopId): Observable<GetShopUsersResponse>;

  updateShopUsers(request: UpdateShopUsersRequest): Observable<EmptyResponse>;

  getShopCollections(
    request: GetShopCollectionsRequest,
  ): Observable<GetShopCollectionsResponse>;

  associateShopCollection(
    request: ShopCollectionsAssociationRequest,
  ): Observable<EmptyResponse>;

  disassociateShopCollection(
    request: ShopCollectionsAssociationRequest,
  ): Observable<EmptyResponse>;

  getInventoryLogs(
    request: GetInventoryLogsRequest,
  ): Observable<GetInventoryLogsResponse>;

  reorderShopCollections(
    request: ReorderShopCollectionsRequest,
  ): Observable<GetShopCollectionsResponse>;
}

export interface ShopsController {
  getShop(request: GetShopRequest): Promise<Shop> | Observable<Shop> | Shop;

  getBestShop(
    request: GetBestShopRequest,
  ): Promise<Shop> | Observable<Shop> | Shop;

  getShops(
    request: GetShopsRequest,
  ):
    | Promise<GetShopsResponse>
    | Observable<GetShopsResponse>
    | GetShopsResponse;

  createShop(request: Shop): Promise<Shop> | Observable<Shop> | Shop;

  updateShop(request: Shop): Promise<Shop> | Observable<Shop> | Shop;

  removeShop(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getInventory(
    request: GetInventoryRequest,
  ): Promise<Inventory> | Observable<Inventory> | Inventory;

  getBestInventory(
    request: GetBestInventoryRequest,
  ): Promise<Inventory> | Observable<Inventory> | Inventory;

  getInventories(
    request: GetInventoriesRequest,
  ):
    | Promise<GetInventoriesResponse>
    | Observable<GetInventoriesResponse>
    | GetInventoriesResponse;

  updateInventory(
    request: UpdateInventoryRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updateShopDistricts(
    request: UpdateShopDistrictsRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  getShopTimeslots(
    request: RequestWithShopId,
  ):
    | Promise<GetShopTimeslotsResponse>
    | Observable<GetShopTimeslotsResponse>
    | GetShopTimeslotsResponse;

  createShopTimeslot(
    request: ShopTimeslot,
  ): Promise<ShopTimeslot> | Observable<ShopTimeslot> | ShopTimeslot;

  updateShopTimeslot(
    request: ShopTimeslot,
  ): Promise<ShopTimeslot> | Observable<ShopTimeslot> | ShopTimeslot;

  removeShopTimeslot(
    request: RequestWithId,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getShopAvailableTimeslots(
    request: GetShopAvailableTimeslotsRequest,
  ):
    | Promise<GetShopAvailableTimeslotsResponse>
    | Observable<GetShopAvailableTimeslotsResponse>
    | GetShopAvailableTimeslotsResponse;

  getAvailableTimeslots(
    request: GetAvailableTimeslotsRequest,
  ):
    | Promise<GetAvailableTimeslotsResponse>
    | Observable<GetAvailableTimeslotsResponse>
    | GetAvailableTimeslotsResponse;

  getShopUsers(
    request: RequestWithShopId,
  ):
    | Promise<GetShopUsersResponse>
    | Observable<GetShopUsersResponse>
    | GetShopUsersResponse;

  updateShopUsers(
    request: UpdateShopUsersRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getShopCollections(
    request: GetShopCollectionsRequest,
  ):
    | Promise<GetShopCollectionsResponse>
    | Observable<GetShopCollectionsResponse>
    | GetShopCollectionsResponse;

  associateShopCollection(
    request: ShopCollectionsAssociationRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  disassociateShopCollection(
    request: ShopCollectionsAssociationRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getInventoryLogs(
    request: GetInventoryLogsRequest,
  ):
    | Promise<GetInventoryLogsResponse>
    | Observable<GetInventoryLogsResponse>
    | GetInventoryLogsResponse;

  reorderShopCollections(
    request: ReorderShopCollectionsRequest,
  ):
    | Promise<GetShopCollectionsResponse>
    | Observable<GetShopCollectionsResponse>
    | GetShopCollectionsResponse;
}

export function ShopsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getShop',
      'getBestShop',
      'getShops',
      'createShop',
      'updateShop',
      'removeShop',
      'getInventory',
      'getBestInventory',
      'getInventories',
      'updateInventory',
      'updateShopDistricts',
      'getShopTimeslots',
      'createShopTimeslot',
      'updateShopTimeslot',
      'removeShopTimeslot',
      'getShopAvailableTimeslots',
      'getAvailableTimeslots',
      'getShopUsers',
      'updateShopUsers',
      'getShopCollections',
      'associateShopCollection',
      'disassociateShopCollection',
      'getInventoryLogs',
      'reorderShopCollections',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Shops', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Shops', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SHOPS_SERVICE_NAME = 'Shops';

export interface ProductsClient {
  getProduct(request: GetProductRequest): Observable<Product>;

  getProducts(request: GetProductsRequest): Observable<GetProductsResponse>;

  createProduct(request: Product): Observable<Product>;

  updateProduct(request: Product): Observable<Product>;

  removeProduct(request: RequestWithUuid): Observable<EmptyResponse>;

  getProductPacks(
    request: GetProductPacksRequest,
  ): Observable<GetProductPacksResponse>;

  associateProductPack(
    request: AssociateProductPackRequest,
  ): Observable<EmptyResponse>;

  disassociateProductPack(
    request: DisassociateProductPackRequest,
  ): Observable<EmptyResponse>;

  reorderProductPacks(
    request: ReorderProductPacksRequest,
  ): Observable<EmptyResponse>;

  getDatafeed(request: GetDatafeedRequest): Observable<GetDatafeedResponse>;
}

export interface ProductsController {
  getProduct(
    request: GetProductRequest,
  ): Promise<Product> | Observable<Product> | Product;

  getProducts(
    request: GetProductsRequest,
  ):
    | Promise<GetProductsResponse>
    | Observable<GetProductsResponse>
    | GetProductsResponse;

  createProduct(
    request: Product,
  ): Promise<Product> | Observable<Product> | Product;

  updateProduct(
    request: Product,
  ): Promise<Product> | Observable<Product> | Product;

  removeProduct(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getProductPacks(
    request: GetProductPacksRequest,
  ):
    | Promise<GetProductPacksResponse>
    | Observable<GetProductPacksResponse>
    | GetProductPacksResponse;

  associateProductPack(
    request: AssociateProductPackRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  disassociateProductPack(
    request: DisassociateProductPackRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  reorderProductPacks(
    request: ReorderProductPacksRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getDatafeed(
    request: GetDatafeedRequest,
  ):
    | Promise<GetDatafeedResponse>
    | Observable<GetDatafeedResponse>
    | GetDatafeedResponse;
}

export function ProductsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getProduct',
      'getProducts',
      'createProduct',
      'updateProduct',
      'removeProduct',
      'getProductPacks',
      'associateProductPack',
      'disassociateProductPack',
      'reorderProductPacks',
      'getDatafeed',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Products', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Products', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PRODUCTS_SERVICE_NAME = 'Products';

export interface PurchaseOrdersClient {
  getPurchaseOrder(request: GetPurchaseOrderRequest): Observable<PurchaseOrder>;

  getPurchaseOrders(
    request: GetPurchaseOrdersRequest,
  ): Observable<GetPurchaseOrdersResponse>;

  createPurchaseOrder(
    request: CreatePurchaseOrderRequest,
  ): Observable<PurchaseOrder>;

  updatePurchaseOrder(request: PurchaseOrder): Observable<PurchaseOrder>;

  cancelPurchaseOrder(
    request: CancelPurchaseOrderRequest,
  ): Observable<PurchaseOrder>;
}

export interface PurchaseOrdersController {
  getPurchaseOrder(
    request: GetPurchaseOrderRequest,
  ): Promise<PurchaseOrder> | Observable<PurchaseOrder> | PurchaseOrder;

  getPurchaseOrders(
    request: GetPurchaseOrdersRequest,
  ):
    | Promise<GetPurchaseOrdersResponse>
    | Observable<GetPurchaseOrdersResponse>
    | GetPurchaseOrdersResponse;

  createPurchaseOrder(
    request: CreatePurchaseOrderRequest,
  ): Promise<PurchaseOrder> | Observable<PurchaseOrder> | PurchaseOrder;

  updatePurchaseOrder(
    request: PurchaseOrder,
  ): Promise<PurchaseOrder> | Observable<PurchaseOrder> | PurchaseOrder;

  cancelPurchaseOrder(
    request: CancelPurchaseOrderRequest,
  ): Promise<PurchaseOrder> | Observable<PurchaseOrder> | PurchaseOrder;
}

export function PurchaseOrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPurchaseOrder',
      'getPurchaseOrders',
      'createPurchaseOrder',
      'updatePurchaseOrder',
      'cancelPurchaseOrder',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('PurchaseOrders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('PurchaseOrders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PURCHASE_ORDERS_SERVICE_NAME = 'PurchaseOrders';

export interface ValidateOrdersClient {
  validateOrder(
    request: ValidateOrderRequest,
  ): Observable<ValidateOrderResponse>;
}

export interface ValidateOrdersController {
  validateOrder(
    request: ValidateOrderRequest,
  ):
    | Promise<ValidateOrderResponse>
    | Observable<ValidateOrderResponse>
    | ValidateOrderResponse;
}

export function ValidateOrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['validateOrder'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('ValidateOrders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('ValidateOrders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const VALIDATE_ORDERS_SERVICE_NAME = 'ValidateOrders';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<QuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ): Promise<QuoteResponse> | Observable<QuoteResponse> | QuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

export interface PricingModelsClient {
  getPricingModels(
    request: GetPricingModelsRequest,
  ): Observable<GetPricingModelsResponse>;

  createPricingModel(request: PricingModel): Observable<PricingModel>;

  updatePricingModel(request: PricingModel): Observable<PricingModel>;

  removePricingModel(request: RequestWithUuid): Observable<EmptyResponse>;
}

export interface PricingModelsController {
  getPricingModels(
    request: GetPricingModelsRequest,
  ):
    | Promise<GetPricingModelsResponse>
    | Observable<GetPricingModelsResponse>
    | GetPricingModelsResponse;

  createPricingModel(
    request: PricingModel,
  ): Promise<PricingModel> | Observable<PricingModel> | PricingModel;

  updatePricingModel(
    request: PricingModel,
  ): Promise<PricingModel> | Observable<PricingModel> | PricingModel;

  removePricingModel(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function PricingModelsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPricingModels',
      'createPricingModel',
      'updatePricingModel',
      'removePricingModel',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('PricingModels', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('PricingModels', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PRICING_MODELS_SERVICE_NAME = 'PricingModels';

export interface CollectionsClient {
  getCollection(request: GetCollectionRequest): Observable<Collection>;

  getCollectionList(
    request: GetCollectionListRequest,
  ): Observable<CollectionListResponse>;

  createCollection(request: Collection): Observable<Collection>;

  updateCollection(request: Collection): Observable<Collection>;

  removeCollection(request: RequestWithId): Observable<EmptyResponse>;

  associateCollectionProduct(
    request: CollectionProductAssociationRequest,
  ): Observable<EmptyResponse>;

  disassociateCollectionProduct(
    request: CollectionProductAssociationRequest,
  ): Observable<EmptyResponse>;

  reorderCollectionProducts(
    request: ReorderCollectionProductsRequest,
  ): Observable<EmptyResponse>;
}

export interface CollectionsController {
  getCollection(
    request: GetCollectionRequest,
  ): Promise<Collection> | Observable<Collection> | Collection;

  getCollectionList(
    request: GetCollectionListRequest,
  ):
    | Promise<CollectionListResponse>
    | Observable<CollectionListResponse>
    | CollectionListResponse;

  createCollection(
    request: Collection,
  ): Promise<Collection> | Observable<Collection> | Collection;

  updateCollection(
    request: Collection,
  ): Promise<Collection> | Observable<Collection> | Collection;

  removeCollection(
    request: RequestWithId,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  associateCollectionProduct(
    request: CollectionProductAssociationRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  disassociateCollectionProduct(
    request: CollectionProductAssociationRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  reorderCollectionProducts(
    request: ReorderCollectionProductsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function CollectionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getCollection',
      'getCollectionList',
      'createCollection',
      'updateCollection',
      'removeCollection',
      'associateCollectionProduct',
      'disassociateCollectionProduct',
      'reorderCollectionProducts',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Collections', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Collections', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const COLLECTIONS_SERVICE_NAME = 'Collections';

export interface PacksClient {
  getPack(request: GetPackRequest): Observable<Pack>;

  getPacks(request: GetPacksRequest): Observable<GetPacksResponse>;

  createPack(request: Pack): Observable<Pack>;

  updatePack(request: Pack): Observable<Pack>;

  removePack(request: RequestWithUuid): Observable<EmptyResponse>;

  associatePackItem(request: PackItem): Observable<EmptyResponse>;

  disassociatePackItem(
    request: DisassociatePackItemRequest,
  ): Observable<EmptyResponse>;

  reorderPackItems(request: ReorderPackItemsRequest): Observable<EmptyResponse>;
}

export interface PacksController {
  getPack(request: GetPackRequest): Promise<Pack> | Observable<Pack> | Pack;

  getPacks(
    request: GetPacksRequest,
  ):
    | Promise<GetPacksResponse>
    | Observable<GetPacksResponse>
    | GetPacksResponse;

  createPack(request: Pack): Promise<Pack> | Observable<Pack> | Pack;

  updatePack(request: Pack): Promise<Pack> | Observable<Pack> | Pack;

  removePack(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  associatePackItem(
    request: PackItem,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  disassociatePackItem(
    request: DisassociatePackItemRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  reorderPackItems(
    request: ReorderPackItemsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function PacksControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPack',
      'getPacks',
      'createPack',
      'updatePack',
      'removePack',
      'associatePackItem',
      'disassociatePackItem',
      'reorderPackItems',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Packs', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Packs', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PACKS_SERVICE_NAME = 'Packs';

export interface SearchesClient {
  index(request: IndexRequest): Observable<EmptyResponse>;

  suggestions(request: SuggestionsRequest): Observable<SuggestionsResponse>;
}

export interface SearchesController {
  index(
    request: IndexRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  suggestions(
    request: SuggestionsRequest,
  ):
    | Promise<SuggestionsResponse>
    | Observable<SuggestionsResponse>
    | SuggestionsResponse;
}

export function SearchesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['index', 'suggestions'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Searches', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Searches', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SEARCHES_SERVICE_NAME = 'Searches';

export interface OwnersClient {
  getOwners(request: GetOwnersRequest): Observable<GetOwnersResponse>;

  getOwner(request: GetOwnerRequest): Observable<Owner>;

  updateOwner(request: Owner): Observable<Owner>;

  createOwner(request: Owner): Observable<Owner>;

  removeOwner(request: RemoveOwnerRequest): Observable<EmptyResponse>;
}

export interface OwnersController {
  getOwners(
    request: GetOwnersRequest,
  ):
    | Promise<GetOwnersResponse>
    | Observable<GetOwnersResponse>
    | GetOwnersResponse;

  getOwner(
    request: GetOwnerRequest,
  ): Promise<Owner> | Observable<Owner> | Owner;

  updateOwner(request: Owner): Promise<Owner> | Observable<Owner> | Owner;

  createOwner(request: Owner): Promise<Owner> | Observable<Owner> | Owner;

  removeOwner(
    request: RemoveOwnerRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function OwnersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOwners',
      'getOwner',
      'updateOwner',
      'createOwner',
      'removeOwner',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Owners', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Owners', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const OWNERS_SERVICE_NAME = 'Owners';

export interface LineHaulsClient {
  getLineHaul(request: GetLineHaulRequest): Observable<GetLineHaulResponse>;

  getLineHauls(request: GetLineHaulsRequest): Observable<GetLineHaulsResponse>;

  upsertLineHaul(
    request: UpsertLineHaulRequest,
  ): Observable<UpsertLineHaulResponse>;

  deleteLineHaul(request: DeleteLineHaulRequest): Observable<EmptyResponse>;

  getDropoffCountry(
    request: GetDropoffCountryRequest,
  ): Observable<GetDropoffCountryResponse>;

  getPickupCountry(
    request: GetPickupCountryRequest,
  ): Observable<GetPickupCountryResponse>;
}

export interface LineHaulsController {
  getLineHaul(
    request: GetLineHaulRequest,
  ):
    | Promise<GetLineHaulResponse>
    | Observable<GetLineHaulResponse>
    | GetLineHaulResponse;

  getLineHauls(
    request: GetLineHaulsRequest,
  ):
    | Promise<GetLineHaulsResponse>
    | Observable<GetLineHaulsResponse>
    | GetLineHaulsResponse;

  upsertLineHaul(
    request: UpsertLineHaulRequest,
  ):
    | Promise<UpsertLineHaulResponse>
    | Observable<UpsertLineHaulResponse>
    | UpsertLineHaulResponse;

  deleteLineHaul(
    request: DeleteLineHaulRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getDropoffCountry(
    request: GetDropoffCountryRequest,
  ):
    | Promise<GetDropoffCountryResponse>
    | Observable<GetDropoffCountryResponse>
    | GetDropoffCountryResponse;

  getPickupCountry(
    request: GetPickupCountryRequest,
  ):
    | Promise<GetPickupCountryResponse>
    | Observable<GetPickupCountryResponse>
    | GetPickupCountryResponse;
}

export function LineHaulsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getLineHaul',
      'getLineHauls',
      'upsertLineHaul',
      'deleteLineHaul',
      'getDropoffCountry',
      'getPickupCountry',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('LineHauls', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('LineHauls', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const LINE_HAULS_SERVICE_NAME = 'LineHauls';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
