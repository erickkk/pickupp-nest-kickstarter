/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  Service,
  Configuration,
  Timeslot,
  OrderFlow,
  Pricing,
  EntityService,
} from './quote.model';
import { Geometry, Item } from './merchant.model';
import { Region } from './address.model';
import { Order } from './order.model';

export const protobufPackage = 'quote';

export interface EmptyResponse {}

export interface RequestWithUuid {
  id: string;
}

export interface GetServiceResponse {
  service: Service | undefined;
  configurations: Configuration[];
}

export interface GetServicesRequest {
  ids: string[];
  query: string;
  serviceTypes: string[];
  serviceTime: string;
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetServicesResponse {
  services: Service[];
  total: number;
}

export interface GetServiceOfferingResponse {
  id: string;
  name: string;
  serviceType: string;
  serviceTime: number;
  earliestPickup: GetTimeslotResponse | undefined;
  latestPickup: GetTimeslotResponse | undefined;
  earliestDropoff: GetTimeslotResponse | undefined;
  latestDropoff: GetTimeslotResponse | undefined;
  pricing: GetPricingResponse | undefined;
  orderFlow: GetOrderFlowResponse | undefined;
  configurations: Configuration[];
}

export interface GetTimeslotResponse {
  timeslot: Timeslot | undefined;
  configurations: Configuration[];
}

export interface GetTimeslotsRequest {
  ids: string[];
  query: string;
  modes: string[];
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetTimeslotsResponse {
  timeslots: Timeslot[];
  total: number;
}

export interface GetAvailableTimeslotsRequest {
  query: string;
  targetIds: string[];
  excludeIds: string[];
}

export interface GetOrderFlowResponse {
  orderFlow: OrderFlow | undefined;
  configurations: Configuration[];
}

export interface GetOrderFlowsRequest {
  ids: string[];
  query: string;
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetOrderFlowsResponse {
  orderFlows: OrderFlow[];
  total: number;
}

export interface GetPricingResponse {
  pricing: Pricing | undefined;
  configurations: Configuration[];
}

export interface GetPricingsRequest {
  ids: string[];
  query: string;
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetPricingsResponse {
  pricings: Pricing[];
  total: number;
}

export interface UpsertGroupedConfigurationsRequest {
  type: string;
  typeId: string;
  configurations: Configuration[];
}

export interface UpsertGroupedConfigurationsResponse {
  configurations: Configuration[];
}

export interface GetConfigurationsRequest {
  ids: string[];
  query: string;
  types: string[];
  typeIds: string[];
  value: string;
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetConfigurationsResponse {
  configurations: Configuration[];
  total: number;
}

export interface GetGroupedConfigurationsRequest {
  ids: string[];
  query: string;
  types: string[];
  typeIds: string[];
  value: string;
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetGroupedConfigurationsResponse {
  groupedConfigurations: GetGroupedConfigurationsResponse_GroupedConfiguration[];
  total: number;
}

export interface GetGroupedConfigurationsResponse_GroupedConfiguration {
  type: string;
  typeId: string;
  name: string;
  configurations: Configuration[];
}

export interface GetEntityServiceResponse {
  entityService: EntityService | undefined;
  services: Service[];
}

export interface GetEntityServicesRequest {
  ids: string[];
  entityIds: number[];
  serviceIds: string[];
  limit: string;
  offset: string;
  sortBy: string;
}

export interface GetEntityServicesResponse {
  entityServices: GetEntityServicesResponse_EntityServiceOffering[];
  total: number;
}

export interface GetEntityServicesResponse_EntityServiceOffering {
  id: string;
  entityId: number;
  serviceId: string;
  priority: number;
  service: Service | undefined;
  configurations: Configuration[];
}

export interface UpdateEntityServicesRequest {
  entityServices: EntityService[];
}

export interface GetQuoteRequest {
  dimension: number;
  weight: number;
  distance: number;
  from: Geometry | undefined;
  to: Geometry[];
  pickupTime: string;
  dropoffTime: string;
  reliability: boolean;
  discount: number;
  canExpress: boolean;
  entityId: number;
  exchangeRate: number;
  serviceType: string;
  serviceTime: number;
  canOutsource: boolean;
  fixedOutsourcePartner: string;
  pickupZipCode: string;
  pickupCity: string;
  dropoffZipCode: string;
  dropoffCity: string;
  length: number;
  height: number;
  width: number;
  omitCalculateTime: boolean;
  pickupAddressLine1: string;
  pickupAddressLine2: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  outsourceId: string;
  pickupRegions: Region[];
  dropoffRegions: Region[];
  items: Item[];
  serviceOfferingId: string;
}

export interface QuoteResponse {
  total: string;
  originalPrice: string;
  currency: string;
  breakdown: QuoteResponse_Breakdown | undefined;
  taxPrice: string;
  partner: string;
  dropoffTime: string;
  errors: { [key: string]: string };
  metadata: { [key: string]: string };
  serviceOffering: Service | undefined;
}

export interface QuoteResponse_Breakdown {
  basicRate: string;
  distanceRate: string;
  pickupSurcharge: string;
  dropoffSurcharge: string;
  expressRate: string;
  expressSurcharge: string;
  reliabilityRate: string;
  adjustmentRate: string;
  discountRate: string;
  weightRate: string;
  wallSurcharge: string;
  exchangeRate: string;
}

export interface QuoteResponse_ErrorsEntry {
  key: string;
  value: string;
}

export interface QuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface GetQuotesRequest {
  order: Order | undefined;
  enforceValidation: boolean;
  region: string;
  pickupTime: string;
  dropoffTime: string;
  entityId: string;
}

export interface GetQuotesResponse {
  quotes: QuoteResponse[];
}

export interface GetOrderTimeslotsRequest {
  pickupTime: string;
  dropoffTime: string;
  serviceOfferingId: string;
}

export interface GetOrderTimeslotsResponse {
  earliestPickup: string;
  latestPickup: string;
  earliestDropoff: string;
  latestDropoff: string;
}

export const QUOTE_PACKAGE_NAME = 'quote';

export interface ServicesClient {
  upsertService(request: Service): Observable<Service>;

  getService(request: RequestWithUuid): Observable<GetServiceResponse>;

  getServices(request: GetServicesRequest): Observable<GetServicesResponse>;

  getServiceOffering(
    request: RequestWithUuid,
  ): Observable<GetServiceOfferingResponse>;

  deleteService(request: RequestWithUuid): Observable<EmptyResponse>;
}

export interface ServicesController {
  upsertService(
    request: Service,
  ): Promise<Service> | Observable<Service> | Service;

  getService(
    request: RequestWithUuid,
  ):
    | Promise<GetServiceResponse>
    | Observable<GetServiceResponse>
    | GetServiceResponse;

  getServices(
    request: GetServicesRequest,
  ):
    | Promise<GetServicesResponse>
    | Observable<GetServicesResponse>
    | GetServicesResponse;

  getServiceOffering(
    request: RequestWithUuid,
  ):
    | Promise<GetServiceOfferingResponse>
    | Observable<GetServiceOfferingResponse>
    | GetServiceOfferingResponse;

  deleteService(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function ServicesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'upsertService',
      'getService',
      'getServices',
      'getServiceOffering',
      'deleteService',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Services', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Services', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SERVICES_SERVICE_NAME = 'Services';

export interface TimeslotsClient {
  upsertTimeslot(request: Timeslot): Observable<Timeslot>;

  getTimeslot(request: RequestWithUuid): Observable<GetTimeslotResponse>;

  getTimeslots(request: GetTimeslotsRequest): Observable<GetTimeslotsResponse>;

  deleteTimeslot(request: RequestWithUuid): Observable<EmptyResponse>;

  getAvailableTimeslots(
    request: GetAvailableTimeslotsRequest,
  ): Observable<GetTimeslotsResponse>;
}

export interface TimeslotsController {
  upsertTimeslot(
    request: Timeslot,
  ): Promise<Timeslot> | Observable<Timeslot> | Timeslot;

  getTimeslot(
    request: RequestWithUuid,
  ):
    | Promise<GetTimeslotResponse>
    | Observable<GetTimeslotResponse>
    | GetTimeslotResponse;

  getTimeslots(
    request: GetTimeslotsRequest,
  ):
    | Promise<GetTimeslotsResponse>
    | Observable<GetTimeslotsResponse>
    | GetTimeslotsResponse;

  deleteTimeslot(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getAvailableTimeslots(
    request: GetAvailableTimeslotsRequest,
  ):
    | Promise<GetTimeslotsResponse>
    | Observable<GetTimeslotsResponse>
    | GetTimeslotsResponse;
}

export function TimeslotsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'upsertTimeslot',
      'getTimeslot',
      'getTimeslots',
      'deleteTimeslot',
      'getAvailableTimeslots',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Timeslots', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Timeslots', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TIMESLOTS_SERVICE_NAME = 'Timeslots';

export interface OrderFlowsClient {
  upsertOrderFlow(request: OrderFlow): Observable<OrderFlow>;

  getOrderFlow(request: RequestWithUuid): Observable<GetOrderFlowResponse>;

  getOrderFlows(
    request: GetOrderFlowsRequest,
  ): Observable<GetOrderFlowsResponse>;

  deleteOrderFlow(request: RequestWithUuid): Observable<EmptyResponse>;
}

export interface OrderFlowsController {
  upsertOrderFlow(
    request: OrderFlow,
  ): Promise<OrderFlow> | Observable<OrderFlow> | OrderFlow;

  getOrderFlow(
    request: RequestWithUuid,
  ):
    | Promise<GetOrderFlowResponse>
    | Observable<GetOrderFlowResponse>
    | GetOrderFlowResponse;

  getOrderFlows(
    request: GetOrderFlowsRequest,
  ):
    | Promise<GetOrderFlowsResponse>
    | Observable<GetOrderFlowsResponse>
    | GetOrderFlowsResponse;

  deleteOrderFlow(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function OrderFlowsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'upsertOrderFlow',
      'getOrderFlow',
      'getOrderFlows',
      'deleteOrderFlow',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('OrderFlows', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('OrderFlows', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDER_FLOWS_SERVICE_NAME = 'OrderFlows';

export interface PricingsClient {
  upsertPricing(request: Pricing): Observable<Pricing>;

  getPricing(request: RequestWithUuid): Observable<GetPricingResponse>;

  getPricings(request: GetPricingsRequest): Observable<GetPricingsResponse>;

  deletePricing(request: RequestWithUuid): Observable<EmptyResponse>;
}

export interface PricingsController {
  upsertPricing(
    request: Pricing,
  ): Promise<Pricing> | Observable<Pricing> | Pricing;

  getPricing(
    request: RequestWithUuid,
  ):
    | Promise<GetPricingResponse>
    | Observable<GetPricingResponse>
    | GetPricingResponse;

  getPricings(
    request: GetPricingsRequest,
  ):
    | Promise<GetPricingsResponse>
    | Observable<GetPricingsResponse>
    | GetPricingsResponse;

  deletePricing(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function PricingsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'upsertPricing',
      'getPricing',
      'getPricings',
      'deletePricing',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Pricings', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Pricings', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PRICINGS_SERVICE_NAME = 'Pricings';

export interface ConfigurationsClient {
  upsertConfiguration(request: Configuration): Observable<Configuration>;

  upsertGroupedConfigurations(
    request: UpsertGroupedConfigurationsRequest,
  ): Observable<UpsertGroupedConfigurationsResponse>;

  getConfiguration(request: RequestWithUuid): Observable<Configuration>;

  getConfigurations(
    request: GetConfigurationsRequest,
  ): Observable<GetConfigurationsResponse>;

  deleteConfiguration(request: RequestWithUuid): Observable<EmptyResponse>;

  getGroupedConfigurations(
    request: GetGroupedConfigurationsRequest,
  ): Observable<GetGroupedConfigurationsResponse>;
}

export interface ConfigurationsController {
  upsertConfiguration(
    request: Configuration,
  ): Promise<Configuration> | Observable<Configuration> | Configuration;

  upsertGroupedConfigurations(
    request: UpsertGroupedConfigurationsRequest,
  ):
    | Promise<UpsertGroupedConfigurationsResponse>
    | Observable<UpsertGroupedConfigurationsResponse>
    | UpsertGroupedConfigurationsResponse;

  getConfiguration(
    request: RequestWithUuid,
  ): Promise<Configuration> | Observable<Configuration> | Configuration;

  getConfigurations(
    request: GetConfigurationsRequest,
  ):
    | Promise<GetConfigurationsResponse>
    | Observable<GetConfigurationsResponse>
    | GetConfigurationsResponse;

  deleteConfiguration(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getGroupedConfigurations(
    request: GetGroupedConfigurationsRequest,
  ):
    | Promise<GetGroupedConfigurationsResponse>
    | Observable<GetGroupedConfigurationsResponse>
    | GetGroupedConfigurationsResponse;
}

export function ConfigurationsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'upsertConfiguration',
      'upsertGroupedConfigurations',
      'getConfiguration',
      'getConfigurations',
      'deleteConfiguration',
      'getGroupedConfigurations',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Configurations', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Configurations', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CONFIGURATIONS_SERVICE_NAME = 'Configurations';

export interface EntityServicesClient {
  createEntityService(request: EntityService): Observable<EntityService>;

  getEntityService(
    request: RequestWithUuid,
  ): Observable<GetEntityServiceResponse>;

  getEntityServices(
    request: GetEntityServicesRequest,
  ): Observable<GetEntityServicesResponse>;

  deleteEntityService(request: RequestWithUuid): Observable<EmptyResponse>;

  updateEntityServices(
    request: UpdateEntityServicesRequest,
  ): Observable<GetEntityServiceResponse>;

  applyDefaultServices(request: RequestWithUuid): Observable<EmptyResponse>;
}

export interface EntityServicesController {
  createEntityService(
    request: EntityService,
  ): Promise<EntityService> | Observable<EntityService> | EntityService;

  getEntityService(
    request: RequestWithUuid,
  ):
    | Promise<GetEntityServiceResponse>
    | Observable<GetEntityServiceResponse>
    | GetEntityServiceResponse;

  getEntityServices(
    request: GetEntityServicesRequest,
  ):
    | Promise<GetEntityServicesResponse>
    | Observable<GetEntityServicesResponse>
    | GetEntityServicesResponse;

  deleteEntityService(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updateEntityServices(
    request: UpdateEntityServicesRequest,
  ):
    | Promise<GetEntityServiceResponse>
    | Observable<GetEntityServiceResponse>
    | GetEntityServiceResponse;

  applyDefaultServices(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function EntityServicesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createEntityService',
      'getEntityService',
      'getEntityServices',
      'deleteEntityService',
      'updateEntityServices',
      'applyDefaultServices',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('EntityServices', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('EntityServices', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ENTITY_SERVICES_SERVICE_NAME = 'EntityServices';

export interface QuotesClient {
  getQuote(request: GetQuoteRequest): Observable<QuoteResponse>;

  getQuotes(request: GetQuotesRequest): Observable<GetQuotesResponse>;
}

export interface QuotesController {
  getQuote(
    request: GetQuoteRequest,
  ): Promise<QuoteResponse> | Observable<QuoteResponse> | QuoteResponse;

  getQuotes(
    request: GetQuotesRequest,
  ):
    | Promise<GetQuotesResponse>
    | Observable<GetQuotesResponse>
    | GetQuotesResponse;
}

export function QuotesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote', 'getQuotes'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quotes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quotes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTES_SERVICE_NAME = 'Quotes';

export interface OrderClient {
  getOrderTimeslots(
    request: GetOrderTimeslotsRequest,
  ): Observable<GetOrderTimeslotsResponse>;
}

export interface OrderController {
  getOrderTimeslots(
    request: GetOrderTimeslotsRequest,
  ):
    | Promise<GetOrderTimeslotsResponse>
    | Observable<GetOrderTimeslotsResponse>
    | GetOrderTimeslotsResponse;
}

export function OrderControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getOrderTimeslots'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Order', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Order', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDER_SERVICE_NAME = 'Order';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
