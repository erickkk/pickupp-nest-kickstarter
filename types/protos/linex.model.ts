/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'linex';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: OrderTrack | undefined;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  errorMessage: string;
}

export interface OrderResponse {
  targetUrl?: string | undefined;
  success: boolean;
  error?: OrderResponse_ResultErr | undefined;
  unAuthorizedRequest: boolean;
  Abp: boolean;
  result: OrderResponse_ResultRes | undefined;
}

export interface OrderResponse_ResultRes {
  hawbNumber?: string | undefined;
  result: boolean;
  resultMsg?: string | undefined;
}

export interface OrderResponse_ResultErr {
  code: number;
  message: string;
  details: string;
  validationErrors: string;
}

export interface OrderTrack {
  statusCode: string;
  trackList: GetStatusHawbForTrack[];
}

export interface GetStatusHawbForTrack {
  desc: string;
  eventTime: string;
  statusCode: string;
  locationName: string;
}

export interface Shipment {
  searchHawbForTrackList: GetStatusHawbForTrack[];
  hawbNumber: string;
  result: boolean;
  signed: boolean;
  resultMsg?: string | undefined;
}

export interface UpdateTripTimeline {
  status: string;
  time: string;
  location: string;
  driver: UpdateTripTimeline_Driver | undefined;
  coordinates: UpdateTripTimeline_Coordinates | undefined;
  proofs: string[];
}

export interface UpdateTripTimeline_Driver {
  name: string;
}

export interface UpdateTripTimeline_Coordinates {
  lat: string;
  long: string;
}

export const LINEX_PACKAGE_NAME = 'linex';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
