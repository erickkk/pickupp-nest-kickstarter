/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'wallet';

export interface PaymentProfile {
  id: number;
  paymentType: string;
  credit: string;
  creditExpireAt: string;
  creditPurchaseRate: number;
  entityId: number;
  createdAt: string;
  updatedAt: string;
  vendor: string;
}

export interface OrderTotalPrice {
  orderId: string;
  totalPrice: number;
}

export interface CreditCard {
  object: string;
  brand: string;
  country: string;
  expMonth: number;
  expYear: number;
  funding: string;
  last4: string;
}

export interface MonthlySummary {
  id: number;
  entityId: number;
  year: number;
  month: number;
  startBalance: string;
  endBalance: string;
  creditUsage: string;
  totalTransactions: number;
  totalOrders: number;
  mailSentAt: string;
  fileHash: string;
  createdAt: string;
  updatedAt: string;
  paymentTerms: string;
  paymentInstructions: string;
  summaryNotes: string;
  customDeal: boolean;
  customContent: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  billToName: string;
  billToAddressLine1: string;
  billToAddressLine2: string;
  billToAttn: string;
  dateOfIssue: string;
  invoiceNumber: string;
  lineItems: MonthlySummary_Item[];
  creditTopup: string;
  totalDue: string;
  legends: string;
}

export interface MonthlySummary_Item {
  label: string;
  value: string;
}

export interface Transaction {
  id: number;
  type: string;
  amount: string;
  status: string;
  paymentProfileId: number;
  paymentGateway: string;
  paymentToken: string;
  description: string;
  orderId: string;
  createdAt: string;
  updatedAt: string;
  refundAmount: string;
  completedAt: string;
  charge: Transaction_Charge | undefined;
  purchaseOrderId: string;
  metadata: { [key: string]: string };
  tax: string;
  paymentIntent: Transaction_PaymentIntent | undefined;
  vendor: string;
}

export interface Transaction_Charge {
  id: string;
  status: string;
  redirectUrl: string;
}

export interface Transaction_PaymentIntent {
  id: string;
  status: string;
}

export interface Transaction_MetadataEntry {
  key: string;
  value: string;
}

export interface Invoice {
  id: number;
  entityId: number;
  startDate: string;
  endDate: string;
  reference: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  billToName: string;
  billToAddressLine1: string;
  billToAddressLine2: string;
  billToAddressLine3: string;
  billToAttn: string;
  paymentTerms: string;
  summaryNotes: string;
  paymentInstructions: string;
  legends: string;
  subtotal: string;
  tax: string;
  totalDue: string;
  status: string;
  dateOfIssue: string;
  mailSentAt: string;
  items: Invoice_Item[];
  createdAt: string;
  updatedAt: string;
  mailStatus: string;
  feePrice: Invoice_Fee | undefined;
}

export interface Invoice_Item {
  id: number;
  label: string;
  value: string;
  tax: string;
}

export interface Invoice_Fee {
  pickuppCare: string;
}

export interface ChargePlan {
  profile: PaymentProfile | undefined;
  amount: number;
  defer: boolean;
  monthly: boolean;
  unsetMonthly: boolean;
  charge: boolean;
}

export interface AtomePaymentItem {
  id: string;
  name: string;
  price: string;
  quantity: string;
}

export interface AtomePaymentCustomerInfo {
  phone: string;
  name: string;
  email: string;
}

export interface AtomePaymentAddress {
  lines: string[];
  postCode: string;
}

export interface Fee {
  id: string;
  orderId: string;
  feeTypeId: string;
  feePrice: string;
  feeCofficient: string;
  feeTypeDisplayName: string;
}

export interface FeeType {
  id: string;
  feeTypeName: string;
  settingName: string;
  isDisabled: boolean;
  feeTypeDisplayName: string;
}

export interface Currency {
  id: string;
  inputCurrency: string;
  rateToUsd: string;
}

export const WALLET_PACKAGE_NAME = 'wallet';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
