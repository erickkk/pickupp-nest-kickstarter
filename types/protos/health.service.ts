/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { HealthResponse } from './health.model';

export const protobufPackage = 'health';

export interface EmptyRequest {}

export const HEALTH_PACKAGE_NAME = 'health';

export interface HealthCheckClient {
  getHealth(request: EmptyRequest): Observable<HealthResponse>;
}

export interface HealthCheckController {
  getHealth(
    request: EmptyRequest,
  ): Promise<HealthResponse> | Observable<HealthResponse> | HealthResponse;
}

export function HealthCheckControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getHealth'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('HealthCheck', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('HealthCheck', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const HEALTH_CHECK_SERVICE_NAME = 'HealthCheck';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
