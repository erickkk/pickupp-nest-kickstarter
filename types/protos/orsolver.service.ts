/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { SolveORProblemResponse, ORProblem } from './orsolver.model';

export const protobufPackage = 'orsolver';

export const ORSOLVER_PACKAGE_NAME = 'orsolver';

export interface ORSolverClient {
  solveORProblem(request: ORProblem): Observable<SolveORProblemResponse>;
}

export interface ORSolverController {
  solveORProblem(
    request: ORProblem,
  ):
    | Promise<SolveORProblemResponse>
    | Observable<SolveORProblemResponse>
    | SolveORProblemResponse;
}

export function ORSolverControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['solveORProblem'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('ORSolver', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('ORSolver', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const O_RSOLVER_SERVICE_NAME = 'ORSolver';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
