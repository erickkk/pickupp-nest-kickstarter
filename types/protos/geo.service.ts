/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  Geo,
  Suggest,
  Dictionary,
  SearchRequest,
  IDRequest,
} from './geo.model';

export const protobufPackage = 'geo';

export interface GetGeoRequest {
  address: string;
}

export interface GetGeosRequest {
  search: string;
  limit: number;
  offset: number;
}

export interface GetGeosResponse {
  geos: Geo[];
  total: number;
}

export interface GetSuggestsRequest {
  address: string;
  region: string;
}

export interface GetSuggestsResponse {
  suggests: Geo[];
}

export interface CreateSuggestsRequest {
  suggests: Suggest[];
}

export interface CreateSuggestsResponse {
  suggests: Suggest[];
}

export interface GetDictionariesResponse {
  dictionaries: Dictionary[];
  total: number;
}

export interface SegmentationRequest {
  query: string;
}

export interface SegmentationResponse {
  parts: { [key: string]: string };
}

export interface SegmentationResponse_PartsEntry {
  key: string;
  value: string;
}

export interface GetPlaceDetailRequest {
  id: string;
  description: string;
}

export interface GetPlaceDetailResponse {
  id: string;
  lat: number;
  lng: number;
}

export interface GeocodeRequest {
  address: string;
  language: string;
  region: string;
  skipFallback: boolean;
  pointType: string;
}

export interface GeocodeV2Response {
  lat: string;
  lng: string;
  parts: { [key: string]: string };
  source: string;
}

export interface GeocodeV2Response_PartsEntry {
  key: string;
  value: string;
}

export interface GeocodeResponse {
  results: Geo[];
}

export const GEO_PACKAGE_NAME = 'geo';

export interface GeosClient {
  getGeo(request: GetGeoRequest): Observable<Geo>;

  getGeos(request: SearchRequest): Observable<GetGeosResponse>;

  createGeo(request: Geo): Observable<Geo>;

  updateGeo(request: Geo): Observable<Geo>;

  removeGeo(request: IDRequest): Observable<Geo>;
}

export interface GeosController {
  getGeo(request: GetGeoRequest): Promise<Geo> | Observable<Geo> | Geo;

  getGeos(
    request: SearchRequest,
  ): Promise<GetGeosResponse> | Observable<GetGeosResponse> | GetGeosResponse;

  createGeo(request: Geo): Promise<Geo> | Observable<Geo> | Geo;

  updateGeo(request: Geo): Promise<Geo> | Observable<Geo> | Geo;

  removeGeo(request: IDRequest): Promise<Geo> | Observable<Geo> | Geo;
}

export function GeosControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getGeo',
      'getGeos',
      'createGeo',
      'updateGeo',
      'removeGeo',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Geos', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Geos', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const GEOS_SERVICE_NAME = 'Geos';

export interface SuggestsClient {
  getSuggests(request: GetSuggestsRequest): Observable<GetSuggestsResponse>;

  createSuggests(
    request: CreateSuggestsRequest,
  ): Observable<CreateSuggestsResponse>;
}

export interface SuggestsController {
  getSuggests(
    request: GetSuggestsRequest,
  ):
    | Promise<GetSuggestsResponse>
    | Observable<GetSuggestsResponse>
    | GetSuggestsResponse;

  createSuggests(
    request: CreateSuggestsRequest,
  ):
    | Promise<CreateSuggestsResponse>
    | Observable<CreateSuggestsResponse>
    | CreateSuggestsResponse;
}

export function SuggestsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getSuggests', 'createSuggests'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Suggests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Suggests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SUGGESTS_SERVICE_NAME = 'Suggests';

export interface DictionariesClient {
  getDictionaries(request: SearchRequest): Observable<GetDictionariesResponse>;

  createDictionary(request: Dictionary): Observable<Dictionary>;

  updateDictionary(request: Dictionary): Observable<Dictionary>;

  removeDictionary(request: IDRequest): Observable<Dictionary>;

  segmentation(request: SegmentationRequest): Observable<SegmentationResponse>;

  segmentationV2(
    request: SegmentationRequest,
  ): Observable<SegmentationResponse>;
}

export interface DictionariesController {
  getDictionaries(
    request: SearchRequest,
  ):
    | Promise<GetDictionariesResponse>
    | Observable<GetDictionariesResponse>
    | GetDictionariesResponse;

  createDictionary(
    request: Dictionary,
  ): Promise<Dictionary> | Observable<Dictionary> | Dictionary;

  updateDictionary(
    request: Dictionary,
  ): Promise<Dictionary> | Observable<Dictionary> | Dictionary;

  removeDictionary(
    request: IDRequest,
  ): Promise<Dictionary> | Observable<Dictionary> | Dictionary;

  segmentation(
    request: SegmentationRequest,
  ):
    | Promise<SegmentationResponse>
    | Observable<SegmentationResponse>
    | SegmentationResponse;

  segmentationV2(
    request: SegmentationRequest,
  ):
    | Promise<SegmentationResponse>
    | Observable<SegmentationResponse>
    | SegmentationResponse;
}

export function DictionariesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getDictionaries',
      'createDictionary',
      'updateDictionary',
      'removeDictionary',
      'segmentation',
      'segmentationV2',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Dictionaries', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Dictionaries', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const DICTIONARIES_SERVICE_NAME = 'Dictionaries';

export interface AddressesClient {
  getPlaceDetail(
    request: GetPlaceDetailRequest,
  ): Observable<GetPlaceDetailResponse>;

  geocode(request: GeocodeRequest): Observable<GeocodeResponse>;

  geocodeV2(request: GeocodeRequest): Observable<GeocodeV2Response>;
}

export interface AddressesController {
  getPlaceDetail(
    request: GetPlaceDetailRequest,
  ):
    | Promise<GetPlaceDetailResponse>
    | Observable<GetPlaceDetailResponse>
    | GetPlaceDetailResponse;

  geocode(
    request: GeocodeRequest,
  ): Promise<GeocodeResponse> | Observable<GeocodeResponse> | GeocodeResponse;

  geocodeV2(
    request: GeocodeRequest,
  ):
    | Promise<GeocodeV2Response>
    | Observable<GeocodeV2Response>
    | GeocodeV2Response;
}

export function AddressesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getPlaceDetail', 'geocode', 'geocodeV2'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Addresses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Addresses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ADDRESSES_SERVICE_NAME = 'Addresses';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
