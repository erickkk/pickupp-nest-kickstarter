/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  SettingResponse,
  FeatureToggleResponse,
  EmptyResponse,
} from './setting.model';

export const protobufPackage = 'setting';

export interface GetNextBusinessDayRequest {
  startDate: string;
  businessDaysLater: number;
}

export interface GetNextBusinessDayResponse {
  nextBusinessDay: string;
}

export interface GetHolidaysRequest {
  fromDate: string;
  toDate: string;
  weekendAsHoliday: { [key: string]: boolean };
}

export interface GetHolidaysRequest_WeekendAsHolidayEntry {
  key: string;
  value: boolean;
}

export interface GetHolidaysResponse {
  holidays: string[];
}

export interface GetSettingsRequest {
  city: string;
}

export interface GetSettingsResponse {
  settings: SettingResponse[];
}

export interface GetSettingRequest {
  key: string;
  city: string;
}

export interface GetSettingListsRequest {
  keys: string[];
  city: string;
}

export interface GetSettingListsResponse {
  settings: { [key: string]: string };
}

export interface GetSettingListsResponse_SettingsEntry {
  key: string;
  value: string;
}

export interface GetSettingResponse {
  setting: SettingResponse | undefined;
}

export interface UpdateSettingRequest {
  key: string;
  value: string;
  updater: string;
  city: string;
}

export interface UpdateSettingsRequest {
  fields: UpdateSettingRequest[];
}

export interface GetFeaturesRequest {
  tag: GetFeaturesRequest_Tag;
}

export enum GetFeaturesRequest_Tag {
  released = 0,
  developing = 1,
  UNRECOGNIZED = -1,
}

export interface CreateFeatureRequest {
  key: string;
  startFrom: string;
  stopFrom: string;
  updatedBy: string;
  tag: CreateFeatureRequest_Tag;
}

export enum CreateFeatureRequest_Tag {
  released = 0,
  developing = 1,
  UNRECOGNIZED = -1,
}

export interface UpdateFeatureRequest {
  key: string;
  startFrom: string;
  stopFrom: string;
  updatedBy: string;
  tag: string;
}

export interface DeleteFeatureRequest {
  id: string;
}

export interface GetFeaturesResponse {
  features: FeatureToggleResponse[];
}

export interface GetFeatureResponse {
  feature: FeatureToggleResponse | undefined;
}

export const SETTING_PACKAGE_NAME = 'setting';

export interface SettingsClient {
  getSettings(request: GetSettingsRequest): Observable<GetSettingsResponse>;

  getSetting(request: GetSettingRequest): Observable<GetSettingResponse>;

  updateSetting(request: UpdateSettingRequest): Observable<SettingResponse>;

  updateSettings(
    request: UpdateSettingsRequest,
  ): Observable<GetSettingsResponse>;

  getHolidays(request: GetHolidaysRequest): Observable<GetHolidaysResponse>;

  getNextBusinessDay(
    request: GetNextBusinessDayRequest,
  ): Observable<GetNextBusinessDayResponse>;

  getSettingLists(
    request: GetSettingListsRequest,
  ): Observable<GetSettingListsResponse>;
}

export interface SettingsController {
  getSettings(
    request: GetSettingsRequest,
  ):
    | Promise<GetSettingsResponse>
    | Observable<GetSettingsResponse>
    | GetSettingsResponse;

  getSetting(
    request: GetSettingRequest,
  ):
    | Promise<GetSettingResponse>
    | Observable<GetSettingResponse>
    | GetSettingResponse;

  updateSetting(
    request: UpdateSettingRequest,
  ): Promise<SettingResponse> | Observable<SettingResponse> | SettingResponse;

  updateSettings(
    request: UpdateSettingsRequest,
  ):
    | Promise<GetSettingsResponse>
    | Observable<GetSettingsResponse>
    | GetSettingsResponse;

  getHolidays(
    request: GetHolidaysRequest,
  ):
    | Promise<GetHolidaysResponse>
    | Observable<GetHolidaysResponse>
    | GetHolidaysResponse;

  getNextBusinessDay(
    request: GetNextBusinessDayRequest,
  ):
    | Promise<GetNextBusinessDayResponse>
    | Observable<GetNextBusinessDayResponse>
    | GetNextBusinessDayResponse;

  getSettingLists(
    request: GetSettingListsRequest,
  ):
    | Promise<GetSettingListsResponse>
    | Observable<GetSettingListsResponse>
    | GetSettingListsResponse;
}

export function SettingsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getSettings',
      'getSetting',
      'updateSetting',
      'updateSettings',
      'getHolidays',
      'getNextBusinessDay',
      'getSettingLists',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Settings', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Settings', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SETTINGS_SERVICE_NAME = 'Settings';

export interface FeatureTogglesClient {
  getFeatures(request: GetFeaturesRequest): Observable<GetFeaturesResponse>;

  createFeature(request: CreateFeatureRequest): Observable<GetFeatureResponse>;

  updateFeature(request: UpdateFeatureRequest): Observable<GetFeatureResponse>;

  deleteFeature(request: DeleteFeatureRequest): Observable<EmptyResponse>;
}

export interface FeatureTogglesController {
  getFeatures(
    request: GetFeaturesRequest,
  ):
    | Promise<GetFeaturesResponse>
    | Observable<GetFeaturesResponse>
    | GetFeaturesResponse;

  createFeature(
    request: CreateFeatureRequest,
  ):
    | Promise<GetFeatureResponse>
    | Observable<GetFeatureResponse>
    | GetFeatureResponse;

  updateFeature(
    request: UpdateFeatureRequest,
  ):
    | Promise<GetFeatureResponse>
    | Observable<GetFeatureResponse>
    | GetFeatureResponse;

  deleteFeature(
    request: DeleteFeatureRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function FeatureTogglesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getFeatures',
      'createFeature',
      'updateFeature',
      'deleteFeature',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('FeatureToggles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('FeatureToggles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const FEATURE_TOGGLES_SERVICE_NAME = 'FeatureToggles';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
