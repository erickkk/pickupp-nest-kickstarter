/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'outsource';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: OrderTrack | undefined;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  errorMessage: string;
  trackingNumber: string;
}

export interface OrderResponse {
  code: string;
  data: string;
  msg: string;
}

export interface TrackingEvent {
  eventTime: string;
  description: string;
  statusName: string;
  statusID: string;
}

export interface OrderTrack {
  status: string;
  trackingList: TrackingEvent[];
}

export interface Store {
  storeId?: string | undefined;
  latitude: string;
  longitude: string;
  name: string;
  address: Store_Address | undefined;
  detailHours: Store_DetailHours[];
  createdAt: string;
  updatedAt: string;
  storeType: string;
}

export interface Store_DetailHours {
  weekday: string;
  workingHours: Store_DetailHours_workingHour[];
}

export interface Store_DetailHours_workingHour {
  startTime: string;
  endTime: string;
}

export interface Store_Address {
  zh: string;
  en: string;
}

export const OUTSOURCE_PACKAGE_NAME = 'outsource';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
