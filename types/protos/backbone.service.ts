/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import {
  OrderAttributeKey,
  Waypoint_Status,
  Order_Status,
  TagEnum,
  Order,
  Waypoint_Type,
  Waypoint,
  OrderFlow,
  OrderFlowTag,
  Item,
  Parcel,
  OrderTrip,
  Timeline,
} from './backbone.model';
import { Observable } from 'rxjs';

export const protobufPackage = 'backbone';

/** backbone.service.proto */

export interface GetOrderRequest {
  orderId: string;
  orderNumber: string;
  crn: string;
}

export interface GetOrderResponse {
  order: Order | undefined;
}

export interface GetOrdersRequest {
  orderIds: string[];
  orderNumbers: string[];
  crns: string[];
  entityIds: number[];
  userIds: number[];
  statuses: Order_Status[];
  attributes: GetOrdersRequest_Attribute[];
  waypoint: GetOrdersRequest_WaypointQuery | undefined;
  options: GetOrdersRequest_Option[];
  preset: string;
}

export interface GetOrdersRequest_Attribute {
  key: OrderAttributeKey;
  value: string;
}

export interface GetOrdersRequest_WaypointQuery {
  types: Waypoint_Type[];
  statuses: Waypoint_Status[];
  cities: string[];
  districtLevel1s: string[];
  districtLevel2s: string[];
  districtLevel3s: string[];
  address: string;
}

export interface GetOrdersRequest_SortByColumns {
  key: string;
  ascOrDesc: string;
}

export interface GetOrdersRequest_Option {
  limit: string;
  offset: string;
  scope: string;
  orders: string[];
}

export interface GetOrdersResponse {
  orders: Order[];
  total: number;
}

export interface CancelOrderRequest {
  orderId: string;
  orderNumber: string;
  crn: string;
}

export interface CancelOrderResponse {
  success: boolean;
  errorMessage: string;
  errorCode: string;
  orderId: string;
}

export interface GetWaypointsRequest {
  orderIds: string[];
  types: Waypoint_Type[];
  statuses: Waypoint_Status[];
  address: string;
  cities: string[];
}

export interface GetWaypointsResponse {
  waypoints: Waypoint[];
}

export interface BulkUpdateWaypointRequest {
  orderIds: string[];
  status: Waypoint_Status;
}

export interface BulkUpdateWaypointResponse {
  waypoints: Waypoint[];
}

export interface BulkUpdateOrderStatusRequest {
  orderIds: string[];
  status: Order_Status;
}

export interface BulkUpdateOrderStatusResponse {
  orders: Order[];
}

export interface LockOrUnlockTableRequest {
  orderId: string;
}

export interface LockOrUnlockTableResponse {
  success: boolean;
}

export interface GetOrderFlowsRequest {
  names: string[];
}

export interface GetOrderFlowsResponse {
  orderFlows: OrderFlow[];
}

export interface CreateOrderFlowRequest {
  name: string;
}

export interface CreateOrderFlowResponse {
  orderFlow: OrderFlow | undefined;
}

export interface UpdateOrderFlowRequest {
  orderFlowId: string;
  name: string;
}

export interface DeleteOrderFlowRequest {
  names: string[];
}

export interface DeleteOrderFlowResponse {
  success: boolean;
}

export interface UpdateOrderFlowTagsRequest {
  orderFlowId: string;
  adds: TagEnum[];
  removes: TagEnum[];
}

export interface UpdateOrderFlowTagsResponse {
  orderFlowTags: OrderFlowTag[];
}

export interface UpdateOrderFlowResponse {
  success: boolean;
  errorMessage: string;
  orderFlows: OrderFlow[];
}

export interface GetTagsRequest {
  tags: string[];
}

export interface GetTagsResponse {
  tags: TagEnum[];
}

export interface IsNextStepRequest {
  serviceName: TagEnum;
  orderNumber: string;
  crn: string;
  orderId: string;
}

export interface IsNextStepResponse {
  isNext: boolean;
  isInvolved: boolean;
}

export interface GetItemsRequest {
  orderId: string;
}

export interface GetItemsResponse {
  items: Item[];
}

export interface GetParcelsRequest {
  orderId: string;
}

export interface GetParcelsResponse {
  parcels: Parcel[];
}

export interface AddTripRequest {
  orderId: string;
  tripId: string;
  tripType: TagEnum;
}

export interface GetTripsRequest {
  orderIds: string[];
  tripIds: string[];
  tripTypes: TagEnum[];
}

export interface GetTripsResponse {
  orderTrips: OrderTrip[];
}

export interface AddTimelineRequest {
  orderId: string;
  tripId: string;
  event: string;
  data: string;
  startedAt: string;
  endedAt: string;
}

export interface AddTimelineResponse {
  timeline: Timeline | undefined;
}

export interface GetTimelinesRequest {
  orderTripIds: string[];
  events: string[];
  data: string[];
}

export interface GetTimelinesResponse {
  timelines: Timeline[];
}

export const BACKBONE_PACKAGE_NAME = 'backbone';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<GetOrderResponse>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;

  createOrder(request: Order): Observable<Order>;

  cancelOrder(request: CancelOrderRequest): Observable<CancelOrderResponse>;

  getWaypoints(request: GetWaypointsRequest): Observable<GetWaypointsResponse>;

  bulkUpdateWaypointStatus(
    request: BulkUpdateWaypointRequest,
  ): Observable<BulkUpdateWaypointResponse>;

  bulkUpdateOrderStatus(
    request: BulkUpdateOrderStatusRequest,
  ): Observable<BulkUpdateOrderStatusResponse>;

  lockOrUnlockTable(
    request: Observable<LockOrUnlockTableRequest>,
  ): Observable<LockOrUnlockTableResponse>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ):
    | Promise<GetOrderResponse>
    | Observable<GetOrderResponse>
    | GetOrderResponse;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;

  createOrder(request: Order): Promise<Order> | Observable<Order> | Order;

  cancelOrder(
    request: CancelOrderRequest,
  ):
    | Promise<CancelOrderResponse>
    | Observable<CancelOrderResponse>
    | CancelOrderResponse;

  getWaypoints(
    request: GetWaypointsRequest,
  ):
    | Promise<GetWaypointsResponse>
    | Observable<GetWaypointsResponse>
    | GetWaypointsResponse;

  bulkUpdateWaypointStatus(
    request: BulkUpdateWaypointRequest,
  ):
    | Promise<BulkUpdateWaypointResponse>
    | Observable<BulkUpdateWaypointResponse>
    | BulkUpdateWaypointResponse;

  bulkUpdateOrderStatus(
    request: BulkUpdateOrderStatusRequest,
  ):
    | Promise<BulkUpdateOrderStatusResponse>
    | Observable<BulkUpdateOrderStatusResponse>
    | BulkUpdateOrderStatusResponse;

  lockOrUnlockTable(
    request: Observable<LockOrUnlockTableRequest>,
  ):
    | Promise<LockOrUnlockTableResponse>
    | Observable<LockOrUnlockTableResponse>
    | LockOrUnlockTableResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOrder',
      'getOrders',
      'createOrder',
      'cancelOrder',
      'getWaypoints',
      'bulkUpdateWaypointStatus',
      'bulkUpdateOrderStatus',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = ['lockOrUnlockTable'];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

export interface OrderFlowsClient {
  getOrderFlows(
    request: GetOrderFlowsRequest,
  ): Observable<GetOrderFlowsResponse>;

  createOrderFlow(
    request: CreateOrderFlowRequest,
  ): Observable<CreateOrderFlowResponse>;

  updateOrderFlow(
    request: UpdateOrderFlowRequest,
  ): Observable<UpdateOrderFlowResponse>;

  deleteOrderFlow(
    request: DeleteOrderFlowRequest,
  ): Observable<DeleteOrderFlowResponse>;

  updateOrderFlowTags(
    request: UpdateOrderFlowTagsRequest,
  ): Observable<UpdateOrderFlowTagsResponse>;

  getTags(request: GetTagsRequest): Observable<GetTagsResponse>;

  isNextStep(request: IsNextStepRequest): Observable<IsNextStepResponse>;
}

export interface OrderFlowsController {
  getOrderFlows(
    request: GetOrderFlowsRequest,
  ):
    | Promise<GetOrderFlowsResponse>
    | Observable<GetOrderFlowsResponse>
    | GetOrderFlowsResponse;

  createOrderFlow(
    request: CreateOrderFlowRequest,
  ):
    | Promise<CreateOrderFlowResponse>
    | Observable<CreateOrderFlowResponse>
    | CreateOrderFlowResponse;

  updateOrderFlow(
    request: UpdateOrderFlowRequest,
  ):
    | Promise<UpdateOrderFlowResponse>
    | Observable<UpdateOrderFlowResponse>
    | UpdateOrderFlowResponse;

  deleteOrderFlow(
    request: DeleteOrderFlowRequest,
  ):
    | Promise<DeleteOrderFlowResponse>
    | Observable<DeleteOrderFlowResponse>
    | DeleteOrderFlowResponse;

  updateOrderFlowTags(
    request: UpdateOrderFlowTagsRequest,
  ):
    | Promise<UpdateOrderFlowTagsResponse>
    | Observable<UpdateOrderFlowTagsResponse>
    | UpdateOrderFlowTagsResponse;

  getTags(
    request: GetTagsRequest,
  ): Promise<GetTagsResponse> | Observable<GetTagsResponse> | GetTagsResponse;

  isNextStep(
    request: IsNextStepRequest,
  ):
    | Promise<IsNextStepResponse>
    | Observable<IsNextStepResponse>
    | IsNextStepResponse;
}

export function OrderFlowsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOrderFlows',
      'createOrderFlow',
      'updateOrderFlow',
      'deleteOrderFlow',
      'updateOrderFlowTags',
      'getTags',
      'isNextStep',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('OrderFlows', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('OrderFlows', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDER_FLOWS_SERVICE_NAME = 'OrderFlows';

export interface ItemsClient {
  getItems(request: GetItemsRequest): Observable<GetItemsResponse>;
}

export interface ItemsController {
  getItems(
    request: GetItemsRequest,
  ):
    | Promise<GetItemsResponse>
    | Observable<GetItemsResponse>
    | GetItemsResponse;
}

export function ItemsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getItems'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Items', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Items', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ITEMS_SERVICE_NAME = 'Items';

export interface ParcelsClient {
  getParcels(request: GetParcelsRequest): Observable<GetParcelsResponse>;
}

export interface ParcelsController {
  getParcels(
    request: GetParcelsRequest,
  ):
    | Promise<GetParcelsResponse>
    | Observable<GetParcelsResponse>
    | GetParcelsResponse;
}

export function ParcelsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getParcels'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Parcels', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Parcels', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PARCELS_SERVICE_NAME = 'Parcels';

export interface TripTimelinesClient {
  addTrip(request: AddTripRequest): Observable<OrderTrip>;

  getTrips(request: GetTripsRequest): Observable<GetTripsResponse>;

  addTimeline(request: AddTimelineRequest): Observable<AddTimelineResponse>;

  getTimelines(request: GetTimelinesRequest): Observable<GetTimelinesResponse>;
}

export interface TripTimelinesController {
  addTrip(
    request: AddTripRequest,
  ): Promise<OrderTrip> | Observable<OrderTrip> | OrderTrip;

  getTrips(
    request: GetTripsRequest,
  ):
    | Promise<GetTripsResponse>
    | Observable<GetTripsResponse>
    | GetTripsResponse;

  addTimeline(
    request: AddTimelineRequest,
  ):
    | Promise<AddTimelineResponse>
    | Observable<AddTimelineResponse>
    | AddTimelineResponse;

  getTimelines(
    request: GetTimelinesRequest,
  ):
    | Promise<GetTimelinesResponse>
    | Observable<GetTimelinesResponse>
    | GetTimelinesResponse;
}

export function TripTimelinesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'addTrip',
      'getTrips',
      'addTimeline',
      'getTimelines',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('TripTimelines', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('TripTimelines', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRIP_TIMELINES_SERVICE_NAME = 'TripTimelines';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
