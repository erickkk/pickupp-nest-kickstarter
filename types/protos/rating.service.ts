/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { Question, Answer, EmptyRequest } from './rating.model';

export const protobufPackage = 'rating';

export interface EmptyResponse {}

export interface GetQuestionsResponse {
  total: number;
  questions: Question[];
}

export interface GetAnswersRequest {
  questionId: string;
  orderId: string;
  agentId: number;
  fromTime: string;
  toTime: string;
}

export interface GetAnswersResponse {
  total: number;
  answers: Answer[];
}

export interface RemarkResponse {
  id: string;
  subjectType: string;
  subjectId: string;
  objectType: string;
  objectId: string;
  content: string;
  isDeleted: boolean;
  updatedAt: string;
  createdAt: string;
  resolved: boolean;
}

export interface GetRemarksRequest {
  subjectType: string;
  subjectId: string;
  objectType: string;
  objectId: string;
}

export interface GetRemarksResponse {
  remarks: RemarkResponse[];
  total: number;
}

export interface CreateRemarkRequest {
  subjectType: string;
  subjectId: string;
  objectType: string;
  objectId: string;
  content: string;
  resolved: boolean;
}

export interface CreateRemarkResponse {
  remark: RemarkResponse | undefined;
}

export interface UpdateRemarkRequest {
  id: string;
  content: string;
  resolved: boolean;
}

export interface UpdateRemarkResponse {
  remark: RemarkResponse | undefined;
}

export interface DeleteRemarkRequest {
  id: string;
}

export const RATING_PACKAGE_NAME = 'rating';

export interface RatingsClient {
  getQuestions(request: EmptyRequest): Observable<GetQuestionsResponse>;

  getAnswers(request: GetAnswersRequest): Observable<GetAnswersResponse>;

  createAnswer(request: Answer): Observable<Answer>;
}

export interface RatingsController {
  getQuestions(
    request: EmptyRequest,
  ):
    | Promise<GetQuestionsResponse>
    | Observable<GetQuestionsResponse>
    | GetQuestionsResponse;

  getAnswers(
    request: GetAnswersRequest,
  ):
    | Promise<GetAnswersResponse>
    | Observable<GetAnswersResponse>
    | GetAnswersResponse;

  createAnswer(request: Answer): Promise<Answer> | Observable<Answer> | Answer;
}

export function RatingsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getQuestions',
      'getAnswers',
      'createAnswer',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Ratings', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Ratings', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const RATINGS_SERVICE_NAME = 'Ratings';

export interface RemarksClient {
  getRemarks(request: GetRemarksRequest): Observable<GetRemarksResponse>;

  createRemark(request: CreateRemarkRequest): Observable<CreateRemarkResponse>;

  updateRemark(request: UpdateRemarkRequest): Observable<UpdateRemarkResponse>;

  deleteRemark(request: DeleteRemarkRequest): Observable<EmptyResponse>;
}

export interface RemarksController {
  getRemarks(
    request: GetRemarksRequest,
  ):
    | Promise<GetRemarksResponse>
    | Observable<GetRemarksResponse>
    | GetRemarksResponse;

  createRemark(
    request: CreateRemarkRequest,
  ):
    | Promise<CreateRemarkResponse>
    | Observable<CreateRemarkResponse>
    | CreateRemarkResponse;

  updateRemark(
    request: UpdateRemarkRequest,
  ):
    | Promise<UpdateRemarkResponse>
    | Observable<UpdateRemarkResponse>
    | UpdateRemarkResponse;

  deleteRemark(
    request: DeleteRemarkRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function RemarksControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getRemarks',
      'createRemark',
      'updateRemark',
      'deleteRemark',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Remarks', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Remarks', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const REMARKS_SERVICE_NAME = 'Remarks';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
