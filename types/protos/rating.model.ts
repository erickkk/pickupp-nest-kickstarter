/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'rating';

export interface EmptyRequest {}

export interface Locale {
  en: string;
  zh: string;
  ms: string;
}

export interface Question {
  id: string;
  topic: string;
  label: Locale | undefined;
  canSkip: boolean;
  choices: Choice[];
}

export interface Choice {
  id: string;
  key: string;
  label: Locale | undefined;
}

export interface Answer {
  id: string;
  questionId: string;
  orderId: string;
  agentId: number;
  score: number;
  notes: string;
  choices: Choice[];
  warehouseId: string;
}

export const RATING_PACKAGE_NAME = 'rating';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
