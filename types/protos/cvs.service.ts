/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { Timestamp } from './google/protobuf/timestamp';
import { Store, Order } from './cvs.model';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';

export const protobufPackage = 'cvs';

export interface GetWarehouseRequest {}

export interface GetWarehouseResponse {
  id: string;
}

export interface GetStoreRequest {
  storeNumber: string;
  startDate: Timestamp | undefined;
  endDate: Timestamp | undefined;
}

export interface GetStoreResponse {
  store: Store | undefined;
}

export interface GetStoresRequest {
  storeNumbers: string[];
}

export interface GetStoresResponse {
  stores: Store[];
}

export interface GetOrderRequest {
  orderId: string;
  orderNumber: string;
  ODNO: string;
}

export interface GetOrderResponse {
  order: Order | undefined;
}

export interface GetOrdersRequest {
  orderIds: string[];
  orderNumbers: string[];
  ODNO: string[];
}

export interface GetOrdersResponse {
  orders: Order[];
}

export interface Empty {}

export interface SuccessResponse {
  success: boolean;
}

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export const CVS_PACKAGE_NAME = 'cvs';

export interface StoresClient {
  getWarehouse(request: GetWarehouseRequest): Observable<GetWarehouseResponse>;

  getStore(request: GetStoreRequest): Observable<GetStoreResponse>;

  getStores(request: GetStoresRequest): Observable<GetStoresResponse>;
}

export interface StoresController {
  getWarehouse(
    request: GetWarehouseRequest,
  ):
    | Promise<GetWarehouseResponse>
    | Observable<GetWarehouseResponse>
    | GetWarehouseResponse;

  getStore(
    request: GetStoreRequest,
  ):
    | Promise<GetStoreResponse>
    | Observable<GetStoreResponse>
    | GetStoreResponse;

  getStores(
    request: GetStoresRequest,
  ):
    | Promise<GetStoresResponse>
    | Observable<GetStoresResponse>
    | GetStoresResponse;
}

export function StoresControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getWarehouse', 'getStore', 'getStores'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Stores', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Stores', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const STORES_SERVICE_NAME = 'Stores';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<GetOrderResponse>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ):
    | Promise<GetOrderResponse>
    | Observable<GetOrderResponse>
    | GetOrderResponse;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getOrder', 'getOrders'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

export interface CronjobsClient {
  syncStores(request: Empty): Observable<SuccessResponse>;

  arrivedWarehouse(request: Empty): Observable<SuccessResponse>;

  arrivedStore(request: Empty): Observable<SuccessResponse>;

  droppedOff(request: Empty): Observable<SuccessResponse>;

  backToWarehouseArrived(request: Empty): Observable<SuccessResponse>;

  cvsCancel(request: Empty): Observable<SuccessResponse>;
}

export interface CronjobsController {
  syncStores(
    request: Empty,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  arrivedWarehouse(
    request: Empty,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  arrivedStore(
    request: Empty,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  droppedOff(
    request: Empty,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  backToWarehouseArrived(
    request: Empty,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  cvsCancel(
    request: Empty,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function CronjobsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'syncStores',
      'arrivedWarehouse',
      'arrivedStore',
      'droppedOff',
      'backToWarehouseArrived',
      'cvsCancel',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Cronjobs', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Cronjobs', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CRONJOBS_SERVICE_NAME = 'Cronjobs';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
