/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'order';

export enum BundleStatus {
  PENDING = 0,
  READY = 1,
  DISPATCHING = 2,
  CONTACTING_AGENT = 3,
  COMPLETED = 4,
  DESTROYED = 5,
  UNRECOGNIZED = -1,
}

export interface Order {
  id: string;
  orderNumber: string;
  status: Order_OrderStatus;
  pickupAddressLine1: string;
  pickupAddressLine2: string;
  pickupContactPerson: string;
  pickupContactPhone: string;
  pickupContactCompany: string;
  pickupZipCode: string;
  pickupLatitude: number;
  pickupLongitude: number;
  pickupTime: Timestamp | undefined;
  pickupNotes: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  dropoffContactCompany: string;
  dropoffZipCode: string;
  dropoffLatitude: number;
  dropoffLongitude: number;
  dropoffTime: Timestamp | undefined;
  dropoffNotes: string;
  width: number;
  height: number;
  length: number;
  weight: number;
  clientReferenceNumber: string;
  cashOnDelivery: boolean;
  cashOnDeliveryAmount: number;
  entityId: number;
  totalPrice: string;
  surgePrice: string;
  isFragile: boolean;
  pickupSms: boolean;
  coreMerchant: boolean;
  promoOrder: boolean;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  startedAt: Timestamp | undefined;
  endedAt: Timestamp | undefined;
  trips: Trip[];
  reliable: boolean;
  paymentProfileId: number;
  hasDeliveryNote: boolean;
  userId: number;
  isInAvailablePool: boolean;
  origin: string;
  metadata: Order_Metadata | undefined;
  errors: { [key: string]: string };
  pickupDistrictLevel1: string;
  pickupDistrictLevel2: string;
  pickupDistrictLevel3: string;
  dropoffDistrictLevel1: string;
  dropoffDistrictLevel2: string;
  dropoffDistrictLevel3: string;
  warehouseMeta: Order_WarehouseMeta | undefined;
  addressMeta: Order_AddressMeta | undefined;
  distributionId: string;
  exchangeOrder: boolean;
  group: string;
  purchaseOrderId: string;
  payOnSuccess: boolean;
  allowUnableToPickup: string;
  distance: number;
  serviceType: string;
  serviceTime: number;
  serviceProperties: Order_ServiceProperties | undefined;
  taxPrice: string;
  projectTagId?: number | undefined;
  pickupCity: string;
  dropoffCity: string;
  outsourcePartner: string;
  outsourceId?: string | undefined;
  flowsState: string;
  currentMasterId: string;
  totalParcel: number;
  parcels: Parcel[];
  items: Item[];
  feesPrice: string;
  isPickuppCare: boolean;
  fees: QuoteFee[];
  /** QuoteFeesResponse fees = 86; // TODO repeated QuoteFee fees = 86; */
  serviceOfferingId: string;
  parcelClientReferenceNumbers: string[];
}

export enum Order_OrderStatus {
  SCHEDULED = 0,
  CONTACTING_AGENT = 1,
  ACCEPTED = 2,
  ENROUTE = 4,
  DELIVERED = 6,
  EXPIRED = 7,
  UNABLE_TO_DELIVER = 8,
  MERCHANT_CANCELLED = 10,
  UNABLE_TO_PICKUP = 11,
  DISPATCHING = 13,
  BACK_TO_WAREHOUSE = 14,
  BACK_TO_MERCHANT = 15,
  RETURNED = 16,
  PREMATCHING = 17,
  DRAFT = 18,
  ITEM_NOT_RECEIVED = 19,
  PENDING_SCHEDULE = 21,
  PENDING_SELF_COLLECT = 22,
  OUT_FOR_DELIVERY = 23,
  RETURN_TO_MERCHANT = 24,
  PROCESSING = 25,
  IN_TRANSIT = 26,
  CREATED = 27,
  ARRANGING_PICKUP = 28,
  PICKING_UP = 29,
  EXCHANGED = 30,
  UNRECOGNIZED = -1,
}

export interface Order_Metadata {
  webhook: string;
  ownCrew: string;
  fcmToken: string;
  shopifyId: string;
  shopifyEmail: string;
  shopifyDomain: string;
  basicRate: string;
  weightRate: string;
  expressRate: string;
  discountRate: string;
  distanceRate: string;
  wallSurcharge: string;
  adjustmentRate: string;
  pickupSurcharge: string;
  reliabilityRate: string;
  dropoffSurcharge: string;
  expressSurcharge: string;
  warehouseMarking: string;
  attempts: string;
  utdReason: string;
  exchangeRate: string;
  nextOrderNumber: string;
  previousOrderNumber: string;
  cancellationReason: string;
  maskPickupNotes: boolean;
  pickupAttempts: string;
  dropoffAttempts: string;
  /** @deprecated */
  cvsStore: string;
  /** @deprecated */
  cvsParcelPrice: string;
  cancellationDetail: string;
  freightCollect: string;
  gbgQuoteId: string;
  convenienceStoreParcelPrice?: string | undefined;
  woocommerceOrderId: string;
  woocommerceShop: string;
  pickupTimeslotId: string;
  sopDeliveryOption: string;
  promotionCode: string;
  dutyType?: string | undefined;
  pathCode: string;
  isFirstOrder?: boolean | undefined;
}

export interface Order_WarehouseMeta {
  warehouseId: string;
  locationId: string;
  method: string;
  warehouseName: string;
  locationBarcode: string;
}

export interface Order_AddressMeta {
  pickupAddressConfidence: string;
  dropoffAddressConfidence: string;
  pickupAddressType: string;
  dropoffAddressType: string;
  pickupAddressSegment: string;
  dropoffAddressSegment: string;
  pickupRecognizedAddress: string;
  dropoffRecognizedAddress: string;
}

export interface Order_ServiceProperties {
  exchangeDestination: string;
  exchangeEstimatedDropoff: string;
  subOrderCount: number;
  timeWindow: string;
}

export interface Order_ErrorsEntry {
  key: string;
  value: string;
}

export interface Trip {
  id: string;
  orderId: string;
  pickupLatitude: number;
  pickupLongitude: number;
  dropoffLatitude: number;
  dropoffLongitude: number;
  status: Trip_TripStatus;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  startedAt: Timestamp | undefined;
  endedAt: Timestamp | undefined;
  pickedUpAt: Timestamp | undefined;
  droppedOffAt: Timestamp | undefined;
  pickupTime: Timestamp | undefined;
  dropoffTime: Timestamp | undefined;
  deliveryAgentId?: number | undefined;
  deliveryAgentTypeId: number;
  cancelReasonType: string;
  detailReason: string;
  tripPrice: string;
  surgePrice: string;
  pickupDistrictLevel1: string;
  pickupDistrictLevel2: string;
  pickupDistrictLevel3: string;
  dropoffDistrictLevel1: string;
  dropoffDistrictLevel2: string;
  dropoffDistrictLevel3: string;
  pickupContactPerson: string;
  pickupContactPhone: string;
  pickupContactCompany: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  dropoffContactCompany: string;
  deliveryType: string;
  isSurging: boolean;
  canDelayPickup: boolean;
  acceptMethod: string;
  bundleId: string;
  order: Order | undefined;
  isWarehouseOrder: boolean;
  pickupAddressLine1: string;
  pickupAddressLine2: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  acceptPrice: string;
  allowBundle: boolean;
  surgeStep: number;
  releaseTime: Timestamp | undefined;
  dispatchExpireTime: Timestamp | undefined;
  pastSurgeMeta: Trip_PastSurgeMeta | undefined;
  recipientMeta: Trip_RecipientMeta | undefined;
  photos: Photo[];
  jobId: string;
  pickupProcessStatus: PickupProcessStatus | undefined;
  dropoffProcessStatus: DropoffProcessStatus | undefined;
  matchingOptions: MatchingOptions | undefined;
  pickupCity: string;
  dropoffCity: string;
}

export enum Trip_TripStatus {
  PENDING = 0,
  CONTACTING_AGENT = 1,
  ACCEPTED = 2,
  ENROUTE = 4,
  DROPPED_OFF = 6,
  MERCHANT_CANCELLED = 8,
  UNABLE_TO_DELIVER = 10,
  UNABLE_TO_PICKUP = 11,
  UNKNOWN = 12,
  ADMIN_CANCELLED = 13,
  COMPLETED = 14,
  IN_BUNDLE = 15,
  BACK_TO_WAREHOUSE = 16,
  ITEM_NOT_RECEIVED = 19,
  UNRECOGNIZED = -1,
}

export interface Trip_PastSurgeMeta {
  routeSurge: string;
  merchantSurge: string;
  dispatchScore: string;
  aHourSurge: string;
  pHourDHourSurge: string;
  targetPrice: string;
  isNonBidding: string;
  fipSurge: string;
  orderFlowsState: string;
}

export interface Trip_RecipientMeta {
  recipientType: string;
  actualRecipient: string;
}

export interface PickupProcessStatus {
  qrOrPasscode: boolean;
  geolocation: boolean;
  pickupPhoto: boolean;
  reportedSubOrderCount: number;
  pickupCallLog: boolean;
}

export interface DropoffProcessStatus {
  qrOrPasscode: boolean;
  geolocation: boolean;
  signaturePhoto: boolean;
  dropoffPhoto: boolean;
  cashOnDelivery: boolean;
  deliveryNote: boolean;
  recipientVerifyCode: boolean;
  contactlessOrUnattended: boolean;
}

export interface MatchingOptions {
  canPool: boolean;
  canDispatch: boolean;
  canPrematch: boolean;
  canJobAssign: boolean;
}

export interface BundleRequest {
  status: BundleStatus;
  pickupLatitude: number;
  pickupLongitude: number;
  dropoffLatitude: number;
  dropoffLongitude: number;
  pickupDistrictLevel1: string;
  pickupDistrictLevel2: string;
  pickupDistrictLevel3: string;
  dropoffDistrictLevel1: string;
  dropoffDistrictLevel2: string;
  dropoffDistrictLevel3: string;
  price: string;
  releaseTime: Timestamp | undefined;
  matchingOptions: MatchingOptions | undefined;
}

export interface BundleResult {
  id: string;
  status: BundleStatus;
  pickupLatitude: number;
  pickupLongitude: number;
  dropoffLatitude: number;
  dropoffLongitude: number;
  pickupDistrictLevel1: string;
  pickupDistrictLevel2: string;
  pickupDistrictLevel3: string;
  dropoffDistrictLevel1: string;
  dropoffDistrictLevel2: string;
  dropoffDistrictLevel3: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  trips: Trip[];
  releaseTime: Timestamp | undefined;
  matchingOptions: MatchingOptions | undefined;
}

export interface DeliveryNote {
  id: string;
  orderId: string;
  tripId: string;
  entityId: number;
  deliveryAgentId: number;
  status: DeliveryNote_Status;
  imageUrl: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  rejectedAt: Timestamp | undefined;
  order: Order | undefined;
  approvedAt: Timestamp | undefined;
  approvedBy: string;
}

export enum DeliveryNote_Status {
  PENDING = 0,
  DONE = 1,
  REJECTED = 2,
  APPROVED = 3,
  UNRECOGNIZED = -1,
}

export interface Cluster {
  trips: Trip[];
}

export interface Job {
  id: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  startTime: string;
  endTime: string;
  cutoffTime: string;
  acceptedAt: string;
  deliveryAgentId: number;
  price: number;
  pickupRegion: string;
  dropoffRegion: string;
  capacity: number;
  notes: string;
  trips: Trip[];
  orderTypes: string[];
  deliveryAgentTypeIds: number[];
  allowDistribute: boolean;
  projectTagId?: number | undefined;
  pickupThreshold: number;
  dropoffThreshold: number;
  serviceTypes: ServiceTypeWithTime[];
  minPayout: number;
  jobNumber: string;
  standbyAddress?: string | undefined;
  standbyLatitude?: number | undefined;
  standbyLongitude?: number | undefined;
  attendanceCheckedAt: boolean;
  isIa?: boolean | undefined;
  isUrgent: boolean;
  releaseTime: string;
}

export interface Parcel {
  id: string;
  orderId: string;
  parcelNumber: string;
  status: string;
  warehouseMeta: Order_WarehouseMeta | undefined;
  createdAt: string;
  updatedAt: string;
  parcelCrn: string;
}

export interface Photo {
  id: string;
  orderId: string;
  tripId: string;
  key: string;
  uri: string;
  createdAt: string;
}

export interface Route {
  tripId: string;
  type: string;
  latitude: number;
  longitude: number;
}

export interface Timeline {
  id: number;
  orderId: string;
  tripId: string;
  object: string;
  objectName: string;
  action: string;
  subject: string;
  subjectName: string;
  details: { [key: string]: string };
  createdAt: string;
  updatedAt: string;
}

export interface Timeline_DetailsEntry {
  key: string;
  value: string;
}

export interface ServiceGroup {
  id: string;
  serviceType: string;
  serviceTime: number;
}

export interface ServiceTypeWithTime {
  serviceType: string;
  serviceTime: number;
}

export interface BundleSetting {
  id: string;
  maxSize: number;
  distanceThreshold: number;
  timeThreshold: number;
  maxConcurrentFragile: number;
  maxWeight: number;
  maxDimension: number;
  samePickupPoint: boolean;
  sameDropoffPoint: boolean;
  payOnSuccess: boolean;
  geofence: string;
  isActive: boolean;
  createdAt: string;
  updatedAt: string;
  serviceGroups: ServiceGroup[];
  cashOnDeliveryThreshold: number;
  cashOnDeliveryCap: number;
}

export interface PoolOrder {
  id: string;
  status: string;
  orderId: string;
  orderNumber: string;
  pickupTime: string;
  pickupAddressLine1: string;
  pickupAddressLine2: string;
  pickupContactPerson: string;
  pickupContactPhone: string;
  pickupContactCompany: string;
  pickupZipCode: string;
  pickupCity: string;
  pickupNotes: string;
  pickupLatitude: number;
  pickupLongitude: number;
  pickupDistrictLevel1: string;
  pickupDistrictLevel2: string;
  pickupDistrictLevel3: string;
  dropoffTime: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  dropoffContactCompany: string;
  dropoffZipCode: string;
  dropoffCity: string;
  dropoffNotes: string;
  dropoffLatitude: number;
  dropoffLongitude: number;
  dropoffDistrictLevel1: string;
  dropoffDistrictLevel2: string;
  dropoffDistrictLevel3: string;
  distance: number;
  width: number;
  height: number;
  length: number;
  weight: number;
  tripPrice: number;
  isSurging: boolean;
  surgePrice: number;
  surgeStep: number;
  canDelayPickup: boolean;
  createdAt: string;
  updatedAt: string;
  releaseTime: string;
  clientReferenceNumber: string;
  cashOnDelivery: boolean;
  cashOnDeliveryAmount: number;
  isFragile: boolean;
  pickupSms: boolean;
  coreMerchant: boolean;
  promoOrder: boolean;
  reliable: boolean;
  paymentProfileId: number;
  hasDeliveryNote: boolean;
  purchaseOrderId: string;
  payOnSuccess: boolean;
  allowUnableToPickup: string;
  serviceTime: number;
  projectTagId: number;
  flowsState: string;
  outsourcePartner: string;
  outsourceId: string;
  currentMasterId: string;
  deliveryType: string;
  isWarehouseOrder: boolean;
  entityId: number;
  userId: number;
  pastSurgeMeta: Trip_PastSurgeMeta | undefined;
  warehouseMeta: Order_WarehouseMeta | undefined;
  metadata: Order_Metadata | undefined;
  addressMeta: Order_AddressMeta | undefined;
  serviceType: string;
  serviceProperties: Order_ServiceProperties | undefined;
  recipientMeta: Trip_RecipientMeta | undefined;
  matchingOptions: MatchingOptions | undefined;
  isBundle: boolean;
  isInAvailablePool: boolean;
  dispatchExpireTime: string;
  orderPickupTime: string;
  serviceOfferingId: string;
}

export interface Item {
  id: string;
  orderId: string;
  name: string;
  count: number;
  declaredValue: string;
  currency: string;
  hsCode: string;
  unitWeight: string;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
}

export interface QuoteFee {
  totalPrice: number;
  coefficient: string;
  feeTypeName: string;
  feeTypeDisplayName: string;
  breakdown: QuoteFee_FeeBreakDown[];
}

export interface QuoteFee_FeeBreakDown {
  feePrice: number;
  name: string;
}

export interface QuoteFeesResponse {
  fees: QuoteFee[];
}

export interface ReleaseJobRequest {
  id: string;
}

export interface OrsolverNormalJobRequest {
  agentId: string;
}

export interface OrsolverJobRepsonse {
  result: { [key: string]: string };
}

export interface OrsolverJobRepsonse_ResultEntry {
  key: string;
  value: string;
}

export const ORDER_PACKAGE_NAME = 'order';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
