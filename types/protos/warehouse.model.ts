/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'warehouse';

export interface CurrentUser {
  id: number;
  userType: string;
  warehouseId: string;
  warehouseUserId: string;
  permissions: Permission[];
  name: string;
}

export interface Permission {
  group: string;
  permission: string;
}

export interface Warehouse {
  id: string;
  name: string;
  lat: number;
  lng: number;
  status: string;
  addressLine1: string;
  addressLine2: string;
  phone: string;
  openingHours: SessionList[];
  createdAt: string;
  updatedAt: string;
  canReturn?: boolean | undefined;
  publicName: string;
  forceDistricts: string[];
  merchantWhitelist: number[];
  zipCode: string;
  city: string;
  pinned?: boolean | undefined;
  partner: string;
  canFirstLeg?: boolean | undefined;
  selfPick?: string | undefined;
}

export interface Location {
  id: string;
  warehouseId: string;
  barcode: string;
  description: string;
  createdAt: string;
  updatedAt: string;
}

export interface BarcodeHistory {
  id: string;
  warehouseId: string;
  locationId: string;
  userId: number;
  userType: string;
  orderId: string;
  barcode: string;
  method: string;
  createdAt: string;
  updatedAt: string;
  isManualInput: boolean;
  parcelId: string;
}

export interface WarehouseContract {
  id: string;
  warehouseId: string;
  contractStart: string;
  contractEnd: string;
  contractLink: string;
  createdAt: string;
  updatedAt: string;
  remarks: string;
}

export interface WarehouseUser {
  id: string;
  warehouseId: string;
  userId: number;
  userType: string;
  createdAt: string;
  updatedAt: string;
}

export interface WarehouseRule {
  id: number;
  warehouseId: string;
  ruleKey: string;
  ruleValue: string;
  createdAt: string;
  updatedAt: string;
}

export interface LocationRule {
  id: number;
  locationId: string;
  ruleKey: string;
  ruleValue: string;
  createdAt: string;
  updatedAt: string;
}

export interface WarehousePermission {
  id: number;
  permissionGroup: string;
  permissionKey: string;
  createdAt: string;
  updatedAt: string;
}

export interface WarehouseUserPermission {
  id: number;
  warehouseUserId: string;
  warehousePermissionId: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
}

export interface RequestWithUUID {
  id: string;
  select: string;
}

export interface EmptyRequest {}

export interface EmptyResponse {}

export interface SessionList {
  sessions: Session[];
}

export interface Session {
  startTime: string;
  endTime: string;
}

export const WAREHOUSE_PACKAGE_NAME = 'warehouse';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
