/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'pchome';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: Order_ShipOrderLogItem[];
  createdAt: string;
  updatedAt: string;
  lastSyncedAt: string;
  errorMessage: string;
}

export interface Order_ShipOrderLogItem {
  shipStatus: string;
  shipStatusCode: string;
  datetime: string;
  status: string;
}

export interface OrderResponse {
  error: number;
  message: string;
  status: number;
  data: { [key: string]: string };
}

export interface OrderResponse_DataEntry {
  key: string;
  value: string;
}

export const PCHOME_PACKAGE_NAME = 'pchome';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
