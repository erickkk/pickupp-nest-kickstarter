/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  AgentProfile,
  AgentResponse,
  CurrentUser,
  DeliveryAgentType,
  Skill,
  AgentSkill,
  LocaleTrainingDescription,
} from './agent.model';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'agent';

export interface EmptyRequest {}

export interface EmptyResponse {}

export interface AuthRequest {
  token: string;
}

export interface AuthResponse {
  id: number;
  email: string;
  deliveryAgentTypeId: number;
  status: string;
  name: string;
}

export interface SignupRequest {
  email: string;
  password: string;
  name: string;
  phone: string;
  profileImageUrl: string;
  agentType: string;
  locale: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface LoginResponse {
  token: string;
  status: string;
}

export interface LogoutRequest {
  id: number;
  token: string;
}

export interface LogoutResponse {
  code: string;
}

export interface ForgotPasswordRequest {
  email: string;
}

export interface ForgotPasswordResponse {
  success: boolean;
}

export interface ForgotPasswordConfirmationRequest {
  email: string;
  token: string;
  password: string;
}

export interface ForgotPasswordConfirmationResponse {
  success: boolean;
}

export interface AgentAnswerRequest {
  agentId: number;
  answer: { [key: string]: string };
}

export interface AgentAnswerRequest_AnswerEntry {
  key: string;
  value: string;
}

export interface TripInfoForHistory {
  distance: number;
  pickupTime: string;
  dropoffTime: string;
  pickupDistrictLevel3: string;
  dropoffDistrictLevel3: string;
  pickupAddressType: string;
  dropoffAddressType: string;
  weight: number;
  bundleId: string;
}

export interface AgentTripHistoryRequest {
  trip: TripInfoForHistory | undefined;
  agentId: number;
}

export interface AgentProfileRequest {
  agentId: number;
}

export interface AgentProfileResponse {
  agentProfile: AgentProfile | undefined;
  totalTripCount: number;
}

export interface CalculateAgentRatingsRequest {
  startTime: string;
}

export interface CalculateFinishedTripPointsRequest {
  deliveryAgentId: number;
  ownCrew: string;
  utdReason: string;
  cancelReasonType: string;
  tripPickupTime: Timestamp | undefined;
  tripDropoffTime: Timestamp | undefined;
  projectTagId?: number | undefined;
}

export interface CalculateAgentRatingsResponse {
  updateRequests: UpdateTierMetaRequest[];
}

export interface UpdateTierMetaRequest {
  deliveryAgentId: number;
  experiencePoint: number;
  ontimeCount: number;
  ratingCount: number;
  totalTrip: number;
  isSingleTier: boolean;
}

export interface AdjustAgentTiersRequest {
  deliveryAgentIds: number[];
}

export interface AdjustAgentTiersResponse {
  registrationIds: string[];
}

export interface GetOnlineAgentsRequest {}

export interface GetOnlineAgentsResponse {
  agents: AgentResponse[];
}

export interface GetAgentsRequest {
  limit: string;
  offset: string;
  query: string;
  sortBy: string;
  tagIds: number[];
}

export interface GetAgentsResponse {
  agents: AgentResponse[];
  total: number;
}

export interface GetAgentRequest {
  currentUser: CurrentUser | undefined;
}

export interface UpdateAgentRequest {
  currentUser: CurrentUser | undefined;
  name: string;
  phone: string;
  profileImageUrl: string;
  status: string;
  deliveryAgentTypeId: string;
  email: string;
  address: string;
  latitude: string;
  longitude: string;
  score: string;
}

export interface UpdateAgentTokenRequest {
  currentUser: CurrentUser | undefined;
  registrationId: string;
  platform: string;
}

export interface UpdateLocationRequest {
  currentUser: CurrentUser | undefined;
  latitude: string;
  longitude: string;
}

export interface UpdateFirstTripRequest {
  id: string;
  firstTripTimestamp: string;
}

export interface AgentOnlineRequest {
  currentUser: CurrentUser | undefined;
}

export interface AgentOfflineRequest {
  currentUser: CurrentUser | undefined;
}

export interface UpdateAgentTokenResponse {}

export interface GetActiveAgentsRequest {
  interval: string;
  agentType: string;
  isOnline: boolean;
}

export interface ChangePasswordRequest {
  currentUser: CurrentUser | undefined;
  password: string;
}

export interface ChangePasswordResponse {
  success: boolean;
}

export interface GetAgentsByIdsRequest {
  ids: number[];
}

export interface GetAgentsByIdsResponse {
  agents: AgentResponse[];
}

export interface AddOutstandingCodRequest {
  currentUser: CurrentUser | undefined;
  amount: string;
}

export interface AddOutstandingCodResponse {
  agent: AgentResponse | undefined;
}

export interface GetAgentsByFilterRequest {
  filter: { [key: string]: string };
  agentTypeIds: number[];
  locationUpdatedAt: string;
  columns: string[];
  statuses: number[];
}

export interface GetAgentsByFilterRequest_FilterEntry {
  key: string;
  value: string;
}

export interface GetAgentsByFilterResponse {
  agents: AgentResponse[];
}

export interface GetAvailableDocumentTypesRequest {
  isVehicle: boolean;
}

export interface DocumentDataOption {
  type: string;
  options: string[];
  default: string;
  lines: number;
  index: number;
}

export interface DocumentTypeFormat {
  name: DocumentDataOption | undefined;
  idNumber: DocumentDataOption | undefined;
  dateOfBirth: DocumentDataOption | undefined;
  phone: DocumentDataOption | undefined;
  gender: DocumentDataOption | undefined;
  address: DocumentDataOption | undefined;
  expiryDate: DocumentDataOption | undefined;
  insuranceNumber: DocumentDataOption | undefined;
  licensePlate: DocumentDataOption | undefined;
  model: DocumentDataOption | undefined;
  manufactureYear: DocumentDataOption | undefined;
  verifyPic: DocumentDataOption | undefined;
  verifyLicense: DocumentDataOption | undefined;
  issueDate: DocumentDataOption | undefined;
  watchVideo: DocumentDataOption | undefined;
  watchSafetyGuideline: DocumentDataOption | undefined;
  takenTest: DocumentDataOption | undefined;
  bankName: DocumentDataOption | undefined;
  branchCode: DocumentDataOption | undefined;
  accountNumber: DocumentDataOption | undefined;
  bankAccountHolderName: DocumentDataOption | undefined;
}

export interface DocumentType {
  type: string;
  data: DocumentTypeFormat | undefined;
  skipProof: boolean;
}

export interface GetAvailableDocumentTypesResponse {
  types: DocumentType[];
}

export interface DocumentResponse {
  id: number;
  documentType: string;
  documentPath: string;
  documentDescription: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  rejectReason: string;
  documentData: { [key: string]: string };
}

export interface DocumentResponse_DocumentDataEntry {
  key: string;
  value: string;
}

export interface GetPendingDocumentsRequest {}

export interface GetPendingDocumentsResponse {
  agents: GetPendingDocumentsResponse_Response[];
}

export interface GetPendingDocumentsResponse_Response {
  id: number;
  email: string;
  phone: string;
  name: string;
  status: string;
  type: string;
  isVehicle: boolean;
  pending: number;
  approved: number;
  required: number;
}

export interface GetAgentDocumentsRequest {
  currentUser: CurrentUser | undefined;
  types: string[];
}

export interface GetAgentDocumentsResponse {
  documents: DocumentResponse[];
  total: number;
}

export interface CreateDocumentRequest {
  currentUser: CurrentUser | undefined;
  documentType: string;
  documentPath: string;
  documentDescription: string;
  documentData: { [key: string]: string };
}

export interface CreateDocumentRequest_DocumentDataEntry {
  key: string;
  value: string;
}

export interface UpdateDocumentRequest {
  currentUser: CurrentUser | undefined;
  id: number;
  status: string;
  documentDescription: string;
  rejectReason: string;
  documentPath: string;
  documentData: { [key: string]: string };
}

export interface UpdateDocumentRequest_DocumentDataEntry {
  key: string;
  value: string;
}

export interface MissingDocumentMailRequest {
  ids: number[];
}

export interface MissingDocumentMailResponse {
  success: boolean;
}

export interface GetAgentTypesRequest {
  isVisible: boolean;
  filter: { [key: string]: string };
}

export interface GetAgentTypesRequest_FilterEntry {
  key: string;
  value: string;
}

export interface GetAgentTypeRequest {
  id: string;
}

export interface GetAgentTierRequest {
  id: string;
}

export interface GetAgentTypesResponse {
  deliveryAgentTypes: DeliveryAgentType[];
  total: number;
}

export interface GetCrewRequest {
  entityId: number;
  search: string;
  limit: string;
  offset: string;
}

export interface GetCrewResponse {
  agents: AgentResponse[];
  total: number;
}

export interface GetEntitiesIdsRequest {
  currentUser: CurrentUser | undefined;
}

export interface GetEntitiesIdsResponse {
  entityIds: number[];
}

export interface GetAgentByEntityRequest {
  deliveryAgentId: number;
  entityId: number;
}

export interface GetAgentByEntityRespose {
  agent: AgentResponse | undefined;
}

export interface AssociateAgentRequest {
  deliveryAgentId: number;
  entityId: number;
}

export interface DisassociateAgentRequest {
  deliveryAgentId: number;
  entityId: number;
}

export interface SuccessResponse {
  success: boolean;
}

export interface RequestWithAgentId {
  agentId: number;
}

export interface AgentsTagsRequest {
  deliveryAgentIds: number[];
  tagIds: number[];
}

export interface SignupTokenRequest {
  phone: string;
}

export interface SignupTokenResponse {
  phone: string;
  token: string;
  expiredAt: string;
}

export interface VerifySignupRequest {
  token: string;
}

export interface GetSkillsRequest {
  query: string;
  isActive: boolean;
  ids: string[];
  limit: string;
  offset: string;
}

export interface GetSkillsResponse {
  skills: Skill[];
  total: number;
}

export interface GetAgentSkillsRequest {
  deliveryAgentIds: number[];
  skillIds: string[];
  statuses: string[];
  limit: string;
  offset: string;
  ids: string[];
}

export interface GetAgentSkillsResponse {
  skills: AgentSkill[];
  total: number;
}

export interface CreateSkillRequest {
  name: string;
  enDisplay: string;
  zhDisplay: string;
  description: { [key: string]: string };
  enRequirements: { [key: string]: string };
  zhRequirements: { [key: string]: string };
  myRequirements: { [key: string]: string };
  trainingDescription: { [key: string]: LocaleTrainingDescription };
  resultUrl: string;
  validityPeriod: number;
  minTierActivation: number[];
  isActive: boolean;
  imageUrl: string;
}

export interface CreateSkillRequest_DescriptionEntry {
  key: string;
  value: string;
}

export interface CreateSkillRequest_EnRequirementsEntry {
  key: string;
  value: string;
}

export interface CreateSkillRequest_ZhRequirementsEntry {
  key: string;
  value: string;
}

export interface CreateSkillRequest_MyRequirementsEntry {
  key: string;
  value: string;
}

export interface CreateSkillRequest_TrainingDescriptionEntry {
  key: string;
  value: LocaleTrainingDescription | undefined;
}

export interface UpdateSkillRequest {
  id: string;
  name: string;
  enDisplay: string;
  zhDisplay: string;
  description: { [key: string]: string };
  enRequirements: { [key: string]: string };
  zhRequirements: { [key: string]: string };
  myRequirements: { [key: string]: string };
  trainingDescription: { [key: string]: LocaleTrainingDescription };
  resultUrl: string;
  validityPeriod: number;
  minTierActivation: number[];
  isActive: boolean;
  imageUrl: string;
}

export interface UpdateSkillRequest_DescriptionEntry {
  key: string;
  value: string;
}

export interface UpdateSkillRequest_EnRequirementsEntry {
  key: string;
  value: string;
}

export interface UpdateSkillRequest_ZhRequirementsEntry {
  key: string;
  value: string;
}

export interface UpdateSkillRequest_MyRequirementsEntry {
  key: string;
  value: string;
}

export interface UpdateSkillRequest_TrainingDescriptionEntry {
  key: string;
  value: LocaleTrainingDescription | undefined;
}

export interface AssociateSkillRequest {
  skillIds: string[];
  deliveryAgentIds: number[];
}

export interface DissociateSkillRequest {
  id: string;
}

export interface UpdateAgentSkillRequest {
  id: string;
  status: string;
  expireAt: string;
}

export interface UpdateAgentSkillsRequest {
  agentSkills: UpdateAgentSkillRequest[];
}

export interface UpdateAgentSkillsResponse {
  agent: AgentSkill[];
}

export const AGENT_PACKAGE_NAME = 'agent';

export interface SessionsClient {
  auth(request: AuthRequest): Observable<AuthResponse>;

  signup(request: SignupRequest): Observable<AgentResponse>;

  login(request: LoginRequest): Observable<LoginResponse>;

  logout(request: LogoutRequest): Observable<LogoutResponse>;

  forgotPassword(
    request: ForgotPasswordRequest,
  ): Observable<ForgotPasswordResponse>;

  forgotPasswordConfirmation(
    request: ForgotPasswordConfirmationRequest,
  ): Observable<ForgotPasswordConfirmationResponse>;
}

export interface SessionsController {
  auth(
    request: AuthRequest,
  ): Promise<AuthResponse> | Observable<AuthResponse> | AuthResponse;

  signup(
    request: SignupRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  login(
    request: LoginRequest,
  ): Promise<LoginResponse> | Observable<LoginResponse> | LoginResponse;

  logout(
    request: LogoutRequest,
  ): Promise<LogoutResponse> | Observable<LogoutResponse> | LogoutResponse;

  forgotPassword(
    request: ForgotPasswordRequest,
  ):
    | Promise<ForgotPasswordResponse>
    | Observable<ForgotPasswordResponse>
    | ForgotPasswordResponse;

  forgotPasswordConfirmation(
    request: ForgotPasswordConfirmationRequest,
  ):
    | Promise<ForgotPasswordConfirmationResponse>
    | Observable<ForgotPasswordConfirmationResponse>
    | ForgotPasswordConfirmationResponse;
}

export function SessionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'auth',
      'signup',
      'login',
      'logout',
      'forgotPassword',
      'forgotPasswordConfirmation',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SESSIONS_SERVICE_NAME = 'Sessions';

export interface AgentsClient {
  getAgents(request: GetAgentsRequest): Observable<GetAgentsResponse>;

  getAgent(request: GetAgentRequest): Observable<AgentResponse>;

  updateAgent(request: UpdateAgentRequest): Observable<AgentResponse>;

  updateAgentToken(
    request: UpdateAgentTokenRequest,
  ): Observable<UpdateAgentTokenResponse>;

  updateLocation(request: UpdateLocationRequest): Observable<AgentResponse>;

  updateFirstTrip(request: UpdateFirstTripRequest): Observable<EmptyResponse>;

  agentOnline(request: AgentOnlineRequest): Observable<AgentResponse>;

  agentOffline(request: AgentOfflineRequest): Observable<AgentResponse>;

  getActiveAgents(
    request: GetActiveAgentsRequest,
  ): Observable<GetAgentsResponse>;

  getOnlineAgents(
    request: GetOnlineAgentsRequest,
  ): Observable<GetOnlineAgentsResponse>;

  changePassword(
    request: ChangePasswordRequest,
  ): Observable<ChangePasswordResponse>;

  getAgentsByIds(
    request: GetAgentsByIdsRequest,
  ): Observable<GetAgentsByIdsResponse>;

  addOutstandingCod(
    request: AddOutstandingCodRequest,
  ): Observable<AddOutstandingCodResponse>;

  getAgentsByFilter(
    request: GetAgentsByFilterRequest,
  ): Observable<GetAgentsByFilterResponse>;

  reacceptTerms(request: RequestWithAgentId): Observable<AgentResponse>;

  createAgent(request: SignupRequest): Observable<AgentResponse>;

  updateTierMeta(request: UpdateTierMetaRequest): Observable<AgentResponse>;

  adjustAgentTiers(
    request: AdjustAgentTiersRequest,
  ): Observable<AdjustAgentTiersResponse>;

  calculateAgentRatings(
    request: CalculateAgentRatingsRequest,
  ): Observable<CalculateAgentRatingsResponse>;

  calculateFinishedTripPoints(
    request: CalculateFinishedTripPointsRequest,
  ): Observable<UpdateTierMetaRequest>;
}

export interface AgentsController {
  getAgents(
    request: GetAgentsRequest,
  ):
    | Promise<GetAgentsResponse>
    | Observable<GetAgentsResponse>
    | GetAgentsResponse;

  getAgent(
    request: GetAgentRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  updateAgent(
    request: UpdateAgentRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  updateAgentToken(
    request: UpdateAgentTokenRequest,
  ):
    | Promise<UpdateAgentTokenResponse>
    | Observable<UpdateAgentTokenResponse>
    | UpdateAgentTokenResponse;

  updateLocation(
    request: UpdateLocationRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  updateFirstTrip(
    request: UpdateFirstTripRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  agentOnline(
    request: AgentOnlineRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  agentOffline(
    request: AgentOfflineRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  getActiveAgents(
    request: GetActiveAgentsRequest,
  ):
    | Promise<GetAgentsResponse>
    | Observable<GetAgentsResponse>
    | GetAgentsResponse;

  getOnlineAgents(
    request: GetOnlineAgentsRequest,
  ):
    | Promise<GetOnlineAgentsResponse>
    | Observable<GetOnlineAgentsResponse>
    | GetOnlineAgentsResponse;

  changePassword(
    request: ChangePasswordRequest,
  ):
    | Promise<ChangePasswordResponse>
    | Observable<ChangePasswordResponse>
    | ChangePasswordResponse;

  getAgentsByIds(
    request: GetAgentsByIdsRequest,
  ):
    | Promise<GetAgentsByIdsResponse>
    | Observable<GetAgentsByIdsResponse>
    | GetAgentsByIdsResponse;

  addOutstandingCod(
    request: AddOutstandingCodRequest,
  ):
    | Promise<AddOutstandingCodResponse>
    | Observable<AddOutstandingCodResponse>
    | AddOutstandingCodResponse;

  getAgentsByFilter(
    request: GetAgentsByFilterRequest,
  ):
    | Promise<GetAgentsByFilterResponse>
    | Observable<GetAgentsByFilterResponse>
    | GetAgentsByFilterResponse;

  reacceptTerms(
    request: RequestWithAgentId,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  createAgent(
    request: SignupRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  updateTierMeta(
    request: UpdateTierMetaRequest,
  ): Promise<AgentResponse> | Observable<AgentResponse> | AgentResponse;

  adjustAgentTiers(
    request: AdjustAgentTiersRequest,
  ):
    | Promise<AdjustAgentTiersResponse>
    | Observable<AdjustAgentTiersResponse>
    | AdjustAgentTiersResponse;

  calculateAgentRatings(
    request: CalculateAgentRatingsRequest,
  ):
    | Promise<CalculateAgentRatingsResponse>
    | Observable<CalculateAgentRatingsResponse>
    | CalculateAgentRatingsResponse;

  calculateFinishedTripPoints(
    request: CalculateFinishedTripPointsRequest,
  ):
    | Promise<UpdateTierMetaRequest>
    | Observable<UpdateTierMetaRequest>
    | UpdateTierMetaRequest;
}

export function AgentsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getAgents',
      'getAgent',
      'updateAgent',
      'updateAgentToken',
      'updateLocation',
      'updateFirstTrip',
      'agentOnline',
      'agentOffline',
      'getActiveAgents',
      'getOnlineAgents',
      'changePassword',
      'getAgentsByIds',
      'addOutstandingCod',
      'getAgentsByFilter',
      'reacceptTerms',
      'createAgent',
      'updateTierMeta',
      'adjustAgentTiers',
      'calculateAgentRatings',
      'calculateFinishedTripPoints',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Agents', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Agents', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const AGENTS_SERVICE_NAME = 'Agents';

export interface AgentProfilesClient {
  recordAgentAnswer(request: AgentAnswerRequest): Observable<EmptyResponse>;

  recordAgentTripHistory(
    request: AgentTripHistoryRequest,
  ): Observable<EmptyResponse>;

  getAgentProfile(
    request: AgentProfileRequest,
  ): Observable<AgentProfileResponse>;
}

export interface AgentProfilesController {
  recordAgentAnswer(
    request: AgentAnswerRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  recordAgentTripHistory(
    request: AgentTripHistoryRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getAgentProfile(
    request: AgentProfileRequest,
  ):
    | Promise<AgentProfileResponse>
    | Observable<AgentProfileResponse>
    | AgentProfileResponse;
}

export function AgentProfilesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'recordAgentAnswer',
      'recordAgentTripHistory',
      'getAgentProfile',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('AgentProfiles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('AgentProfiles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const AGENT_PROFILES_SERVICE_NAME = 'AgentProfiles';

export interface DocumentsClient {
  getAvailableDocumentTypes(
    request: GetAvailableDocumentTypesRequest,
  ): Observable<GetAvailableDocumentTypesResponse>;

  getPendingDocuments(
    request: GetPendingDocumentsRequest,
  ): Observable<GetPendingDocumentsResponse>;

  getAgentDocuments(
    request: GetAgentDocumentsRequest,
  ): Observable<GetAgentDocumentsResponse>;

  createDocument(request: CreateDocumentRequest): Observable<DocumentResponse>;

  updateDocument(request: UpdateDocumentRequest): Observable<DocumentResponse>;

  missingDocumentMail(
    request: MissingDocumentMailRequest,
  ): Observable<MissingDocumentMailResponse>;
}

export interface DocumentsController {
  getAvailableDocumentTypes(
    request: GetAvailableDocumentTypesRequest,
  ):
    | Promise<GetAvailableDocumentTypesResponse>
    | Observable<GetAvailableDocumentTypesResponse>
    | GetAvailableDocumentTypesResponse;

  getPendingDocuments(
    request: GetPendingDocumentsRequest,
  ):
    | Promise<GetPendingDocumentsResponse>
    | Observable<GetPendingDocumentsResponse>
    | GetPendingDocumentsResponse;

  getAgentDocuments(
    request: GetAgentDocumentsRequest,
  ):
    | Promise<GetAgentDocumentsResponse>
    | Observable<GetAgentDocumentsResponse>
    | GetAgentDocumentsResponse;

  createDocument(
    request: CreateDocumentRequest,
  ):
    | Promise<DocumentResponse>
    | Observable<DocumentResponse>
    | DocumentResponse;

  updateDocument(
    request: UpdateDocumentRequest,
  ):
    | Promise<DocumentResponse>
    | Observable<DocumentResponse>
    | DocumentResponse;

  missingDocumentMail(
    request: MissingDocumentMailRequest,
  ):
    | Promise<MissingDocumentMailResponse>
    | Observable<MissingDocumentMailResponse>
    | MissingDocumentMailResponse;
}

export function DocumentsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getAvailableDocumentTypes',
      'getPendingDocuments',
      'getAgentDocuments',
      'createDocument',
      'updateDocument',
      'missingDocumentMail',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Documents', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Documents', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const DOCUMENTS_SERVICE_NAME = 'Documents';

export interface AgentTypesClient {
  getAgentTypes(
    request: GetAgentTypesRequest,
  ): Observable<GetAgentTypesResponse>;

  getAgentType(request: GetAgentTypeRequest): Observable<DeliveryAgentType>;

  getAgentTier(request: GetAgentTierRequest): Observable<DeliveryAgentType>;

  createAgentType(request: DeliveryAgentType): Observable<DeliveryAgentType>;

  updateAgentType(request: DeliveryAgentType): Observable<DeliveryAgentType>;

  updateAgentTier(request: DeliveryAgentType): Observable<DeliveryAgentType>;
}

export interface AgentTypesController {
  getAgentTypes(
    request: GetAgentTypesRequest,
  ):
    | Promise<GetAgentTypesResponse>
    | Observable<GetAgentTypesResponse>
    | GetAgentTypesResponse;

  getAgentType(
    request: GetAgentTypeRequest,
  ):
    | Promise<DeliveryAgentType>
    | Observable<DeliveryAgentType>
    | DeliveryAgentType;

  getAgentTier(
    request: GetAgentTierRequest,
  ):
    | Promise<DeliveryAgentType>
    | Observable<DeliveryAgentType>
    | DeliveryAgentType;

  createAgentType(
    request: DeliveryAgentType,
  ):
    | Promise<DeliveryAgentType>
    | Observable<DeliveryAgentType>
    | DeliveryAgentType;

  updateAgentType(
    request: DeliveryAgentType,
  ):
    | Promise<DeliveryAgentType>
    | Observable<DeliveryAgentType>
    | DeliveryAgentType;

  updateAgentTier(
    request: DeliveryAgentType,
  ):
    | Promise<DeliveryAgentType>
    | Observable<DeliveryAgentType>
    | DeliveryAgentType;
}

export function AgentTypesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getAgentTypes',
      'getAgentType',
      'getAgentTier',
      'createAgentType',
      'updateAgentType',
      'updateAgentTier',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('AgentTypes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('AgentTypes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const AGENT_TYPES_SERVICE_NAME = 'AgentTypes';

export interface CrewsClient {
  getCrew(request: GetCrewRequest): Observable<GetCrewResponse>;

  getEntitiesIds(
    request: GetEntitiesIdsRequest,
  ): Observable<GetEntitiesIdsResponse>;

  getAgentByEntity(
    request: GetAgentByEntityRequest,
  ): Observable<GetAgentByEntityRespose>;

  associateAgent(request: AssociateAgentRequest): Observable<SuccessResponse>;

  disassociateAgent(
    request: DisassociateAgentRequest,
  ): Observable<SuccessResponse>;
}

export interface CrewsController {
  getCrew(
    request: GetCrewRequest,
  ): Promise<GetCrewResponse> | Observable<GetCrewResponse> | GetCrewResponse;

  getEntitiesIds(
    request: GetEntitiesIdsRequest,
  ):
    | Promise<GetEntitiesIdsResponse>
    | Observable<GetEntitiesIdsResponse>
    | GetEntitiesIdsResponse;

  getAgentByEntity(
    request: GetAgentByEntityRequest,
  ):
    | Promise<GetAgentByEntityRespose>
    | Observable<GetAgentByEntityRespose>
    | GetAgentByEntityRespose;

  associateAgent(
    request: AssociateAgentRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  disassociateAgent(
    request: DisassociateAgentRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function CrewsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getCrew',
      'getEntitiesIds',
      'getAgentByEntity',
      'associateAgent',
      'disassociateAgent',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Crews', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Crews', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CREWS_SERVICE_NAME = 'Crews';

export interface TagsClient {
  associateAgentsTags(request: AgentsTagsRequest): Observable<EmptyResponse>;

  dissociateAgentsTags(request: AgentsTagsRequest): Observable<EmptyResponse>;
}

export interface TagsController {
  associateAgentsTags(
    request: AgentsTagsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  dissociateAgentsTags(
    request: AgentsTagsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function TagsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'associateAgentsTags',
      'dissociateAgentsTags',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Tags', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Tags', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TAGS_SERVICE_NAME = 'Tags';

export interface SignupsClient {
  getRecord(request: SignupTokenRequest): Observable<SignupTokenResponse>;

  generateSignupToken(
    request: SignupTokenRequest,
  ): Observable<SignupTokenResponse>;

  verifySignupToken(request: VerifySignupRequest): Observable<SuccessResponse>;
}

export interface SignupsController {
  getRecord(
    request: SignupTokenRequest,
  ):
    | Promise<SignupTokenResponse>
    | Observable<SignupTokenResponse>
    | SignupTokenResponse;

  generateSignupToken(
    request: SignupTokenRequest,
  ):
    | Promise<SignupTokenResponse>
    | Observable<SignupTokenResponse>
    | SignupTokenResponse;

  verifySignupToken(
    request: VerifySignupRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function SignupsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getRecord',
      'generateSignupToken',
      'verifySignupToken',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Signups', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Signups', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SIGNUPS_SERVICE_NAME = 'Signups';

export interface SkillsClient {
  getSkills(request: GetSkillsRequest): Observable<GetSkillsResponse>;

  getAgentSkills(
    request: GetAgentSkillsRequest,
  ): Observable<GetAgentSkillsResponse>;

  createSkill(request: CreateSkillRequest): Observable<Skill>;

  updateSkill(request: UpdateSkillRequest): Observable<Skill>;

  associateSkill(request: AssociateSkillRequest): Observable<SuccessResponse>;

  dissociateSkill(request: DissociateSkillRequest): Observable<SuccessResponse>;

  updateAgentSkills(
    request: UpdateAgentSkillsRequest,
  ): Observable<UpdateAgentSkillsResponse>;
}

export interface SkillsController {
  getSkills(
    request: GetSkillsRequest,
  ):
    | Promise<GetSkillsResponse>
    | Observable<GetSkillsResponse>
    | GetSkillsResponse;

  getAgentSkills(
    request: GetAgentSkillsRequest,
  ):
    | Promise<GetAgentSkillsResponse>
    | Observable<GetAgentSkillsResponse>
    | GetAgentSkillsResponse;

  createSkill(
    request: CreateSkillRequest,
  ): Promise<Skill> | Observable<Skill> | Skill;

  updateSkill(
    request: UpdateSkillRequest,
  ): Promise<Skill> | Observable<Skill> | Skill;

  associateSkill(
    request: AssociateSkillRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  dissociateSkill(
    request: DissociateSkillRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  updateAgentSkills(
    request: UpdateAgentSkillsRequest,
  ):
    | Promise<UpdateAgentSkillsResponse>
    | Observable<UpdateAgentSkillsResponse>
    | UpdateAgentSkillsResponse;
}

export function SkillsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getSkills',
      'getAgentSkills',
      'createSkill',
      'updateSkill',
      'associateSkill',
      'dissociateSkill',
      'updateAgentSkills',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Skills', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Skills', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SKILLS_SERVICE_NAME = 'Skills';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
