/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'admin';

export enum AdministratorStatus {
  ACTIVE = 0,
  SUSPENDED = 1,
  UNRECOGNIZED = -1,
}

export interface CurrentUser {
  id: number;
  email: string;
  permissions: Permissions | undefined;
}

export interface Administrator {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  region: string;
  createdAt: string;
  updatedAt: string;
  permissions: Permissions | undefined;
  status: AdministratorStatus;
  metadata: Metadata | undefined;
}

export interface Permissions {
  order: string[];
  admin: string[];
  agent: string[];
  wallet: string[];
  payroll: string[];
  setting: string[];
  merchant: string[];
  address: string[];
  log: string[];
  bitbucket: string[];
  sop: string[];
  tools: string[];
  fulfillment: string[];
  warehouse: string[];
}

export interface Metadata {
  termVersion: number;
}

export const ADMIN_PACKAGE_NAME = 'admin';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
