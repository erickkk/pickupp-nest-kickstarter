/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  FCMRequest,
  FCMData,
  Recipient,
  AgentMissingDocument,
  AgentOweMoney,
  MerchantOutstandingCOD,
  ExpireOrder,
  MerchantNotification,
  PurchaseItem,
  Action,
  Link,
} from './notification.model';

export const protobufPackage = 'notification';

export interface EmptyResponse {}

export interface PushNotificationSendRequest {
  registrationIds: string[];
  notification: FCMRequest | undefined;
  data: FCMData | undefined;
}

export interface PushNotificationSendResponse {
  response: string;
}

export interface SMSSendRequest {
  to: string;
  msg: string;
}

export interface SMSSendResponse {
  response: string;
}

export interface HashRequest {
  word: string;
}

export interface HashResponse {
  hash: string;
}

export interface WhatsAppSendRequest {
  to: string;
  msg: string;
  fallback: boolean;
}

export interface WhatsAppSendResponse {
  response: string;
}

export interface WhatsAppInboundRequest {
  from: string;
  msg: string;
  timestamp: string;
}

export interface WhatsAppInboundResponse {}

export interface MailerResponse {
  status: number;
}

export interface AgentPayslipRequest {
  to: Recipient | undefined;
  locale: string;
  startDate: string;
  endDate: string;
  totalBonus: number;
  totalPenalty: number;
  totalCommission: number;
  totalAdjustment: number;
  totalOrders: number;
  totalEarnings: number;
  totalOriginal: number;
  issueDate: string;
}

export interface SendInvoiceRequest {
  to: Recipient[];
  pdfFilepath: string;
  name: string;
  startDate: string;
  endDate: string;
  cc: Recipient[];
  csvFilepath: string;
  reference: string;
}

export interface SendMonthlyInvoiceRequest {
  to: Recipient[];
  filepath: string;
  name: string;
  year: string;
  month: string;
  cc: Recipient[];
}

export interface EntityVerificationRequest {
  to: Recipient | undefined;
  callbackUrl: string;
  locale: string;
}

export interface EntityWelcomeRequest {
  to: Recipient | undefined;
  locale: string;
}

export interface ForgotPasswordRequest {
  to: Recipient | undefined;
  callbackUrl: string;
  locale: string;
}

export interface AdminVerificationRequest {
  to: Recipient | undefined;
  password: string;
}

export interface AgentActivationRequest {
  to: Recipient | undefined;
}

export interface AgentMissingDocumentRequest {
  request: AgentMissingDocument[];
}

export interface AgentOweMoneyRequest {
  request: AgentOweMoney[];
}

export interface MerchantOutstandingCODRequest {
  request: MerchantOutstandingCOD[];
}

export interface ExpireOrderRequest {
  request: ExpireOrder | undefined;
  locale: string;
}

export interface MerchantNotificationRequest {
  request: MerchantNotification[];
}

export interface CreditCardFailureMailRequest {
  to: Recipient | undefined;
  name: string;
  orderNumber: string;
}

export interface SendOauthRegisterCompleteRequest {
  to: Recipient | undefined;
  password: string;
}

export interface PurchaseOrderNotificationRequest {
  toSeller: Recipient[];
  toBuyer: Recipient[];
  items: PurchaseItem[];
  sellerName: string;
  buyerName: string;
  recipientName: string;
  deliveryDate: string;
  purchaseOrderUrl: string;
  slotFrom: string;
  slotTo: string;
  bannerImageUrl: string;
  orderNumber: string;
  purchaseOrderNumber: string;
  confirmationCode: string;
  collectionAddress: string;
}

export interface PurchaseOrderCancellationRequest {
  toSeller: Recipient[];
  toBuyer: Recipient[];
  items: PurchaseItem[];
  sellerName: string;
  buyerName: string;
  recipientName: string;
  deliveryDate: string;
  purchaseOrderReferenceCode: string;
  bannerImageUrl: string;
}

export interface ShopifyOrderNotificationRequest {
  entityId: number;
  clientReferenceNumber: string;
  orderNumber: string;
  error: string;
  shopifyOrderLink: string;
}

export interface TestimonialNotificationRequest {
  to: Recipient | undefined;
  locale: string;
}

export interface LoginNotificationRequest {
  to: Recipient | undefined;
  locale: string;
}

export interface SendRequest {
  url: string;
  data: string;
}

export interface SendResponse {
  success: boolean;
}

export interface SlackRequest {
  channelName: string;
  messageKey: string;
  payload: { [key: string]: string };
}

export interface SlackRequest_PayloadEntry {
  key: string;
  value: string;
}

export interface SlackResponse {
  success: boolean;
}

export interface IntercomRequest {
  email: string;
  payload: { [key: string]: string };
  title: string;
  message: string;
  registrationId: string;
}

export interface IntercomRequest_PayloadEntry {
  key: string;
  value: string;
}

export interface IntercomResponse {
  status: number;
}

export interface GetActionsRequest {
  limit: string;
  offset: string;
  onlyUndone: boolean;
  ids: string[];
}

export interface GetActionsResponse {
  actions: Action[];
  total: number;
}

export interface RunActionRequest {
  id: string;
}

export interface StopActionRequest {
  type: string;
  objectId: string;
}

export const NOTIFICATION_PACKAGE_NAME = 'notification';

export interface PushNotificationClient {
  send(
    request: PushNotificationSendRequest,
  ): Observable<PushNotificationSendResponse>;
}

export interface PushNotificationController {
  send(
    request: PushNotificationSendRequest,
  ):
    | Promise<PushNotificationSendResponse>
    | Observable<PushNotificationSendResponse>
    | PushNotificationSendResponse;
}

export function PushNotificationControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('PushNotification', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('PushNotification', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PUSH_NOTIFICATION_SERVICE_NAME = 'PushNotification';

export interface SMSClient {
  send(request: SMSSendRequest): Observable<SMSSendResponse>;

  shortenLink(request: Link): Observable<Link>;

  hash(request: HashRequest): Observable<HashResponse>;
}

export interface SMSController {
  send(
    request: SMSSendRequest,
  ): Promise<SMSSendResponse> | Observable<SMSSendResponse> | SMSSendResponse;

  shortenLink(request: Link): Promise<Link> | Observable<Link> | Link;

  hash(
    request: HashRequest,
  ): Promise<HashResponse> | Observable<HashResponse> | HashResponse;
}

export function SMSControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send', 'shortenLink', 'hash'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('SMS', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('SMS', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const S_MS_SERVICE_NAME = 'SMS';

export interface WhatsAppClient {
  send(request: WhatsAppSendRequest): Observable<WhatsAppSendResponse>;

  inbound(request: WhatsAppInboundRequest): Observable<WhatsAppInboundResponse>;
}

export interface WhatsAppController {
  send(
    request: WhatsAppSendRequest,
  ):
    | Promise<WhatsAppSendResponse>
    | Observable<WhatsAppSendResponse>
    | WhatsAppSendResponse;

  inbound(
    request: WhatsAppInboundRequest,
  ):
    | Promise<WhatsAppInboundResponse>
    | Observable<WhatsAppInboundResponse>
    | WhatsAppInboundResponse;
}

export function WhatsAppControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send', 'inbound'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('WhatsApp', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('WhatsApp', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const WHATS_APP_SERVICE_NAME = 'WhatsApp';

export interface MailerClient {
  entityVerification(
    request: EntityVerificationRequest,
  ): Observable<MailerResponse>;

  entityWelcome(request: EntityWelcomeRequest): Observable<MailerResponse>;

  forgotPassword(request: ForgotPasswordRequest): Observable<MailerResponse>;

  adminVerification(
    request: AdminVerificationRequest,
  ): Observable<MailerResponse>;

  agentActivation(request: AgentActivationRequest): Observable<MailerResponse>;

  agentMissingDocument(
    request: AgentMissingDocumentRequest,
  ): Observable<MailerResponse>;

  agentOweMoney(request: AgentOweMoneyRequest): Observable<MailerResponse>;

  merchantOutstandingCOD(
    request: MerchantOutstandingCODRequest,
  ): Observable<MailerResponse>;

  expireOrder(request: ExpireOrderRequest): Observable<MailerResponse>;

  merchantNotification(
    request: MerchantNotificationRequest,
  ): Observable<MailerResponse>;

  sendInvoice(request: SendInvoiceRequest): Observable<MailerResponse>;

  sendMonthlyInvoice(
    request: SendMonthlyInvoiceRequest,
  ): Observable<MailerResponse>;

  creditCardFailure(
    request: CreditCardFailureMailRequest,
  ): Observable<MailerResponse>;

  purchaseOrderNotification(
    request: PurchaseOrderNotificationRequest,
  ): Observable<MailerResponse>;

  agentPayslip(request: AgentPayslipRequest): Observable<MailerResponse>;

  purchaseOrderCancellation(
    request: PurchaseOrderCancellationRequest,
  ): Observable<MailerResponse>;

  shopifyOrderNotification(
    request: ShopifyOrderNotificationRequest,
  ): Observable<MailerResponse>;

  testimonialEcommerceNotification(
    request: TestimonialNotificationRequest,
  ): Observable<MailerResponse>;

  testimonialRetailFashionNotification(
    request: TestimonialNotificationRequest,
  ): Observable<MailerResponse>;

  trustedByTheBestNotification(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;

  whyPickuppNotification(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;

  fAQNotification(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;

  referralNotification(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;

  fulfillmentServiceNotification(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;

  review(request: LoginNotificationRequest): Observable<MailerResponse>;

  redeemFree300Reminder(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;

  redeem6000CreditReminder(
    request: LoginNotificationRequest,
  ): Observable<MailerResponse>;
}

export interface MailerController {
  entityVerification(
    request: EntityVerificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  entityWelcome(
    request: EntityWelcomeRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  forgotPassword(
    request: ForgotPasswordRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  adminVerification(
    request: AdminVerificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  agentActivation(
    request: AgentActivationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  agentMissingDocument(
    request: AgentMissingDocumentRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  agentOweMoney(
    request: AgentOweMoneyRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  merchantOutstandingCOD(
    request: MerchantOutstandingCODRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  expireOrder(
    request: ExpireOrderRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  merchantNotification(
    request: MerchantNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  sendInvoice(
    request: SendInvoiceRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  sendMonthlyInvoice(
    request: SendMonthlyInvoiceRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  creditCardFailure(
    request: CreditCardFailureMailRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  purchaseOrderNotification(
    request: PurchaseOrderNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  agentPayslip(
    request: AgentPayslipRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  purchaseOrderCancellation(
    request: PurchaseOrderCancellationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  shopifyOrderNotification(
    request: ShopifyOrderNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  testimonialEcommerceNotification(
    request: TestimonialNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  testimonialRetailFashionNotification(
    request: TestimonialNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  trustedByTheBestNotification(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  whyPickuppNotification(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  fAQNotification(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  referralNotification(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  fulfillmentServiceNotification(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  review(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  redeemFree300Reminder(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;

  redeem6000CreditReminder(
    request: LoginNotificationRequest,
  ): Promise<MailerResponse> | Observable<MailerResponse> | MailerResponse;
}

export function MailerControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'entityVerification',
      'entityWelcome',
      'forgotPassword',
      'adminVerification',
      'agentActivation',
      'agentMissingDocument',
      'agentOweMoney',
      'merchantOutstandingCOD',
      'expireOrder',
      'merchantNotification',
      'sendInvoice',
      'sendMonthlyInvoice',
      'creditCardFailure',
      'purchaseOrderNotification',
      'agentPayslip',
      'purchaseOrderCancellation',
      'shopifyOrderNotification',
      'testimonialEcommerceNotification',
      'testimonialRetailFashionNotification',
      'trustedByTheBestNotification',
      'whyPickuppNotification',
      'fAQNotification',
      'referralNotification',
      'fulfillmentServiceNotification',
      'review',
      'redeemFree300Reminder',
      'redeem6000CreditReminder',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Mailer', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Mailer', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const MAILER_SERVICE_NAME = 'Mailer';

export interface WebhookClient {
  send(request: SendRequest): Observable<SendResponse>;
}

export interface WebhookController {
  send(
    request: SendRequest,
  ): Promise<SendResponse> | Observable<SendResponse> | SendResponse;
}

export function WebhookControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Webhook', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Webhook', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const WEBHOOK_SERVICE_NAME = 'Webhook';

export interface SlackClient {
  send(request: SlackRequest): Observable<SlackResponse>;
}

export interface SlackController {
  send(
    request: SlackRequest,
  ): Promise<SlackResponse> | Observable<SlackResponse> | SlackResponse;
}

export function SlackControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Slack', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Slack', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SLACK_SERVICE_NAME = 'Slack';

export interface IntercomClient {
  send(request: IntercomRequest): Observable<IntercomResponse>;
}

export interface IntercomController {
  send(
    request: IntercomRequest,
  ):
    | Promise<IntercomResponse>
    | Observable<IntercomResponse>
    | IntercomResponse;
}

export function IntercomControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Intercom', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Intercom', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const INTERCOM_SERVICE_NAME = 'Intercom';

export interface ActionsClient {
  createAction(request: Action): Observable<Action>;

  getActions(request: GetActionsRequest): Observable<GetActionsResponse>;

  runAction(request: RunActionRequest): Observable<EmptyResponse>;

  stopAction(request: StopActionRequest): Observable<EmptyResponse>;
}

export interface ActionsController {
  createAction(request: Action): Promise<Action> | Observable<Action> | Action;

  getActions(
    request: GetActionsRequest,
  ):
    | Promise<GetActionsResponse>
    | Observable<GetActionsResponse>
    | GetActionsResponse;

  runAction(
    request: RunActionRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  stopAction(
    request: StopActionRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function ActionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createAction',
      'getActions',
      'runAction',
      'stopAction',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Actions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Actions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ACTIONS_SERVICE_NAME = 'Actions';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
