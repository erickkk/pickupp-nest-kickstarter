/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import {
  PropertyKey,
  DistanceType,
  GeofenceModel,
  Region,
  Geometry,
  District,
  PropertyQuery,
} from './address.model';
import { Observable } from 'rxjs';

export const protobufPackage = 'address';

export interface EmptyResponse {}

export interface GetGeofencesRequest {
  entityId: number;
  serviceType: string;
}

export interface GetGeofencesResponse {
  geofences: GeofenceModel[];
  total: number;
}

export interface CreateGeofenceRequest {
  entityId: number;
  serviceType: string;
  serviceTime: number;
  name: string;
  url: string;
}

export interface UpdateGeofenceRequest {
  id: string;
  url: string;
}

export interface RemoveGeofenceRequest {
  id: string;
}

export interface GetRegionRequest {
  lat: string;
  lng: string;
  name: PropertyKey;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  noCascade: boolean;
}

export interface GetRegionResponse {
  region: Region | undefined;
}

export interface GetRegionsRequest {
  lat: string;
  lng: string;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  noCascade: boolean;
}

export interface GetRegionsResponse {
  regions: Region[];
}

export interface GetWallSurchargeRequest {
  from: Geometry | undefined;
  to: Geometry | undefined;
  entityId: number;
  serviceType: string;
  serviceTime: number;
  noCascade: boolean;
}

export interface GetWallSurchargeResponse {
  value: string;
}

export interface GetDistrictResponse {
  district: District | undefined;
}

export interface GetDistrictsRequest {
  ids: string[];
  queries: PropertyQuery[];
  serviceType: string;
  serviceTime: number;
  entityId: number;
  limit: number;
  offset: number;
  propertyKeys: PropertyKey[];
  fallback: boolean;
}

export interface GetDistrictsResponse {
  districts: District[];
  total: number;
}

export interface CreateDistrictsRequest {
  url: string;
  serviceType?: string | undefined;
  serviceTime?: number | undefined;
  entityId?: number | undefined;
  city: string;
}

export interface UpsertPropertiesRequest {
  url: string;
  serviceType: string;
  serviceTime: number;
  entityId: number;
}

export interface DeletePropertiesRequest {
  ids: string[];
  serviceType?: string | undefined;
  serviceTime?: number | undefined;
  entityId?: number | undefined;
  queries: PropertyQuery[];
}

export interface TotalResponse {
  total: number;
}

export interface GetDistanceRequest {
  from: Geometry | undefined;
  to: Geometry[];
  region?: string | undefined;
  type: DistanceType;
}

export interface GetDistanceResponse {
  distance: number;
  unit: string;
}

export const ADDRESS_PACKAGE_NAME = 'address';

export interface GeofenceClient {
  getGeofences(request: GetGeofencesRequest): Observable<GetGeofencesResponse>;

  createGeofence(request: CreateGeofenceRequest): Observable<GeofenceModel>;

  updateGeofence(request: UpdateGeofenceRequest): Observable<GeofenceModel>;

  removeGeofence(request: RemoveGeofenceRequest): Observable<EmptyResponse>;

  getRegion(request: GetRegionRequest): Observable<GetRegionResponse>;

  getRegions(request: GetRegionsRequest): Observable<GetRegionsResponse>;

  getWallSurcharge(
    request: GetWallSurchargeRequest,
  ): Observable<GetWallSurchargeResponse>;
}

export interface GeofenceController {
  getGeofences(
    request: GetGeofencesRequest,
  ):
    | Promise<GetGeofencesResponse>
    | Observable<GetGeofencesResponse>
    | GetGeofencesResponse;

  createGeofence(
    request: CreateGeofenceRequest,
  ): Promise<GeofenceModel> | Observable<GeofenceModel> | GeofenceModel;

  updateGeofence(
    request: UpdateGeofenceRequest,
  ): Promise<GeofenceModel> | Observable<GeofenceModel> | GeofenceModel;

  removeGeofence(
    request: RemoveGeofenceRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getRegion(
    request: GetRegionRequest,
  ):
    | Promise<GetRegionResponse>
    | Observable<GetRegionResponse>
    | GetRegionResponse;

  getRegions(
    request: GetRegionsRequest,
  ):
    | Promise<GetRegionsResponse>
    | Observable<GetRegionsResponse>
    | GetRegionsResponse;

  getWallSurcharge(
    request: GetWallSurchargeRequest,
  ):
    | Promise<GetWallSurchargeResponse>
    | Observable<GetWallSurchargeResponse>
    | GetWallSurchargeResponse;
}

export function GeofenceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getGeofences',
      'createGeofence',
      'updateGeofence',
      'removeGeofence',
      'getRegion',
      'getRegions',
      'getWallSurcharge',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Geofence', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Geofence', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const GEOFENCE_SERVICE_NAME = 'Geofence';

export interface DistrictsClient {
  getDistrict(request: GetRegionRequest): Observable<GetDistrictResponse>;

  getWallDistricts(
    request: GetWallSurchargeRequest,
  ): Observable<GetDistrictsResponse>;

  getDistricts(request: GetDistrictsRequest): Observable<GetDistrictsResponse>;

  createDistricts(request: CreateDistrictsRequest): Observable<TotalResponse>;
}

export interface DistrictsController {
  getDistrict(
    request: GetRegionRequest,
  ):
    | Promise<GetDistrictResponse>
    | Observable<GetDistrictResponse>
    | GetDistrictResponse;

  getWallDistricts(
    request: GetWallSurchargeRequest,
  ):
    | Promise<GetDistrictsResponse>
    | Observable<GetDistrictsResponse>
    | GetDistrictsResponse;

  getDistricts(
    request: GetDistrictsRequest,
  ):
    | Promise<GetDistrictsResponse>
    | Observable<GetDistrictsResponse>
    | GetDistrictsResponse;

  createDistricts(
    request: CreateDistrictsRequest,
  ): Promise<TotalResponse> | Observable<TotalResponse> | TotalResponse;
}

export function DistrictsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getDistrict',
      'getWallDistricts',
      'getDistricts',
      'createDistricts',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Districts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Districts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const DISTRICTS_SERVICE_NAME = 'Districts';

export interface PropertiesClient {
  upsertProperties(request: UpsertPropertiesRequest): Observable<TotalResponse>;

  deleteProperties(request: DeletePropertiesRequest): Observable<TotalResponse>;
}

export interface PropertiesController {
  upsertProperties(
    request: UpsertPropertiesRequest,
  ): Promise<TotalResponse> | Observable<TotalResponse> | TotalResponse;

  deleteProperties(
    request: DeletePropertiesRequest,
  ): Promise<TotalResponse> | Observable<TotalResponse> | TotalResponse;
}

export function PropertiesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['upsertProperties', 'deleteProperties'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Properties', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Properties', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PROPERTIES_SERVICE_NAME = 'Properties';

export interface RouteClient {
  getDistance(request: GetDistanceRequest): Observable<GetDistanceResponse>;
}

export interface RouteController {
  getDistance(
    request: GetDistanceRequest,
  ):
    | Promise<GetDistanceResponse>
    | Observable<GetDistanceResponse>
    | GetDistanceResponse;
}

export function RouteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getDistance'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Route', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Route', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ROUTE_SERVICE_NAME = 'Route';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
