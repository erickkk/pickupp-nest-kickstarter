/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'cronjob';

export interface Task {
  id: string;
  name: string;
  crontab: string;
  isEnabled: boolean;
  isUserDefined: boolean;
  createdAt: string;
  updatedAt: string;
  retry: number;
}

export interface NatsTask {
  id: string;
  name: string;
  crontab: string;
  natsEvent: string;
  isEnabled: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface UpdateTask {
  crontab: string;
  isEnabled: boolean;
  isUserDefined: boolean;
}

export const CRONJOB_PACKAGE_NAME = 'cronjob';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
