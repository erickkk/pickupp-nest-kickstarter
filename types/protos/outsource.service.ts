/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Partner } from './merchant.model';
import { Observable } from 'rxjs';
import { Order, Store } from './outsource.model';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';

export const protobufPackage = 'outsource';

export interface GetOrderRequest {
  id: string;
  partner: Partner;
}

export interface GetOrdersRequest {
  orderIds: string[];
  partner: Partner;
}

export interface GetOrdersResponse {
  orders: Order[];
}

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface UpdateTripRequest {
  trackingNumber: string;
  event: string;
  partner: Partner;
  payload: string;
}

export interface UpdateTripResponse {
  success: boolean;
}

export interface GetSelfPickStoreRequest {
  lat?: string | undefined;
  lng?: string | undefined;
  search?: string | undefined;
  limit?: string | undefined;
  cursor: string;
}

export interface GetSelfPickStoreResponse {
  stores: Store[];
  cursor?: string | undefined;
}

export const OUTSOURCE_PACKAGE_NAME = 'outsource';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<Order>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getOrder', 'getOrders'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

export interface QuotesClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuotesController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuotesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quotes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quotes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTES_SERVICE_NAME = 'Quotes';

export interface TripsClient {
  updateTrip(request: UpdateTripRequest): Observable<UpdateTripResponse>;
}

export interface TripsController {
  updateTrip(
    request: UpdateTripRequest,
  ):
    | Promise<UpdateTripResponse>
    | Observable<UpdateTripResponse>
    | UpdateTripResponse;
}

export function TripsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['updateTrip'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Trips', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Trips', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRIPS_SERVICE_NAME = 'Trips';

export interface StoresClient {
  getSelfPickStore(
    request: GetSelfPickStoreRequest,
  ): Observable<GetSelfPickStoreResponse>;
}

export interface StoresController {
  getSelfPickStore(
    request: GetSelfPickStoreRequest,
  ):
    | Promise<GetSelfPickStoreResponse>
    | Observable<GetSelfPickStoreResponse>
    | GetSelfPickStoreResponse;
}

export function StoresControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getSelfPickStore'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Stores', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Stores', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const STORES_SERVICE_NAME = 'Stores';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
