/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'royale';

export interface Store {
  pudoId: string;
  pudoIdInDoc: string;
  pudoLogoUrl: string;
  locationId: string;
  locationIdInDoc: string;
  lastModifiedAt: string;
  address1: string;
  address2: string;
  address3: string;
  address4: string;
  localizedAddress: string;
  tel: string;
  effectiveDateFrom: string;
  effectiveDateTo: string;
  latitude: string;
  longitude: string;
  maxCapacity: number;
  availableCapacity: number;
  availableDateTime: string;
  openingHours: Store_OpeningHours | undefined;
  additionalLocationInfo: Store_AdditionalLocationInfo | undefined;
}

export interface Store_OpeningHour {
  weekday: string;
  opening: string;
  closing: string;
}

export interface Store_OpeningHours {
  description: string;
  details: Store_OpeningHour[];
}

export interface Store_AdditionalLocationInfo {
  region: string;
  district: string;
  localizedRegion: string;
  localizedDistrict: string;
}

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  content?: Order_ResponseContent | undefined;
  status: string;
  errorMessage?: Order_Error | undefined;
  createdAt: string;
  updatedAt: string;
  trackResponse: OrderTrack | undefined;
}

export interface Order_ResponseContent {
  merchantId: string;
  merchantRef: string;
  waybillNo: string;
  status: string;
}

export interface Order_Error {
  errorCode: string;
  errorMsg: string;
}

export interface TrackingEvent {
  eventTime: string;
  description: string;
  statusName: string;
  statusID: string;
}

export interface OrderTrack {
  status: string;
  trackingList: TrackingEvent[];
}

export const ROYALE_PACKAGE_NAME = 'royale';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
