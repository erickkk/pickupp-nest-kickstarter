/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { Order } from './order.model';
import { DeliveryAgentType } from './agent.model';
import {
  Payroll,
  Transaction,
  TotalPayrolls,
  Incentive,
  BankIn,
  CashOnDelivery,
  CashOut,
} from './payroll.model';

export const protobufPackage = 'payroll';

export interface CreatePayrollRequest {
  order: Order | undefined;
  agentType: DeliveryAgentType | undefined;
}

export interface CreatePayrollResponse {
  payrolls: Payroll[];
}

export interface CreatePayrollByAdminRequest {
  deliveryAgentId: number;
  bonus: string;
  penalty: string;
  description: string;
  commission: string;
  adjustment: string;
  payrollTime: string;
  orderId: string;
  tripId: string;
  orderPrice: string;
}

export interface CreatePayrollByAdminResponse {
  payroll: Payroll | undefined;
}

export interface ConfirmPayrollByAdminResponse {
  walletTransactions: Transaction[];
}

export interface CreatePayrollByOrderRequest {
  order: CreatePayrollByOrderRequest_OrderParams | undefined;
  hasLatePenalty: boolean;
}

export interface CreatePayrollByOrderRequest_OrderParams {
  orderId: string;
  tripId: string;
  deliveryAgentId: number;
  orderPrice: string;
  tripPrice: string;
  pickupOntime: boolean;
  dropoffOntime: boolean;
  isWarehouse: boolean;
  description: string;
  acceptMethod: string;
}

export interface GetPayrollsByOrderRequest {
  orderId: string;
}

export interface GetPayrollsByOrderResponse {
  payrolls: Payroll[];
  total: number;
}

export interface GetPayrollsByAgentRequest {
  deliveryAgentId: number;
  startDate: string;
  endDate: string;
  acceptMethods: string[];
  statuses: string[];
  offset: number;
  limit: number;
}

export interface GetPayrollsByAgentResponse {
  payrolls: Payroll[];
  total: number;
}

export interface GetPayrollsRequest {
  startDate: string;
  endDate: string;
  status: string;
  offset: number;
  limit: number;
}

export interface GetPayrollSummaryResponse {
  payrolls: TotalPayrolls[];
  total: number;
}

export interface UpdatePayrollRequest {
  id: string;
  bonus: string;
  penalty: string;
  description: string;
  pickupOntime: boolean;
  dropoffOntime: boolean;
  commission: string;
  isWarehouse: boolean;
  adjustment: string;
  acceptMethod: string;
  payrollTime: string;
  status: string;
  confirmReason: string;
}

export interface UpdatePayrollResponse {
  payroll: Payroll | undefined;
}

export interface ConfirmPayrollRequest {
  orderNumber: string;
  skipValidation: boolean;
}

export interface SendPayslipEmailRequest {
  id: string;
}

export interface ConfirmPayrollByAdminRequest {
  adminId: number;
  fromDate: string;
  toDate: string;
  deliveryAgentIds: number[];
  sendEmail: boolean;
}

export interface ConfirmPayrollResponse {
  payrolls: Payroll[];
}

export interface UpdateIncentiveRequest {
  id: string;
  incentive: Incentive | undefined;
}

export interface GetPendingAgentIdsRequest {
  startDate: string;
  endDate: string;
}

export interface GetPendingAgentIdsResponse {
  deliveryAgentIds: number[];
}

export interface GetTransactionalRequest {
  deliveryAgentId: number;
  fromDate: string;
  toDate: string;
  statuses: string[];
  types: string[];
  limit: number;
  offset: number;
  sort: string;
}

export interface TransactionsResponse {
  transactions: Transaction[];
  total: number;
}

export interface GetBalanceRequest {
  deliveryAgentId: number;
}

export interface GetBulkBalanceRequest {
  deliveryAgentIds: number[];
}

export interface GetBulkBalanceResponse {
  balances: { [key: number]: number };
}

export interface GetBulkBalanceResponse_BalancesEntry {
  key: number;
  value: number;
}

export interface GetBalanceResponse {
  balance: number;
}

export interface BankInsResponse {
  bankIns: BankIn[];
  total: number;
}

export interface ConfirmBankInRequest {
  bankInIds: string[];
  adminId: number;
}

export interface RejectCashOnDeliveryRequest {
  ids: string[];
  adminId: number;
}

export interface UpdateBankInsRequest {
  bankInIds: string[];
  status: string;
  adminId: number;
}

export interface UpdateBankInsResponse {
  bankIns: BankIn[];
}

export interface GetCashOnDeliveryRequest {
  id: string;
}

export interface GetCashOnDeliveryResponse {
  cashOnDelivery: CashOnDelivery | undefined;
}

export interface GetCashOnDeliveriesRequest {
  status: string;
  limit: number;
  offset: number;
  sort: string;
  query: string;
  startTime: string;
  endTime: string;
}

export interface GetCashOnDeliveriesResponse {
  cashOnDeliveries: CashOnDelivery[];
  total: number;
}

export interface GetCashOnDeliveriesByAgentRequest {
  deliveryAgentId: number;
  startDate: string;
  endDate: string;
}

export interface GetCashOnDeliveriesByAgentResponse {
  cashOnDeliveries: CashOnDelivery[];
  total: number;
}

export interface GetCashOnDeliveriesByOrderRequest {
  orderId: string;
}

export interface GetCashOnDeliveriesByOrderResponse {
  cashOnDeliveries: CashOnDelivery[];
  total: number;
}

export interface GetCashOnDeliveriesByEntityRequest {
  entityId: number;
  fromDate: string;
  toDate: string;
  query: string;
  orderIds: string[];
  limit: number;
  offset: number;
}

export interface GetCashOnDeliveriesByEntityResponse {
  cashOnDeliveries: CashOnDelivery[];
  total: number;
}

export interface UpdateCashOnDeliveryRequest {
  id: string;
  cashOnDelivery: string;
  status: string;
  adminId: number;
}

export interface UpdateCashOnDeliveryResponse {
  cashOnDelivery: CashOnDelivery | undefined;
}

export interface CreateCashOnDeliveryRequest {
  order: Order | undefined;
}

export interface CreateCashOnDeliveryResponse {
  cashOnDelivery: CashOnDelivery | undefined;
}

export interface CreateCashOnDeliveryByOrderRequest {
  order: CreateCashOnDeliveryByOrderRequest_OrderParams | undefined;
}

export interface CreateCashOnDeliveryByOrderRequest_OrderParams {
  orderId: string;
  tripId: string;
  entityId: number;
  deliveryAgentId: number;
  cashOnDelivery: number;
  clientReferenceNumber: string;
}

export interface BulkUpdateCashOnDeliveriesRequest {
  ids: string[];
  status: string;
  adminId: number;
}

export interface BulkUpdateCashOnDeliveriesResponse {
  cashOnDeliveries: CashOnDelivery[];
  total: number;
}

export interface AccountResponse {
  id: string;
  bankName: string;
  bankAccount: string;
  accountName: string;
  isActive: boolean;
  updatedAt: string;
  createdAt: string;
  deliveryAgentId: number;
}

export interface CreateAccountRequest {
  deliveryAgentId: number;
  bankName: string;
  bankAccount: string;
  accountName: string;
}

export interface CreateAccountResponse {
  account: AccountResponse | undefined;
}

export interface GetAccountsRequest {
  deliveryAgentIds: number[];
  isActive: boolean;
}

export interface GetAccountsResponse {
  accounts: AccountResponse[];
  total: number;
}

export interface GetCashOutsResponse {
  cashOuts: CashOut[];
  total: number;
}

export interface CashOutRequest {
  deliveryAgentId: number;
  amount: number;
  bankName: string;
  bankAccount: string;
  accountName: string;
  adminId: number;
  description: string;
}

export interface BounceCashOutRequest {
  cashOutIds: string[];
  adminId: number;
}

export interface ConfirmTransactionsPayroll {
  deliveryAgentId: string;
  fromDate: string;
  toDate: string;
  total: string;
  bankName: string;
  bankAccount: string;
  accountName: string;
}

export interface ConfirmCashOutsRequest {
  cashOutIds: string[];
  adminId: number;
}

export interface BasicResponse {
  success: boolean;
}

export interface UpdateCashOutsRequest {
  cashOutIds: string[];
  status: string;
  adminId: number;
}

export const PAYROLL_PACKAGE_NAME = 'payroll';

export interface PayrollsClient {
  createPayroll(
    request: CreatePayrollRequest,
  ): Observable<CreatePayrollResponse>;

  createPayrollByAdmin(
    request: CreatePayrollByAdminRequest,
  ): Observable<CreatePayrollByAdminResponse>;

  createPayrollByOrder(
    request: CreatePayrollByOrderRequest,
  ): Observable<CreatePayrollByAdminResponse>;

  getPayrollsByOrder(
    request: GetPayrollsByOrderRequest,
  ): Observable<GetPayrollsByOrderResponse>;

  getPayrollsByAgent(
    request: GetPayrollsByAgentRequest,
  ): Observable<GetPayrollsByAgentResponse>;

  getPayrolls(request: GetPayrollsRequest): Observable<Payroll>;

  getPayrollSummary(
    request: GetPayrollsRequest,
  ): Observable<GetPayrollSummaryResponse>;

  updatePayroll(
    request: UpdatePayrollRequest,
  ): Observable<UpdatePayrollResponse>;

  confirmPayroll(
    request: ConfirmPayrollRequest,
  ): Observable<ConfirmPayrollResponse>;

  confirmPayrollByAdmin(
    request: ConfirmPayrollByAdminRequest,
  ): Observable<ConfirmPayrollByAdminResponse>;

  updateIncentive(request: UpdateIncentiveRequest): Observable<BasicResponse>;

  getPendingAgentIds(
    request: GetPendingAgentIdsRequest,
  ): Observable<GetPendingAgentIdsResponse>;

  getPayroll(request: GetCashOnDeliveryRequest): Observable<Payroll>;

  sendPayslipEmail(request: SendPayslipEmailRequest): Observable<BasicResponse>;
}

export interface PayrollsController {
  createPayroll(
    request: CreatePayrollRequest,
  ):
    | Promise<CreatePayrollResponse>
    | Observable<CreatePayrollResponse>
    | CreatePayrollResponse;

  createPayrollByAdmin(
    request: CreatePayrollByAdminRequest,
  ):
    | Promise<CreatePayrollByAdminResponse>
    | Observable<CreatePayrollByAdminResponse>
    | CreatePayrollByAdminResponse;

  createPayrollByOrder(
    request: CreatePayrollByOrderRequest,
  ):
    | Promise<CreatePayrollByAdminResponse>
    | Observable<CreatePayrollByAdminResponse>
    | CreatePayrollByAdminResponse;

  getPayrollsByOrder(
    request: GetPayrollsByOrderRequest,
  ):
    | Promise<GetPayrollsByOrderResponse>
    | Observable<GetPayrollsByOrderResponse>
    | GetPayrollsByOrderResponse;

  getPayrollsByAgent(
    request: GetPayrollsByAgentRequest,
  ):
    | Promise<GetPayrollsByAgentResponse>
    | Observable<GetPayrollsByAgentResponse>
    | GetPayrollsByAgentResponse;

  getPayrolls(request: GetPayrollsRequest): Observable<Payroll>;

  getPayrollSummary(
    request: GetPayrollsRequest,
  ):
    | Promise<GetPayrollSummaryResponse>
    | Observable<GetPayrollSummaryResponse>
    | GetPayrollSummaryResponse;

  updatePayroll(
    request: UpdatePayrollRequest,
  ):
    | Promise<UpdatePayrollResponse>
    | Observable<UpdatePayrollResponse>
    | UpdatePayrollResponse;

  confirmPayroll(
    request: ConfirmPayrollRequest,
  ):
    | Promise<ConfirmPayrollResponse>
    | Observable<ConfirmPayrollResponse>
    | ConfirmPayrollResponse;

  confirmPayrollByAdmin(
    request: ConfirmPayrollByAdminRequest,
  ):
    | Promise<ConfirmPayrollByAdminResponse>
    | Observable<ConfirmPayrollByAdminResponse>
    | ConfirmPayrollByAdminResponse;

  updateIncentive(
    request: UpdateIncentiveRequest,
  ): Promise<BasicResponse> | Observable<BasicResponse> | BasicResponse;

  getPendingAgentIds(
    request: GetPendingAgentIdsRequest,
  ):
    | Promise<GetPendingAgentIdsResponse>
    | Observable<GetPendingAgentIdsResponse>
    | GetPendingAgentIdsResponse;

  getPayroll(
    request: GetCashOnDeliveryRequest,
  ): Promise<Payroll> | Observable<Payroll> | Payroll;

  sendPayslipEmail(
    request: SendPayslipEmailRequest,
  ): Promise<BasicResponse> | Observable<BasicResponse> | BasicResponse;
}

export function PayrollsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createPayroll',
      'createPayrollByAdmin',
      'createPayrollByOrder',
      'getPayrollsByOrder',
      'getPayrollsByAgent',
      'getPayrolls',
      'getPayrollSummary',
      'updatePayroll',
      'confirmPayroll',
      'confirmPayrollByAdmin',
      'updateIncentive',
      'getPendingAgentIds',
      'getPayroll',
      'sendPayslipEmail',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Payrolls', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Payrolls', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PAYROLLS_SERVICE_NAME = 'Payrolls';

export interface TransactionsClient {
  getTransactions(
    request: GetTransactionalRequest,
  ): Observable<TransactionsResponse>;

  getBalanceByAgent(request: GetBalanceRequest): Observable<GetBalanceResponse>;

  getBulkBalanceByAgent(
    request: GetBulkBalanceRequest,
  ): Observable<GetBulkBalanceResponse>;
}

export interface TransactionsController {
  getTransactions(
    request: GetTransactionalRequest,
  ):
    | Promise<TransactionsResponse>
    | Observable<TransactionsResponse>
    | TransactionsResponse;

  getBalanceByAgent(
    request: GetBalanceRequest,
  ):
    | Promise<GetBalanceResponse>
    | Observable<GetBalanceResponse>
    | GetBalanceResponse;

  getBulkBalanceByAgent(
    request: GetBulkBalanceRequest,
  ):
    | Promise<GetBulkBalanceResponse>
    | Observable<GetBulkBalanceResponse>
    | GetBulkBalanceResponse;
}

export function TransactionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getTransactions',
      'getBalanceByAgent',
      'getBulkBalanceByAgent',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Transactions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Transactions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRANSACTIONS_SERVICE_NAME = 'Transactions';

export interface BankInsClient {
  getBankIns(request: GetTransactionalRequest): Observable<BankInsResponse>;

  createBankIns(request: BankIn): Observable<BankIn>;

  confirmBankIns(request: ConfirmBankInRequest): Observable<BankInsResponse>;

  updateBankIn(request: BankIn): Observable<BankIn>;

  updateBankIns(
    request: UpdateBankInsRequest,
  ): Observable<UpdateBankInsResponse>;
}

export interface BankInsController {
  getBankIns(
    request: GetTransactionalRequest,
  ): Promise<BankInsResponse> | Observable<BankInsResponse> | BankInsResponse;

  createBankIns(request: BankIn): Promise<BankIn> | Observable<BankIn> | BankIn;

  confirmBankIns(
    request: ConfirmBankInRequest,
  ): Promise<BankInsResponse> | Observable<BankInsResponse> | BankInsResponse;

  updateBankIn(request: BankIn): Promise<BankIn> | Observable<BankIn> | BankIn;

  updateBankIns(
    request: UpdateBankInsRequest,
  ):
    | Promise<UpdateBankInsResponse>
    | Observable<UpdateBankInsResponse>
    | UpdateBankInsResponse;
}

export function BankInsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getBankIns',
      'createBankIns',
      'confirmBankIns',
      'updateBankIn',
      'updateBankIns',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('BankIns', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('BankIns', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const BANK_INS_SERVICE_NAME = 'BankIns';

export interface CashOnDeliveriesClient {
  getCashOnDelivery(
    request: GetCashOnDeliveryRequest,
  ): Observable<GetCashOnDeliveryResponse>;

  getCashOnDeliveries(
    request: GetCashOnDeliveriesRequest,
  ): Observable<GetCashOnDeliveriesResponse>;

  getCashOnDeliveriesByAgent(
    request: GetCashOnDeliveriesByAgentRequest,
  ): Observable<GetCashOnDeliveriesByAgentResponse>;

  getCashOnDeliveriesByOrder(
    request: GetCashOnDeliveriesByOrderRequest,
  ): Observable<GetCashOnDeliveriesByOrderResponse>;

  getCashOnDeliveriesByEntity(
    request: GetCashOnDeliveriesByEntityRequest,
  ): Observable<GetCashOnDeliveriesByEntityResponse>;

  updateCashOnDelivery(
    request: UpdateCashOnDeliveryRequest,
  ): Observable<UpdateCashOnDeliveryResponse>;

  createCashOnDelivery(
    request: CreateCashOnDeliveryRequest,
  ): Observable<CreateCashOnDeliveryResponse>;

  createCashOnDeliveryByOrder(
    request: CreateCashOnDeliveryByOrderRequest,
  ): Observable<CreateCashOnDeliveryResponse>;

  rejectCashOnDelivery(
    request: RejectCashOnDeliveryRequest,
  ): Observable<GetCashOnDeliveriesResponse>;

  bulkUpdateCashOnDeliveries(
    request: BulkUpdateCashOnDeliveriesRequest,
  ): Observable<BulkUpdateCashOnDeliveriesResponse>;
}

export interface CashOnDeliveriesController {
  getCashOnDelivery(
    request: GetCashOnDeliveryRequest,
  ):
    | Promise<GetCashOnDeliveryResponse>
    | Observable<GetCashOnDeliveryResponse>
    | GetCashOnDeliveryResponse;

  getCashOnDeliveries(
    request: GetCashOnDeliveriesRequest,
  ):
    | Promise<GetCashOnDeliveriesResponse>
    | Observable<GetCashOnDeliveriesResponse>
    | GetCashOnDeliveriesResponse;

  getCashOnDeliveriesByAgent(
    request: GetCashOnDeliveriesByAgentRequest,
  ):
    | Promise<GetCashOnDeliveriesByAgentResponse>
    | Observable<GetCashOnDeliveriesByAgentResponse>
    | GetCashOnDeliveriesByAgentResponse;

  getCashOnDeliveriesByOrder(
    request: GetCashOnDeliveriesByOrderRequest,
  ):
    | Promise<GetCashOnDeliveriesByOrderResponse>
    | Observable<GetCashOnDeliveriesByOrderResponse>
    | GetCashOnDeliveriesByOrderResponse;

  getCashOnDeliveriesByEntity(
    request: GetCashOnDeliveriesByEntityRequest,
  ):
    | Promise<GetCashOnDeliveriesByEntityResponse>
    | Observable<GetCashOnDeliveriesByEntityResponse>
    | GetCashOnDeliveriesByEntityResponse;

  updateCashOnDelivery(
    request: UpdateCashOnDeliveryRequest,
  ):
    | Promise<UpdateCashOnDeliveryResponse>
    | Observable<UpdateCashOnDeliveryResponse>
    | UpdateCashOnDeliveryResponse;

  createCashOnDelivery(
    request: CreateCashOnDeliveryRequest,
  ):
    | Promise<CreateCashOnDeliveryResponse>
    | Observable<CreateCashOnDeliveryResponse>
    | CreateCashOnDeliveryResponse;

  createCashOnDeliveryByOrder(
    request: CreateCashOnDeliveryByOrderRequest,
  ):
    | Promise<CreateCashOnDeliveryResponse>
    | Observable<CreateCashOnDeliveryResponse>
    | CreateCashOnDeliveryResponse;

  rejectCashOnDelivery(
    request: RejectCashOnDeliveryRequest,
  ):
    | Promise<GetCashOnDeliveriesResponse>
    | Observable<GetCashOnDeliveriesResponse>
    | GetCashOnDeliveriesResponse;

  bulkUpdateCashOnDeliveries(
    request: BulkUpdateCashOnDeliveriesRequest,
  ):
    | Promise<BulkUpdateCashOnDeliveriesResponse>
    | Observable<BulkUpdateCashOnDeliveriesResponse>
    | BulkUpdateCashOnDeliveriesResponse;
}

export function CashOnDeliveriesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getCashOnDelivery',
      'getCashOnDeliveries',
      'getCashOnDeliveriesByAgent',
      'getCashOnDeliveriesByOrder',
      'getCashOnDeliveriesByEntity',
      'updateCashOnDelivery',
      'createCashOnDelivery',
      'createCashOnDeliveryByOrder',
      'rejectCashOnDelivery',
      'bulkUpdateCashOnDeliveries',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('CashOnDeliveries', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('CashOnDeliveries', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CASH_ON_DELIVERIES_SERVICE_NAME = 'CashOnDeliveries';

export interface AccountsClient {
  getAccounts(request: GetAccountsRequest): Observable<GetAccountsResponse>;

  createAccount(
    request: CreateAccountRequest,
  ): Observable<CreateAccountResponse>;
}

export interface AccountsController {
  getAccounts(
    request: GetAccountsRequest,
  ):
    | Promise<GetAccountsResponse>
    | Observable<GetAccountsResponse>
    | GetAccountsResponse;

  createAccount(
    request: CreateAccountRequest,
  ):
    | Promise<CreateAccountResponse>
    | Observable<CreateAccountResponse>
    | CreateAccountResponse;
}

export function AccountsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getAccounts', 'createAccount'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Accounts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Accounts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ACCOUNTS_SERVICE_NAME = 'Accounts';

export interface CashOutsClient {
  getCashOuts(
    request: GetTransactionalRequest,
  ): Observable<GetCashOutsResponse>;

  confirmCashOuts(
    request: ConfirmCashOutsRequest,
  ): Observable<GetCashOutsResponse>;

  updateCashOuts(
    request: UpdateCashOutsRequest,
  ): Observable<GetCashOutsResponse>;

  createCashOutRequest(request: CashOutRequest): Observable<CashOut>;

  bounceCashOut(request: BounceCashOutRequest): Observable<GetCashOutsResponse>;
}

export interface CashOutsController {
  getCashOuts(
    request: GetTransactionalRequest,
  ):
    | Promise<GetCashOutsResponse>
    | Observable<GetCashOutsResponse>
    | GetCashOutsResponse;

  confirmCashOuts(
    request: ConfirmCashOutsRequest,
  ):
    | Promise<GetCashOutsResponse>
    | Observable<GetCashOutsResponse>
    | GetCashOutsResponse;

  updateCashOuts(
    request: UpdateCashOutsRequest,
  ):
    | Promise<GetCashOutsResponse>
    | Observable<GetCashOutsResponse>
    | GetCashOutsResponse;

  createCashOutRequest(
    request: CashOutRequest,
  ): Promise<CashOut> | Observable<CashOut> | CashOut;

  bounceCashOut(
    request: BounceCashOutRequest,
  ):
    | Promise<GetCashOutsResponse>
    | Observable<GetCashOutsResponse>
    | GetCashOutsResponse;
}

export function CashOutsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getCashOuts',
      'confirmCashOuts',
      'updateCashOuts',
      'createCashOutRequest',
      'bounceCashOut',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('CashOuts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('CashOuts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CASH_OUTS_SERVICE_NAME = 'CashOuts';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
