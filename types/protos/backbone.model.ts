/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'backbone';

export enum TagEnum {
  warehouse = 0,
  fulfillment = 1,
  self_pickup = 2,
  outsource = 3,
  cross_border = 4,
  UNRECOGNIZED = -1,
}

export enum OrderAttributeKey {
  /** webhook - ordermeta */
  webhook = 0,
  own_crew = 1,
  fcm_token = 2,
  shopify_id = 3,
  shopify_email = 4,
  shopify_domain = 5,
  basic_rate = 6,
  weight_rate = 7,
  express_rate = 8,
  discount_rate = 9,
  distance_rate = 10,
  wall_surcharge = 11,
  adjustment_rate = 12,
  pickup_surcharge = 13,
  realiability_rate = 14,
  dropoff_surcharge = 15,
  express_surcharge = 16,
  warehouse_marking = 17,
  attempts = 18,
  utd_reason = 19,
  exchange_rate = 20,
  next_order_number = 21,
  previous_order_number = 22,
  cancellation_reason = 23,
  mask_pickup_notes = 24,
  pickup_attempts = 25,
  dropoff_attempts = 26,
  /** @deprecated */
  cvs_store = 27,
  /** @deprecated */
  cvs_parcel_price = 28,
  cancellation_detail = 29,
  freight_collect = 30,
  gbg_quote_id = 31,
  convenience_store_parcel_price = 32,
  woocommerce_order_id = 33,
  woocommerce_shop = 34,
  pickup_timeslot_id = 35,
  sop_delivery_option = 36,
  promotion_code = 37,
  duty_type = 38,
  path_code = 39,
  /** pickup_address_confidence - Address */
  pickup_address_confidence = 40,
  dropoff_address_confidence = 41,
  pickup_address_type = 42,
  dropoff_address_type = 43,
  pickup_address_segment = 44,
  dropoff_address_segment = 45,
  pickup_recognized_address = 46,
  dropoff_recognized_address = 47,
  /** warehouse_id - Warehouse */
  warehouse_id = 48,
  location_id = 49,
  method = 50,
  warehouse_name = 51,
  location_barcode = 52,
  /** straight_distance - public */
  straight_distance = 53,
  driving_distance = 54,
  walking_distance = 55,
  cash_on_delivery = 56,
  is_in_available_pool = 57,
  is_fragile = 58,
  pickup_sms = 59,
  core_merchant = 60,
  promo_order = 61,
  reliable = 62,
  has_delivery_note = 63,
  distribution_id = 64,
  pay_on_success = 65,
  allow_unable_to_pickup = 66,
  project_tag_id = 67,
  outsource_partner = 68,
  outsource_id = 69,
  UNRECOGNIZED = -1,
}

export interface OrderFlow {
  id: string;
  name: string;
  tags: TagEnum[];
  serviceOfferingIds: string[];
}

export interface FlowTag {
  id: string;
  tag: TagEnum;
  createdAt: string;
  updatedAt: string;
}

export interface OrderFlowTag {
  id: string;
  orderFlowId: string;
  orderTagId: string;
  createdAt: string;
  updatedAt: string;
}

export interface Waypoint {
  id: string;
  orderId: string;
  ordering: string;
  type: Waypoint_Type;
  status: Waypoint_Status;
  contactName: string;
  contactPhone: string;
  contactCompany: string;
  earliestTime: string;
  latestTime: string;
  address: string;
  lat: string;
  lng: string;
  districtLevel1: string;
  districtLevel2: string;
  districtLevel3: string;
  city: string;
  createdAt: string;
  updatedAt: string;
  zipCode: string;
  note: string;
}

export enum Waypoint_Type {
  PICK = 0,
  DROP = 1,
  UNRECOGNIZED = -1,
}

export enum Waypoint_Status {
  PENDING = 0,
  ENROUTE = 1,
  ARRIVED = 2,
  UNRECOGNIZED = -1,
}

export interface Timeline {
  orderTripId: string;
  event: string;
  data: { [key: string]: string };
}

export interface Timeline_DataEntry {
  key: string;
  value: string;
}

/**
 * enum Type {
 *   warehouse = 0;
 *   fulfillment = 1;
 *   self_pickup = 2;
 *   outsource = 3;
 *   cross_border = 4;
 * }
 */
export interface OrderTrip {
  orderId: string;
  tripId: string;
  tripType: TagEnum;
  timelines: Timeline[];
  createdAt: string;
  updatedAt: string;
}

export interface ReferenceNumber {
  orderId: string;
  type: ReferenceNumber_Type;
  value: string;
  createdAt: string;
  updatedAt: string;
}

export enum ReferenceNumber_Type {
  PICKUPP = 0,
  CLIENT_REFERENCE_NUMBER = 1,
  UNRECOGNIZED = -1,
}

export interface OrderAttribute {
  orderId: string;
  key: OrderAttributeKey;
  value: string;
  createdAt: string;
  updatedAt: string;
}

export interface Order {
  id: string;
  entityId: number;
  userId: number;
  origin: string;
  serviceOfferingId: string;
  status: Order_Status;
  errors: string;
  createdAt: string;
  updatedAt: string;
  waypoints: Waypoint[];
  referenceNumbers: ReferenceNumber[];
  attributes: OrderAttribute[];
  orderTrips: OrderTrip[];
  parcels: Parcel[];
  items: Item[];
  purchaseOrderId?: string | undefined;
}

export enum Order_Status {
  VALIDATED = 0,
  PROCESSING = 1,
  CANCELLED = 2,
  COMPLETED = 3,
  SCHEDULED = 4,
  UNRECOGNIZED = -1,
}

export interface Item {
  orderId: string;
  name: string;
  count: string;
  width: string;
  height: string;
  length: string;
  weight: string;
  declaredValue: string;
  currency: string;
  hsCode: string;
  createdAt: string;
  updatedAt: string;
}

export interface Parcel {
  orderId: string;
  parcelNumber: string;
  status: Parcel_Status;
  createdAt: string;
  updatedAt: string;
}

export enum Parcel_Status {
  PENDING = 0,
  IN_WAREHOUSE = 1,
  DA_PICKED = 2,
  DA_DROPPED = 3,
  CANCELLED = 4,
  UNRECOGNIZED = -1,
}

export const BACKBONE_PACKAGE_NAME = 'backbone';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
