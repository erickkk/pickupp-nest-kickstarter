/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'payroll';

export interface TotalPayrolls {
  deliveryAgentId: number;
  totalOrderPrice: string;
  totalTripPrice: string;
  totalBonus: string;
  totalPenalty: string;
  totalCommission: string;
  totalAdjustment: string;
  total: string;
}

export interface Incentive {
  bonus: string;
  penalty: string;
  commission: string;
}

export interface Transaction {
  id: string;
  deliveryAgentId: number;
  type: string;
  transactableId: string;
  amount: number;
  status: string;
  createdAt: string;
  description: string;
  runningBalance: number;
}

export interface CashOnDelivery {
  id: string;
  orderId: string;
  deliveryAgentId: number;
  cashOnDelivery: number;
  cashOnDeliveryBankIn: boolean;
  cashOnDeliveryReceipt: string;
  updatedAt: string;
  createdAt: string;
  entityId: number;
  status: string;
  tripId: string;
  clientReferenceNumber: string;
  walletTransactionId: string;
}

export interface CashOut {
  id: string;
  walletTransactionId: string;
  deliveryAgentId: number;
  amount: number;
  createdAt: string;
  updatedAt: string;
  status: string;
  bankName: string;
  bankAccount: string;
  accountName: string;
  adminId: number;
  description: string;
}

export interface BankIn {
  id: string;
  walletTransactionId: string;
  deliveryAgentId: number;
  amount: number;
  createdAt: string;
  updatedAt: string;
  photoUrl: string;
  adminId: number;
  status: string;
  description: string;
  method: string;
}

export interface Payroll {
  id: string;
  orderId: string;
  deliveryAgentId: number;
  orderPrice: string;
  tripPrice: string;
  pickupOntime: boolean;
  dropoffOntime: boolean;
  bonus: string;
  penalty: string;
  commission: string;
  description: string;
  updatedAt: string;
  createdAt: string;
  status: string;
  walletTransactionId: string;
  tripId: string;
  isWarehouse: boolean;
  adjustment: string;
  acceptMethod: string;
  payrollTime: string;
  confirmReason: string;
}

export const PAYROLL_PACKAGE_NAME = 'payroll';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
