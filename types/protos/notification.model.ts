/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'notification';

/** Push Notification */
export interface FCMRequest {
  title: string;
  body: string;
}

export interface FCMData {
  payload: string;
}

/** SMS */
export interface Link {
  url: string;
}

/** Email */
export interface Recipient {
  email: string;
  name: string;
}

export interface PurchaseItem {
  name: string;
  quantity: number;
  imageUrl: string;
  notes: string;
  subItems: PurchaseSubItem[];
}

export interface PurchaseSubItem {
  name: string;
  quantity: number;
}

export interface AgentMissingDocument {
  to: Recipient | undefined;
  documents: string;
}

export interface AgentOweMoney {
  to: Recipient | undefined;
  amount: string;
  orderNumber: string;
}

export interface MerchantOutstandingCOD {
  to: Recipient[];
  amount: string;
  name: string;
  dayOfWeekStart: string;
  dayOfWeekEnd: string;
}

export interface ExpireOrder {
  to: Recipient[];
  name: string;
  orderNumber: string;
}

export interface MerchantNotification {
  to: Recipient | undefined;
  logo: string;
  customContent: string;
  orderNumber: string;
  triggerAt: string;
  title: string;
}

export interface Action {
  id: string;
  type: string;
  objectId: string;
  objectName: string;
  runsLeft: number;
  done: boolean;
  payload: string;
  scheduledAt: string;
}

export interface ActionLog {
  id: string;
  actionId: string;
  success: boolean;
  request: string;
  response: string;
}

export const NOTIFICATION_PACKAGE_NAME = 'notification';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
