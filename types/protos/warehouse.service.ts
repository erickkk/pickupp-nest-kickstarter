/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  WarehouseContract,
  WarehouseRule,
  Warehouse,
  WarehouseUser,
  LocationRule,
  Location,
  BarcodeHistory,
  WarehousePermission,
  EmptyResponse,
  CurrentUser,
  RequestWithUUID,
  EmptyRequest,
} from './warehouse.model';

export const protobufPackage = 'warehouse';

export interface GetWarehouseContractsRequest {
  warehouseId: string;
}

export interface GetWarehouseContractsResponse {
  warehouseContracts: WarehouseContract[];
  total: number;
}

export interface UpdateWarehouseRulesRequest {
  warehouseRules: WarehouseRule[];
}

export interface GetWarehouseRulesRequest {
  warehouseId: string;
}

export interface GetWarehouseRulesResponse {
  warehouseRules: WarehouseRule[];
}

export interface GetWarehousesRequest {
  limit: number;
  offset: number;
  name: string;
  ids: string[];
  statuses: string[];
  canReturn: boolean;
  canFirstLeg: boolean;
  lat: number;
  lng: number;
  latDistance: number;
  lngDistance: number;
  onlySelfPick: boolean;
  search: string;
  latCursor: number;
  lngCursor: number;
  createdAtCursor: string;
  sortBy: string;
  sortOrder: string;
}

export interface GetWarehousesResponse {
  warehouses: Warehouse[];
  total: number;
}

export interface GetNearestWarehouseRequest {
  lat: number;
  lng: number;
  districtLevel1: string;
  districtLevel2: string;
  districtLevel3: string;
  dimension: number[];
  weight: number;
  entityId: number;
  canReturn: boolean;
  cashOnDeliveryAmount: string;
  addressLine1: string;
  addressLine2: string;
  canFirstLeg: boolean;
}

export interface GetWarehouseUsersRequest {
  limit: number;
  offset: number;
  userId: number;
  warehouseId: string;
  userType: string;
}

export interface GetWarehouseUsersResponse {
  warehouseUsers: WarehouseUser[];
  total: number;
}

export interface GetMyWarehousesResponse {
  warehouses: Warehouse[];
}

export interface GetSortedLocationRequest {
  lat: number;
  lng: number;
  districtLevel1: string;
  districtLevel2: string;
  districtLevel3: string;
  dimension: number[];
  weight: number;
  entityId: number;
  canReturn: boolean;
  cashOnDeliveryAmount: string;
  addressLine1: string;
  addressLine2: string;
  warehouseId: string;
}

export interface UpdateLocationRulesRequest {
  locationRules: LocationRule[];
}

export interface GetLocationRulesRequest {
  locationId: string;
}

export interface GetLocationRulesResponse {
  locationRules: LocationRule[];
}

export interface GetLocationsRequest {
  limit: number;
  offset: number;
  barcodes: string[];
  ids: string[];
  warehouseIds: string[];
}

export interface GetLocationsResponse {
  locations: Location[];
  total: number;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface LoginResponse {
  token: string;
  userId: string;
}

export interface AuthRequest {
  token: string;
}

export interface SwitchWarehouseRequest {
  token: string;
  warehouseId: string;
}

export interface GetBarcodeHistoriesRequest {
  limit: number;
  offset: number;
  orderIds: string[];
  methods: string[];
}

export interface GetBarcodeHistoriesResponse {
  barcodeHistories: BarcodeHistory[];
  total: number;
}

export interface GetUserPermissionsRequest {
  warehouseUserId: string;
}

export interface GetWarehousePermissionsResponse {
  warehousePermissions: WarehousePermission[];
}

export interface UpdateUserPermissionsRequest {
  warehouseUserId: string;
  permissions: WarehousePermission[];
}

export const WAREHOUSE_PACKAGE_NAME = 'warehouse';

export interface WarehousesClient {
  getWarehouse(request: RequestWithUUID): Observable<Warehouse>;

  createWarehouse(request: Warehouse): Observable<Warehouse>;

  getWarehouses(
    request: GetWarehousesRequest,
  ): Observable<GetWarehousesResponse>;

  updateWarehouse(request: Warehouse): Observable<Warehouse>;

  getNearestWarehouse(
    request: GetNearestWarehouseRequest,
  ): Observable<Warehouse>;

  updateWarehouseRules(
    request: UpdateWarehouseRulesRequest,
  ): Observable<EmptyResponse>;

  getWarehouseRules(
    request: GetWarehouseRulesRequest,
  ): Observable<GetWarehouseRulesResponse>;

  createWarehouseContract(
    request: WarehouseContract,
  ): Observable<WarehouseContract>;

  getWarehouseContracts(
    request: GetWarehouseContractsRequest,
  ): Observable<GetWarehouseContractsResponse>;

  updateWarehouseContract(
    request: WarehouseContract,
  ): Observable<WarehouseContract>;
}

export interface WarehousesController {
  getWarehouse(
    request: RequestWithUUID,
  ): Promise<Warehouse> | Observable<Warehouse> | Warehouse;

  createWarehouse(
    request: Warehouse,
  ): Promise<Warehouse> | Observable<Warehouse> | Warehouse;

  getWarehouses(
    request: GetWarehousesRequest,
  ):
    | Promise<GetWarehousesResponse>
    | Observable<GetWarehousesResponse>
    | GetWarehousesResponse;

  updateWarehouse(
    request: Warehouse,
  ): Promise<Warehouse> | Observable<Warehouse> | Warehouse;

  getNearestWarehouse(
    request: GetNearestWarehouseRequest,
  ): Promise<Warehouse> | Observable<Warehouse> | Warehouse;

  updateWarehouseRules(
    request: UpdateWarehouseRulesRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getWarehouseRules(
    request: GetWarehouseRulesRequest,
  ):
    | Promise<GetWarehouseRulesResponse>
    | Observable<GetWarehouseRulesResponse>
    | GetWarehouseRulesResponse;

  createWarehouseContract(
    request: WarehouseContract,
  ):
    | Promise<WarehouseContract>
    | Observable<WarehouseContract>
    | WarehouseContract;

  getWarehouseContracts(
    request: GetWarehouseContractsRequest,
  ):
    | Promise<GetWarehouseContractsResponse>
    | Observable<GetWarehouseContractsResponse>
    | GetWarehouseContractsResponse;

  updateWarehouseContract(
    request: WarehouseContract,
  ):
    | Promise<WarehouseContract>
    | Observable<WarehouseContract>
    | WarehouseContract;
}

export function WarehousesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getWarehouse',
      'createWarehouse',
      'getWarehouses',
      'updateWarehouse',
      'getNearestWarehouse',
      'updateWarehouseRules',
      'getWarehouseRules',
      'createWarehouseContract',
      'getWarehouseContracts',
      'updateWarehouseContract',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Warehouses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Warehouses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const WAREHOUSES_SERVICE_NAME = 'Warehouses';

export interface WarehouseUsersClient {
  getMyWarehouses(request: WarehouseUser): Observable<GetMyWarehousesResponse>;

  associateUser(request: WarehouseUser): Observable<WarehouseUser>;

  dissociateUser(request: WarehouseUser): Observable<WarehouseUser>;

  getWarehouseUsers(
    request: GetWarehouseUsersRequest,
  ): Observable<GetWarehouseUsersResponse>;

  getWarehouseUser(request: WarehouseUser): Observable<WarehouseUser>;
}

export interface WarehouseUsersController {
  getMyWarehouses(
    request: WarehouseUser,
  ):
    | Promise<GetMyWarehousesResponse>
    | Observable<GetMyWarehousesResponse>
    | GetMyWarehousesResponse;

  associateUser(
    request: WarehouseUser,
  ): Promise<WarehouseUser> | Observable<WarehouseUser> | WarehouseUser;

  dissociateUser(
    request: WarehouseUser,
  ): Promise<WarehouseUser> | Observable<WarehouseUser> | WarehouseUser;

  getWarehouseUsers(
    request: GetWarehouseUsersRequest,
  ):
    | Promise<GetWarehouseUsersResponse>
    | Observable<GetWarehouseUsersResponse>
    | GetWarehouseUsersResponse;

  getWarehouseUser(
    request: WarehouseUser,
  ): Promise<WarehouseUser> | Observable<WarehouseUser> | WarehouseUser;
}

export function WarehouseUsersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getMyWarehouses',
      'associateUser',
      'dissociateUser',
      'getWarehouseUsers',
      'getWarehouseUser',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('WarehouseUsers', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('WarehouseUsers', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const WAREHOUSE_USERS_SERVICE_NAME = 'WarehouseUsers';

export interface LocationsClient {
  getLocation(request: RequestWithUUID): Observable<Location>;

  getLocations(request: GetLocationsRequest): Observable<GetLocationsResponse>;

  getLocationByBarcode(request: Location): Observable<Location>;

  createLocation(request: Location): Observable<Location>;

  updateLocation(request: Location): Observable<Location>;

  removeLocation(request: RequestWithUUID): Observable<EmptyResponse>;

  updateLocationRules(
    request: UpdateLocationRulesRequest,
  ): Observable<EmptyResponse>;

  getLocationRules(
    request: GetLocationRulesRequest,
  ): Observable<GetLocationRulesResponse>;

  getSortedLocation(request: GetSortedLocationRequest): Observable<Location>;
}

export interface LocationsController {
  getLocation(
    request: RequestWithUUID,
  ): Promise<Location> | Observable<Location> | Location;

  getLocations(
    request: GetLocationsRequest,
  ):
    | Promise<GetLocationsResponse>
    | Observable<GetLocationsResponse>
    | GetLocationsResponse;

  getLocationByBarcode(
    request: Location,
  ): Promise<Location> | Observable<Location> | Location;

  createLocation(
    request: Location,
  ): Promise<Location> | Observable<Location> | Location;

  updateLocation(
    request: Location,
  ): Promise<Location> | Observable<Location> | Location;

  removeLocation(
    request: RequestWithUUID,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  updateLocationRules(
    request: UpdateLocationRulesRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getLocationRules(
    request: GetLocationRulesRequest,
  ):
    | Promise<GetLocationRulesResponse>
    | Observable<GetLocationRulesResponse>
    | GetLocationRulesResponse;

  getSortedLocation(
    request: GetSortedLocationRequest,
  ): Promise<Location> | Observable<Location> | Location;
}

export function LocationsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getLocation',
      'getLocations',
      'getLocationByBarcode',
      'createLocation',
      'updateLocation',
      'removeLocation',
      'updateLocationRules',
      'getLocationRules',
      'getSortedLocation',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Locations', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Locations', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const LOCATIONS_SERVICE_NAME = 'Locations';

export interface SessionsClient {
  login(request: LoginRequest): Observable<LoginResponse>;

  auth(request: AuthRequest): Observable<CurrentUser>;

  switchWarehouse(request: SwitchWarehouseRequest): Observable<LoginResponse>;
}

export interface SessionsController {
  login(
    request: LoginRequest,
  ): Promise<LoginResponse> | Observable<LoginResponse> | LoginResponse;

  auth(
    request: AuthRequest,
  ): Promise<CurrentUser> | Observable<CurrentUser> | CurrentUser;

  switchWarehouse(
    request: SwitchWarehouseRequest,
  ): Promise<LoginResponse> | Observable<LoginResponse> | LoginResponse;
}

export function SessionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['login', 'auth', 'switchWarehouse'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SESSIONS_SERVICE_NAME = 'Sessions';

export interface BarcodeHistoriesClient {
  createBarcodeHistory(request: BarcodeHistory): Observable<BarcodeHistory>;

  getBarcodeHistory(request: RequestWithUUID): Observable<BarcodeHistory>;

  getBarcodeHistories(
    request: GetBarcodeHistoriesRequest,
  ): Observable<GetBarcodeHistoriesResponse>;
}

export interface BarcodeHistoriesController {
  createBarcodeHistory(
    request: BarcodeHistory,
  ): Promise<BarcodeHistory> | Observable<BarcodeHistory> | BarcodeHistory;

  getBarcodeHistory(
    request: RequestWithUUID,
  ): Promise<BarcodeHistory> | Observable<BarcodeHistory> | BarcodeHistory;

  getBarcodeHistories(
    request: GetBarcodeHistoriesRequest,
  ):
    | Promise<GetBarcodeHistoriesResponse>
    | Observable<GetBarcodeHistoriesResponse>
    | GetBarcodeHistoriesResponse;
}

export function BarcodeHistoriesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createBarcodeHistory',
      'getBarcodeHistory',
      'getBarcodeHistories',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('BarcodeHistories', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('BarcodeHistories', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const BARCODE_HISTORIES_SERVICE_NAME = 'BarcodeHistories';

export interface WarehousePermissionsClient {
  getWarehousePermissions(
    request: EmptyRequest,
  ): Observable<GetWarehousePermissionsResponse>;

  getUserPermissions(
    request: GetUserPermissionsRequest,
  ): Observable<GetWarehousePermissionsResponse>;

  updateUserPermissions(
    request: UpdateUserPermissionsRequest,
  ): Observable<EmptyResponse>;
}

export interface WarehousePermissionsController {
  getWarehousePermissions(
    request: EmptyRequest,
  ):
    | Promise<GetWarehousePermissionsResponse>
    | Observable<GetWarehousePermissionsResponse>
    | GetWarehousePermissionsResponse;

  getUserPermissions(
    request: GetUserPermissionsRequest,
  ):
    | Promise<GetWarehousePermissionsResponse>
    | Observable<GetWarehousePermissionsResponse>
    | GetWarehousePermissionsResponse;

  updateUserPermissions(
    request: UpdateUserPermissionsRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function WarehousePermissionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getWarehousePermissions',
      'getUserPermissions',
      'updateUserPermissions',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('WarehousePermissions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('WarehousePermissions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const WAREHOUSE_PERMISSIONS_SERVICE_NAME = 'WarehousePermissions';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
