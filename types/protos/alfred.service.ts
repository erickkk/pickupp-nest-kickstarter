/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';
import { Order, Shipment, Locker, OrderTrack } from './alfred.model';

export const protobufPackage = 'alfred';

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface UpdateTripResponse {
  status: string;
}

export interface GetOrdersRequest {
  orderIds: string[];
  orderNumbers: string[];
}

export interface GetOrdersResponse {
  orders: Order[];
}

export interface GetOrderRequest {
  id: string;
}

export interface GetWaybillsRequest {
  orderNumbers: string[];
}

export interface GetWaybillsResponse {
  waybills: GetWaybillsResponse_Waybill[];
}

export interface GetWaybillsResponse_Waybill {
  waybillNumber: string;
  orderNumber: string;
  locationId: string;
  locationName: string;
  address: string;
  addressEn: string;
}

export interface GetShipmentResponse {
  alfredOrders: Shipment[];
}

export interface GetShipmentRequest {
  id: string;
}

export interface SyncLockerRequest {}

export interface SyncLockerResponse {
  success: boolean;
}

export interface GetLockerRequest {
  locationId: string;
}

export interface GetLockerResponse {
  location: Locker | undefined;
}

export interface GetLockersRequest {
  locationIds: string[];
  lat: number;
  lng: number;
  search: string;
  limit: number;
  latDistance: number;
  lngDistance: number;
  latCursor: number;
  lngCursor: number;
  createdAtCursor: string;
  selfpick: boolean;
}

export interface GetLockersResponse {
  locations: Locker[];
}

export interface GetWarehouseRequest {}

export interface GetWarehouseResponse {
  id: string;
}

export const ALFRED_PACKAGE_NAME = 'alfred';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

export interface TripClient {
  updateTrip(request: OrderTrack): Observable<UpdateTripResponse>;
}

export interface TripController {
  updateTrip(
    request: OrderTrack,
  ):
    | Promise<UpdateTripResponse>
    | Observable<UpdateTripResponse>
    | UpdateTripResponse;
}

export function TripControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['updateTrip'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Trip', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Trip', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRIP_SERVICE_NAME = 'Trip';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<Order>;

  getShipments(request: GetShipmentRequest): Observable<GetShipmentResponse>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;

  getWaybills(request: GetWaybillsRequest): Observable<GetWaybillsResponse>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  getShipments(
    request: GetShipmentRequest,
  ):
    | Promise<GetShipmentResponse>
    | Observable<GetShipmentResponse>
    | GetShipmentResponse;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;

  getWaybills(
    request: GetWaybillsRequest,
  ):
    | Promise<GetWaybillsResponse>
    | Observable<GetWaybillsResponse>
    | GetWaybillsResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOrder',
      'getShipments',
      'getOrders',
      'getWaybills',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

export interface LockersClient {
  getWarehouse(request: GetWarehouseRequest): Observable<GetWarehouseResponse>;

  syncLockers(request: SyncLockerRequest): Observable<SyncLockerResponse>;

  getLocker(request: GetLockerRequest): Observable<GetLockerResponse>;

  getLockers(request: GetLockersRequest): Observable<GetLockersResponse>;
}

export interface LockersController {
  getWarehouse(
    request: GetWarehouseRequest,
  ):
    | Promise<GetWarehouseResponse>
    | Observable<GetWarehouseResponse>
    | GetWarehouseResponse;

  syncLockers(
    request: SyncLockerRequest,
  ):
    | Promise<SyncLockerResponse>
    | Observable<SyncLockerResponse>
    | SyncLockerResponse;

  getLocker(
    request: GetLockerRequest,
  ):
    | Promise<GetLockerResponse>
    | Observable<GetLockerResponse>
    | GetLockerResponse;

  getLockers(
    request: GetLockersRequest,
  ):
    | Promise<GetLockersResponse>
    | Observable<GetLockersResponse>
    | GetLockersResponse;
}

export function LockersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getWarehouse',
      'syncLockers',
      'getLocker',
      'getLockers',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Lockers', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Lockers', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const LOCKERS_SERVICE_NAME = 'Lockers';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
