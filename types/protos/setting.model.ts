/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'setting';

export interface SettingResponse {
  id: string;
  key: string;
  value: string;
  updatedBy: string;
  updatedAt: string;
  createdAt: string;
  city: string;
}

export interface EmptyResponse {}

export interface FeatureToggleResponse {
  id: string;
  key: string;
  startFrom: string;
  stopFrom: string;
  updatedBy: string;
  updatedAt: string;
  createdAt: string;
  tag: string;
}

export const SETTING_PACKAGE_NAME = 'setting';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
