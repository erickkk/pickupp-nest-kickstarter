/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'kerry';

export interface KerryOrderNumber {
  id: string;
  number: string;
  lastUsedAt?: string | undefined;
  createdAt: string;
  updatedAt: string;
  deletedAt?: string | undefined;
}

export interface CreateOrderFailedDetail {
  BLN: string;
  Error: string;
}

export interface CreateOrderResponse {
  Result: string;
  Count: number;
  Data: CreateOrderFailedDetail[];
}

export interface KerryTripInfo {
  BLN: string;
  Station: string;
  Date: string;
  Time: string;
  StatusCode: string;
  status: string;
  StatusCodeType: number;
}

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  kerryOrderNumber: string;
  kerryStationNumber?: string | undefined;
  createOrderResponse?: CreateOrderResponse | undefined;
  errorMessage?: string | undefined;
  kerryTripHistory: KerryTripInfo[];
  status: string;
  lastSyncedAt?: string | undefined;
  createdAt: string;
  updatedAt: string;
}

export interface Waybill {
  orderId: string;
  orderNumber: string;
  kerryOrderNumber: string;
  kerryStationNumber: string;
}

export const KERRY_PACKAGE_NAME = 'kerry';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
