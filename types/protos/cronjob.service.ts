/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { Task, UpdateTask } from './cronjob.model';

export const protobufPackage = 'cronjob';

export interface GetTasksRequest {
  names: string[];
}

export interface GetTasksResponse {
  tasks: Task[];
  total: number;
}

export interface GetTaskRequest {
  name: string;
}

export interface GetTaskResponse {
  task: Task | undefined;
}

export interface UpdateTaskRequest {
  name: string;
  task: UpdateTask | undefined;
}

export interface UpdateTaskResponse {
  task: Task | undefined;
}

export interface RegisterRequest {
  key: string;
  crontab: string;
  event: string;
}

export interface RegisterResponse {
  success: boolean;
}

export const CRONJOB_PACKAGE_NAME = 'cronjob';

export interface TasksClient {
  getTasks(request: GetTasksRequest): Observable<GetTasksResponse>;

  getTask(request: GetTaskRequest): Observable<GetTaskResponse>;

  updateTask(request: UpdateTaskRequest): Observable<UpdateTaskResponse>;
}

export interface TasksController {
  getTasks(
    request: GetTasksRequest,
  ):
    | Promise<GetTasksResponse>
    | Observable<GetTasksResponse>
    | GetTasksResponse;

  getTask(
    request: GetTaskRequest,
  ): Promise<GetTaskResponse> | Observable<GetTaskResponse> | GetTaskResponse;

  updateTask(
    request: UpdateTaskRequest,
  ):
    | Promise<UpdateTaskResponse>
    | Observable<UpdateTaskResponse>
    | UpdateTaskResponse;
}

export function TasksControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getTasks', 'getTask', 'updateTask'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Tasks', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Tasks', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TASKS_SERVICE_NAME = 'Tasks';

export interface CronjobClient {
  register(request: RegisterRequest): Observable<RegisterResponse>;
}

export interface CronjobController {
  register(
    request: RegisterRequest,
  ):
    | Promise<RegisterResponse>
    | Observable<RegisterResponse>
    | RegisterResponse;
}

export function CronjobControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['register'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Cronjob', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Cronjob', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CRONJOB_SERVICE_NAME = 'Cronjob';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
