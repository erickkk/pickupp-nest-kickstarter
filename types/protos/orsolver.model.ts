/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Job, Trip, Order } from './order.model';
import { DeliveryAgentType } from './agent.model';
import { Any } from './google/protobuf/any';

export const protobufPackage = 'orsolver';

/** store repeated string in map for general use case */
export interface RepeatedString {
  stringList: string[];
}

export interface DistributionJobRequest {
  job: Job | undefined;
  ongoingTrips: number;
  capacityOverLimitRate: number;
  capacityUnderLimitRate: number;
  blacklist: number[];
  deliveryAgentType: DeliveryAgentType | undefined;
  dispatchScoreThreshold: string;
  weights: { [key: string]: string };
  pickupCentroidLng: string;
  pickupCentroidLat: string;
  dropoffCentroidLng: string;
  dropoffCentroidLat: string;
  daWalletAmount: number;
  ongoingParcelSize: number;
}

export interface DistributionJobRequest_WeightsEntry {
  key: string;
  value: string;
}

export interface DistributionTripRequest {
  trip: Trip | undefined;
  order: Order | undefined;
}

export interface ORProblem {
  model: string;
  payload: Any | undefined;
}

export interface SolveORProblemResponse {
  duration: number;
  result: Any | undefined;
}

export interface NormalJobDistributionRequest {
  jobs: DistributionJobRequest[];
  trips: DistributionTripRequest[];
  weights: { [key: string]: string };
}

export interface NormalJobDistributionRequest_WeightsEntry {
  key: string;
  value: string;
}

export interface NormalJobDistributionResponse {
  resultPayload: { [key: string]: RepeatedString };
}

export interface NormalJobDistributionResponse_ResultPayloadEntry {
  key: string;
  value: RepeatedString | undefined;
}

export interface IAJobDistributionRequest {
  jobs: DistributionJobRequest[];
  trips: DistributionTripRequest[];
  weights: { [key: string]: string };
}

export interface IAJobDistributionRequest_WeightsEntry {
  key: string;
  value: string;
}

export interface IAJobDistributionResponse {
  resultPayload: { [key: string]: RepeatedString };
}

export interface IAJobDistributionResponse_ResultPayloadEntry {
  key: string;
  value: RepeatedString | undefined;
}

export const ORSOLVER_PACKAGE_NAME = 'orsolver';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
