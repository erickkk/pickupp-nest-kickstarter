/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'international';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  dropoffOrderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: OrderTrack | undefined;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  errorMessage: string;
  dropoffRegion: string;
  pickupRegion: string;
  remarks: string;
}

export interface OrderResponse {
  error: number;
  message: string;
  data: { [key: string]: string };
}

export interface OrderResponse_DataEntry {
  key: string;
  value: string;
}

export interface OrderTrack {
  orderNumber: string;
  status: string;
  clientReferenceNumber: string;
  updatedAt: string;
  unableToDeliverReason: string;
  attempts: string;
  pickupAttempts: string;
  dropoffAttempts: string;
  dropoffProofImageUrl: string;
  signatureUrl: string;
  deliveryAgentLocation: string;
  trackingUrl: string;
  dropoffContactPerson: string;
  cancelReasonType: string;
  warehouseId: string;
  locationBarcode: string;
  tripDropoffAddress: string;
  deliveryAgentName: string;
  deliveryAgentPhone: string;
  packageWidth: number;
  packageLength: number;
  packageHeight: number;
  packageWeight: number;
  orderPrice: number;
  pickupTime: string;
  dropoffTime: string;
  rescheduleUrl: string;
  serviceType: string;
  serviceTime: number;
}

export const INTERNATIONAL_PACKAGE_NAME = 'international';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
