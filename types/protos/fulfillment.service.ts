/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  InboundRequest,
  Inventory,
  InventoryStock,
  InboundReceipt,
  OutboundRequest,
  OutboundItem,
  PickRequest,
  PickItem,
  PackStation,
  PackItem,
  InboundItem,
} from './fulfillment.model';

export const protobufPackage = 'fulfillment';

export interface GetInboundItemRequest {
  id: string;
}

export interface CreateInboundRequestRequest {
  inboundRequest: InboundRequest | undefined;
  region: string;
}

export interface FetchInboundRequestsRequest {
  limit: string;
  offset: string;
  query: string;
  sortBy: string;
  warehouseId: string;
  statuses: string[];
}

export interface CountInboundRequestsRequest {
  warehouseId: string;
  startTime: string;
  endTime: string;
}

export interface CountInboundRequestsResponse {
  counts: { [key: string]: number };
}

export interface CountInboundRequestsResponse_CountsEntry {
  key: string;
  value: number;
}

export interface GetInboundRequestsRequest {
  entityId: string;
}

export interface InboundRequestsList {
  inboundRequests: InboundRequest[];
  total: number;
}

export interface GetInventoryStockRequest {
  inventoryStockId: string;
}

export interface GetInventoryStockResponse {
  inventory: Inventory | undefined;
  inventoryStock: InventoryStock | undefined;
}

export interface UploadInventoryRequest {
  entityId: string;
  name: string;
  upc: string;
  weight: number;
  width: number;
  height: number;
  length: number;
  specialCare: string;
  price: number;
}

export interface GetInventoryStocksByInventoryIdsRequest {
  limit: string;
  offset: string;
  query: string;
  ids: string[];
}

export interface GetInventoryStocksByInventoryIdsResponse {
  inventoryStocks: InventoryStock[];
}

export interface GetInventoriesWithStocksByUpcRequest {
  upc: string;
}

export interface GetInventoriesWithStocksByUpcResponse {
  inventories: Inventory[];
}

export interface GetInventoryHoldingsRequest {
  warehouseUserId: string;
}

export interface GetInventoryHoldingsResponse {
  inventories: Inventory[];
}

export interface GetInventoryStockHoldingsCountRequest {
  warehouseUserId: string;
}

export interface GetInventoryStockHoldingsCountResponse {
  count: number;
}

export interface CreateInventoryRequest {
  entityId: string;
  name: string;
  photo: string;
  upc: string;
  weight: number;
  width: number;
  height: number;
  length: number;
  quantity: number;
  expiredAt: string;
  holdingBy: string;
  locationId: string;
  batchNumber: string;
  requestNumber: string;
  specialCare: string;
  price: number;
  manufacturedAt: string;
  remarks: string;
  inboundItemId: string;
  inventoryStockId?: string | undefined;
  inboundReceiptId?: string | undefined;
  received: number;
}

export interface CreateInventoryResponse {
  inventory: Inventory | undefined;
  inventoryStock: InventoryStock | undefined;
}

export interface UpdateInventoryRequest {
  id: string;
  name: string;
  photo: string;
  upc: string;
  weight: number;
  width: number;
  height: number;
  length: number;
  adminId: number;
}

export interface UpdateInventoryResponse {
  inventory: Inventory | undefined;
}

export interface UpdateInventoryStockRequest {
  id: string;
  quantity: number;
  expiredAt: string;
  holdingBy: string;
  locationId: string;
  adminId: string;
  pendingQuantity: number;
}

export interface UpdateInventoryStockResponse {
  inventoryStock: InventoryStock | undefined;
}

export interface UpdateInventoryStockPossessionRequest {
  id: string;
  quantity: number;
  holdingBy: string;
  locationId: string;
  warehouseUserId: string;
}

export interface UpdateInventoryStockPossessionResponse {
  inventoryStock: InventoryStock | undefined;
}

export interface GetInventoryRequest {
  id: string;
}

export interface GetInventoriesByIdsRequest {
  ids: string[];
}

export interface GetInventoriesByEntityIdsRequest {
  limit: string;
  offset: string;
  query: string;
  ids: string[];
  search: string;
}

export interface InventoriesResponse {
  inventories: Inventory[];
}

export interface DeleteInboundReceiptRequest {
  id: string;
}

export interface GetInboundReceiptRequest {
  inventoryStockId: string;
  inboundItemId: string;
}

export interface GetInboundReceiptResponse {
  inboundReceipt: InboundReceipt | undefined;
}

export interface CreateInboundReceiptRequest {
  inboundItemId: string;
  inventoryStockId: string;
  quantity: number;
  remark: string;
  received: number;
  receivedBy: string;
}

export interface CreateInboundReceiptResponse {
  inboundReceipt: InboundReceipt | undefined;
}

export interface GetInboundReceiptsRequest {
  inboundItemId: string;
  limit: string;
  offset: string;
  query: string;
}

export interface GetInboundReceiptsResponse {
  inboundReceipts: InboundReceipt[];
  total: number;
}

export interface GetInboundRequestDetailsRequest {
  entityId: string;
  requestNumber: string;
}

export interface AdminGetInboundRequestDetailsRequest {
  id: string;
}

export interface InboundRequestWrapper {
  inboundRequest: InboundRequest | undefined;
}

export interface CancelInboundRequestRequest {
  entityId: string;
  requestNumber: string;
}

export interface EmptyRequest {}

export interface EmptyResponse {}

export interface GetOutboundRequestDetailsRequest {
  entityId: string;
  requestNumber: string;
}

export interface SingleOutboundRequestResponse {
  outboundRequest: OutboundRequest | undefined;
}

export interface GetOutboundRequestDetailsByIdRequest {
  id: string;
}

export interface GetOutboundRequestsByEntityIdRequest {
  entityId: string;
  limit: string;
  offset: string;
}

export interface MultipleOutboundRequestsResponse {
  outboundRequests: OutboundRequest[];
  total: number;
}

export interface FetchOutboundRequestsRequest {
  status: string;
  pickRequestId: string;
  usePickuppDelivery: boolean;
}

export interface CreateOutboundRequestsRequest {
  orderId?: string | undefined;
  entityId: string;
  warehouseId: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  dropoffAddressLine1: string;
  dropoffAddressLine2: string;
  usePickuppDelivery: boolean;
  estimatedDeliveryDate: string;
  region: string;
  outboundItems: OutboundItem[];
  dropoffAddressLatitude: number;
  dropoffAddressLongitude: number;
  pickRequestId?: string | undefined;
}

export interface UpdateOutboundRequestRequest {
  id: string;
  status: string;
  estimatedDeliveryDate: string;
  dropoffContactPerson: string;
  dropoffContactPhone: string;
  pickRequestId: string;
}

export interface CancelOutboundRequestRequest {
  entityId: string;
  requestNumber: string;
}

export interface GetOutboundItemRequest {
  id: string;
}

export interface GetPickRequestRequest {
  id: string;
}

export interface GetPickRequestsRequest {
  handleBy: string;
  statuses: string[];
}

export interface SinglePickRequestResponse {
  pickRequest: PickRequest | undefined;
}

export interface MultiplePickRequestsResponse {
  pickRequests: PickRequest[];
}

export interface createPickItems {
  inventoryStockId: string;
  outboundRequestId: string;
  quantity: number;
  upc: string;
}

export interface CreatePickRequestRequest {
  warehouseId: string;
  handleBy: string;
  pickItems: createPickItems[];
  region: string;
  pickList: OutboundRequest[];
  inventoryStocks: InventoryStock[];
}

export interface UpdatePickRequestRequest {
  id: string;
  status: string;
  handleBy: string;
}

export interface GetPickItemRequest {
  id: string;
}

export interface UpdatePickItemRequest {
  id: string;
  status: string;
  handleBy: string;
  pendingQuantity: number;
}

export interface SinglePickItemResponse {
  pickItem: PickItem | undefined;
}

export interface CreatePackStationRequest {
  warehouseId: string;
  name: string;
  sectionNumbers: number;
  remark?: string | undefined;
}

export interface CreatePackStationResponse {
  packStation: PackStation | undefined;
}

export interface DeletePackStationRequest {
  warehouseId: string;
  packStationId: string;
}

export interface GetPackStationRequest {
  warehouseId: string;
}

export interface GetPackStationResponse {
  packStations: PackStation[];
}

export interface RequestPackStationForPickRequestRequest {
  pickRequestId: string;
  warehouseId: string;
}

export interface RequestPackStationForPickRequestResponse {
  packStation: PackStation | undefined;
}

export interface ConfirmReceivePickRequestRequest {
  pickRequestId: string;
  packStationName: string;
  handlerId: string;
  warehouseId: string;
}

export interface GetPackStationStatusRequest {
  packStationId: string;
}

export interface GetPackStationStatusResponse {
  packStation: PackStation | undefined;
  outboundRequests: OutboundRequest[];
}

export interface SwitchPackStationStatusRequest {
  packStationId: string;
  canAcceptNewPickList: boolean;
}

export interface ScanItemRequest {
  upc: string;
  packStationId: string;
  handlerId: string;
}

export interface ScanItemResponse {
  packItem: PackItem | undefined;
  packStation: PackStation | undefined;
  outboundRequests: OutboundRequest[];
}

export interface CompletePackingRequest {
  outboundRequestId: string;
  packSectionId: string;
  handlerId: string;
}

export const FULFILLMENT_PACKAGE_NAME = 'fulfillment';

export interface InboundRequestsClient {
  getInboundRequests(
    request: GetInboundRequestsRequest,
  ): Observable<InboundRequestsList>;

  getInboundRequestDetails(
    request: GetInboundRequestDetailsRequest,
  ): Observable<InboundRequestWrapper>;

  getAllInboundRequests(request: EmptyRequest): Observable<InboundRequestsList>;

  adminGetInboundRequestDetails(
    request: AdminGetInboundRequestDetailsRequest,
  ): Observable<InboundRequestWrapper>;

  createInboundRequest(
    request: CreateInboundRequestRequest,
  ): Observable<InboundRequestWrapper>;

  updateInboundRequest(
    request: InboundRequestWrapper,
  ): Observable<InboundRequestWrapper>;

  cancelInboundRequest(
    request: CancelInboundRequestRequest,
  ): Observable<EmptyResponse>;

  countInboundRequests(
    request: CountInboundRequestsRequest,
  ): Observable<CountInboundRequestsResponse>;

  fetchInboundRequests(
    request: FetchInboundRequestsRequest,
  ): Observable<InboundRequestsList>;

  getInboundItem(request: GetInboundItemRequest): Observable<InboundItem>;
}

export interface InboundRequestsController {
  getInboundRequests(
    request: GetInboundRequestsRequest,
  ):
    | Promise<InboundRequestsList>
    | Observable<InboundRequestsList>
    | InboundRequestsList;

  getInboundRequestDetails(
    request: GetInboundRequestDetailsRequest,
  ):
    | Promise<InboundRequestWrapper>
    | Observable<InboundRequestWrapper>
    | InboundRequestWrapper;

  getAllInboundRequests(
    request: EmptyRequest,
  ):
    | Promise<InboundRequestsList>
    | Observable<InboundRequestsList>
    | InboundRequestsList;

  adminGetInboundRequestDetails(
    request: AdminGetInboundRequestDetailsRequest,
  ):
    | Promise<InboundRequestWrapper>
    | Observable<InboundRequestWrapper>
    | InboundRequestWrapper;

  createInboundRequest(
    request: CreateInboundRequestRequest,
  ):
    | Promise<InboundRequestWrapper>
    | Observable<InboundRequestWrapper>
    | InboundRequestWrapper;

  updateInboundRequest(
    request: InboundRequestWrapper,
  ):
    | Promise<InboundRequestWrapper>
    | Observable<InboundRequestWrapper>
    | InboundRequestWrapper;

  cancelInboundRequest(
    request: CancelInboundRequestRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  countInboundRequests(
    request: CountInboundRequestsRequest,
  ):
    | Promise<CountInboundRequestsResponse>
    | Observable<CountInboundRequestsResponse>
    | CountInboundRequestsResponse;

  fetchInboundRequests(
    request: FetchInboundRequestsRequest,
  ):
    | Promise<InboundRequestsList>
    | Observable<InboundRequestsList>
    | InboundRequestsList;

  getInboundItem(
    request: GetInboundItemRequest,
  ): Promise<InboundItem> | Observable<InboundItem> | InboundItem;
}

export function InboundRequestsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getInboundRequests',
      'getInboundRequestDetails',
      'getAllInboundRequests',
      'adminGetInboundRequestDetails',
      'createInboundRequest',
      'updateInboundRequest',
      'cancelInboundRequest',
      'countInboundRequests',
      'fetchInboundRequests',
      'getInboundItem',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('InboundRequests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('InboundRequests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const INBOUND_REQUESTS_SERVICE_NAME = 'InboundRequests';

export interface InventoriesClient {
  getInventory(request: GetInventoryRequest): Observable<Inventory>;

  getInventoriesByIds(
    request: GetInventoriesByIdsRequest,
  ): Observable<InventoriesResponse>;

  getInventoriesByEntityIds(
    request: GetInventoriesByEntityIdsRequest,
  ): Observable<InventoriesResponse>;

  getInventoryStocksByInventoryIds(
    request: GetInventoryStocksByInventoryIdsRequest,
  ): Observable<GetInventoryStocksByInventoryIdsResponse>;

  getInventoriesWithStocksByUpc(
    request: GetInventoriesWithStocksByUpcRequest,
  ): Observable<GetInventoriesWithStocksByUpcResponse>;

  getInventoryStockHoldingsCount(
    request: GetInventoryStockHoldingsCountRequest,
  ): Observable<GetInventoryStockHoldingsCountResponse>;

  getInventoryHoldings(
    request: GetInventoryHoldingsRequest,
  ): Observable<GetInventoryHoldingsResponse>;

  createInventory(
    request: CreateInventoryRequest,
  ): Observable<CreateInventoryResponse>;

  recreateInventory(
    request: CreateInventoryRequest,
  ): Observable<CreateInventoryResponse>;

  updateInventory(
    request: UpdateInventoryRequest,
  ): Observable<UpdateInventoryResponse>;

  updateInventoryStock(
    request: UpdateInventoryStockRequest,
  ): Observable<UpdateInventoryStockResponse>;

  getInventoryStock(
    request: GetInventoryStockRequest,
  ): Observable<GetInventoryStockResponse>;

  uploadInventory(request: UploadInventoryRequest): Observable<Inventory>;

  updateInventoryStockPossession(
    request: UpdateInventoryStockPossessionRequest,
  ): Observable<UpdateInventoryStockPossessionResponse>;
}

export interface InventoriesController {
  getInventory(
    request: GetInventoryRequest,
  ): Promise<Inventory> | Observable<Inventory> | Inventory;

  getInventoriesByIds(
    request: GetInventoriesByIdsRequest,
  ):
    | Promise<InventoriesResponse>
    | Observable<InventoriesResponse>
    | InventoriesResponse;

  getInventoriesByEntityIds(
    request: GetInventoriesByEntityIdsRequest,
  ):
    | Promise<InventoriesResponse>
    | Observable<InventoriesResponse>
    | InventoriesResponse;

  getInventoryStocksByInventoryIds(
    request: GetInventoryStocksByInventoryIdsRequest,
  ):
    | Promise<GetInventoryStocksByInventoryIdsResponse>
    | Observable<GetInventoryStocksByInventoryIdsResponse>
    | GetInventoryStocksByInventoryIdsResponse;

  getInventoriesWithStocksByUpc(
    request: GetInventoriesWithStocksByUpcRequest,
  ):
    | Promise<GetInventoriesWithStocksByUpcResponse>
    | Observable<GetInventoriesWithStocksByUpcResponse>
    | GetInventoriesWithStocksByUpcResponse;

  getInventoryStockHoldingsCount(
    request: GetInventoryStockHoldingsCountRequest,
  ):
    | Promise<GetInventoryStockHoldingsCountResponse>
    | Observable<GetInventoryStockHoldingsCountResponse>
    | GetInventoryStockHoldingsCountResponse;

  getInventoryHoldings(
    request: GetInventoryHoldingsRequest,
  ):
    | Promise<GetInventoryHoldingsResponse>
    | Observable<GetInventoryHoldingsResponse>
    | GetInventoryHoldingsResponse;

  createInventory(
    request: CreateInventoryRequest,
  ):
    | Promise<CreateInventoryResponse>
    | Observable<CreateInventoryResponse>
    | CreateInventoryResponse;

  recreateInventory(
    request: CreateInventoryRequest,
  ):
    | Promise<CreateInventoryResponse>
    | Observable<CreateInventoryResponse>
    | CreateInventoryResponse;

  updateInventory(
    request: UpdateInventoryRequest,
  ):
    | Promise<UpdateInventoryResponse>
    | Observable<UpdateInventoryResponse>
    | UpdateInventoryResponse;

  updateInventoryStock(
    request: UpdateInventoryStockRequest,
  ):
    | Promise<UpdateInventoryStockResponse>
    | Observable<UpdateInventoryStockResponse>
    | UpdateInventoryStockResponse;

  getInventoryStock(
    request: GetInventoryStockRequest,
  ):
    | Promise<GetInventoryStockResponse>
    | Observable<GetInventoryStockResponse>
    | GetInventoryStockResponse;

  uploadInventory(
    request: UploadInventoryRequest,
  ): Promise<Inventory> | Observable<Inventory> | Inventory;

  updateInventoryStockPossession(
    request: UpdateInventoryStockPossessionRequest,
  ):
    | Promise<UpdateInventoryStockPossessionResponse>
    | Observable<UpdateInventoryStockPossessionResponse>
    | UpdateInventoryStockPossessionResponse;
}

export function InventoriesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getInventory',
      'getInventoriesByIds',
      'getInventoriesByEntityIds',
      'getInventoryStocksByInventoryIds',
      'getInventoriesWithStocksByUpc',
      'getInventoryStockHoldingsCount',
      'getInventoryHoldings',
      'createInventory',
      'recreateInventory',
      'updateInventory',
      'updateInventoryStock',
      'getInventoryStock',
      'uploadInventory',
      'updateInventoryStockPossession',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Inventories', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Inventories', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const INVENTORIES_SERVICE_NAME = 'Inventories';

export interface InboundReceiptsClient {
  getInboundReceipts(
    request: GetInboundReceiptsRequest,
  ): Observable<GetInboundReceiptsResponse>;

  getInboundReceipt(
    request: GetInboundReceiptRequest,
  ): Observable<GetInboundReceiptResponse>;

  createInboundReceipt(
    request: CreateInboundReceiptRequest,
  ): Observable<CreateInboundReceiptResponse>;

  deleteInboundReceipt(
    request: DeleteInboundReceiptRequest,
  ): Observable<EmptyResponse>;
}

export interface InboundReceiptsController {
  getInboundReceipts(
    request: GetInboundReceiptsRequest,
  ):
    | Promise<GetInboundReceiptsResponse>
    | Observable<GetInboundReceiptsResponse>
    | GetInboundReceiptsResponse;

  getInboundReceipt(
    request: GetInboundReceiptRequest,
  ):
    | Promise<GetInboundReceiptResponse>
    | Observable<GetInboundReceiptResponse>
    | GetInboundReceiptResponse;

  createInboundReceipt(
    request: CreateInboundReceiptRequest,
  ):
    | Promise<CreateInboundReceiptResponse>
    | Observable<CreateInboundReceiptResponse>
    | CreateInboundReceiptResponse;

  deleteInboundReceipt(
    request: DeleteInboundReceiptRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function InboundReceiptsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getInboundReceipts',
      'getInboundReceipt',
      'createInboundReceipt',
      'deleteInboundReceipt',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('InboundReceipts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('InboundReceipts', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const INBOUND_RECEIPTS_SERVICE_NAME = 'InboundReceipts';

export interface OutboundRequestsClient {
  getOutboundRequestDetails(
    request: GetOutboundRequestDetailsRequest,
  ): Observable<SingleOutboundRequestResponse>;

  getOutboundRequestDetailsById(
    request: GetOutboundRequestDetailsByIdRequest,
  ): Observable<SingleOutboundRequestResponse>;

  getOutboundRequestsByEntityId(
    request: GetOutboundRequestsByEntityIdRequest,
  ): Observable<MultipleOutboundRequestsResponse>;

  getAllOutboundRequests(
    request: EmptyRequest,
  ): Observable<MultipleOutboundRequestsResponse>;

  fetchOutboundRequests(
    request: FetchOutboundRequestsRequest,
  ): Observable<MultipleOutboundRequestsResponse>;

  createOutboundRequests(
    request: CreateOutboundRequestsRequest,
  ): Observable<SingleOutboundRequestResponse>;

  updateOutboundRequest(
    request: UpdateOutboundRequestRequest,
  ): Observable<SingleOutboundRequestResponse>;

  cancelOutboundRequest(
    request: CancelOutboundRequestRequest,
  ): Observable<EmptyResponse>;

  getOutboundItem(request: GetOutboundItemRequest): Observable<OutboundItem>;
}

export interface OutboundRequestsController {
  getOutboundRequestDetails(
    request: GetOutboundRequestDetailsRequest,
  ):
    | Promise<SingleOutboundRequestResponse>
    | Observable<SingleOutboundRequestResponse>
    | SingleOutboundRequestResponse;

  getOutboundRequestDetailsById(
    request: GetOutboundRequestDetailsByIdRequest,
  ):
    | Promise<SingleOutboundRequestResponse>
    | Observable<SingleOutboundRequestResponse>
    | SingleOutboundRequestResponse;

  getOutboundRequestsByEntityId(
    request: GetOutboundRequestsByEntityIdRequest,
  ):
    | Promise<MultipleOutboundRequestsResponse>
    | Observable<MultipleOutboundRequestsResponse>
    | MultipleOutboundRequestsResponse;

  getAllOutboundRequests(
    request: EmptyRequest,
  ):
    | Promise<MultipleOutboundRequestsResponse>
    | Observable<MultipleOutboundRequestsResponse>
    | MultipleOutboundRequestsResponse;

  fetchOutboundRequests(
    request: FetchOutboundRequestsRequest,
  ):
    | Promise<MultipleOutboundRequestsResponse>
    | Observable<MultipleOutboundRequestsResponse>
    | MultipleOutboundRequestsResponse;

  createOutboundRequests(
    request: CreateOutboundRequestsRequest,
  ):
    | Promise<SingleOutboundRequestResponse>
    | Observable<SingleOutboundRequestResponse>
    | SingleOutboundRequestResponse;

  updateOutboundRequest(
    request: UpdateOutboundRequestRequest,
  ):
    | Promise<SingleOutboundRequestResponse>
    | Observable<SingleOutboundRequestResponse>
    | SingleOutboundRequestResponse;

  cancelOutboundRequest(
    request: CancelOutboundRequestRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getOutboundItem(
    request: GetOutboundItemRequest,
  ): Promise<OutboundItem> | Observable<OutboundItem> | OutboundItem;
}

export function OutboundRequestsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOutboundRequestDetails',
      'getOutboundRequestDetailsById',
      'getOutboundRequestsByEntityId',
      'getAllOutboundRequests',
      'fetchOutboundRequests',
      'createOutboundRequests',
      'updateOutboundRequest',
      'cancelOutboundRequest',
      'getOutboundItem',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('OutboundRequests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('OutboundRequests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const OUTBOUND_REQUESTS_SERVICE_NAME = 'OutboundRequests';

export interface PickRequestsClient {
  getPickRequest(request: GetPickRequestRequest): Observable<PickRequest>;

  getPickRequests(
    request: GetPickRequestsRequest,
  ): Observable<MultiplePickRequestsResponse>;

  createPickRequest(
    request: CreatePickRequestRequest,
  ): Observable<SinglePickRequestResponse>;

  updatePickRequest(
    request: UpdatePickRequestRequest,
  ): Observable<SinglePickRequestResponse>;

  getPickItem(request: GetPickItemRequest): Observable<PickItem>;

  updatePickItem(
    request: UpdatePickItemRequest,
  ): Observable<SinglePickItemResponse>;
}

export interface PickRequestsController {
  getPickRequest(
    request: GetPickRequestRequest,
  ): Promise<PickRequest> | Observable<PickRequest> | PickRequest;

  getPickRequests(
    request: GetPickRequestsRequest,
  ):
    | Promise<MultiplePickRequestsResponse>
    | Observable<MultiplePickRequestsResponse>
    | MultiplePickRequestsResponse;

  createPickRequest(
    request: CreatePickRequestRequest,
  ):
    | Promise<SinglePickRequestResponse>
    | Observable<SinglePickRequestResponse>
    | SinglePickRequestResponse;

  updatePickRequest(
    request: UpdatePickRequestRequest,
  ):
    | Promise<SinglePickRequestResponse>
    | Observable<SinglePickRequestResponse>
    | SinglePickRequestResponse;

  getPickItem(
    request: GetPickItemRequest,
  ): Promise<PickItem> | Observable<PickItem> | PickItem;

  updatePickItem(
    request: UpdatePickItemRequest,
  ):
    | Promise<SinglePickItemResponse>
    | Observable<SinglePickItemResponse>
    | SinglePickItemResponse;
}

export function PickRequestsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPickRequest',
      'getPickRequests',
      'createPickRequest',
      'updatePickRequest',
      'getPickItem',
      'updatePickItem',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('PickRequests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('PickRequests', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PICK_REQUESTS_SERVICE_NAME = 'PickRequests';

export interface PackClient {
  /** Only for admin */

  createPackStation(
    request: CreatePackStationRequest,
  ): Observable<CreatePackStationResponse>;

  deletePackStation(
    request: DeletePackStationRequest,
  ): Observable<EmptyResponse>;

  /** Can access by warehouse assistant and admin */

  requestPackStationForPickRequest(
    request: RequestPackStationForPickRequestRequest,
  ): Observable<RequestPackStationForPickRequestResponse>;

  confirmReceivePickRequest(
    request: ConfirmReceivePickRequestRequest,
  ): Observable<EmptyResponse>;

  getPackStations(
    request: GetPackStationRequest,
  ): Observable<GetPackStationResponse>;

  getPackStationStatus(
    request: GetPackStationStatusRequest,
  ): Observable<GetPackStationStatusResponse>;

  switchPackStationStatus(
    request: SwitchPackStationStatusRequest,
  ): Observable<EmptyResponse>;

  scanItem(request: ScanItemRequest): Observable<ScanItemResponse>;

  completePacking(request: CompletePackingRequest): Observable<EmptyResponse>;
}

export interface PackController {
  /** Only for admin */

  createPackStation(
    request: CreatePackStationRequest,
  ):
    | Promise<CreatePackStationResponse>
    | Observable<CreatePackStationResponse>
    | CreatePackStationResponse;

  deletePackStation(
    request: DeletePackStationRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  /** Can access by warehouse assistant and admin */

  requestPackStationForPickRequest(
    request: RequestPackStationForPickRequestRequest,
  ):
    | Promise<RequestPackStationForPickRequestResponse>
    | Observable<RequestPackStationForPickRequestResponse>
    | RequestPackStationForPickRequestResponse;

  confirmReceivePickRequest(
    request: ConfirmReceivePickRequestRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getPackStations(
    request: GetPackStationRequest,
  ):
    | Promise<GetPackStationResponse>
    | Observable<GetPackStationResponse>
    | GetPackStationResponse;

  getPackStationStatus(
    request: GetPackStationStatusRequest,
  ):
    | Promise<GetPackStationStatusResponse>
    | Observable<GetPackStationStatusResponse>
    | GetPackStationStatusResponse;

  switchPackStationStatus(
    request: SwitchPackStationStatusRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  scanItem(
    request: ScanItemRequest,
  ):
    | Promise<ScanItemResponse>
    | Observable<ScanItemResponse>
    | ScanItemResponse;

  completePacking(
    request: CompletePackingRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function PackControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createPackStation',
      'deletePackStation',
      'requestPackStationForPickRequest',
      'confirmReceivePickRequest',
      'getPackStations',
      'getPackStationStatus',
      'switchPackStationStatus',
      'scanItem',
      'completePacking',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Pack', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Pack', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PACK_SERVICE_NAME = 'Pack';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
