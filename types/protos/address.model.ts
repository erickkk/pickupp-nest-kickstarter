/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';

export const protobufPackage = 'address';

export enum PropertyKey {
  DISTRICT_LEVEL_1 = 0,
  DISTRICT_LEVEL_2 = 1,
  DISTRICT_LEVEL_3 = 2,
  IS_SERVICE_AREA = 3,
  SERVICE_HOUR = 4,
  SURCHARGE = 5,
  ABX_SERVICE_AREA = 6,
  CJ_SERVICE_AREA = 7,
  DHL_SERVICE_AREA = 8,
  ECHOICE_SERVICE_AREA = 9,
  ISEND_SERVICE_AREA = 10,
  PCHOME_SERVICE_AREA = 11,
  KERRY_SERVICE_AREA = 12,
  UNRECOGNIZED = -1,
}

export enum DistanceType {
  DRIVING = 0,
  FOOT = 1,
  STRAIGHT_LINE = 2,
  UNRECOGNIZED = -1,
}

export interface Geometry {
  lat: string;
  lng: string;
}

export interface GeofenceModel {
  id: string;
  entityId: string;
  serviceType: string;
  serviceTime: number;
  name: string;
  data: string;
  createdAt: string;
  updatedAt: string;
}

export interface Region {
  name: PropertyKey;
  value: string;
  city: string;
}

export interface District {
  id: string;
  key: string;
  maxLatitude: number;
  maxLongitude: number;
  minLatitude: number;
  minLongitude: number;
  city: string;
  data?: string | undefined;
  createdAt?: string | undefined;
  updatedAt?: string | undefined;
  properties: Property[];
  entityId?: string | undefined;
  serviceType?: string | undefined;
  serviceTime?: number | undefined;
}

export interface PropertyQuery {
  key: PropertyKey;
  value: string;
}

export interface Property {
  id: string;
  districtId?: string | undefined;
  entityId?: string | undefined;
  serviceType?: string | undefined;
  serviceTime?: number | undefined;
  key: PropertyKey;
  value: string;
  createdAt?: string | undefined;
  updatedAt?: string | undefined;
  district?: District | undefined;
}

export const ADDRESS_PACKAGE_NAME = 'address';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
