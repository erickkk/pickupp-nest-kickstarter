/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import {
  Order,
  Timeline,
  Order_OrderStatus,
  Trip_RecipientMeta,
  Trip,
  PickupProcessStatus,
  DropoffProcessStatus,
  Photo,
  BundleResult,
  BundleStatus,
  BundleRequest,
  Job,
  DeliveryNote,
  Order_WarehouseMeta,
  Route,
  BundleSetting,
  Parcel,
  PoolOrder,
  Item,
  Cluster,
  ReleaseJobRequest,
} from './order.model';
import { Timestamp } from './google/protobuf/timestamp';
import { AgentResponse, DeliveryAgentType, AgentProfile } from './agent.model';

export const protobufPackage = 'order';

export interface EmptyRequest {}

export interface EmptyResponse {}

export interface RequestWithUuid {
  id: string;
}

export interface RequestWithTripId {
  tripId: string;
}

export interface RequestWithOrderId {
  orderId: string;
}

export interface RequestWithAgentId {
  deliveryAgentId: number;
}

export interface CalculateOngoingWeightRequest {
  deliveryAgentId: number;
  weightThreshold: number;
}

export interface GetTripsCountRequest {
  deliveryAgentIds: number[];
  statuses: string[];
}

export interface DispatchRequest {
  id: string;
  orders: Order[];
  /** @deprecated */
  tripId: string;
  createdAt: Timestamp | undefined;
  expireAt: Timestamp | undefined;
  deliveryAgentId: number;
}

export interface ReviveOrderRequest {
  orderId: string;
  recharge: boolean;
}

export interface CancelOrderRequest {
  orderId: string;
  userId: string;
  refundAmount: number;
  cancellationReason: string;
  cancellationDetail: string;
}

export interface CancelOrderResponse {
  success: boolean;
  errorMessage: string;
  errorCode: string;
  orderId: string;
}

export interface GetOrdersResponse {
  success: boolean;
  result: Order[];
  errorMessage: string;
  errorCode: string;
  count: number;
}

export interface GetOrderRequest {
  orderId: string;
  orderNumber: string;
  orderType: string;
  deliveryAgentId: number;
}

export interface GetOrderResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface GetTimelineRequest {
  orderId: string;
  id: number;
}

export interface GetTimelineResponse {
  timelines: Timeline[];
}

export interface ReplayEventRequest {
  id: number;
}

export interface FetchOrdersRequest {
  orderIds: string[];
  orderNumbers: string[];
  statuses: Order_OrderStatus[];
  entityIds: number[];
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
  sortField: string;
  sortOrder: string;
  limit: number;
  offset: number;
  includeTrips: boolean;
  userIds: number[];
  metadata: { [key: string]: string };
  search: string;
  clientReferenceNumbers: string[];
  pickupDistrictsL2: string[];
  dropoffDistrictsL2: string[];
  dropoffContacts: string[];
  warehouseMeta: { [key: string]: string };
  addressMeta: { [key: string]: string };
  purchaseOrderId: string;
  latestTripOnly: boolean;
  projectTagIds: number[];
  warehouseIds: string[];
  columns: string[];
  outsourcePartner: string;
  outsourceId: string;
  serviceTypes: string[];
  masterPreference: FetchOrdersRequest_MasterSubOrderPreference;
  currentMasterId: string;
  matchingOptions: { [key: string]: boolean };
  select: string;
  pickupCity: string;
  dropoffCity: string;
}

export enum FetchOrdersRequest_MasterSubOrderPreference {
  NO_PREFERENCE = 0,
  WITHOUT_SUB_ORDER = 1,
  WITHOUT_MASTER = 2,
  UNRECOGNIZED = -1,
}

export interface FetchOrdersRequest_MetadataEntry {
  key: string;
  value: string;
}

export interface FetchOrdersRequest_WarehouseMetaEntry {
  key: string;
  value: string;
}

export interface FetchOrdersRequest_AddressMetaEntry {
  key: string;
  value: string;
}

export interface FetchOrdersRequest_MatchingOptionsEntry {
  key: string;
  value: boolean;
}

export interface FetchOrdersResponse {
  success: boolean;
  orders: Order[];
  count: number;
  errorMessage: string;
  errorCode: string;
}

export interface DispatchOrderCommandRequest {
  agents: AgentResponse[];
}

export interface DispatchOrderCommandResponse {
  success: boolean;
}

export interface GetMyOrdersRequest {
  deliveryAgentId: number;
  limit: number;
  offset: number;
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
  statuses: string[];
  pickupDistrictsL2: string[];
  dropoffDistrictsL2: string[];
}

export interface GetMyOrdersResponse {
  result: Order[];
  count: number;
}

export interface GetMyAvailableOrdersRequest {
  deliveryAgent: AgentResponse | undefined;
  metadata: { [key: string]: string };
  pickupDistrictsL2: string[];
  dropoffDistrictsL2: string[];
  sortBy: string;
  maxDimension: number;
  minDimension: number;
  maxWeight: number;
  minWeight: number;
  group: string;
  limit: number;
  walletBalance: number;
  projectTagId: number;
  serviceTypes: string[];
  distanceRange: number[];
  isBundle: boolean;
  isWarehouse: boolean;
  pickupOrderTypes: string[];
  dropoffOrderTypes: string[];
  select: string;
  offset: number;
  city: string;
}

export interface GetMyAvailableOrdersRequest_MetadataEntry {
  key: string;
  value: string;
}

export interface GetMyAvailableOrdersResponse {
  success: boolean;
  result: Order[];
  errorMessage: string;
  errorCode: string;
}

export interface AcceptAvailableOrderRequest {
  deliveryAgentId: number;
  orderId: string;
  deliveryAgentType: DeliveryAgentType | undefined;
  outstandingCod: string;
  acceptMethod: string;
  adminAssignPrice: string;
  lastlegOrderId: string;
}

export interface AcceptAvailableOrderResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface TripEnrouteRequest {
  tripId: string;
  deliveryAgentId: number;
  userId: number;
  userType: string;
  method: string;
  barcode: string;
}

export interface TripEnrouteResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface TripDroppedOffRequest {
  tripId: string;
  deliveryAgentId: number;
  proofImageUrl: string;
  signatureImageUrl: string;
  deliveryType: string;
  callLogUrl: string;
  recipientMeta: Trip_RecipientMeta | undefined;
}

export interface TripDroppedOffResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface TripMerchantCancelledRequest {
  tripId: string;
  deliveryAgentId: number;
}

export interface TripMerchantCancelledResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface TripUnableToDeliverRequest {
  tripId: string;
  deliveryAgentId: number;
  reasonType: string;
  detailReason: string;
  rescheduleTime: Timestamp | undefined;
  warehouseId: string;
  userType: string;
}

export interface TripUnableToDeliverResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface TripUnableToPickupRequest {
  tripId: string;
  deliveryAgentId: number;
  reasonType: string;
  detailReason: string;
  userType: string;
}

export interface TripUnableToPickupResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface UpdateTripRequest {
  tripId: string;
  request: Trip | undefined;
}

export interface UpdateTripProcessRequest {
  tripId: string;
  pickupProcessStatus: PickupProcessStatus | undefined;
  dropoffProcessStatus: DropoffProcessStatus | undefined;
  deliveryType: string;
  recipientMeta: Trip_RecipientMeta | undefined;
}

export interface TripUpdateResponse {
  success: boolean;
  result: Trip | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface UpdateOrderRequest {
  orderId: string;
  request: Order | undefined;
  description: string;
}

export interface OrderUpdateResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface ExpireOrderRequest {
  orderId: string;
}

export interface ExpireOrderResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface CancelTripRequest {
  tripId: string;
  releaseTime: Timestamp | undefined;
  resetSurge: boolean;
  cancellationFee: string;
}

export interface TripCancelResponse {
  success: boolean;
  result: Order | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface GetPhotosResponse {
  photos: Photo[];
}

export interface GetBundleRequest {
  bundleId: string;
  deliveryAgentId: number;
}

export interface GetBundleResponse {
  success: boolean;
  bundle: BundleResult | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface GetBundlesRequest {
  statuses: BundleStatus[];
  limit: number;
  offset: number;
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
}

export interface GetBundlesResponse {
  success: boolean;
  bundles: BundleResult[];
  errorMessage: string;
  errorCode: string;
  count: number;
}

export interface UpdateBundleRequest {
  bundleId: string;
  bundle: BundleRequest | undefined;
}

export interface UpdateBundleResponse {
  success: boolean;
  bundle: BundleResult | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface AddTripToBundleRequest {
  bundleId: string;
  orderNumber: string;
}

export interface AddTripToBundleResponse {
  success: boolean;
  bundle: BundleResult | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface RemoveTripFromBundleRequest {
  bundleId: string;
  orderNumber: string;
  isAutomated: boolean;
}

export interface RemoveTripFromBundleResponse {
  success: boolean;
  bundle: BundleResult | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface AcceptBundleRequest {
  deliveryAgentId: number;
  bundleId: string;
  deliveryAgentType: DeliveryAgentType | undefined;
  outstandingCod: string;
  tripIds: string[];
  acceptMethod: string;
  adminAssignPrice: string;
}

export interface AcceptBundleResponse {
  success: boolean;
  bundle: BundleResult | undefined;
  errorMessage: string;
  errorCode: string;
}

export interface DispatchBundleRequest {
  agents: AgentResponse[];
}

export interface DispatchBundleResponse {
  success: boolean;
}

export interface GetJobRequest {
  id: string;
}

export interface GetJobsRequest {
  ids: string[];
  statuses: string[];
  fromTime: string;
  toTime: string;
  deliveryAgentIds: number[];
  limit: number;
  offset: number;
  deliveryAgentTypeIds: string[];
  orderTypes: string[];
  queryOrder: { [key: string]: string };
  projectTagIds: number[];
  fromEndTime: string;
  toEndTime: string;
  fromReleaseTime: string;
  toReleaseTime: string;
}

export interface GetJobsRequest_QueryOrderEntry {
  key: string;
  value: string;
}

export interface GetAvailableJobsRequest {
  deliveryAgentId: number;
  dropoffRegions: string[];
  pickupRegions: string[];
  projectTagIds: number[];
  queryOrder: { [key: string]: string };
}

export interface GetAvailableJobsRequest_QueryOrderEntry {
  key: string;
  value: string;
}

export interface GetJobsResponse {
  jobs: Job[];
  total: number;
}

export interface AcceptJobRequest {
  id: string;
  deliveryAgentId: number;
}

export interface FinishJobRequest {
  id: string;
}

export interface CancelJobRequest {
  id: string;
}

export interface AddOrderToJobRequest {
  jobId: string;
  orderNumber: string;
  adminAssignPrice: string;
}

export interface DistributeOrdersToJobsRequest {
  jobs: Job[];
}

export interface InstantAssignOrdersToJobsRequest {
  jobs: Job[];
}

export interface DistributeOrdersToJobsResponse {
  fulfilledJobIds: string[];
}

export interface InstantAssignOrdersToJobsResponse {
  fulfilledJobIds: string[];
}

export interface CreateDeliveryNoteRequest {
  deliveryNote: DeliveryNote | undefined;
}

export interface GetDeliveryNotesRequest {
  ids: string[];
  orderIds: string[];
  tripIds: string[];
  entityIds: number[];
  deliveryAgentIds: number[];
  statuses: string[];
  limit: number;
  offset: number;
  orderNumbers: string[];
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
}

export interface GetDeliveryNotesResponse {
  success: boolean;
  deliveryNotes: DeliveryNote[];
  errorMessage: string;
  errorCode: string;
  count: number;
}

export interface UpdateDeliveryNoteImageRequest {
  id: string;
  imageUrl: string;
}

export interface RejectDeliveryNoteRequest {
  id: string;
}

export interface BasicResponseStatus {
  success: boolean;
  errorMessage: string;
  errorCode: string;
}

export interface ApproveDeliveryNoteRequest {
  id: string;
  approvedBy: string;
}

export interface GetClustersRequest {
  threshold: number;
  size: number;
  time: number;
  payOnSuccess: boolean;
  samePickupPoint: boolean;
  sameDropoffPoint: boolean;
  metadata: { [key: string]: string };
  fragileClusterSize: number;
  maxWeight: number;
  maxDimension: number;
  geofence: string;
  bundleSettingId: string;
  cashOnDeliveryThreshold: number;
  cashOnDeliveryCap: number;
}

export interface GetClustersRequest_MetadataEntry {
  key: string;
  value: string;
}

export interface PrematchOrderRequest {
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
}

export interface UnPrematchOrderRequest {
  orderId: string;
}

export interface GetOrdersCountRequest {
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
  warehouseMeta: { [key: string]: string };
  entityId: number;
  userId: number;
  userRole: string;
  statuses: Order_OrderStatus[];
  entityIds: number[];
}

export interface GetOrdersCountRequest_WarehouseMetaEntry {
  key: string;
  value: string;
}

export interface GetOrdersCountResponse {
  success: boolean;
  counts: { [key: string]: number };
}

export interface GetOrdersCountResponse_CountsEntry {
  key: string;
  value: number;
}

export interface GetLocationsCountRequest {
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
  statuses: Order_OrderStatus[];
  warehouseMeta: { [key: string]: string };
}

export interface GetLocationsCountRequest_WarehouseMetaEntry {
  key: string;
  value: string;
}

export interface GetLocationsCountResponse {
  counts: { [key: string]: number };
}

export interface GetLocationsCountResponse_CountsEntry {
  key: string;
  value: number;
}

export interface GetNddAttemptsCountRequest {
  entityId: number;
  currentUserId: number;
  currentUserRole: string;
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
}

export interface PendingSelfCollectOrderRequest {
  orderId: string;
  rescheduleTime: string;
  contactPhone: string;
}

export interface RescheduleOrderRequest {
  orderId: string;
  tripId: string;
  dropoffContactPhone: string;
  dropoffNotes: string;
  slotEnd: Timestamp | undefined;
}

export interface GetNddAttemptsCountResponse {
  counts: { [key: number]: number };
}

export interface GetNddAttemptsCountResponse_CountsEntry {
  key: number;
  value: number;
}

export interface CalculatePastSurgeRequest {
  startTime: Timestamp | undefined;
  endTime: Timestamp | undefined;
}

export interface UpdateSIPFactorRequest {
  name: string;
  value: number;
}

export interface CalculateOngoingCODResponse {
  ongoingCod: number;
}

export interface CalculateOngoingWeightResponse {
  ongoingWeight: number;
}

export interface CalculateOngoingVolumeResponse {
  ongoingVolume: number;
}

export interface GetTripsCountResponse {
  counts: number;
}

export interface InboundOrderRequest {
  id: string;
  warehouseMeta: Order_WarehouseMeta | undefined;
  warehouseMarking: string;
  userType: string;
  userId: number;
  parcelNumbers: string[];
  forceInboundParcels: boolean;
}

export interface ReleaseOrderRequest {
  id: string;
  releaseTime: Timestamp | undefined;
}

export interface GetMyBestRouteRequest {
  deliveryAgentId: number;
}

export interface GetMyBestRouteResponse {
  routes: Route[];
}

export interface ChangeOrdersFromMasterRequest {
  masterOrderId: string;
  orderIds: string[];
}

export interface GetBundleSettingsRequest {
  excludingInactive: boolean;
  city: string;
}

export interface GetBundleSettingsResponse {
  total: number;
  bundleSettings: BundleSetting[];
}

export interface GetSubOrderRequest {
  orderId: string;
}

export interface IsFirstEntityOrderRequest {
  entityId: number;
}

export interface IsFirstEntityOrderResponse {
  isFirstOrder: boolean;
}

export interface GetInboundAgentsRequest {
  warehouseMeta: { [key: string]: string };
}

export interface GetInboundAgentsRequest_WarehouseMetaEntry {
  key: string;
  value: string;
}

export interface GetInboundAgentsResponse {
  count: number;
  inboundAgents: GetInboundAgentsResponse_InboundAgentData[];
}

export interface GetInboundAgentsResponse_InboundAgentData {
  deliveryAgentId: number;
  ordersCount: number;
  assistantName: string;
  pickData: string;
  pickupTime: Timestamp | undefined;
}

export interface GetInboundAgentDataRequest {
  deliveryAgentId: number;
  warehouseId: string;
}

export interface GetInboundAgentDataResponse {
  deliveryAgentId: number;
  warehouseId: string;
  assistantName: string;
  pickData: string;
}

export interface UpdateInboundAgentDataRequest {
  deliveryAgentId: number;
  warehouseId: string;
  assistantName: string;
  pickData: string;
}

export interface FetchParcelsRequest {
  orderId: string;
  status: string;
  parcelNumbers: string[];
  parcelCrns: string[];
}

export interface FetchParcelsResponse {
  parcels: Parcel[];
  count: number;
}

export interface UpdateParcelsRequest {
  status: string;
  warehouseMeta: Order_WarehouseMeta | undefined;
  parcelNumbers: string[];
  orderId: string;
}

export interface GetAgentPrefAvailableOrdersRequest {
  deliveryAgent: AgentResponse | undefined;
  walletBalance: number;
  iteration: number;
  limit: number;
}

export interface GetOpsPrefAvailableOrdersRequest {
  deliveryAgent: AgentResponse | undefined;
  walletBalance: number;
  answers: AgentProfile | undefined;
  offset: number;
  limit: number;
}

export interface StandbyJobRequest {
  id: string;
}

export interface PoolOrdersResponse {
  poolOrders: PoolOrder[];
  total: number;
}

export interface RemoveFromPoolRequest {
  ids: string[];
}

export interface RefreshPoolOrderRequest {}

export interface GetItemsRequest {
  orderId: string;
}

export interface GetItemsResponse {
  items: Item[];
}

export const ORDER_PACKAGE_NAME = 'order';

export interface OrdersClient {
  createOrder(request: Order): Observable<Order>;

  addOrdersToMaster(
    request: ChangeOrdersFromMasterRequest,
  ): Observable<EmptyResponse>;

  removeOrdersFromMaster(
    request: ChangeOrdersFromMasterRequest,
  ): Observable<EmptyResponse>;

  reviveOrder(request: ReviveOrderRequest): Observable<EmptyResponse>;

  cancelOrder(request: CancelOrderRequest): Observable<CancelOrderResponse>;

  getOrder(request: GetOrderRequest): Observable<GetOrderResponse>;

  fetchOrders(request: FetchOrdersRequest): Observable<FetchOrdersResponse>;

  getMyOrders(request: GetMyOrdersRequest): Observable<GetMyOrdersResponse>;

  getMyAvailableOrders(
    request: GetMyAvailableOrdersRequest,
  ): Observable<GetMyAvailableOrdersResponse>;

  getSubOrders(request: GetSubOrderRequest): Observable<FetchOrdersResponse>;

  isFirstEntityOrder(
    request: IsFirstEntityOrderRequest,
  ): Observable<IsFirstEntityOrderResponse>;

  acceptAvailableOrder(
    request: AcceptAvailableOrderRequest,
  ): Observable<AcceptAvailableOrderResponse>;

  expireOrder(request: ExpireOrderRequest): Observable<ExpireOrderResponse>;

  updateOrder(request: UpdateOrderRequest): Observable<OrderUpdateResponse>;

  prematchOrder(request: PrematchOrderRequest): Observable<BasicResponseStatus>;

  unPrematchOrder(
    request: UnPrematchOrderRequest,
  ): Observable<BasicResponseStatus>;

  getOrdersCount(
    request: GetOrdersCountRequest,
  ): Observable<GetOrdersCountResponse>;

  getLocationsCount(
    request: GetLocationsCountRequest,
  ): Observable<GetLocationsCountResponse>;

  getNddAttemptsCount(
    request: GetNddAttemptsCountRequest,
  ): Observable<GetNddAttemptsCountResponse>;

  pendingSelfCollectOrder(
    request: PendingSelfCollectOrderRequest,
  ): Observable<Order>;

  rescheduleOrder(request: RescheduleOrderRequest): Observable<Order>;

  createEvent(request: Timeline): Observable<Timeline>;

  getTimeline(request: GetTimelineRequest): Observable<GetTimelineResponse>;

  replayEvent(request: ReplayEventRequest): Observable<BasicResponseStatus>;
}

export interface OrdersController {
  createOrder(request: Order): Promise<Order> | Observable<Order> | Order;

  addOrdersToMaster(
    request: ChangeOrdersFromMasterRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  removeOrdersFromMaster(
    request: ChangeOrdersFromMasterRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  reviveOrder(
    request: ReviveOrderRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  cancelOrder(
    request: CancelOrderRequest,
  ):
    | Promise<CancelOrderResponse>
    | Observable<CancelOrderResponse>
    | CancelOrderResponse;

  getOrder(
    request: GetOrderRequest,
  ):
    | Promise<GetOrderResponse>
    | Observable<GetOrderResponse>
    | GetOrderResponse;

  fetchOrders(
    request: FetchOrdersRequest,
  ):
    | Promise<FetchOrdersResponse>
    | Observable<FetchOrdersResponse>
    | FetchOrdersResponse;

  getMyOrders(
    request: GetMyOrdersRequest,
  ):
    | Promise<GetMyOrdersResponse>
    | Observable<GetMyOrdersResponse>
    | GetMyOrdersResponse;

  getMyAvailableOrders(
    request: GetMyAvailableOrdersRequest,
  ):
    | Promise<GetMyAvailableOrdersResponse>
    | Observable<GetMyAvailableOrdersResponse>
    | GetMyAvailableOrdersResponse;

  getSubOrders(
    request: GetSubOrderRequest,
  ):
    | Promise<FetchOrdersResponse>
    | Observable<FetchOrdersResponse>
    | FetchOrdersResponse;

  isFirstEntityOrder(
    request: IsFirstEntityOrderRequest,
  ):
    | Promise<IsFirstEntityOrderResponse>
    | Observable<IsFirstEntityOrderResponse>
    | IsFirstEntityOrderResponse;

  acceptAvailableOrder(
    request: AcceptAvailableOrderRequest,
  ):
    | Promise<AcceptAvailableOrderResponse>
    | Observable<AcceptAvailableOrderResponse>
    | AcceptAvailableOrderResponse;

  expireOrder(
    request: ExpireOrderRequest,
  ):
    | Promise<ExpireOrderResponse>
    | Observable<ExpireOrderResponse>
    | ExpireOrderResponse;

  updateOrder(
    request: UpdateOrderRequest,
  ):
    | Promise<OrderUpdateResponse>
    | Observable<OrderUpdateResponse>
    | OrderUpdateResponse;

  prematchOrder(
    request: PrematchOrderRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  unPrematchOrder(
    request: UnPrematchOrderRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  getOrdersCount(
    request: GetOrdersCountRequest,
  ):
    | Promise<GetOrdersCountResponse>
    | Observable<GetOrdersCountResponse>
    | GetOrdersCountResponse;

  getLocationsCount(
    request: GetLocationsCountRequest,
  ):
    | Promise<GetLocationsCountResponse>
    | Observable<GetLocationsCountResponse>
    | GetLocationsCountResponse;

  getNddAttemptsCount(
    request: GetNddAttemptsCountRequest,
  ):
    | Promise<GetNddAttemptsCountResponse>
    | Observable<GetNddAttemptsCountResponse>
    | GetNddAttemptsCountResponse;

  pendingSelfCollectOrder(
    request: PendingSelfCollectOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  rescheduleOrder(
    request: RescheduleOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  createEvent(
    request: Timeline,
  ): Promise<Timeline> | Observable<Timeline> | Timeline;

  getTimeline(
    request: GetTimelineRequest,
  ):
    | Promise<GetTimelineResponse>
    | Observable<GetTimelineResponse>
    | GetTimelineResponse;

  replayEvent(
    request: ReplayEventRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createOrder',
      'addOrdersToMaster',
      'removeOrdersFromMaster',
      'reviveOrder',
      'cancelOrder',
      'getOrder',
      'fetchOrders',
      'getMyOrders',
      'getMyAvailableOrders',
      'getSubOrders',
      'isFirstEntityOrder',
      'acceptAvailableOrder',
      'expireOrder',
      'updateOrder',
      'prematchOrder',
      'unPrematchOrder',
      'getOrdersCount',
      'getLocationsCount',
      'getNddAttemptsCount',
      'pendingSelfCollectOrder',
      'rescheduleOrder',
      'createEvent',
      'getTimeline',
      'replayEvent',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

export interface TripsClient {
  getTrip(request: RequestWithTripId): Observable<Trip>;

  tripEnroute(request: TripEnrouteRequest): Observable<TripEnrouteResponse>;

  tripDroppedOff(
    request: TripDroppedOffRequest,
  ): Observable<TripDroppedOffResponse>;

  tripUnableToDeliver(
    request: TripUnableToDeliverRequest,
  ): Observable<TripUnableToDeliverResponse>;

  tripUnableToPickup(
    request: TripUnableToPickupRequest,
  ): Observable<TripUnableToPickupResponse>;

  getPhotos(request: RequestWithTripId): Observable<GetPhotosResponse>;

  addPhoto(request: Photo): Observable<BasicResponseStatus>;

  deletePhoto(request: RequestWithUuid): Observable<EmptyResponse>;

  getCustomizedAvailableOrders(
    request: GetMyAvailableOrdersRequest,
  ): Observable<PoolOrdersResponse>;

  getAgentPrefAvailableOrders(
    request: GetAgentPrefAvailableOrdersRequest,
  ): Observable<PoolOrdersResponse>;

  getOpsPrefAvailableOrders(
    request: GetOpsPrefAvailableOrdersRequest,
  ): Observable<PoolOrdersResponse>;

  refreshPoolOrders(
    request: RefreshPoolOrderRequest,
  ): Observable<BasicResponseStatus>;

  fetchTrips(request: FetchOrdersRequest): Observable<PoolOrdersResponse>;

  cancelTrip(request: CancelTripRequest): Observable<TripCancelResponse>;

  updateTrip(request: UpdateTripRequest): Observable<TripUpdateResponse>;

  updateTripProcess(
    request: UpdateTripProcessRequest,
  ): Observable<TripUpdateResponse>;

  itemNotReceived(request: RequestWithTripId): Observable<Order>;

  calculateOngoingCOD(
    request: RequestWithAgentId,
  ): Observable<CalculateOngoingCODResponse>;

  calculateOngoingVolume(
    request: RequestWithAgentId,
  ): Observable<CalculateOngoingVolumeResponse>;

  calculateOngoingWeight(
    request: CalculateOngoingWeightRequest,
  ): Observable<CalculateOngoingWeightResponse>;

  removeFromPool(request: RemoveFromPoolRequest): Observable<EmptyResponse>;

  getTripsCount(
    request: GetTripsCountRequest,
  ): Observable<GetTripsCountResponse>;
}

export interface TripsController {
  getTrip(request: RequestWithTripId): Promise<Trip> | Observable<Trip> | Trip;

  tripEnroute(
    request: TripEnrouteRequest,
  ):
    | Promise<TripEnrouteResponse>
    | Observable<TripEnrouteResponse>
    | TripEnrouteResponse;

  tripDroppedOff(
    request: TripDroppedOffRequest,
  ):
    | Promise<TripDroppedOffResponse>
    | Observable<TripDroppedOffResponse>
    | TripDroppedOffResponse;

  tripUnableToDeliver(
    request: TripUnableToDeliverRequest,
  ):
    | Promise<TripUnableToDeliverResponse>
    | Observable<TripUnableToDeliverResponse>
    | TripUnableToDeliverResponse;

  tripUnableToPickup(
    request: TripUnableToPickupRequest,
  ):
    | Promise<TripUnableToPickupResponse>
    | Observable<TripUnableToPickupResponse>
    | TripUnableToPickupResponse;

  getPhotos(
    request: RequestWithTripId,
  ):
    | Promise<GetPhotosResponse>
    | Observable<GetPhotosResponse>
    | GetPhotosResponse;

  addPhoto(
    request: Photo,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  deletePhoto(
    request: RequestWithUuid,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getCustomizedAvailableOrders(
    request: GetMyAvailableOrdersRequest,
  ):
    | Promise<PoolOrdersResponse>
    | Observable<PoolOrdersResponse>
    | PoolOrdersResponse;

  getAgentPrefAvailableOrders(
    request: GetAgentPrefAvailableOrdersRequest,
  ):
    | Promise<PoolOrdersResponse>
    | Observable<PoolOrdersResponse>
    | PoolOrdersResponse;

  getOpsPrefAvailableOrders(
    request: GetOpsPrefAvailableOrdersRequest,
  ):
    | Promise<PoolOrdersResponse>
    | Observable<PoolOrdersResponse>
    | PoolOrdersResponse;

  refreshPoolOrders(
    request: RefreshPoolOrderRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  fetchTrips(
    request: FetchOrdersRequest,
  ):
    | Promise<PoolOrdersResponse>
    | Observable<PoolOrdersResponse>
    | PoolOrdersResponse;

  cancelTrip(
    request: CancelTripRequest,
  ):
    | Promise<TripCancelResponse>
    | Observable<TripCancelResponse>
    | TripCancelResponse;

  updateTrip(
    request: UpdateTripRequest,
  ):
    | Promise<TripUpdateResponse>
    | Observable<TripUpdateResponse>
    | TripUpdateResponse;

  updateTripProcess(
    request: UpdateTripProcessRequest,
  ):
    | Promise<TripUpdateResponse>
    | Observable<TripUpdateResponse>
    | TripUpdateResponse;

  itemNotReceived(
    request: RequestWithTripId,
  ): Promise<Order> | Observable<Order> | Order;

  calculateOngoingCOD(
    request: RequestWithAgentId,
  ):
    | Promise<CalculateOngoingCODResponse>
    | Observable<CalculateOngoingCODResponse>
    | CalculateOngoingCODResponse;

  calculateOngoingVolume(
    request: RequestWithAgentId,
  ):
    | Promise<CalculateOngoingVolumeResponse>
    | Observable<CalculateOngoingVolumeResponse>
    | CalculateOngoingVolumeResponse;

  calculateOngoingWeight(
    request: CalculateOngoingWeightRequest,
  ):
    | Promise<CalculateOngoingWeightResponse>
    | Observable<CalculateOngoingWeightResponse>
    | CalculateOngoingWeightResponse;

  removeFromPool(
    request: RemoveFromPoolRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getTripsCount(
    request: GetTripsCountRequest,
  ):
    | Promise<GetTripsCountResponse>
    | Observable<GetTripsCountResponse>
    | GetTripsCountResponse;
}

export function TripsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getTrip',
      'tripEnroute',
      'tripDroppedOff',
      'tripUnableToDeliver',
      'tripUnableToPickup',
      'getPhotos',
      'addPhoto',
      'deletePhoto',
      'getCustomizedAvailableOrders',
      'getAgentPrefAvailableOrders',
      'getOpsPrefAvailableOrders',
      'refreshPoolOrders',
      'fetchTrips',
      'cancelTrip',
      'updateTrip',
      'updateTripProcess',
      'itemNotReceived',
      'calculateOngoingCOD',
      'calculateOngoingVolume',
      'calculateOngoingWeight',
      'removeFromPool',
      'getTripsCount',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Trips', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Trips', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRIPS_SERVICE_NAME = 'Trips';

export interface DispatchesClient {
  dispatchOrderCommand(
    request: DispatchOrderCommandRequest,
  ): Observable<DispatchOrderCommandResponse>;

  dispatchBundle(
    request: DispatchBundleRequest,
  ): Observable<DispatchBundleResponse>;

  getMyBestRoute(
    request: GetMyBestRouteRequest,
  ): Observable<GetMyBestRouteResponse>;
}

export interface DispatchesController {
  dispatchOrderCommand(
    request: DispatchOrderCommandRequest,
  ):
    | Promise<DispatchOrderCommandResponse>
    | Observable<DispatchOrderCommandResponse>
    | DispatchOrderCommandResponse;

  dispatchBundle(
    request: DispatchBundleRequest,
  ):
    | Promise<DispatchBundleResponse>
    | Observable<DispatchBundleResponse>
    | DispatchBundleResponse;

  getMyBestRoute(
    request: GetMyBestRouteRequest,
  ):
    | Promise<GetMyBestRouteResponse>
    | Observable<GetMyBestRouteResponse>
    | GetMyBestRouteResponse;
}

export function DispatchesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'dispatchOrderCommand',
      'dispatchBundle',
      'getMyBestRoute',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Dispatches', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Dispatches', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const DISPATCHES_SERVICE_NAME = 'Dispatches';

export interface BundlesClient {
  getBundle(request: GetBundleRequest): Observable<GetBundleResponse>;

  getBundles(request: GetBundlesRequest): Observable<GetBundlesResponse>;

  updateBundle(request: UpdateBundleRequest): Observable<UpdateBundleResponse>;

  addTripToBundle(
    request: AddTripToBundleRequest,
  ): Observable<AddTripToBundleResponse>;

  removeTripFromBundle(
    request: RemoveTripFromBundleRequest,
  ): Observable<RemoveTripFromBundleResponse>;

  acceptBundle(request: AcceptBundleRequest): Observable<AcceptBundleResponse>;
}

export interface BundlesController {
  getBundle(
    request: GetBundleRequest,
  ):
    | Promise<GetBundleResponse>
    | Observable<GetBundleResponse>
    | GetBundleResponse;

  getBundles(
    request: GetBundlesRequest,
  ):
    | Promise<GetBundlesResponse>
    | Observable<GetBundlesResponse>
    | GetBundlesResponse;

  updateBundle(
    request: UpdateBundleRequest,
  ):
    | Promise<UpdateBundleResponse>
    | Observable<UpdateBundleResponse>
    | UpdateBundleResponse;

  addTripToBundle(
    request: AddTripToBundleRequest,
  ):
    | Promise<AddTripToBundleResponse>
    | Observable<AddTripToBundleResponse>
    | AddTripToBundleResponse;

  removeTripFromBundle(
    request: RemoveTripFromBundleRequest,
  ):
    | Promise<RemoveTripFromBundleResponse>
    | Observable<RemoveTripFromBundleResponse>
    | RemoveTripFromBundleResponse;

  acceptBundle(
    request: AcceptBundleRequest,
  ):
    | Promise<AcceptBundleResponse>
    | Observable<AcceptBundleResponse>
    | AcceptBundleResponse;
}

export function BundlesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getBundle',
      'getBundles',
      'updateBundle',
      'addTripToBundle',
      'removeTripFromBundle',
      'acceptBundle',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Bundles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Bundles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const BUNDLES_SERVICE_NAME = 'Bundles';

export interface JobsClient {
  getJob(request: GetJobRequest): Observable<Job>;

  getJobs(request: GetJobsRequest): Observable<GetJobsResponse>;

  getAvailableJobs(
    request: GetAvailableJobsRequest,
  ): Observable<GetJobsResponse>;

  createJob(request: Job): Observable<Job>;

  updateJob(request: Job): Observable<Job>;

  acceptJob(request: AcceptJobRequest): Observable<Job>;

  finishJob(request: FinishJobRequest): Observable<Job>;

  cancelJob(request: CancelJobRequest): Observable<Job>;

  addOrderToJob(request: AddOrderToJobRequest): Observable<Job>;

  distributeOrdersToJobs(
    request: DistributeOrdersToJobsRequest,
  ): Observable<DistributeOrdersToJobsResponse>;

  getTopUnfinishedJobs(request: GetJobsRequest): Observable<GetJobsResponse>;

  instantAssignOrdersToJobs(
    request: InstantAssignOrdersToJobsRequest,
  ): Observable<InstantAssignOrdersToJobsResponse>;

  standby(request: StandbyJobRequest): Observable<Job>;

  sendAttendanceCheckNotifications(
    request: EmptyRequest,
  ): Observable<EmptyResponse>;

  releaseJob(request: ReleaseJobRequest): Observable<Job>;

  orsolverNormalJobDistribution(
    request: EmptyRequest,
  ): Observable<EmptyResponse>;

  orsolverInstantJobDistribution(
    request: EmptyRequest,
  ): Observable<EmptyResponse>;
}

export interface JobsController {
  getJob(request: GetJobRequest): Promise<Job> | Observable<Job> | Job;

  getJobs(
    request: GetJobsRequest,
  ): Promise<GetJobsResponse> | Observable<GetJobsResponse> | GetJobsResponse;

  getAvailableJobs(
    request: GetAvailableJobsRequest,
  ): Promise<GetJobsResponse> | Observable<GetJobsResponse> | GetJobsResponse;

  createJob(request: Job): Promise<Job> | Observable<Job> | Job;

  updateJob(request: Job): Promise<Job> | Observable<Job> | Job;

  acceptJob(request: AcceptJobRequest): Promise<Job> | Observable<Job> | Job;

  finishJob(request: FinishJobRequest): Promise<Job> | Observable<Job> | Job;

  cancelJob(request: CancelJobRequest): Promise<Job> | Observable<Job> | Job;

  addOrderToJob(
    request: AddOrderToJobRequest,
  ): Promise<Job> | Observable<Job> | Job;

  distributeOrdersToJobs(
    request: DistributeOrdersToJobsRequest,
  ):
    | Promise<DistributeOrdersToJobsResponse>
    | Observable<DistributeOrdersToJobsResponse>
    | DistributeOrdersToJobsResponse;

  getTopUnfinishedJobs(
    request: GetJobsRequest,
  ): Promise<GetJobsResponse> | Observable<GetJobsResponse> | GetJobsResponse;

  instantAssignOrdersToJobs(
    request: InstantAssignOrdersToJobsRequest,
  ):
    | Promise<InstantAssignOrdersToJobsResponse>
    | Observable<InstantAssignOrdersToJobsResponse>
    | InstantAssignOrdersToJobsResponse;

  standby(request: StandbyJobRequest): Promise<Job> | Observable<Job> | Job;

  sendAttendanceCheckNotifications(
    request: EmptyRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  releaseJob(request: ReleaseJobRequest): Promise<Job> | Observable<Job> | Job;

  orsolverNormalJobDistribution(
    request: EmptyRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  orsolverInstantJobDistribution(
    request: EmptyRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function JobsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getJob',
      'getJobs',
      'getAvailableJobs',
      'createJob',
      'updateJob',
      'acceptJob',
      'finishJob',
      'cancelJob',
      'addOrderToJob',
      'distributeOrdersToJobs',
      'getTopUnfinishedJobs',
      'instantAssignOrdersToJobs',
      'standby',
      'sendAttendanceCheckNotifications',
      'releaseJob',
      'orsolverNormalJobDistribution',
      'orsolverInstantJobDistribution',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Jobs', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Jobs', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const JOBS_SERVICE_NAME = 'Jobs';

export interface DeliveryNotesClient {
  createDeliveryNote(
    request: CreateDeliveryNoteRequest,
  ): Observable<BasicResponseStatus>;

  getDeliveryNotes(
    request: GetDeliveryNotesRequest,
  ): Observable<GetDeliveryNotesResponse>;

  updateDeliveryNoteImage(
    request: UpdateDeliveryNoteImageRequest,
  ): Observable<BasicResponseStatus>;

  rejectDeliveryNote(
    request: RejectDeliveryNoteRequest,
  ): Observable<BasicResponseStatus>;

  approveDeliveryNote(
    request: ApproveDeliveryNoteRequest,
  ): Observable<BasicResponseStatus>;
}

export interface DeliveryNotesController {
  createDeliveryNote(
    request: CreateDeliveryNoteRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  getDeliveryNotes(
    request: GetDeliveryNotesRequest,
  ):
    | Promise<GetDeliveryNotesResponse>
    | Observable<GetDeliveryNotesResponse>
    | GetDeliveryNotesResponse;

  updateDeliveryNoteImage(
    request: UpdateDeliveryNoteImageRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  rejectDeliveryNote(
    request: RejectDeliveryNoteRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  approveDeliveryNote(
    request: ApproveDeliveryNoteRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;
}

export function DeliveryNotesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'createDeliveryNote',
      'getDeliveryNotes',
      'updateDeliveryNoteImage',
      'rejectDeliveryNote',
      'approveDeliveryNote',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('DeliveryNotes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('DeliveryNotes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const DELIVERY_NOTES_SERVICE_NAME = 'DeliveryNotes';

export interface ClustersClient {
  getClusters(request: GetClustersRequest): Observable<Cluster>;

  getBundleSettings(
    request: GetBundleSettingsRequest,
  ): Observable<GetBundleSettingsResponse>;

  updateBundleSetting(request: BundleSetting): Observable<BundleSetting>;
}

export interface ClustersController {
  getClusters(request: GetClustersRequest): Observable<Cluster>;

  getBundleSettings(
    request: GetBundleSettingsRequest,
  ):
    | Promise<GetBundleSettingsResponse>
    | Observable<GetBundleSettingsResponse>
    | GetBundleSettingsResponse;

  updateBundleSetting(
    request: BundleSetting,
  ): Promise<BundleSetting> | Observable<BundleSetting> | BundleSetting;
}

export function ClustersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getClusters',
      'getBundleSettings',
      'updateBundleSetting',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Clusters', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Clusters', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CLUSTERS_SERVICE_NAME = 'Clusters';

export interface PastSurgeClient {
  calculatePastSurge(
    request: CalculatePastSurgeRequest,
  ): Observable<BasicResponseStatus>;

  updateSIPFactor(
    request: UpdateSIPFactorRequest,
  ): Observable<BasicResponseStatus>;
}

export interface PastSurgeController {
  calculatePastSurge(
    request: CalculatePastSurgeRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  updateSIPFactor(
    request: UpdateSIPFactorRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;
}

export function PastSurgeControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['calculatePastSurge', 'updateSIPFactor'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('PastSurge', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('PastSurge', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PAST_SURGE_SERVICE_NAME = 'PastSurge';

export interface WarehousesClient {
  inboundOrder(request: InboundOrderRequest): Observable<Order>;

  releaseOrder(request: ReleaseOrderRequest): Observable<Order>;

  rereleaseOrdersByWarehouse(
    request: ReleaseOrderRequest,
  ): Observable<BasicResponseStatus>;

  addOrderToWarehouse(
    request: InboundOrderRequest,
  ): Observable<BasicResponseStatus>;

  removeOrderFromWarehouse(
    request: RequestWithOrderId,
  ): Observable<BasicResponseStatus>;

  getInboundAgents(
    request: GetInboundAgentsRequest,
  ): Observable<GetInboundAgentsResponse>;

  updateInboundAgentData(
    request: UpdateInboundAgentDataRequest,
  ): Observable<EmptyResponse>;

  getInboundAgentData(
    request: GetInboundAgentDataRequest,
  ): Observable<GetInboundAgentDataResponse>;
}

export interface WarehousesController {
  inboundOrder(
    request: InboundOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  releaseOrder(
    request: ReleaseOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  rereleaseOrdersByWarehouse(
    request: ReleaseOrderRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  addOrderToWarehouse(
    request: InboundOrderRequest,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  removeOrderFromWarehouse(
    request: RequestWithOrderId,
  ):
    | Promise<BasicResponseStatus>
    | Observable<BasicResponseStatus>
    | BasicResponseStatus;

  getInboundAgents(
    request: GetInboundAgentsRequest,
  ):
    | Promise<GetInboundAgentsResponse>
    | Observable<GetInboundAgentsResponse>
    | GetInboundAgentsResponse;

  updateInboundAgentData(
    request: UpdateInboundAgentDataRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getInboundAgentData(
    request: GetInboundAgentDataRequest,
  ):
    | Promise<GetInboundAgentDataResponse>
    | Observable<GetInboundAgentDataResponse>
    | GetInboundAgentDataResponse;
}

export function WarehousesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'inboundOrder',
      'releaseOrder',
      'rereleaseOrdersByWarehouse',
      'addOrderToWarehouse',
      'removeOrderFromWarehouse',
      'getInboundAgents',
      'updateInboundAgentData',
      'getInboundAgentData',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Warehouses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Warehouses', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const WAREHOUSES_SERVICE_NAME = 'Warehouses';

export interface ParcelsClient {
  fetchParcels(request: FetchParcelsRequest): Observable<FetchParcelsResponse>;

  updateParcels(
    request: UpdateParcelsRequest,
  ): Observable<FetchParcelsResponse>;
}

export interface ParcelsController {
  fetchParcels(
    request: FetchParcelsRequest,
  ):
    | Promise<FetchParcelsResponse>
    | Observable<FetchParcelsResponse>
    | FetchParcelsResponse;

  updateParcels(
    request: UpdateParcelsRequest,
  ):
    | Promise<FetchParcelsResponse>
    | Observable<FetchParcelsResponse>
    | FetchParcelsResponse;
}

export function ParcelsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['fetchParcels', 'updateParcels'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Parcels', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Parcels', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PARCELS_SERVICE_NAME = 'Parcels';

export interface ItemsClient {
  getItems(request: GetItemsRequest): Observable<GetItemsResponse>;
}

export interface ItemsController {
  getItems(
    request: GetItemsRequest,
  ):
    | Promise<GetItemsResponse>
    | Observable<GetItemsResponse>
    | GetItemsResponse;
}

export function ItemsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getItems'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Items', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Items', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ITEMS_SERVICE_NAME = 'Items';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
