/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'isend';

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: OrderTrack | undefined;
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
  errorMessage: string;
}

export interface OrderResponse {
  error: number;
  message: string;
  data: { [key: string]: string };
}

export interface OrderResponse_DataEntry {
  key: string;
  value: string;
}

export interface OrderTrack {
  trackingNo: string;
  status: string;
  timeline: UpdateTripTimeline[];
}

export interface UpdateTripTimeline {
  status: string;
  time: string;
  location: string;
  driver: UpdateTripTimeline_Driver | undefined;
  coordinates: UpdateTripTimeline_Coordinates | undefined;
  proofs: string[];
}

export interface UpdateTripTimeline_Driver {
  name: string;
}

export interface UpdateTripTimeline_Coordinates {
  lat: string;
  long: string;
}

export const ISEND_PACKAGE_NAME = 'isend';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
