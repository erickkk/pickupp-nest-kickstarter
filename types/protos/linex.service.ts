/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';
import { Order, Shipment } from './linex.model';

export const protobufPackage = 'linex';

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface GetOrderRequest {
  id: string;
}

export interface GetOrdersRequest {
  orderIds: string[];
}

export interface GetOrdersResponse {
  orders: Order[];
}

export interface GetWaybillsRequest {
  orderNumbers: string[];
}

export interface GetWaybillsResponse {
  waybills: GetWaybillsResponse_Waybill[];
}

export interface GetWaybillsResponse_Waybill {
  hawbNumber: string;
  orderNumber: string;
  labelUrl: string;
}

export interface GetShipmentResponse {
  linexOrders: Shipment[];
}

export interface GetShipmentRequest {
  id: string;
}

export interface GetInvoicesRequest {
  orderNumbers: string[];
}

export interface GetInvoicesResponse {
  invoices: GetInvoicesResponse_Invoice[];
}

export interface GetInvoicesResponse_Invoice {
  hawbNumber: string;
  orderNumber: string;
  invoiceUrl: string;
}

export const LINEX_PACKAGE_NAME = 'linex';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<Order>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;

  getShipments(request: GetShipmentRequest): Observable<GetShipmentResponse>;

  getWaybills(request: GetWaybillsRequest): Observable<GetWaybillsResponse>;

  getInvoices(request: GetInvoicesRequest): Observable<GetInvoicesResponse>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ): Promise<Order> | Observable<Order> | Order;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;

  getShipments(
    request: GetShipmentRequest,
  ):
    | Promise<GetShipmentResponse>
    | Observable<GetShipmentResponse>
    | GetShipmentResponse;

  getWaybills(
    request: GetWaybillsRequest,
  ):
    | Promise<GetWaybillsResponse>
    | Observable<GetWaybillsResponse>
    | GetWaybillsResponse;

  getInvoices(
    request: GetInvoicesRequest,
  ):
    | Promise<GetInvoicesResponse>
    | Observable<GetInvoicesResponse>
    | GetInvoicesResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOrder',
      'getOrders',
      'getShipments',
      'getWaybills',
      'getInvoices',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
