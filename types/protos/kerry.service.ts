/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { GetQuoteRequest as GetQuoteRequest1 } from './merchant.service';
import { Order, Waybill } from './kerry.model';

export const protobufPackage = 'kerry';

export interface GetQuoteRequest {
  quote: GetQuoteRequest1 | undefined;
}

export interface GetQuoteResponse {
  outsource: string;
  available: boolean;
  priority: number;
  price: string;
  metadata: { [key: string]: string };
}

export interface GetQuoteResponse_MetadataEntry {
  key: string;
  value: string;
}

export interface GetOrderRequest {
  id: string;
}

export interface GetOrderResponse {
  order: Order | undefined;
}

export interface GetOrdersRequest {
  orderIds: string[];
}

export interface GetOrdersResponse {
  orders: Order[];
}

export interface GetWaybillsRequest {
  orderNumbers: string[];
}

export interface GetWaybillsResponse {
  waybills: Waybill[];
}

export interface GetWarehouseResponse {
  id: string;
}

export interface UpdateKerryOrderNumberRequest {
  url: string;
}

export interface UpdateKerryOrderNumberResponse {
  count: number;
}

export interface GetAvailableOrderNumberCountResponse {
  count: number;
}

export interface EmptyRequest {}

export interface EmptyResponse {}

export const KERRY_PACKAGE_NAME = 'kerry';

export interface QuoteClient {
  getQuote(request: GetQuoteRequest): Observable<GetQuoteResponse>;
}

export interface QuoteController {
  getQuote(
    request: GetQuoteRequest,
  ):
    | Promise<GetQuoteResponse>
    | Observable<GetQuoteResponse>
    | GetQuoteResponse;
}

export function QuoteControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getQuote'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Quote', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const QUOTE_SERVICE_NAME = 'Quote';

export interface OrdersClient {
  getOrder(request: GetOrderRequest): Observable<GetOrderResponse>;

  getOrders(request: GetOrdersRequest): Observable<GetOrdersResponse>;

  getWaybills(request: GetWaybillsRequest): Observable<GetWaybillsResponse>;

  checkAvailableKerryOrderNumber(
    request: EmptyRequest,
  ): Observable<EmptyResponse>;

  getWarehouse(request: EmptyRequest): Observable<GetWarehouseResponse>;
}

export interface OrdersController {
  getOrder(
    request: GetOrderRequest,
  ):
    | Promise<GetOrderResponse>
    | Observable<GetOrderResponse>
    | GetOrderResponse;

  getOrders(
    request: GetOrdersRequest,
  ):
    | Promise<GetOrdersResponse>
    | Observable<GetOrdersResponse>
    | GetOrdersResponse;

  getWaybills(
    request: GetWaybillsRequest,
  ):
    | Promise<GetWaybillsResponse>
    | Observable<GetWaybillsResponse>
    | GetWaybillsResponse;

  checkAvailableKerryOrderNumber(
    request: EmptyRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getWarehouse(
    request: EmptyRequest,
  ):
    | Promise<GetWarehouseResponse>
    | Observable<GetWarehouseResponse>
    | GetWarehouseResponse;
}

export function OrdersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getOrder',
      'getOrders',
      'getWaybills',
      'checkAvailableKerryOrderNumber',
      'getWarehouse',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Orders', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDERS_SERVICE_NAME = 'Orders';

export interface TripClient {
  updateTrip(request: EmptyRequest): Observable<EmptyResponse>;
}

export interface TripController {
  updateTrip(
    request: EmptyRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function TripControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['updateTrip'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Trip', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Trip', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRIP_SERVICE_NAME = 'Trip';

export interface OrderNumberClient {
  updateKerryOrderNumber(
    request: UpdateKerryOrderNumberRequest,
  ): Observable<UpdateKerryOrderNumberResponse>;

  getAvailableOrderNumberCount(
    request: EmptyRequest,
  ): Observable<GetAvailableOrderNumberCountResponse>;
}

export interface OrderNumberController {
  updateKerryOrderNumber(
    request: UpdateKerryOrderNumberRequest,
  ):
    | Promise<UpdateKerryOrderNumberResponse>
    | Observable<UpdateKerryOrderNumberResponse>
    | UpdateKerryOrderNumberResponse;

  getAvailableOrderNumberCount(
    request: EmptyRequest,
  ):
    | Promise<GetAvailableOrderNumberCountResponse>
    | Observable<GetAvailableOrderNumberCountResponse>
    | GetAvailableOrderNumberCountResponse;
}

export function OrderNumberControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'updateKerryOrderNumber',
      'getAvailableOrderNumberCount',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('OrderNumber', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('OrderNumber', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ORDER_NUMBER_SERVICE_NAME = 'OrderNumber';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
