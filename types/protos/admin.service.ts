/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import {
  AdministratorStatus,
  Permissions,
  Administrator,
  CurrentUser,
} from './admin.model';
import { Observable } from 'rxjs';

export const protobufPackage = 'admin';

export interface AuthRequest {
  token: string;
}

export interface AuthResponse {
  id: number;
  email: string;
  permissions: Permissions | undefined;
  firstName: string;
  lastName: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface LoginResponse {
  token: string;
  admin: Administrator | undefined;
}

export interface LogoutRequest {
  token: string;
}

export interface LogoutResponse {}

export interface GetAdminsRequest {
  statuses: AdministratorStatus[];
  ids: number[];
  limit: number;
  offset: number;
  query: string;
  sortBy: string;
  sortDirection: string;
}

export interface GetAdminsResponse {
  administrators: Administrator[];
  total: number;
}

export interface GetAdminRequest {
  id: number;
}

export interface CreateAdminRequest {
  email: string;
  firstName: string;
  lastName: string;
  region: string;
}

export interface ResetPasswordRequest {
  currentUser: CurrentUser | undefined;
  password: string;
}

export interface UpdateAdminRequest {
  currentUser: CurrentUser | undefined;
  id: string;
  fields: UpdateAdminRequest_UpdateFields | undefined;
}

export interface UpdateAdminRequest_UpdateFields {
  firstName: string;
  lastName: string;
  status: AdministratorStatus;
  termVersion: number;
}

export interface GetPermissionTypesRequest {
  currentUser: CurrentUser | undefined;
}

export interface GetPermissionTypesResponse {
  permissions: Permissions | undefined;
}

export interface UpdatePermissionRequest {
  id: number;
  permissions: Permissions | undefined;
}

export interface GetAdminPermissionsRequest {
  id: number;
}

export const ADMIN_PACKAGE_NAME = 'admin';

export interface SessionsClient {
  auth(request: AuthRequest): Observable<AuthResponse>;

  login(request: LoginRequest): Observable<LoginResponse>;

  logout(request: LogoutRequest): Observable<LogoutResponse>;
}

export interface SessionsController {
  auth(
    request: AuthRequest,
  ): Promise<AuthResponse> | Observable<AuthResponse> | AuthResponse;

  login(
    request: LoginRequest,
  ): Promise<LoginResponse> | Observable<LoginResponse> | LoginResponse;

  logout(
    request: LogoutRequest,
  ): Promise<LogoutResponse> | Observable<LogoutResponse> | LogoutResponse;
}

export function SessionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['auth', 'login', 'logout'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Sessions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const SESSIONS_SERVICE_NAME = 'Sessions';

export interface AdministratorsClient {
  getAdmins(request: GetAdminsRequest): Observable<GetAdminsResponse>;

  getAdmin(request: GetAdminRequest): Observable<Administrator>;

  createAdmin(request: CreateAdminRequest): Observable<Administrator>;

  resetPassword(request: ResetPasswordRequest): Observable<Administrator>;

  updateAdmin(request: UpdateAdminRequest): Observable<Administrator>;
}

export interface AdministratorsController {
  getAdmins(
    request: GetAdminsRequest,
  ):
    | Promise<GetAdminsResponse>
    | Observable<GetAdminsResponse>
    | GetAdminsResponse;

  getAdmin(
    request: GetAdminRequest,
  ): Promise<Administrator> | Observable<Administrator> | Administrator;

  createAdmin(
    request: CreateAdminRequest,
  ): Promise<Administrator> | Observable<Administrator> | Administrator;

  resetPassword(
    request: ResetPasswordRequest,
  ): Promise<Administrator> | Observable<Administrator> | Administrator;

  updateAdmin(
    request: UpdateAdminRequest,
  ): Promise<Administrator> | Observable<Administrator> | Administrator;
}

export function AdministratorsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getAdmins',
      'getAdmin',
      'createAdmin',
      'resetPassword',
      'updateAdmin',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Administrators', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Administrators', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ADMINISTRATORS_SERVICE_NAME = 'Administrators';

export interface AdminPermissionsClient {
  getPermissionTypes(
    request: GetPermissionTypesRequest,
  ): Observable<GetPermissionTypesResponse>;

  updatePermission(request: UpdatePermissionRequest): Observable<Administrator>;

  getAdminPermissions(
    request: GetAdminPermissionsRequest,
  ): Observable<GetPermissionTypesResponse>;
}

export interface AdminPermissionsController {
  getPermissionTypes(
    request: GetPermissionTypesRequest,
  ):
    | Promise<GetPermissionTypesResponse>
    | Observable<GetPermissionTypesResponse>
    | GetPermissionTypesResponse;

  updatePermission(
    request: UpdatePermissionRequest,
  ): Promise<Administrator> | Observable<Administrator> | Administrator;

  getAdminPermissions(
    request: GetAdminPermissionsRequest,
  ):
    | Promise<GetPermissionTypesResponse>
    | Observable<GetPermissionTypesResponse>
    | GetPermissionTypesResponse;
}

export function AdminPermissionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getPermissionTypes',
      'updatePermission',
      'getAdminPermissions',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('AdminPermissions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('AdminPermissions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const ADMIN_PERMISSIONS_SERVICE_NAME = 'AdminPermissions';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
