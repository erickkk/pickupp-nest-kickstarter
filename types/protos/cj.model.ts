/* eslint-disable */
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Timestamp } from './google/protobuf/timestamp';

export const protobufPackage = 'cj';

export interface OrderResponse {
  reqDt: string;
  invcNo: string;
  custUseNo: string;
  RESULT: string;
  ERROR: string;
  DESTCODE: string;
}

export interface Order {
  id: string;
  orderId: string;
  orderNumber: string;
  status: string;
  orderResponse: OrderResponse | undefined;
  trackResponse: TrackRequest[];
  createdAt: Timestamp | undefined;
  updatedAt: Timestamp | undefined;
}

export interface TrackRequest {
  reqId: string;
  custId: string;
  custUseNo: string;
  invcNo: string;
  rtinvNo: string;
  rcptDv: string;
  crgSt: string;
  crgStNm: string;
  stsMsg: string;
  rsnCd: string;
  rsnNm: string;
  detailRsn: string;
  workBranNm: string;
  scanDt: string;
  WEIGHT: string;
}

export interface TrackResponse {
  reqDt: string;
  reqId: string;
  invcNo: string;
  custUseNo: string;
  RESULT: string;
  ERROR: string;
}

export const CJ_PACKAGE_NAME = 'cj';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
