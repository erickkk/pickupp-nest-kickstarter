/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';
import * as Long from 'long';
import * as _m0 from 'protobufjs/minimal';
import { Observable } from 'rxjs';
import { CurrentUser } from './merchant.model';
import {
  PaymentProfile,
  CreditCard,
  AtomePaymentItem,
  AtomePaymentCustomerInfo,
  AtomePaymentAddress,
  Transaction,
  OrderTotalPrice,
  Invoice,
  ChargePlan,
  Fee,
  Currency,
  FeeType,
  MonthlySummary,
} from './wallet.model';
import { QuoteFee, QuoteFeesResponse } from './order.model';

export const protobufPackage = 'wallet';

export interface EmptyResponse {}

export interface SuccessResponse {
  success: boolean;
}

export interface RequestWithEntityId {
  entityId: string;
}

export interface RequestWithId {
  id: number;
}

export interface CompleteExternalPaymentRequest {
  transactionId: string;
}

export interface GetPricingPlanRequest {
  topupAmount: string;
}

export interface GetPricingPlanResponse {
  name: string;
  cashback: number;
}

export interface UpdatePaymentIntentRequest {
  transactionId: string;
  description: string;
  vendor: string;
}

export interface FetchActiveMerchantsRequest {
  year: number;
  month: number;
}

export interface FetchActiveMerchantsResponse {
  entityIds: number[];
}

export interface CreateMolPaymentRequest {
  orderid: string;
  amount: string;
  tranID: string;
  domain: string;
  status: string;
  appcode: string;
  skey: string;
  currency: string;
  paydate: string;
  extraP: string;
  errorCode: string;
  errorDesc: string;
  nbcb: string;
  channel: string;
}

export interface CreateMolPaymentResponse {
  status: string;
}

export interface CreatePaypalPaymentRequest {
  currentUser: CurrentUser | undefined;
  amount: string;
}

export interface CreatePaypalPaymentResponse {
  id: string;
}

export interface ExecutePaypalPaymentRequest {
  payment: ExecutePaypalPaymentRequest_Payment | undefined;
}

export interface ExecutePaypalPaymentRequest_Payment {
  paymentId: string;
  payerId: string;
}

export interface ExecutePaypalPaymentResponse {
  profile: PaymentProfile | undefined;
}

export interface AddCreditByCreditCardRequest {
  currentUser: CurrentUser | undefined;
  profileId: number;
  amount: string;
}

export interface AdminCreditRequest {
  currentUser: CurrentUser | undefined;
  amount: string;
  description: string;
  type: string;
  expiry: boolean;
  expiryAt: string;
  orderId: string;
  paymentProfileId: number;
}

export interface AdminUpdateProfileExpiryRequest {
  paymentProfileId: number;
  creditExpireAt: string;
}

export interface GetEntityProfilesRequest {
  entityId: string;
}

export interface GetEntityProfilesResponse {
  credit: PaymentProfile | undefined;
  creditCard: PaymentProfile | undefined;
  expiryProfiles: PaymentProfile[];
  monthlyProfiles: PaymentProfile[];
  creditLine: PaymentProfile | undefined;
}

export interface AvailableProfileRequest {
  currentUser: CurrentUser | undefined;
  amount: string;
}

export interface CreateCreditCardRequest {
  user: CurrentUser | undefined;
  token: string;
  expireDate: string;
  vendor: string;
}

export interface ProcessPaymentRequest {
  entityId: number;
  orderId: string;
  orderNumber: string;
  amount: string;
  description: string;
  orderStatus: string;
  fees: QuoteFee[];
  pickupCity: string;
  dropoffCity: string;
}

export interface ProcessPaymentResponse {
  details: ProcessPaymentResponse_PaymentDetail[];
}

export interface ProcessPaymentResponse_PaymentDetail {
  id: number;
  type: string;
  amount: string;
}

export interface GetCreditProfilesByEntityIdsRequest {
  ids: number[];
}

export interface GetCreditProfilesByEntityIdsResponse {
  profiles: PaymentProfile[];
}

export interface CreateBulkPaymentRequest {
  currentUser: CurrentUser | undefined;
  payments: CreateBulkPaymentRequest_PaymentRequest[];
}

export interface CreateBulkPaymentRequest_PaymentRequest {
  orderId: string;
  description: string;
  amount: string;
  pickupCity: string;
  dropoffCity: string;
}

export interface RenewMonthlyPlanRequest {
  currentUser: CurrentUser | undefined;
  amount: string;
}

export interface CheckMonthlyCreditRequest {}

export interface RefundOrderRequest {
  orderId: string;
  entityId: number;
  amount: string;
  description: string;
  purchaseOrderId: string;
}

export interface RenewCreditLineRequest {
  entityIds: string[];
  creditLine: string;
}

export interface CreateExpiryCreditTransactionRequest {
  startTime: string;
  endTime: string;
}

export interface GetCreditCardsRequest {
  currentUser: CurrentUser | undefined;
}

export interface GetCreditCardsResponse {
  cards: CreditCard[];
}

export interface ChargeOnOrderRequest {
  orderId: string;
  entityId: number;
  orderNumber: string;
}

export interface ChargeOnDifferenceRequest {
  orderId: string;
  entityId: number;
  newPrice: string;
  orderNumber: string;
  paymentProfileId: string;
  orderStatus: string;
  description: string;
}

export interface ChargeOnDifferenceResponse {
  differencePrice: string;
  taxPrice: string;
}

export interface CreateChargeRequest {
  entityId: number;
  profileId: number;
  amount: string;
  tax: string;
  description: string;
  orderId: string;
  purchaseOrderId: string;
  paymentToken: string;
  referenceId: string;
  items: AtomePaymentItem[];
  origin: string;
  customerInfo: AtomePaymentCustomerInfo | undefined;
  billingAddress: AtomePaymentAddress | undefined;
  shippingAddress: AtomePaymentAddress | undefined;
}

export interface GetProfilesRequest {
  paymentType: string;
  excludeExpired: boolean;
  limit: number;
  offset: number;
  vendor: string;
  entityId: number;
}

export interface GetProfilesResponse {
  profiles: PaymentProfile[];
}

export interface CreateHiTrustPaymentRequest {
  type: string;
  ordernumber: string;
  storeid: string;
  orderdesc: string;
  currency: string;
  retcode: string;
  authCode: string;
  authRRN: string;
  orderstatus: string;
  approveamount: string;
  depositamount: string;
  credamount: string;
  orderdate: string;
  paybatchnumber: string;
  capDate: string;
  credbatchnumber: string;
  credcode: string;
  credRRN: string;
  creddate: string;
  eci: string;
  e06: string;
  e07: string;
  e08: string;
  e09: string;
  redemordernum: string;
  redemDiscountPoint: string;
  redemDiscountAmount: string;
  redemPurchaseAmount: string;
  redemBalancePoint: string;
  e10: string;
  trxToken: string;
  expiry: string;
  maskPan: string;
  pan: string;
  PAYWAY: string;
  txnmode: string;
}

export interface CreatePaymentIntentRequest {
  amount: string;
  vendor: string;
  entityId: number;
  description: string;
  tax: string;
  orderId: string;
  purchaseOrderId: string;
  paymentToken: string;
  referenceId: string;
  items: AtomePaymentItem[];
  origin: string;
  customerInfo: AtomePaymentCustomerInfo | undefined;
  billingAddress: AtomePaymentAddress | undefined;
  shippingAddress: AtomePaymentAddress | undefined;
  transactionVendor: string;
  type: string;
  paymentGateway: string;
  status: string;
  transactionId: string;
  clientIp: string;
}

export interface CreateHiTrustPaymentResponse {
  status: string;
}

export interface CreateTransactionRequest {
  tax: string;
  amount: string;
  entityId: string;
  description: string;
  purchaseOrderId: string;
  vendor: string;
  transactionVendor: string;
  paymentGateway: string;
  type: string;
  status: number;
}

export interface RefundTransactionRequest {
  paymentToken: string;
  description: string;
}

export interface GetReceiptResponse {
  path: string;
}

export interface GetReceiptRequest {
  entityId: number;
  transactionId: string;
}

export interface GetTransactionRequest {
  id: number;
  paymentToken: string;
  includeCharge: boolean;
  purchaseOrderId: string;
  includePaymentIntent: boolean;
}

export interface GetTransactionResponse {
  transaction: Transaction | undefined;
}

export interface GetTransactionsResponse {
  transactions: Transaction[];
  total: number;
}

export interface GetTransactionsRequest {
  entityId: number;
  limit: string;
  offset: string;
  order: string;
  startDate: string;
  endDate: string;
  statuses: string[];
  type: string;
  paymentGateways: string[];
  search: string;
  paymentProfileIds: string[];
  vendors: string[];
  ids: number[];
}

export interface UpdateTransactionRequest {
  transaction: Transaction | undefined;
}

export interface GetOrderTransactionsRequest {
  orderId: string;
  sortField: string;
  sortOrder: string;
}

export interface GetMultiOrderTransactionsRequest {
  orderIds: string[];
  sortField: string;
  sortOrder: string;
}

export interface GetProfileTransactionRequest {
  paymentProfileId: string;
}

export interface RefundToCreditCardRequest {
  currentUser: CurrentUser | undefined;
  id: string;
  amount: string;
  paymentToken: string;
}

export interface UpdateOrderCompletedTimeRequest {
  orderId: string;
  completedTime: string;
}

export interface TierResponse {
  id: number;
  planName: string;
  planPrice: string;
  creditAmount: string;
  isDefault: boolean;
  status: string;
  createdAt: string;
  updatedAt: string;
  discount: number;
  isPrimary: boolean;
}

export interface GetTiersRequest {}

export interface GetTiersResponse {
  tiers: TierResponse[];
  total: number;
}

export interface GetTierRequest {
  id: string;
}

export interface GetTierResponse {
  tier: TierResponse | undefined;
}

export interface CreateTierRequest {
  tier: CreateTierRequest_Tier | undefined;
}

export interface CreateTierRequest_Tier {
  planName: string;
  planPrice: string;
  creditAmount: string;
  isDefault: boolean;
  discount: string;
  status: string;
  isPrimary: boolean;
}

export interface CreateTierResponse {
  tier: TierResponse | undefined;
}

export interface UpdateTierRequest {
  id: string;
  tier: UpdateTierRequest_Tier | undefined;
}

export interface UpdateTierRequest_Tier {
  planName: string;
  planPrice: string;
  creditAmount: string;
  isDefault: boolean;
  discount: string;
  status: string;
  isPrimary: boolean;
}

export interface UpdateTierResponse {
  tier: TierResponse | undefined;
}

export interface BankSlipResponse {
  id: string;
  entityId: number;
  amount: string;
  link: string;
  slipDate: string;
  credit: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  description: string;
  method: string;
}

export interface GetBankSlipsRequest {
  currentUser: CurrentUser | undefined;
  sortBy: string[];
  limit: number;
  offset: number;
  status: string;
  startDate: string;
  endDate: string;
}

export interface GetBankSlipsResponse {
  slips: BankSlipResponse[];
  total: number;
}

export interface GetBankSlipRequest {
  id: string;
}

export interface GetBankSlipResponse {
  slip: BankSlipResponse | undefined;
}

export interface CreateBankSlipRequest {
  currentUser: CurrentUser | undefined;
  slip: CreateBankSlipRequest_BankSlip | undefined;
}

export interface CreateBankSlipRequest_BankSlip {
  amount: string;
  link: string;
  slipDate: string;
  description: string;
  method: string;
}

export interface CreateBankSlipResponse {
  slip: BankSlipResponse | undefined;
}

export interface UpdateBankSlipRequest {
  id: string;
  slip: UpdateBankSlipRequest_BankSlip | undefined;
}

export interface UpdateBankSlipRequest_BankSlip {
  entityId: number;
  amount: string;
  link: string;
  slipDate: string;
  credit: string;
  status: string;
  description: string;
  method: string;
}

export interface UpdateBankSlipResponse {
  slip: BankSlipResponse | undefined;
}

export interface GetTotalOrderResponse {
  orderTotalPrices: OrderTotalPrice[];
}

export interface EntityMonthYearRequest {
  entityId: number;
  month: number;
  year: number;
}

export interface MonthlyStatementResponse {
  details: { [key: string]: string };
}

export interface MonthlyStatementResponse_DetailsEntry {
  key: string;
  value: string;
}

export interface GenerateMonthlyStatementResponse {
  path: string;
}

export interface MonthlyTransactionsResponse {
  transactions: MonthlyTransactionsResponse_OrderTransaction[];
}

export interface MonthlyTransactionsResponse_OrderTransaction {
  transactionTime: string;
  amount: number;
  description: string;
  orderNumber: string;
  clientReferenceNumber: string;
  orderStatus: string;
  pickupContactPerson: string;
  pickupAddress: string;
  dropoffContactPerson: string;
  dropoffAddress: string;
  pickupNotes: string;
  width: number;
  length: number;
  height: number;
  dimension: number;
  weight: number;
}

export interface FetchInvoicesRequest {
  entityId: string;
  filter: { [key: string]: string };
}

export interface FetchInvoicesRequest_FilterEntry {
  key: string;
  value: string;
}

export interface FetchInvoicesResponse {
  list: Invoice[];
  total: number;
}

export interface CreateInvoiceRequest {
  entityId: number;
  startDate: string;
  endDate: string;
}

export interface GenerateInvoiceRequest {
  id: number;
  reference: string;
}

export interface GenerateInvoiceResponse {
  path: string;
}

export interface RequestWithInvoicesIds {
  invoiceIds: string[];
}

export interface GetChargePlansRequest {
  entityId: number;
  amount: number;
  pickupCity: string;
  dropoffCity: string;
}

export interface GetChargePlansResponse {
  plans: ChargePlan[];
}

export interface QuoteFeeRequest {
  declaredValue: string;
  name: string;
  count: number;
}

export interface QuoteFeesRequest {
  items: QuoteFeeRequest[];
  currency: string;
  city: string;
}

export interface GetFeesRequest {
  orderNumbers: string[];
}

export interface GetFeesResponse {
  fees: Fee[];
}

export interface CreateFeeRequest {
  orderId: string;
  orderNumber: string;
  fees: QuoteFee[];
}

export interface GetCurrencyRequest {
  inputCurrency: string;
}

export interface GetCurrenciesRequest {
  inputCurrency: GetCurrencyRequest[];
}

export interface UpsertCurrencyRequest {
  inputCurrency: string;
  rateToUsd: string;
}

export interface DeleteCurrencyRequest {
  id: string;
}

export interface GetCurrencyResponse {
  currency: Currency | undefined;
}

export interface GetCurrenciesResponse {
  currencies: Currency[];
}

export interface GetFeeTypeRequest {
  feeTypeName: string;
}

export interface GetFeeTypesRequest {
  feeTypeName: GetFeeTypeRequest[];
}

export interface UpsertFeeTypeRequest {
  feeTypeName: UpsertFeeTypeRequest_FeeTypes;
  settingName: string;
  isDisabled: boolean;
  feeTypeDisplayName: string;
}

export enum UpsertFeeTypeRequest_FeeTypes {
  pickupp_care = 0,
  UNRECOGNIZED = -1,
}

export interface RemoveFeeTypeRequest {
  id: string;
}

export interface GetFeeTypeResponse {
  feeType: FeeType | undefined;
}

export interface GetFeeTypesResponse {
  feeTypes: FeeType[];
}

export const WALLET_PACKAGE_NAME = 'wallet';

export interface ProfilesClient {
  getEntityProfiles(
    request: GetEntityProfilesRequest,
  ): Observable<GetEntityProfilesResponse>;

  getCreditProfilesByEntityIds(
    request: GetCreditProfilesByEntityIdsRequest,
  ): Observable<GetCreditProfilesByEntityIdsResponse>;

  createCreditCard(
    request: CreateCreditCardRequest,
  ): Observable<PaymentProfile>;

  availableProfile(
    request: AvailableProfileRequest,
  ): Observable<PaymentProfile>;

  processPayment(
    request: ProcessPaymentRequest,
  ): Observable<ProcessPaymentResponse>;

  adminAddCredit(request: AdminCreditRequest): Observable<SuccessResponse>;

  adminUpdateProfileExpiry(
    request: AdminUpdateProfileExpiryRequest,
  ): Observable<SuccessResponse>;

  createBulkPayment(
    request: CreateBulkPaymentRequest,
  ): Observable<PaymentProfile>;

  removeProfile(request: RequestWithId): Observable<PaymentProfile>;

  addCreditByCreditCard(
    request: AddCreditByCreditCardRequest,
  ): Observable<PaymentProfile>;

  renewMonthlyPlan(
    request: RenewMonthlyPlanRequest,
  ): Observable<PaymentProfile>;

  checkMonthlyCredit(
    request: CheckMonthlyCreditRequest,
  ): Observable<SuccessResponse>;

  refundOrder(request: RefundOrderRequest): Observable<SuccessResponse>;

  renewCreditLine(request: RenewCreditLineRequest): Observable<SuccessResponse>;

  createExpiryCreditTransaction(
    request: CreateExpiryCreditTransactionRequest,
  ): Observable<SuccessResponse>;

  createPaypalPayment(
    request: CreatePaypalPaymentRequest,
  ): Observable<CreatePaypalPaymentResponse>;

  executePaypalPayment(
    request: ExecutePaypalPaymentRequest,
  ): Observable<ExecutePaypalPaymentResponse>;

  getCreditCards(
    request: GetCreditCardsRequest,
  ): Observable<GetCreditCardsResponse>;

  createMolPayment(
    request: CreateMolPaymentRequest,
  ): Observable<CreateMolPaymentResponse>;

  fetchActiveMerchants(
    request: FetchActiveMerchantsRequest,
  ): Observable<FetchActiveMerchantsResponse>;

  chargeOnOrder(request: ChargeOnOrderRequest): Observable<PaymentProfile>;

  chargeOnDifference(
    request: ChargeOnDifferenceRequest,
  ): Observable<ChargeOnDifferenceResponse>;

  createCreditLine(request: RequestWithEntityId): Observable<SuccessResponse>;

  createCharge(request: CreateChargeRequest): Observable<Transaction>;

  getProfiles(request: GetProfilesRequest): Observable<GetProfilesResponse>;

  getChargePlans(
    request: GetChargePlansRequest,
  ): Observable<GetChargePlansResponse>;

  createHiTrustPayment(
    request: CreateHiTrustPaymentRequest,
  ): Observable<CreateHiTrustPaymentResponse>;

  createPaymentIntent(
    request: CreatePaymentIntentRequest,
  ): Observable<Transaction>;

  updatePaymentIntent(
    request: UpdatePaymentIntentRequest,
  ): Observable<Transaction>;

  getPricingPlan(
    request: GetPricingPlanRequest,
  ): Observable<GetPricingPlanResponse>;

  completeExternalPayment(
    request: CompleteExternalPaymentRequest,
  ): Observable<SuccessResponse>;
}

export interface ProfilesController {
  getEntityProfiles(
    request: GetEntityProfilesRequest,
  ):
    | Promise<GetEntityProfilesResponse>
    | Observable<GetEntityProfilesResponse>
    | GetEntityProfilesResponse;

  getCreditProfilesByEntityIds(
    request: GetCreditProfilesByEntityIdsRequest,
  ):
    | Promise<GetCreditProfilesByEntityIdsResponse>
    | Observable<GetCreditProfilesByEntityIdsResponse>
    | GetCreditProfilesByEntityIdsResponse;

  createCreditCard(
    request: CreateCreditCardRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  availableProfile(
    request: AvailableProfileRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  processPayment(
    request: ProcessPaymentRequest,
  ):
    | Promise<ProcessPaymentResponse>
    | Observable<ProcessPaymentResponse>
    | ProcessPaymentResponse;

  adminAddCredit(
    request: AdminCreditRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  adminUpdateProfileExpiry(
    request: AdminUpdateProfileExpiryRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  createBulkPayment(
    request: CreateBulkPaymentRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  removeProfile(
    request: RequestWithId,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  addCreditByCreditCard(
    request: AddCreditByCreditCardRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  renewMonthlyPlan(
    request: RenewMonthlyPlanRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  checkMonthlyCredit(
    request: CheckMonthlyCreditRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  refundOrder(
    request: RefundOrderRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  renewCreditLine(
    request: RenewCreditLineRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  createExpiryCreditTransaction(
    request: CreateExpiryCreditTransactionRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  createPaypalPayment(
    request: CreatePaypalPaymentRequest,
  ):
    | Promise<CreatePaypalPaymentResponse>
    | Observable<CreatePaypalPaymentResponse>
    | CreatePaypalPaymentResponse;

  executePaypalPayment(
    request: ExecutePaypalPaymentRequest,
  ):
    | Promise<ExecutePaypalPaymentResponse>
    | Observable<ExecutePaypalPaymentResponse>
    | ExecutePaypalPaymentResponse;

  getCreditCards(
    request: GetCreditCardsRequest,
  ):
    | Promise<GetCreditCardsResponse>
    | Observable<GetCreditCardsResponse>
    | GetCreditCardsResponse;

  createMolPayment(
    request: CreateMolPaymentRequest,
  ):
    | Promise<CreateMolPaymentResponse>
    | Observable<CreateMolPaymentResponse>
    | CreateMolPaymentResponse;

  fetchActiveMerchants(
    request: FetchActiveMerchantsRequest,
  ):
    | Promise<FetchActiveMerchantsResponse>
    | Observable<FetchActiveMerchantsResponse>
    | FetchActiveMerchantsResponse;

  chargeOnOrder(
    request: ChargeOnOrderRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  chargeOnDifference(
    request: ChargeOnDifferenceRequest,
  ):
    | Promise<ChargeOnDifferenceResponse>
    | Observable<ChargeOnDifferenceResponse>
    | ChargeOnDifferenceResponse;

  createCreditLine(
    request: RequestWithEntityId,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;

  createCharge(
    request: CreateChargeRequest,
  ): Promise<Transaction> | Observable<Transaction> | Transaction;

  getProfiles(
    request: GetProfilesRequest,
  ):
    | Promise<GetProfilesResponse>
    | Observable<GetProfilesResponse>
    | GetProfilesResponse;

  getChargePlans(
    request: GetChargePlansRequest,
  ):
    | Promise<GetChargePlansResponse>
    | Observable<GetChargePlansResponse>
    | GetChargePlansResponse;

  createHiTrustPayment(
    request: CreateHiTrustPaymentRequest,
  ):
    | Promise<CreateHiTrustPaymentResponse>
    | Observable<CreateHiTrustPaymentResponse>
    | CreateHiTrustPaymentResponse;

  createPaymentIntent(
    request: CreatePaymentIntentRequest,
  ): Promise<Transaction> | Observable<Transaction> | Transaction;

  updatePaymentIntent(
    request: UpdatePaymentIntentRequest,
  ): Promise<Transaction> | Observable<Transaction> | Transaction;

  getPricingPlan(
    request: GetPricingPlanRequest,
  ):
    | Promise<GetPricingPlanResponse>
    | Observable<GetPricingPlanResponse>
    | GetPricingPlanResponse;

  completeExternalPayment(
    request: CompleteExternalPaymentRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function ProfilesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getEntityProfiles',
      'getCreditProfilesByEntityIds',
      'createCreditCard',
      'availableProfile',
      'processPayment',
      'adminAddCredit',
      'adminUpdateProfileExpiry',
      'createBulkPayment',
      'removeProfile',
      'addCreditByCreditCard',
      'renewMonthlyPlan',
      'checkMonthlyCredit',
      'refundOrder',
      'renewCreditLine',
      'createExpiryCreditTransaction',
      'createPaypalPayment',
      'executePaypalPayment',
      'getCreditCards',
      'createMolPayment',
      'fetchActiveMerchants',
      'chargeOnOrder',
      'chargeOnDifference',
      'createCreditLine',
      'createCharge',
      'getProfiles',
      'getChargePlans',
      'createHiTrustPayment',
      'createPaymentIntent',
      'updatePaymentIntent',
      'getPricingPlan',
      'completeExternalPayment',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Profiles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Profiles', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const PROFILES_SERVICE_NAME = 'Profiles';

export interface TransactionsClient {
  getTransaction(
    request: GetTransactionRequest,
  ): Observable<GetTransactionResponse>;

  getTransactions(
    request: GetTransactionsRequest,
  ): Observable<GetTransactionsResponse>;

  updateTransaction(
    request: UpdateTransactionRequest,
  ): Observable<GetTransactionResponse>;

  getOrderTransactions(
    request: GetOrderTransactionsRequest,
  ): Observable<GetTransactionsResponse>;

  getMultiOrderTransactions(
    request: GetMultiOrderTransactionsRequest,
  ): Observable<GetTransactionsResponse>;

  getProfileTransactions(
    request: GetProfileTransactionRequest,
  ): Observable<GetTransactionsResponse>;

  refundToCreditCard(
    request: RefundToCreditCardRequest,
  ): Observable<PaymentProfile>;

  getTotalOrderPrice(
    request: GetMultiOrderTransactionsRequest,
  ): Observable<GetTotalOrderResponse>;

  getReceipt(request: GetReceiptRequest): Observable<GetReceiptResponse>;

  updateOrderCompletedTime(
    request: UpdateOrderCompletedTimeRequest,
  ): Observable<GetTransactionsResponse>;

  refundTransaction(request: RefundTransactionRequest): Observable<Transaction>;

  createTransaction(request: CreateTransactionRequest): Observable<Transaction>;
}

export interface TransactionsController {
  getTransaction(
    request: GetTransactionRequest,
  ):
    | Promise<GetTransactionResponse>
    | Observable<GetTransactionResponse>
    | GetTransactionResponse;

  getTransactions(
    request: GetTransactionsRequest,
  ):
    | Promise<GetTransactionsResponse>
    | Observable<GetTransactionsResponse>
    | GetTransactionsResponse;

  updateTransaction(
    request: UpdateTransactionRequest,
  ):
    | Promise<GetTransactionResponse>
    | Observable<GetTransactionResponse>
    | GetTransactionResponse;

  getOrderTransactions(
    request: GetOrderTransactionsRequest,
  ):
    | Promise<GetTransactionsResponse>
    | Observable<GetTransactionsResponse>
    | GetTransactionsResponse;

  getMultiOrderTransactions(
    request: GetMultiOrderTransactionsRequest,
  ):
    | Promise<GetTransactionsResponse>
    | Observable<GetTransactionsResponse>
    | GetTransactionsResponse;

  getProfileTransactions(
    request: GetProfileTransactionRequest,
  ):
    | Promise<GetTransactionsResponse>
    | Observable<GetTransactionsResponse>
    | GetTransactionsResponse;

  refundToCreditCard(
    request: RefundToCreditCardRequest,
  ): Promise<PaymentProfile> | Observable<PaymentProfile> | PaymentProfile;

  getTotalOrderPrice(
    request: GetMultiOrderTransactionsRequest,
  ):
    | Promise<GetTotalOrderResponse>
    | Observable<GetTotalOrderResponse>
    | GetTotalOrderResponse;

  getReceipt(
    request: GetReceiptRequest,
  ):
    | Promise<GetReceiptResponse>
    | Observable<GetReceiptResponse>
    | GetReceiptResponse;

  updateOrderCompletedTime(
    request: UpdateOrderCompletedTimeRequest,
  ):
    | Promise<GetTransactionsResponse>
    | Observable<GetTransactionsResponse>
    | GetTransactionsResponse;

  refundTransaction(
    request: RefundTransactionRequest,
  ): Promise<Transaction> | Observable<Transaction> | Transaction;

  createTransaction(
    request: CreateTransactionRequest,
  ): Promise<Transaction> | Observable<Transaction> | Transaction;
}

export function TransactionsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getTransaction',
      'getTransactions',
      'updateTransaction',
      'getOrderTransactions',
      'getMultiOrderTransactions',
      'getProfileTransactions',
      'refundToCreditCard',
      'getTotalOrderPrice',
      'getReceipt',
      'updateOrderCompletedTime',
      'refundTransaction',
      'createTransaction',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Transactions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Transactions', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TRANSACTIONS_SERVICE_NAME = 'Transactions';

export interface TiersClient {
  getTiers(request: GetTiersRequest): Observable<GetTiersResponse>;

  getTier(request: GetTierRequest): Observable<GetTierResponse>;

  createTier(request: CreateTierRequest): Observable<CreateTierResponse>;

  updateTier(request: UpdateTierRequest): Observable<UpdateTierResponse>;
}

export interface TiersController {
  getTiers(
    request: GetTiersRequest,
  ):
    | Promise<GetTiersResponse>
    | Observable<GetTiersResponse>
    | GetTiersResponse;

  getTier(
    request: GetTierRequest,
  ): Promise<GetTierResponse> | Observable<GetTierResponse> | GetTierResponse;

  createTier(
    request: CreateTierRequest,
  ):
    | Promise<CreateTierResponse>
    | Observable<CreateTierResponse>
    | CreateTierResponse;

  updateTier(
    request: UpdateTierRequest,
  ):
    | Promise<UpdateTierResponse>
    | Observable<UpdateTierResponse>
    | UpdateTierResponse;
}

export function TiersControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getTiers',
      'getTier',
      'createTier',
      'updateTier',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Tiers', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Tiers', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const TIERS_SERVICE_NAME = 'Tiers';

export interface BankSlipsClient {
  getBankSlips(request: GetBankSlipsRequest): Observable<GetBankSlipsResponse>;

  getBankSlip(request: GetBankSlipRequest): Observable<GetBankSlipResponse>;

  createBankSlip(
    request: CreateBankSlipRequest,
  ): Observable<CreateBankSlipResponse>;

  updateBankSlip(
    request: UpdateBankSlipRequest,
  ): Observable<UpdateBankSlipResponse>;
}

export interface BankSlipsController {
  getBankSlips(
    request: GetBankSlipsRequest,
  ):
    | Promise<GetBankSlipsResponse>
    | Observable<GetBankSlipsResponse>
    | GetBankSlipsResponse;

  getBankSlip(
    request: GetBankSlipRequest,
  ):
    | Promise<GetBankSlipResponse>
    | Observable<GetBankSlipResponse>
    | GetBankSlipResponse;

  createBankSlip(
    request: CreateBankSlipRequest,
  ):
    | Promise<CreateBankSlipResponse>
    | Observable<CreateBankSlipResponse>
    | CreateBankSlipResponse;

  updateBankSlip(
    request: UpdateBankSlipRequest,
  ):
    | Promise<UpdateBankSlipResponse>
    | Observable<UpdateBankSlipResponse>
    | UpdateBankSlipResponse;
}

export function BankSlipsControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getBankSlips',
      'getBankSlip',
      'createBankSlip',
      'updateBankSlip',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('BankSlips', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('BankSlips', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const BANK_SLIPS_SERVICE_NAME = 'BankSlips';

export interface MonthlySummariesClient {
  updateMonthlySummary(request: MonthlySummary): Observable<MonthlySummary>;

  generateMonthlySummary(
    request: EntityMonthYearRequest,
  ): Observable<SuccessResponse>;
}

export interface MonthlySummariesController {
  updateMonthlySummary(
    request: MonthlySummary,
  ): Promise<MonthlySummary> | Observable<MonthlySummary> | MonthlySummary;

  generateMonthlySummary(
    request: EntityMonthYearRequest,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function MonthlySummariesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'updateMonthlySummary',
      'generateMonthlySummary',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('MonthlySummaries', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('MonthlySummaries', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const MONTHLY_SUMMARIES_SERVICE_NAME = 'MonthlySummaries';

export interface InvoicesClient {
  fetchInvoices(
    request: FetchInvoicesRequest,
  ): Observable<FetchInvoicesResponse>;

  createInvoice(request: CreateInvoiceRequest): Observable<Invoice>;

  updateInvoice(request: Invoice): Observable<Invoice>;

  removeInvoice(request: RequestWithId): Observable<EmptyResponse>;

  getInvoicePdf(
    request: GenerateInvoiceRequest,
  ): Observable<GenerateInvoiceResponse>;

  getInvoiceCsv(
    request: GenerateInvoiceRequest,
  ): Observable<GenerateInvoiceResponse>;

  sendInvoiceMail(request: RequestWithId): Observable<SuccessResponse>;
}

export interface InvoicesController {
  fetchInvoices(
    request: FetchInvoicesRequest,
  ):
    | Promise<FetchInvoicesResponse>
    | Observable<FetchInvoicesResponse>
    | FetchInvoicesResponse;

  createInvoice(
    request: CreateInvoiceRequest,
  ): Promise<Invoice> | Observable<Invoice> | Invoice;

  updateInvoice(
    request: Invoice,
  ): Promise<Invoice> | Observable<Invoice> | Invoice;

  removeInvoice(
    request: RequestWithId,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;

  getInvoicePdf(
    request: GenerateInvoiceRequest,
  ):
    | Promise<GenerateInvoiceResponse>
    | Observable<GenerateInvoiceResponse>
    | GenerateInvoiceResponse;

  getInvoiceCsv(
    request: GenerateInvoiceRequest,
  ):
    | Promise<GenerateInvoiceResponse>
    | Observable<GenerateInvoiceResponse>
    | GenerateInvoiceResponse;

  sendInvoiceMail(
    request: RequestWithId,
  ): Promise<SuccessResponse> | Observable<SuccessResponse> | SuccessResponse;
}

export function InvoicesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'fetchInvoices',
      'createInvoice',
      'updateInvoice',
      'removeInvoice',
      'getInvoicePdf',
      'getInvoiceCsv',
      'sendInvoiceMail',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Invoices', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Invoices', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const INVOICES_SERVICE_NAME = 'Invoices';

export interface FeesClient {
  quoteFees(request: QuoteFeesRequest): Observable<QuoteFeesResponse>;

  getFees(request: GetFeesRequest): Observable<GetFeesResponse>;
}

export interface FeesController {
  quoteFees(
    request: QuoteFeesRequest,
  ):
    | Promise<QuoteFeesResponse>
    | Observable<QuoteFeesResponse>
    | QuoteFeesResponse;

  getFees(
    request: GetFeesRequest,
  ): Promise<GetFeesResponse> | Observable<GetFeesResponse> | GetFeesResponse;
}

export function FeesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['quoteFees', 'getFees'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Fees', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Fees', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const FEES_SERVICE_NAME = 'Fees';

export interface CurrenciesClient {
  getCurrency(request: GetCurrencyRequest): Observable<GetCurrencyResponse>;

  getCurrencies(
    request: GetCurrenciesRequest,
  ): Observable<GetCurrenciesResponse>;

  upsertCurrency(
    request: UpsertCurrencyRequest,
  ): Observable<GetCurrencyResponse>;

  deleteCurrency(request: DeleteCurrencyRequest): Observable<EmptyResponse>;
}

export interface CurrenciesController {
  getCurrency(
    request: GetCurrencyRequest,
  ):
    | Promise<GetCurrencyResponse>
    | Observable<GetCurrencyResponse>
    | GetCurrencyResponse;

  getCurrencies(
    request: GetCurrenciesRequest,
  ):
    | Promise<GetCurrenciesResponse>
    | Observable<GetCurrenciesResponse>
    | GetCurrenciesResponse;

  upsertCurrency(
    request: UpsertCurrencyRequest,
  ):
    | Promise<GetCurrencyResponse>
    | Observable<GetCurrencyResponse>
    | GetCurrencyResponse;

  deleteCurrency(
    request: DeleteCurrencyRequest,
  ): Promise<EmptyResponse> | Observable<EmptyResponse> | EmptyResponse;
}

export function CurrenciesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getCurrency',
      'getCurrencies',
      'upsertCurrency',
      'deleteCurrency',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('Currencies', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('Currencies', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const CURRENCIES_SERVICE_NAME = 'Currencies';

export interface FeeTypesClient {
  getFeeType(request: GetFeeTypeRequest): Observable<GetFeeTypeResponse>;

  getFeeTypes(request: GetFeeTypesRequest): Observable<GetFeeTypesResponse>;

  upsertFeeType(request: UpsertFeeTypeRequest): Observable<GetFeeTypeResponse>;
}

export interface FeeTypesController {
  getFeeType(
    request: GetFeeTypeRequest,
  ):
    | Promise<GetFeeTypeResponse>
    | Observable<GetFeeTypeResponse>
    | GetFeeTypeResponse;

  getFeeTypes(
    request: GetFeeTypesRequest,
  ):
    | Promise<GetFeeTypesResponse>
    | Observable<GetFeeTypesResponse>
    | GetFeeTypesResponse;

  upsertFeeType(
    request: UpsertFeeTypeRequest,
  ):
    | Promise<GetFeeTypeResponse>
    | Observable<GetFeeTypeResponse>
    | GetFeeTypeResponse;
}

export function FeeTypesControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = [
      'getFeeType',
      'getFeeTypes',
      'upsertFeeType',
    ];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcMethod('FeeTypes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(
        constructor.prototype,
        method,
      );
      GrpcStreamMethod('FeeTypes', method)(
        constructor.prototype[method],
        method,
        descriptor,
      );
    }
  };
}

export const FEE_TYPES_SERVICE_NAME = 'FeeTypes';

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
