import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import config from 'src/core/config';
import { OrderController } from './controllers/order.controller';

const protoPath = join(
  process.cwd(),
  '../../shared/protos',
  'order.service.proto',
);

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'ORDER_PACKAGE',
        transport: Transport.GRPC,
        options: {
          url: config.serviceMap.ORDER_SERVICE,
          package: 'order',
          protoPath: protoPath,
        },
      },
    ]),
  ],
  controllers: [OrderController],
  providers: [],
})
export class OrderServiceModule {}
