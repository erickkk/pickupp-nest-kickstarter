import { Metadata } from '@grpc/grpc-js';
import { Controller, Inject, OnModuleInit } from '@nestjs/common';
import { ClientGrpc, GrpcMethod } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { FetchOrdersRequest, OrdersClient } from 'grpc/order.service';

@Controller()
export class OrderController implements OnModuleInit {
  constructor(@Inject('ORDER_PACKAGE') private client: ClientGrpc) {}
  private orderService: OrdersClient;

  onModuleInit() {
    this.orderService = this.client.getService<OrdersClient>('Orders');
  }

  @GrpcMethod('Sessions', 'Logout')
  async findOne(data: any, metadata: Metadata): Promise<any> {
    const result = await lastValueFrom(
      this.orderService.fetchOrders({ limit: 12 } as FetchOrdersRequest),
    );

    return {
      token: '123',
      status: 'active',
    };
  }
}
