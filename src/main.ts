import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';

const protoPath = join(
  process.cwd(),
  '../../shared/protos',
  'agent-trip.service.proto',
);

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.GRPC,
      options: {
        url: `127.0.0.1:${process.env.PORT}`,
        package: 'agentTrip',
        protoPath,
      },
    },
  );

  await app.listen();
}
bootstrap();
