import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/services/prisma.service';
import { AgentTrip } from '@prisma/client';

@Injectable()
export class AgentTripService {
  constructor(private prisma: PrismaService) {}

  async findAll(): Promise<AgentTrip[]> {
    return this.prisma.agentTrip.findMany();
  }
}
