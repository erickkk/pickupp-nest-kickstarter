import { Metadata } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { AgentTripService } from '../services/agent-trip.service';

@Controller()
export class AgentTripController {
  constructor(private agentTripService: AgentTripService) {}

  @GrpcMethod('AgentTrip', 'getAgentTrips')
  async findOne(data: any, metadata: Metadata): Promise<any> {
    const items = await this.agentTripService.findAll();
    console.log(items);
    return { items };
  }
}
