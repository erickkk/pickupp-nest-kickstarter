import { Module } from '@nestjs/common';
import { AgentTripController } from './controllers/agent-trip.controller';
import { AgentTripService } from './services/agent-trip.service';

@Module({
  imports: [],
  controllers: [AgentTripController],
  providers: [AgentTripService],
  exports: [AgentTripService],
})
export class AgentTripModule {}
