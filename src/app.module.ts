import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AgentTripModule } from './agentTrips/agent-trip.module';
import { AppService } from './app.service';
import { GrpcInterceptor } from './core/interceptors/grpc.Interceptor';
import { LoggerModule } from './core/logger/logger.module';
import { OrderServiceModule } from './orders/order.module';
import { PrismaModule } from './prisma/prisma.module';

@Module({
  imports: [AgentTripModule, OrderServiceModule, PrismaModule, LoggerModule],
  controllers: [],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: GrpcInterceptor,
    },
  ],
})
export class AppModule {}
