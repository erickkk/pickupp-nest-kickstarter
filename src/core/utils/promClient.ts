// eslint-disable-next-line @typescript-eslint/no-var-requires
const client = require('prom-client');
export const register = new client.Registry();

const labelNames = [
  'region',
  'env',
  'package',
  'service',
  'method',
  'namespace',
];

export const pageCounter = new client.Counter({
  name: 'pickupp_page_count',
  help: 'count_number_of_request',
  labelNames: labelNames,
});

export const failedCounter = new client.Counter({
  name: 'pickupp_failed_services',
  help: 'number_of_failed_services',
  labelNames: ['region', 'env', 'package', 'service', 'namespace'],
});

export const concurrentGauge = new client.Gauge({
  name: 'pickupp_concurrent_request',
  help: 'number_of_concurrent_request',
  labelNames: labelNames,
});

export const catchErrorCounter = new client.Counter({
  name: 'pickupp_error_number',
  help: 'Count_error_number_of_request',
  labelNames: labelNames,
});

export const responseTimeHistogram = new client.Histogram({
  name: 'pickupp_response_time',
  help: 'reponse_time_of_each_request',
  buckets: [0.1, 0.2, 0.5, 1, 3, 5, 10, 20],
  labelNames: labelNames,
});

export const getMetrics = async () => {
  const metric = await client.register.metrics();
  return metric;
};
