const config = {
  serviceMap: {
    AGENT_TRIP_SERVICE: process.env.AGENT_TRIP_SERVICE,
    ORDER_SERVICE: process.env.AGENT_TRIP_SERVICE,
  },
  NATS_HOST: process.env.NATS_HOST,
  REDIS_HOST: process.env.REDIS_HOST,
  DATABASE_URL: process.env.DATABASE_URL,
};

export default config;
