import { Injectable, NestMiddleware } from '@nestjs/common';
import * as cls from 'cls-hooked';
import { NextFunction, Request, Response } from 'express';
import { v4 } from 'uuid';

export const clsNamespace = cls.createNamespace('app');
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const traceId = v4();
    clsNamespace.bindEmitter(req);
    clsNamespace.bindEmitter(res);

    clsNamespace.run(() => {
      clsNamespace.set('traceId', traceId);
      req.res.header('X-Trace-Id', traceId);
      next();
    });
  }
}
