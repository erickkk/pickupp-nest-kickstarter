import { Injectable, LoggerService } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as cls from 'cls-hooked';
import * as winston from 'winston';

type Action = 'start' | 'end';

@Injectable()
export class CustomWinstonLoggerService implements LoggerService {
  logger: winston.Logger;
  constructor(private configService: ConfigService) {
    const { transports, format } = winston;
    const { combine, timestamp, printf, colorize, align } = format;
    const levels = {
      error: 0,
      warn: 1,
      info: 2,
    };
    const customFormat = combine(
      colorize(),
      timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      align(),
      printf((info) => {
        const { timestamp, level, message, ...args } = info;

        return `${timestamp} [${level}]: ${message} ${
          Object.keys(args).length ? JSON.stringify(args, null, 2) : ''
        }`;
      }),
    );
    this.logger = winston.createLogger({
      levels,
      transports: [
        new transports.Console({
          format: customFormat,
          silent:
            this.configService.get<string>('DISABLED_LOGGING') === 'true' ||
            false,
        }),
      ],
    });
  }

  setTraceId(traceId: string) {
    const clsNamespace = cls.getNamespace('app');
    const ctx = clsNamespace.createContext();
    ctx.traceId = traceId;
    clsNamespace.enter(ctx);
  }

  get traceId() {
    const clsNamespace = cls.getNamespace('app');
    return clsNamespace.get('traceId');
  }

  log(resource: string, functionName: string, action: Action, payload: any) {
    return this.logger.log({
      level: 'info',
      message: `${resource}-${functionName}-${action}`,
      payload: payload,
      traceId: this.traceId,
    });
  }

  error(resource: string, functionName: string, action: string, payload: any) {
    return this.logger.log({
      level: 'error',
      message: `${resource}-${functionName}-${action}`,
      payload,
      traceId: this.traceId,
    });
  }

  warn(resource: string, functionName: string, action: string, payload: any) {
    return this.logger.log({
      level: 'warn',
      message: `${resource}-${functionName}-${action}`,
      payload: payload,
      traceId: this.traceId,
    });
  }
}
