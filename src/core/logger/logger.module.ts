import {
  Global,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { LoggerMiddleware } from './middlewares/logger.middleware';
import { CustomWinstonLoggerService } from './services/logger.service';

@Global()
@Module({
  imports: [ConfigModule.forRoot()],
  exports: [CustomWinstonLoggerService],
  providers: [CustomWinstonLoggerService],
  controllers: [],
})
export class LoggerModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes({
      method: RequestMethod.ALL,
      path: '*',
    });
  }
}
