import { Inject } from '@nestjs/common';
import dayjs from 'dayjs';
import { CustomWinstonLoggerService } from 'src/core/logger/services/logger.service';

export function Log(): MethodDecorator {
  const injectCustomWinstonLoggerService = Inject(CustomWinstonLoggerService);

  return (
    target: any,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    const originalMethod = descriptor.value;
    injectCustomWinstonLoggerService(target, 'logger');

    const isAsync = originalMethod.constructor.name === 'AsyncFunction';

    descriptor.value = asyncDescriptorValue(
      target,
      propertyKey,
      originalMethod,
      isAsync,
    );

    return descriptor;
  };
}

const asyncDescriptorValue = (target, propertyKey, originalMethod, isAsync) =>
  async function (...args: any[]) {
    console.log('hello');
    const logger: CustomWinstonLoggerService = (this as any).logger;
    const newArgs = args.filter((arg) =>
      ['Object', 'String', 'Array', 'Number', 'Symbol'].includes(
        arg.constructor.name,
      ),
    );
    const startedAt = dayjs();

    try {
      logger.log(target.constructor.name, propertyKey as string, 'start', {
        args: newArgs,
      });
      if (isAsync) {
        return originalMethod.apply(this, args).then((result) => {
          logger.log(target.constructor.name, propertyKey as string, 'end', {
            args: newArgs,
            result,
            executeDuration: dayjs().diff(startedAt),
          });
          return result;
        });
      }
      const result = await originalMethod.apply(this, args);

      logger.log(target.constructor.name, propertyKey as string, 'end', {
        args: newArgs,
        result,
        executeDuration: dayjs().diff(startedAt),
      });

      return result;
    } catch (e: any) {
      logger.error(target.constructor.name, propertyKey as string, 'end', {
        args: newArgs,
        executeDuration: dayjs().diff(startedAt),
        error: {
          stack: e.stack,
          message: e.message,
        },
      });

      throw e;
    }
  };
