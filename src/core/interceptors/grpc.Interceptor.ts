import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpException,
} from '@nestjs/common';
import { catchError, Observable, tap } from 'rxjs';
import { CustomWinstonLoggerService } from '../logger/services/logger.service';
import {
  responseTimeHistogram,
  concurrentGauge,
  pageCounter,
} from '../utils/promClient';
import * as dayjs from 'dayjs';

interface GrpcContext {
  internalRepr: Map<'x-pickupp-env' | 'x-pickupp-region', string>;
}

@Injectable()
export class GrpcInterceptor implements NestInterceptor {
  constructor(private logger: CustomWinstonLoggerService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const rpc = context.switchToRpc();
    const requestData = rpc.getData();

    const metaData = rpc.getContext<GrpcContext>();
    const env =
      metaData.internalRepr.get('x-pickupp-env') || process.env.NODE_ENV;
    const region = metaData.internalRepr.get('x-pickupp-region') || 'hk';
    const label = { region, env };
    const responseTimer = responseTimeHistogram.startTimer();
    concurrentGauge.inc(label, 1);
    pageCounter.inc(label, 1);

    const basePayload = {
      label,
      requestData,
      metaData,
      startedAt: dayjs(),
    };

    this.logger.log('grpc', 'request', 'start', basePayload);

    return next.handle().pipe(
      tap((data) => {
        responseTimer(label);
        concurrentGauge.dec(label, 1);
        this.handleResponse(data, basePayload);
        return data;
      }),
      catchError((error: HttpException) => {
        this.handleErrorResponse(error, basePayload);
        throw error;
      }),
    );
  }

  handleResponse(data: any, basePayload: any) {
    const endAt = dayjs();
    this.logger.log('http', 'request', 'end', {
      ...basePayload,
      endAt,
      executeDuration: endAt.diff(basePayload.startedAt),
      data,
    });
  }

  handleErrorResponse(error: HttpException, basePayload: any) {
    const endAt = dayjs();

    this.logger.error('http', 'request', 'end', {
      ...basePayload,
      executeDuration: endAt.diff(basePayload.startedAt),
      error: {
        stack: error.stack,
      },
    });
  }
}
